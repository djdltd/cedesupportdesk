VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynDatabaseEX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Robust Dynamic array class designed to act as an internal database for any text or spreadsheets that might be loaded in to the program.
' The array can change it's size when needed, therefore quite memory efficient
Option Explicit
Dim DynArray() As String
Dim ArraySizeX As Single
Dim ArraySizeY As Single
Dim XSizeSet As Boolean
Dim YSizeSet As Boolean

Public Function SetElement(Col As Single, Row As Single, strItem As String)
    If YSizeSet = True And XSizeSet = True Then
        If Col >= 0 And Col <= ArraySizeX And Row >= 0 And Row <= ArraySizeY Then
            DynArray(Col, Row) = strItem
        Else
            'MsgBox "Col and Row Index out of range!", vbCritical, "SetElement in DynDatabase Class"
        End If
    Else
        MsgBox "DynArray Size not Set!", vbCritical, "SetElement in DynDatabase Class"
    End If
End Function

Public Function GetElement(Col As Single, Row As Single) As String
    If Col <= ArraySizeX And Row <= ArraySizeY Then
        If Col >= 0 And Row >= 0 Then
            GetElement = DynArray(Col, Row)
        Else
            MsgBox "Col and Row index below 0!", vbCritical, "GetElement in DynDatabase"
        End If
    Else
        'MsgBox "Col and Row index above allowed range!", vbCritical, "GetElement in DynDatabase"
    End If
End Function
Public Function SetSize(XSize As Single, YSize As Single)
        If XSize >= 0 Then
            If YSize >= 0 Then
                ArraySizeX = XSize
                XSizeSet = True
                ArraySizeY = YSize
                YSizeSet = True
                ReDim DynArray(ArraySizeX, ArraySizeY)
            End If
        End If
End Function
Public Function GetNumCols() As Single
    GetNumCols = ArraySizeX
End Function
Public Function GetNumRows() As Single
    GetNumRows = ArraySizeY
End Function
Private Sub Class_Initialize()
    ArraySizeX = 0
    ArraySizeY = 0
    XSizeSet = False
    YSizeSet = False
End Sub



