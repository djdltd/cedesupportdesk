VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SQLHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim MULTIADOCon As New ADODB.Connection
Dim MULTIsOpen As Boolean
Dim CMF As New CommonFunctions
Dim dlAccessTypes As New DynList

Public Function ReadSQLTable(ServerAddr As String, ServerPort As String, ServerCatalog As String, UserID As String, Password As String, _
                                                TableName As String, ChosenFields As DynDatabaseEX, DynArrayResults As DynDatabaseEX, TrustCon As Boolean)

On Error GoTo err

Dim ADOCon As New ADODB.Connection
Dim ADORs As New ADODB.Recordset
Dim ConnectionString As String
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim CurrentField As String
Dim Row As Single
Dim Col As Single
RowCount = 0

If TrustCon = False Then
'*** NORMAL
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
                                
'*** NORMAL WITH TRUSTED CONNECTION
Else
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "Trusted_Connection=yes;" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
End If
                                
' *** HARDCODED WITH TRUSTED CONNECTION
'ConnectionString = "Provider=sqloledb;" & _
                                "Network Provider=DBMSSOCN;" & _
                                "Data Source=localhost,1143;" & _
                                "Initial Catalog=ABdesk;" & _
                                "Trusted_Connection=yes;" & _
                                "User ID=djd;" & _
                                "Password=djd"
                                
'*** HARDCODED WITHOUT TRUSTED CONNECTION
'ConnectionString = "Provider=sqloledb;" & _
                                "Network Provider=DBMSSOCN;" & _
                                "Data Source=localhost,1143;" & _
                                "Initial Catalog=ABdesk;" & _
                                "User ID=djd;" & _
                                "Password=djd"
    

SQLInitString = "SELECT * FROM " & TableName

'SQLInitString = "SELECT * FROM items"
ADOCon.Open (ConnectionString)
ADORs.Open SQLInitString, ADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DynArrayResults.SetSize(ChosenFields.GetNumRows, RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To ChosenFields.GetNumRows
        CurrentField = ChosenFields.GetElement(1, Col)
        TempElement = Trim(ADORs.Fields(CurrentField).Value)
        If IsNull(TempElement) = False Then Call DynArrayResults.SetElement(Col, Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
ADOCon.Close

Exit Function

err:
    MsgBox "Could not Open Database. Please check connection details.", vbCritical, "Cede SupportDesk"
End Function

Public Function UpdateRecord(KeyField As String, KeyValue As String, FieldsToUpdate As DynList, NewValues As DynList, _
                                                    ServerAddr As String, ServerPort As String, ServerCatalog As String, UserID As String, Password As String, _
                                                    TableName As String, TrustCon As Boolean)
On Error GoTo err

Dim ADOCon As New ADODB.Connection
Dim ConnectionString As String
Dim TempElement As Variant
Dim Field As Integer
Dim SQLUpdateString As String
SQLUpdateString = ""
If TrustCon = False Then
'*** NORMAL
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
                                
'*** NORMAL WITH TRUSTED CONNECTION
Else
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "Trusted_Connection=yes;" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
End If

ADOCon.Open (ConnectionString)

SQLUpdateString = "UPDATE " + TableName + " SET "

For Field = 1 To FieldsToUpdate.GetSize
    SQLUpdateString = SQLUpdateString + FieldsToUpdate.GetItem(Field) + " = '" + NewValues.GetItem(Field) + "'"
    If Field < FieldsToUpdate.GetSize Then SQLUpdateString = SQLUpdateString + ","
Next

SQLUpdateString = SQLUpdateString + " WHERE " + KeyField + " = '" + KeyValue + "'"

ADOCon.Execute (SQLUpdateString)
ADOCon.Close
Exit Function

err:
    MsgBox "Could not Update Record. Possible Server Connection Problem.", vbCritical, "CedeSupportDesk"

End Function

Public Function ReadSQLQuery(ServerAddr As String, ServerPort As String, ServerCatalog As String, UserID As String, Password As String, _
                                                TableName As String, ChosenField As String, QueryFields As DynList, QueryValues As DynList, DynArrayResults As DynDatabaseEX, _
                                                TrustCon As Boolean, UseDistinct As Boolean)

On Error GoTo err

Dim ADOCon As New ADODB.Connection
Dim ADORs As New ADODB.Recordset
Dim ConnectionString As String
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Single
RowCount = 0
Dim Query As Integer
Dim CurrentQueryField As String
Dim CurrentQueryValue As String
Dim NumChosenFields As Integer
Dim ChosenFieldList As New DynList
Dim LC As Integer

If TrustCon = False Then
'*** NORMAL
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
                                
'*** NORMAL WITH TRUSTED CONNECTION
Else
    ConnectionString = "Provider=sqloledb;" & _
                                    "Network Provider=DBMSSOCN;" & _
                                    "Data Source=" & ServerAddr & "," & ServerPort & ";" & _
                                    "Initial Catalog=" & ServerCatalog & ";" & _
                                    "Trusted_Connection=yes;" & _
                                    "User ID=" & UserID & ";" & _
                                    "Password=" & Password
End If

NumChosenFields = CMF.GetNumDataParts(ChosenField, ",")

For LC = 1 To NumChosenFields
    ChosenFieldList.AddItem (Trim(CMF.GetDataPart(ChosenField, LC, ",")))
Next

If UseDistinct = False Then
    SQLInitString = "SELECT " + ChosenField + " FROM " & TableName & " WHERE "
Else
    SQLInitString = "SELECT DISTINCT " + ChosenField + " FROM " & TableName & " WHERE "
End If

For Query = 1 To QueryFields.GetSize
    CurrentQueryField = QueryFields.GetItem(Query)
    CurrentQueryValue = QueryValues.GetItem(Query)
    SQLInitString = SQLInitString + CurrentQueryField + " = '" + CurrentQueryValue + "'"
    If Query < QueryFields.GetSize Then SQLInitString = SQLInitString + " AND "
Next
'Debug.Print SQLInitString
'SQLInitString = "SELECT * FROM items"
ADOCon.Open (ConnectionString)
ADORs.Open SQLInitString, ADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

' Get the number of columns in the returned query
ADORs.MoveFirst



'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DynArrayResults.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem((Col))).Value)
        Debug.Print TempElement
        'MsgBox ChosenFieldList.GetItem(Col)
        'TempElement = Trim(ADORs.Fields(ChosenField).Value)
        If IsNull(TempElement) = False Then Call DynArrayResults.SetElement(Col, Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
ADOCon.Close
Exit Function

err:
'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"

End Function

Public Function MULTOpen(ConnectionString As String)
On Error GoTo err

'MsgBox (ConnectionString)
MULTIADOCon.Open (ConnectionString)
MULTIsOpen = True
Exit Function
err:
    MsgBox "Could not open the database, Error: " + err.Description, vbCritical
End Function
Public Function MULTClose()
    If MULTIsOpen = True Then
        MULTIADOCon.Close
        MULTIsOpen = False
    End If
End Function

Public Function MULTAddRecord(TableName As String, NewValues As DynList) As Boolean
On Error GoTo err
    Dim SQLString As String
    Dim Value As Integer
    
    SQLString = "INSERT INTO " + TableName + " VALUES ("

    For Value = 1 To NewValues.GetSize
        SQLString = SQLString + NewValues.GetItem(Value)
        If Value < NewValues.GetSize Then SQLString = SQLString + ","
    Next
    
    SQLString = SQLString + ");"
        
    'MsgBox SQLString
    MULTIADOCon.Execute (SQLString)
    MULTAddRecord = True
    Exit Function
    
err:
    MULTAddRecord = False
    'MsgBox "Could not add the record. Possible Connection Problem", vbCritical, "CedeSupportDesk"
    'MsgBox "Problem Adding Record: " + err.Description + " SQL String: " + SQLString
End Function
Public Function MULTAddRecordAutID(TableName As String, NewValues As DynList, AffectFields As String) As Boolean
On Error GoTo err
    Dim SQLString As String
    Dim Value As Integer
    
    SQLString = "INSERT INTO " + TableName + " (" + AffectFields + ") " + " VALUES ("

    For Value = 1 To NewValues.GetSize
        SQLString = SQLString + NewValues.GetItem(Value)
        If Value < NewValues.GetSize Then SQLString = SQLString + ","
    Next
    
    SQLString = SQLString + ");"
    'MsgBox SQLString
    MULTIADOCon.Execute (SQLString)
    MULTAddRecordAutID = True
    Exit Function
    
err:
    'MsgBox "MULTAddRecordAutID error: " + SQLString + " Error: " + err.Description
    MULTAddRecordAutID = False
    'MsgBox "Could not add the record. Possible Connection Problem", vbCritical, "CedeSupportDesk"
End Function
Public Function MULTDeleteRecord(TableName As String, Key_Field As String, Key_Value As String, FieldToSearch As String, ValueToDelete As String) As Boolean
On Error GoTo err
    
    Dim SQLString As String
    
    
    If Main.UsingAccessDB = True Then
    
        If IsNumberField(TableName, FieldToSearch) = True Then
        
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "=" + ValueToDelete + " AND " + Key_Field + "=" + Key_Value
            Else
                SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "=" + ValueToDelete + " AND " + Key_Field + "='" + Key_Value + "'"
            End If
        
        Else
        
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + ValueToDelete + "' AND " + Key_Field + "=" + Key_Value
            Else
                SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + ValueToDelete + "' AND " + Key_Field + "='" + Key_Value + "'"
            End If
            
        End If
        
    Else
        SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + ValueToDelete + "' AND " + Key_Field + "='" + Key_Value + "'"
    End If
    
    
    
    MULTIADOCon.Execute (SQLString)
    MULTDeleteRecord = True
    Exit Function
err:
    'MsgBox "MULTDeleteRecord error: " + SQLString + " Error: " + err.Description
    MULTDeleteRecord = False
End Function
Public Function MULTDeleteRecordSIMPLE(TableName As String, FieldToSearch As String, FieldValueToDelete As String) As Boolean
On Error GoTo err
    
    Dim SQLString As String
    
    If Main.UsingAccessDB = True Then
        
        If IsNumberField(TableName, FieldToSearch) = True Then
            SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "=" + FieldValueToDelete
        Else
            SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + FieldValueToDelete + "'"
        End If
        
    Else
        SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + FieldValueToDelete + "'"
    End If
        
    MULTIADOCon.Execute (SQLString)
    MULTDeleteRecordSIMPLE = True
    Exit Function
err:
    'MsgBox "MULTDeleteRecordSIMPLE Error: " + err.Description + " SQL String: " + SQLString
    MULTDeleteRecordSIMPLE = False
End Function
Public Function MULTDeleteRecord2(TableName As String, Key_Field As String, Key_Value As String, FieldToSearch As String, ValueToDelete As String, FieldToSearch2 As String, ValueToDelete2 As String)
    On Error GoTo err
    
    Dim SQLString As String
    
    SQLString = "DELETE FROM " + TableName + " WHERE " + FieldToSearch + "='" + ValueToDelete + "' AND " + FieldToSearch2 + "='" + ValueToDelete2 + "' AND " + Key_Field + "='" + Key_Value + "'"
    
    MULTIADOCon.Execute (SQLString)
    Exit Function
err:
    MsgBox "Could not delete the record. Possible Connection Problem", vbCritical, "CedeSupportDesk"
End Function
Public Function MULTUpdateRecord(TableName As String, Key_Field As String, Key_Value As String, FieldToUpdate As String, NewValue As String) As Boolean
    On Error GoTo err
    
    Dim SQLString As String
    Dim AmmendedValue As String
    
    AmmendedValue = CMF.ReplaceString(NewValue, "'", " ")
    
    If Main.UsingAccessDB = True Then
    
        If IsNumberField(TableName, FieldToUpdate) = True Then
        
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "=" + AmmendedValue + " WHERE " + Key_Field + "=" + Key_Value
            Else
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "=" + AmmendedValue + " WHERE " + Key_Field + "='" + Key_Value + "'"
            End If
            
        Else
        
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + AmmendedValue + "' WHERE " + Key_Field + "=" + Key_Value
            Else
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + AmmendedValue + "' WHERE " + Key_Field + "='" + Key_Value + "'"
            End If
            
        End If
        
    Else
        SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + AmmendedValue + "' WHERE " + Key_Field + "='" + Key_Value + "'"
    End If
    

    'MsgBox SQLString
    MULTIADOCon.Execute (SQLString)
    MULTUpdateRecord = True
    Exit Function
err:
    'MsgBox "MULTUpdateRecord Error: " + err.Description + " SQL String: " + SQLString
    MULTUpdateRecord = False
End Function

Public Function MULTUpdateRecordEX(TableName As String, Key_Field As String, Key_Value As String, FieldToUpdate As String, NewValue As String) As Boolean
    ' Modified versin of the update record function which does the same as the above update record function, but without the quotes on the ammendedvalue
    On Error GoTo err
    
    Dim SQLString As String
    
    
    If Main.UsingAccessDB = True Then
        
        If IsNumberField(TableName, FieldToUpdate) = True Then
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "=" + NewValue + " WHERE " + Key_Field + "=" + Key_Value
            Else
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "=" + NewValue + " WHERE " + Key_Field + "='" + Key_Value + "'"
            End If
        Else
            If IsNumberField(TableName, Key_Field) = True Then
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + NewValue + "' WHERE " + Key_Field + "=" + Key_Value
            Else
                SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + NewValue + "' WHERE " + Key_Field + "='" + Key_Value + "'"
            End If
        End If
    Else
        SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "=" + NewValue + " WHERE " + Key_Field + "='" + Key_Value + "'"
    End If
    
    

    'MsgBox SQLString
    MULTIADOCon.Execute (SQLString)
    MULTUpdateRecordEX = True
    Exit Function
err:
    'MsgBox "MULTUpdateRecordEX Error: " + err.Description + " SQL String: " + SQLString
    MULTUpdateRecordEX = False
End Function

Public Function MULTUpdateSoftware(TableName As String, Key_Field As String, Key_Value As String, SoftwareField As String, SoftwareValue As String, FieldToUpdate As String, NewValue As String)
On Error GoTo err
    Dim TempString As String
    Dim TempString2 As String
    
    ' Replace any quotes found with double quotes so the SQL parser doesn't complain!
    TempString2 = SoftwareValue
    If CMF.FindString(SoftwareValue, "'", True) <> 0 Then
        TempString = CMF.ReplaceString(SoftwareValue, "'", "##")
        TempString2 = CMF.ReplaceString(TempString, "##", "''") 'Can't replace with more of the same due to a bug with the function. - Must sort out!.
    End If
    
    Dim SQLString As String
    
    SQLString = "UPDATE " + TableName + " SET " + FieldToUpdate + "='" + NewValue + "' WHERE " + Key_Field + "='" + Key_Value + "'" + " AND " + SoftwareField + "='" + TempString2 + "'"
    'Debug.Print SQLString
    MULTIADOCon.Execute (SQLString)
    Exit Function
err:
    MsgBox "Could not update the record. Possible Connection Problem", vbCritical, "CedeSupportDesk"
End Function

Public Function IsNumberField(TableName As String, FieldName As String) As Boolean
    ' Search the Access types list and find the table name and field name
    
    Dim CurTableName As String
    Dim CurFieldName As String
    Dim CurFieldType As String
    Dim I As Integer
    Dim bNumberField As Boolean
    
    bNumberField = False
    
    For I = 1 To dlAccessTypes.GetSize
        
        CurTableName = CMF.GetDataPart(dlAccessTypes.GetItem(I), 1, ",")
        CurFieldName = CMF.GetDataPart(dlAccessTypes.GetItem(I), 2, ",")
        CurFieldType = CMF.GetDataPart(dlAccessTypes.GetItem(I), 3, ",")
        
        If Trim(UCase(TableName)) = Trim(UCase(CurTableName)) Then
            If Trim(UCase(FieldName)) = Trim(UCase(CurFieldName)) Then
                If Trim(CurFieldType) = "3" Then
                    bNumberField = True
                End If
                
                If Trim(CurFieldType) = "7" Then
                    bNumberField = False
                End If
                
                If Trim(CurFieldType) = "202" Then
                    bNumberField = False
                End If
                
                If Trim(CurFieldType) = "203" Then
                    bNumberField = False
                End If
                
                IsNumberField = bNumberField
                Exit Function
            End If
        End If
    Next
    
    IsNumberField = bNumberField
    
End Function

Public Function GetAllFieldTypes()
    GetFieldTypes ("AssociatedCallers")
    GetFieldTypes ("CallActions")
    GetFieldTypes ("CallActionTemplates")
    GetFieldTypes ("CallActionTypes")
    GetFieldTypes ("CallActivities")
    GetFieldTypes ("CallAttachDetails")
    GetFieldTypes ("CallAttachments")
    GetFieldTypes ("CallAttachPath")
    GetFieldTypes ("CallCategories")
    GetFieldTypes ("Callers")
    GetFieldTypes ("CallPriorities")
    GetFieldTypes ("CallStatus")
    GetFieldTypes ("CallTemplates")
    GetFieldTypes ("ResolvedRequests")
    GetFieldTypes ("Specialists")
    GetFieldTypes ("SupportCallLog")
    GetFieldTypes ("SupportCalls")
    GetFieldTypes ("SupportGroups")
    
    
    'Dim I As Integer
    'Open "C:\Temp\Outlist.txt" For Output As #1
        
    '    For I = 1 To dlAccessTypes.GetSize
    '        Print #1, dlAccessTypes.GetItem(I)
    '    Next
        
    'Close #1
    
End Function

Public Function GetFieldTypes(TableName As String)
' This function retrieves all of the field names and field types for the specified table
' This is only called if we're using an access database. We need to know the field types for an access database
' because the sql queries have a slightly different syntax depending on the field types

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String

SQLInitString = "SELECT * FROM " + TableName

ADORs.Open SQLInitString, MULTIADOCon

Dim I As Integer

For I = 0 To ADORs.Fields.Count - 1
    dlAccessTypes.AddItem (TableName + "," + ADORs.Fields(I).Name + "," + Trim(Str(ADORs.Fields(I).Type)))
Next

End Function

Public Function MULTReadQuery(TableName As String, QueryFields As DynList, QueryValues As DynList, ChosenField As String, _
                                                    DestDynArray As DynDatabaseEX, UseDistinct As Boolean)
On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Single
RowCount = 0
Dim Query As Integer
Dim CurrentQueryField As String
Dim CurrentQueryValue As String
Dim NumChosenFields As Integer
Dim ChosenFieldList As New DynList
Dim LC As Integer

NumChosenFields = CMF.GetNumDataParts(ChosenField, ",")

For LC = 1 To NumChosenFields
    ChosenFieldList.AddItem (Trim(CMF.GetDataPart(ChosenField, LC, ",")))
Next

If UseDistinct = False Then
    SQLInitString = "SELECT " + ChosenField + " FROM " & TableName & " WHERE "
Else
    SQLInitString = "SELECT DISTINCT " + ChosenField + " FROM " & TableName & " WHERE "
End If

For Query = 1 To QueryFields.GetSize
    CurrentQueryField = QueryFields.GetItem(Query)
    CurrentQueryValue = QueryValues.GetItem(Query)
    
    If Main.UsingAccessDB = False Then
        SQLInitString = SQLInitString + CurrentQueryField + " = '" + CurrentQueryValue + "'"
    Else
        If IsNumberField(TableName, CurrentQueryField) = True Then
            SQLInitString = SQLInitString + CurrentQueryField + " = " + CurrentQueryValue
        Else
            SQLInitString = SQLInitString + CurrentQueryField + " = '" + CurrentQueryValue + "'"
        End If
    End If
    
    If Query < QueryFields.GetSize Then SQLInitString = SQLInitString + " AND "
Next

'MsgBox SQLInitString
'Call GetFieldType("SupportCalls", "ID")


ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

ADORs.MoveFirst


'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DestDynArray.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem((Col))).Value)
        'Debug.Print TempElement
        If IsNull(TempElement) = False Then Call DestDynArray.SetElement(Col, Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
Exit Function

err:

    If err.Number <> 3021 And err.Number <> 3709 Then
        'MsgBox "MULTReadQuery Error: " + SQLInitString + " ErrDesc: " + err.Description + " ErrNum: " + Trim(Str(err.Number))
    End If

'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"
End Function

Public Function MULTDirectQuery(SQLQuery As String, DestDynArray As DynDatabaseEX, NumChosenFields As Integer, ChosenFieldList As DynList) As Boolean

On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Single
RowCount = 0

SQLInitString = SQLQuery

'Open "c:\sqlquery.txt" For Output As #1
 '   Print #1, SQLInitString
'Close #1

'MsgBox SQLInitString

ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend
ADORs.MoveFirst

'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DestDynArray.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem((Col))).Value)
        'Debug.Print TempElement
        If IsNull(TempElement) = False Then
            Call DestDynArray.SetElement(Col, Row, (TempElement))
            'MsgBox TempElement
        End If
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
    MULTDirectQuery = True
Exit Function

err:
'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"
    If err.Number <> 3021 And err.Number <> 3709 And err.Number <> 3265 Then
        'MsgBox "MULTDirectQuery error: " + SQLInitString + " Error: " + err.Description + " ErrNum: " + Trim(Str(err.Number))
    End If
    
    MULTDirectQuery = False
End Function


Public Function MULTReadQueryMultVals(TableName As String, QueryField As String, QueryValues As DynList, ChosenField As String, _
                                                    DestDynArray As DynDatabaseEX)
On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Single
RowCount = 0
Dim Query As Integer
Dim CurrentQueryField As String
Dim CurrentQueryValue As String
Dim NumChosenFields As Integer
Dim ChosenFieldList As New DynList
Dim LC As Integer

NumChosenFields = CMF.GetNumDataParts(ChosenField, ",")

For LC = 1 To NumChosenFields
    ChosenFieldList.AddItem (Trim(CMF.GetDataPart(ChosenField, LC, ",")))
Next


SQLInitString = "SELECT " + ChosenField + " FROM " & TableName & " WHERE "


For Query = 1 To QueryValues.GetSize
    CurrentQueryField = QueryField
    CurrentQueryValue = QueryValues.GetItem(Query)
    SQLInitString = SQLInitString + CurrentQueryField + " = '" + CurrentQueryValue + "'"
    If Query < QueryValues.GetSize Then SQLInitString = SQLInitString + " OR "
Next

'frmMain.txtDetails.Text = SQLInitString

ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

ADORs.MoveFirst

'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DestDynArray.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem((Col))).Value)
        'Debug.Print TempElement
        If IsNull(TempElement) = False Then Call DestDynArray.SetElement(Col, Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
Exit Function

err:
'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"
End Function

Public Function MULTReadTable(TableName As String, ChosenField As String, DestDynArray As DynDatabaseEX)

On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Single
RowCount = 0
Dim NumChosenFields As Integer
Dim ChosenFieldList As New DynList
Dim LC As Integer

NumChosenFields = CMF.GetNumDataParts(ChosenField, ",")

For LC = 1 To NumChosenFields
    ChosenFieldList.AddItem (Trim(CMF.GetDataPart(ChosenField, LC, ",")))
Next

SQLInitString = "SELECT " + ChosenField + " FROM " & TableName

ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

ADORs.MoveFirst

'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DestDynArray.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem((Col))).Value)
        'Debug.Print TempElement
        If IsNull(TempElement) = False Then Call DestDynArray.SetElement(Col, Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
Exit Function

err:
'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"

End Function

Public Function MULTReadTableEX(TableName As String, ChosenField As String, DestDynArray As DynDatabaseEX)

On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim TempElement As Variant
Dim RowCount As Single
Dim Row As Single
Dim Col As Integer
RowCount = 0
Dim NumChosenFields As Integer
Dim ChosenFieldList As New DynList
Dim LC As Integer

NumChosenFields = CMF.GetNumDataParts(ChosenField, ",")

For LC = 1 To NumChosenFields
    ChosenFieldList.AddItem (Trim(CMF.GetDataPart(ChosenField, LC, ",")))
Next

SQLInitString = "SELECT " + ChosenField + " FROM " & TableName

ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

ADORs.MoveFirst

'Now we have the number of rows, and the number of chosen cols (from chosenfields), we can set the size of the dest dynarray.
Call DestDynArray.SetSize((NumChosenFields), RowCount)

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

For Row = 1 To RowCount
    For Col = 1 To NumChosenFields
        TempElement = Trim(ADORs.Fields(ChosenFieldList.GetItem(Col)).Value)
        If IsNull(TempElement) = False Then Call DestDynArray.SetElement((Col), Row, (TempElement))
    Next
    ADORs.MoveNext
    DoEvents
Next

ADORs.Close
Exit Function

err:

'Msgbox "MULTReadTableEX error: " + SQLInitString + " Error: " + err.Description
'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"

End Function

Public Function MULTGetMaxValue(TableName As String, ChosenField As String) As String

On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim SQLInitString As String
Dim RowCount As Single
Dim Row As Single
Dim Col As Integer
RowCount = 0

Dim LC As Integer

SQLInitString = "SELECT MAX(" + ChosenField + ") FROM " & TableName

ADORs.Open SQLInitString, MULTIADOCon

' Okay, firstly we need to know how many rows are in the table. There is no function to tell us, so we'll have to do it manually.
ADORs.MoveFirst
While Not ADORs.EOF
    RowCount = RowCount + 1
    ADORs.MoveNext
Wend

'Now begin reading the opened recordset into the dest dynarray
ADORs.MoveFirst

MULTGetMaxValue = Trim(ADORs.Fields(0).Value)

ADORs.Close
Exit Function

err:

'Msgbox "MULTGetMaxValue error: " + SQLInitString + " Error: " + err.Description

'MsgBox "Could not open the database. Possible Connection Problem.", vbCritical, "CedeSupportDesk"

End Function

Public Function MULTGetBLOB(TableName As String, BLOBField As String, KeyField As String, KeyValue As String, DestFile As String) As Boolean

On Error GoTo err
Dim ADORs As New ADODB.Recordset
Dim ADOSt As New ADODB.Stream
Dim SQLInitString As String


If Main.UsingAccessDB = True Then
    If IsNumberField(TableName, KeyField) = True Then
        SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "=" & KeyValue
    Else
        SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "='" & KeyValue & "'"
    End If
Else
    SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "='" & KeyValue & "'"
End If


ADORs.Open SQLInitString, MULTIADOCon, adOpenForwardOnly, adLockReadOnly


'Initialise the stream object used to persist the file
ADOSt.Type = adTypeBinary
ADOSt.Open

ADORs.MoveFirst

'Write the value of the field to the stream
ADOSt.Write ADORs.Fields(BLOBField).Value

' Try and delete any file that may exist in the destfile value
CMF.SmartDelete (DestFile)

'Save the content of the stream to the file
ADOSt.SaveToFile (DestFile)

'Close the stream
ADOSt.Close
ADORs.Close
MULTGetBLOB = True
Exit Function
err:
    'Msgbox "MULTGetBLOB error: " + SQLInitString + " Error: " + err.Description
    MULTGetBLOB = False
End Function

Public Function MULTPutBLOB(TableName As String, BLOBField As String, KeyField As String, KeyValue As String, SourceFile As String) As Boolean
    
On Error GoTo err

Dim ADORs As New ADODB.Recordset
Dim ADOSt As New ADODB.Stream
Dim SQLInitString As String

If Main.UsingAccessDB = True Then
    If IsNumberField(TableName, KeyField) = True Then
        SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "=" & KeyValue
    Else
        SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "='" & KeyValue & "'"
    End If
    
Else
    SQLInitString = "SELECT " + BLOBField + " FROM " & TableName & " WHERE " & KeyField & "='" & KeyValue & "'"
End If

ADORs.Open SQLInitString, MULTIADOCon, adOpenKeyset, adLockOptimistic

'Initialise the stream object used to persist the file
ADOSt.Type = adTypeBinary
ADOSt.Open
ADOSt.LoadFromFile (SourceFile)

ADORs.MoveFirst

'Write the stream to the database value
ADORs.Fields(BLOBField).Value = ADOSt.Read

' Must update the record!
ADORs.Update

'Close the stream
ADOSt.Close
ADORs.Close
MULTPutBLOB = True
Exit Function
err:
    'Msgbox "MULTPutBLOB error: " + SQLInitString + " Error: " + err.Description + " ErrNum: " + Trim(Str(err.Number))
    MULTPutBLOB = False
End Function
