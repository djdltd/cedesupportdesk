VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InventoryHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Essential DJD Base classes
Dim SQL As New SQLHandler
Dim CMF As New CommonFunctions

' Inventory Database Details
Dim SQLServer As String
Dim SQLPort As String
Dim SQLCatalog As String
Dim SQLUser As String
Dim SQLPassword As String
Dim TrustedCon As Boolean

Public Function Initialise(Server As String, Port As String, Catalog As String, User As String, Password As String, TrustCon As Boolean)
    
    'Before we can do anything we need to load the Inventory Database
    ' Make the database settings global
    SQLServer = Server
    SQLPort = Port
    SQLCatalog = Catalog
    SQLUser = User
    SQLPassword = Password
    TrustedCon = TrustCon

    ' Now load the DB
    'Call SQL.MULTOpen(SQLServer, SQLPort, SQLCatalog, SQLUser, SQLPassword, TrustedCon)
    
End Function

Public Function GetPCDetails(Username As String, DestDynDB As DynDatabaseEX)
    ' Function to retrieve PC details given a username.
    ' This function contacts the inventory database and executes the relevant query
    
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    
    QueryFields.AddItem ("Username")
    QueryValues.AddItem (Username)
    
    ' Now read the query and place the values into the destination Dyn DB
    Call SQL.MULTReadQuery("LiveInvent", QueryFields, QueryValues, "ComputerName,OS,COEVersion,Model,CPUSpeed,RAM", DestDynDB, False)

End Function
Public Function GetSpecialistStatus(DisplayName As String) As String
    ' First we query all tables for the existance of the displayname
    ' First the corp table
    
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim ResultDB As New DynDatabaseEX
    Dim AbsentVal As Boolean
    Dim LunchVal As Boolean
        
    QueryFields.AddItem ("DisplayName")
    QueryValues.AddItem (DisplayName)
    
    ' Now read the query into a temp table
    'Call SQL.MULTReadQuery("SpecialistStatus", QueryFields, QueryValues, "Absent,Lunch", ResultDB, False)
    
    'If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
    '    If ResultDB.GetElement(1, 1) = "1" Then AbsentVal = True Else AbsentVal = False
    '    If ResultDB.GetElement(2, 1) = "1" Then LunchVal = True Else LunchVal = False
    'End If
    
    ' Now read the PD Table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadQuery("SpecialistStatus_PD", QueryFields, QueryValues, "Absent,Lunch", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
        If ResultDB.GetElement(1, 1) = "1" Then AbsentVal = True Else AbsentVal = False
        If ResultDB.GetElement(2, 1) = "1" Then LunchVal = True Else LunchVal = False
    End If
    
    ' Now read the RX Table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadQuery("SpecialistStatus_RX", QueryFields, QueryValues, "Absent,Lunch", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
        If ResultDB.GetElement(1, 1) = "1" Then AbsentVal = True Else AbsentVal = False
        If ResultDB.GetElement(2, 1) = "1" Then LunchVal = True Else LunchVal = False
    End If
    
    If AbsentVal = False And LunchVal = False Then
        GetSpecialistStatus = "Present"
    End If
    
    If AbsentVal = True And LunchVal = False Then
        GetSpecialistStatus = "Absent"
    End If
    
    If AbsentVal = False And LunchVal = True Then
        GetSpecialistStatus = "Lunch"
    End If
    
End Function

Public Function GetAllDisplayNames(DestDynList As DynList)
    Dim Row As Integer
    Dim ResultDB As New DynDatabaseEX
    
    ' Read the PD table
    Call SQL.MULTReadTable("SpecialistStatus_PD", "DisplayName", ResultDB)
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        For Row = 1 To ResultDB.GetNumRows
            DestDynList.AddItem (ResultDB.GetElement(1, (Row)))
        Next
    End If
    
    ' Now read the RX table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadTable("SpecialistStatus_RX", "DisplayName", ResultDB)
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        For Row = 1 To ResultDB.GetNumRows
            DestDynList.AddItem (ResultDB.GetElement(1, (Row)))
        Next
    End If
End Function

Public Function GetDisplayName(Username As String) As String
    ' We need to query the specialist tables for the existance of the username
    ' First the corp table
    
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim ResultDB As New DynDatabaseEX
    Dim AbsentVal As Boolean
    Dim LunchVal As Boolean
        
    QueryFields.AddItem ("Specialist")
    QueryValues.AddItem (Username)
    
    ' Now read the query into a temp table
    'Call SQL.MULTReadQuery("SpecialistStatus", QueryFields, QueryValues, "DisplayName", ResultDB, False)
    
    'If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
    '    GetDisplayName = ResultDB.GetElement(1, 1)
    'End If
    
    ' Now the PD table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadQuery("SpecialistStatus_PD", QueryFields, QueryValues, "DisplayName", ResultDB, False)
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
        GetDisplayName = ResultDB.GetElement(1, 1)
    End If
    
    ' Now the RX table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadQuery("SpecialistStatus_RX", QueryFields, QueryValues, "DisplayName", ResultDB, False)
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This DisplayName exists in the corp table, so get the value!
        GetDisplayName = ResultDB.GetElement(1, 1)
    End If
End Function
Public Function SetSpecialistStatusManual(DisplayName As String, Status As String)
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim ResultDB As New DynDatabaseEX
    
    QueryFields.AddItem ("DisplayName")
    QueryValues.AddItem (DisplayName)
    
    ' Read the query into the results table
    Call SQL.MULTReadQuery("SpecialistStatus_PD", QueryFields, QueryValues, "DisplayName", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in this table, so update it's status value
        If Status = "Absent" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Absent", "1") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Lunch", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        If Status = "Lunch" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Lunch", "1") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Absent", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        If Status = "Present" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Absent", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_PD", "DisplayName", DisplayName, "Lunch", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        Call LogChange(DisplayName, Status, "SpecialistStatusLog_PD")
    End If
    
    ' Try the RX table
    Call ResultDB.SetSize(0, 0)
    Call SQL.MULTReadQuery("SpecialistStatus_RX", QueryFields, QueryValues, "DisplayName", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in this table, so update it's status value
        If Status = "Absent" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Absent", "1") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Lunch", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        If Status = "Lunch" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Lunch", "1") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Absent", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        If Status = "Present" Then
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Absent", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
            If SQL.MULTUpdateRecord("SpecialistStatus_RX", "DisplayName", DisplayName, "Lunch", "0") = False Then
                MsgBox "Cede SupportDesk had a problem updating the Status.", vbExclamation, "Cede SupportDesk"
            End If
        End If
        
        Call LogChange(DisplayName, Status, "SpecialistStatusLog_RX")
    End If
    
End Function

Public Function SetSpecialistStatus(UserID As String, Presence As Boolean)
    
    ' First we query all tables for the existence of the user
    ' First the Corp Table
    
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim ResultDB As New DynDatabaseEX
    Dim AbsentVal As String
    
    QueryFields.AddItem ("Specialist")
    QueryValues.AddItem (UserID)
    
    ' Now read the query into a temp table
    'Call SQL.MULTReadQuery("SpecialistStatus", QueryFields, QueryValues, "Specialist", ResultDB, False)
    
    'If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the corp table, so update it's value!!
    '    If Presence = True Then AbsentVal = "0" Else AbsentVal = "1"
        
    '    If SQL.MULTUpdateRecord("SpecialistStatus", "Specialist", UserID, "Absent", AbsentVal) = False Then
    '        MsgBox "Live Support Desk had a problem updating your Status.", vbExclamation, "Cede SupportDesk"
    '    Else
    '        Call LogStatusChange(UserID, Presence, "SpecialistStatusLog")
    '    End If
    'End If
    
    ' Now check the PD table
    
    Call ResultDB.SetSize(0, 0)
       
    ' Read the query into the temp table
    Call SQL.MULTReadQuery("SpecialistStatus_PD", QueryFields, QueryValues, "Specialist", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the PD table, so update it's value!!
        If Presence = True Then AbsentVal = "0" Else AbsentVal = "1"
        
        If SQL.MULTUpdateRecord("SpecialistStatus_PD", "Specialist", UserID, "Absent", AbsentVal) = False Then
            MsgBox "Cede SupportDesk had a problem updating your Status.", vbExclamation, "Cede SupportDesk"
        Else
            Call LogStatusChange(UserID, Presence, "SpecialistStatusLog_PD")
            Call SQL.MULTUpdateRecord("SpecialistStatus_PD", "Specialist", UserID, "Lunch", "0")
        End If
    End If
    
    ' Now check the RX table
    
    Call ResultDB.SetSize(0, 0)
       
    ' Read the query into the temp table
    Call SQL.MULTReadQuery("SpecialistStatus_RX", QueryFields, QueryValues, "Specialist", ResultDB, False)
    
    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the RX table, so update it's value!!
        If Presence = True Then AbsentVal = "0" Else AbsentVal = "1"
        
        If SQL.MULTUpdateRecord("SpecialistStatus_RX", "Specialist", UserID, "Absent", AbsentVal) = False Then
            MsgBox "Cede SupportDesk had a problem updating your Status.", vbExclamation, "Cede SupportDesk"
        Else
            Call LogStatusChange(UserID, Presence, "SpecialistStatusLog_RX")
            Call SQL.MULTUpdateRecord("SpecialistStatus_RX", "Specialist", UserID, "Lunch", "0")
        End If
    End If
    
End Function

Private Function LogStatusChange(UserID As String, Presence As Boolean, TableName As String)
    ' Function to add a record to the status log to indicate a change to the system.
    
    Dim ActionDesc As String
    Dim ActionTime As String
    Dim NewAddedItems As New DynList
    
    If Presence = False Then
        ActionDesc = "Status Autoset to Absent. Idle value set to " + Trim(Str(Main.IdleMinutes)) + " minutes."
    Else
        ActionDesc = "Status Autoset to Present."
    End If
    
    ActionTime = Trim(Format(Date, "DD/MM/YYYY")) + " " + Trim(Format(Time, "HH:MM:SS"))
    
    NewAddedItems.AddItem ("'" + ActionDesc + "'")
    NewAddedItems.AddItem ("'" + ActionTime + "'")
    NewAddedItems.AddItem ("'" + UserID + "'")
    
    If SQL.MULTAddRecord(TableName, NewAddedItems) = False Then
        ' Don't do anything, if it fails then tough.
    End If
    
End Function

Public Function LogChange(DisplayName As String, Status As String, TableName As String)
    Dim strLogTime As String
    Dim NewAddedItems As New DynList
    Dim LogText As String
    
    LogText = DisplayName + " set to " + Status + "."
    strLogTime = Trim(Format(Date, "DD/MM/YYYY")) + " " + Trim(Format(Time, "HH:MM:SS"))
    
    NewAddedItems.AddItem ("'" + LogText + "'")
    NewAddedItems.AddItem ("'" + strLogTime + "'")
    NewAddedItems.AddItem ("'" + Trim(Main.GetCurrentLoggedOnUser) + "'")
    
    Call SQL.MULTAddRecord(TableName, NewAddedItems)
End Function

Public Function GetSpecialistMobile(UserID As String) As String
    ' This function opens the CorpCSTMobiles table and the PD CST Mobiles table to see
    ' If the user in question as a mobile number in this table for sending a reminder over sms
    
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim ResultDB As New DynDatabaseEX
    
    QueryFields.AddItem ("Specialist")
    QueryValues.AddItem (UserID)
    
    ' Now read the query into a temp table
    'Call SQL.MULTReadQuery("CorpCSTMobiles", QueryFields, QueryValues, "Specialist,Mobile", ResultDB, False)
    
    'If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the corp table, so get the mobile number!
    '    GetSpecialistMobile = ResultDB.GetElement(2, 1)
    '    Exit Function
    'End If
    
    Call ResultDB.SetSize(0, 0)
    
    ' Now read the query into a temp table
    Call SQL.MULTReadQuery("PDCSTMobiles", QueryFields, QueryValues, "Specialist,Mobile", ResultDB, False)

    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the pd table, so get the mobile number!
        GetSpecialistMobile = ResultDB.GetElement(2, 1)
        Exit Function
    End If
    
    Call ResultDB.SetSize(0, 0)
    
    ' Now read the query into a temp table
    Call SQL.MULTReadQuery("RXCSTMobiles", QueryFields, QueryValues, "Specialist,Mobile", ResultDB, False)

    If ResultDB.GetNumRows > 0 And ResultDB.GetNumCols > 0 Then
        ' This user exists in the rx table, so get the mobile number!
        GetSpecialistMobile = ResultDB.GetElement(2, 1)
        Exit Function
    End If
    
End Function
