Attribute VB_Name = "HotKeyHandler"
Option Explicit

Public OldWindowProc As Long

Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Const GWL_WNDPROC = (-4)

Public Declare Function RegisterHotKey Lib "user32" (ByVal hWnd As Long, ByVal id As Long, ByVal fsModifiers As Long, ByVal vk As Long) As Long
Public Declare Function UnregisterHotKey Lib "user32" (ByVal hWnd As Long, ByVal id As Long) As Long
Public Const MOD_ALT = &H1
Public Const VK_F10 = &H79
Public Const VK_F9 = &H78

Public Const HOTKEY_ID = 1
Public Const HOTKEYTWO_ID = 2

' Look for the WM_HOTKEY message.
Public Function NewWindowProc(ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Const WM_NCDESTROY = &H82
Const WM_HOTKEY = &H312

    ' If we're being destroyed,
    ' restore the original WindowProc and
    ' unregister the hotkey.
    If Msg = WM_NCDESTROY Then
        SetWindowLong hWnd, GWL_WNDPROC, OldWindowProc
        UnregisterHotKey hWnd, HOTKEY_ID
    End If

    ' See if this is the WM_HOTKEY message. - HERE WE MUST SET THE NAME OF THE FORM CONTAINING THE HOTKEY EVENT
    ' IF WE DON'T DO THIS, THE PROGRAM QUITS OUT IMMEDIATELY AFTER THE FUNCTION IS CALLED. EVEN VB QUITS OUT!!
    If Msg = WM_HOTKEY Then
        If wParam = 1 Then frmMain.Hotkey
        If wParam = 2 Then frmMain.HotkeyTwo
    End If

    ' Process the message normally.
    NewWindowProc = CallWindowProc(OldWindowProc, hWnd, Msg, wParam, lParam)
End Function

