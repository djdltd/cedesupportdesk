VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSingleRequestDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Single Request Details"
   ClientHeight    =   10725
   ClientLeft      =   3330
   ClientTop       =   2265
   ClientWidth     =   12945
   Icon            =   "frmSingleRequestDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10725
   ScaleWidth      =   12945
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAdminCategories 
      Caption         =   "..."
      Height          =   315
      Left            =   9360
      TabIndex        =   56
      Top             =   2220
      Width           =   315
   End
   Begin VB.TextBox txtTimeOnHold 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6900
      Locked          =   -1  'True
      TabIndex        =   54
      Top             =   3540
      Width           =   2775
   End
   Begin VB.OptionButton optChange 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Change"
      Height          =   195
      Left            =   10200
      TabIndex        =   52
      Top             =   2940
      Width           =   915
   End
   Begin VB.OptionButton optIncident 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Incident"
      Height          =   195
      Left            =   11460
      TabIndex        =   51
      Top             =   2940
      Width           =   915
   End
   Begin VB.CheckBox chkRequestReminder 
      BackColor       =   &H8000000C&
      Caption         =   "Remind me about this Request on:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   720
      MaskColor       =   &H8000000C&
      TabIndex        =   39
      Top             =   60
      Width           =   2775
   End
   Begin VB.CheckBox chkWatch 
      Caption         =   "Keep a watch on this Request"
      Height          =   255
      Left            =   3360
      TabIndex        =   36
      Top             =   10260
      Width           =   2475
   End
   Begin VB.CommandButton cmdFileAttachments 
      Caption         =   "File Attachments.."
      Height          =   315
      Left            =   11160
      TabIndex        =   34
      Top             =   3540
      Width           =   1515
   End
   Begin VB.CommandButton cmdActionTemplates 
      Caption         =   "From Template"
      Height          =   255
      Left            =   60
      TabIndex        =   33
      Top             =   10380
      Width           =   1515
   End
   Begin VB.CommandButton cmbAddActivity 
      Caption         =   "Add Activity"
      Height          =   435
      Left            =   1680
      TabIndex        =   32
      Top             =   10140
      Width           =   1515
   End
   Begin VB.CommandButton cmdRequestLog 
      Cancel          =   -1  'True
      Caption         =   "Request Log"
      Height          =   315
      Left            =   5760
      TabIndex        =   29
      Top             =   120
      Width           =   1515
   End
   Begin VB.CommandButton cmdAddAction 
      Caption         =   "Add Action"
      Height          =   255
      Left            =   60
      TabIndex        =   28
      Top             =   10140
      Width           =   1515
   End
   Begin MSFlexGridLib.MSFlexGrid FGridActions 
      Height          =   1695
      Left            =   0
      TabIndex        =   27
      Top             =   6300
      Width           =   12915
      _ExtentX        =   22781
      _ExtentY        =   2990
      _Version        =   393216
      Rows            =   0
      Cols            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColorFixed  =   12632256
      ForeColorFixed  =   0
      BackColorBkg    =   16777215
      GridColor       =   16777215
      GridColorFixed  =   16777215
      AllowBigSelection=   0   'False
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update Request"
      Height          =   435
      Left            =   8940
      TabIndex        =   26
      Top             =   10140
      Width           =   1935
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   435
      Left            =   10980
      TabIndex        =   25
      Top             =   10140
      Width           =   1935
   End
   Begin VB.TextBox txtLoggedDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   9900
      Locked          =   -1  'True
      TabIndex        =   24
      Top             =   2220
      Width           =   2775
   End
   Begin VB.TextBox txtGroup 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   9900
      Locked          =   -1  'True
      TabIndex        =   22
      Top             =   1560
      Width           =   2775
   End
   Begin VB.TextBox txtLoggedBy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   9900
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   900
      Width           =   2775
   End
   Begin VB.TextBox txtResolution 
      Appearance      =   0  'Flat
      Height          =   1695
      Left            =   6480
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   16
      Top             =   4260
      Width           =   6435
   End
   Begin VB.TextBox txtDetails 
      Appearance      =   0  'Flat
      Height          =   1695
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   14
      Top             =   4260
      Width           =   6435
   End
   Begin VB.TextBox txtSummary 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   0
      MaxLength       =   50
      TabIndex        =   12
      Top             =   3600
      Width           =   6675
   End
   Begin VB.ComboBox cmbPriority 
      Height          =   315
      Left            =   6900
      Sorted          =   -1  'True
      TabIndex        =   7
      Top             =   900
      Width           =   2775
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      Left            =   6900
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   1560
      Width           =   2775
   End
   Begin VB.ComboBox cmbCategory 
      Height          =   315
      Left            =   6900
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   2220
      Width           =   2475
   End
   Begin VB.ComboBox cmbSpecialist 
      Height          =   315
      Left            =   6900
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   2880
      Width           =   2775
   End
   Begin VB.ListBox lstUsers 
      Appearance      =   0  'Flat
      Height          =   1980
      Left            =   3900
      TabIndex        =   2
      Top             =   900
      Width           =   2775
   End
   Begin VB.CommandButton cmdAddUsers 
      Caption         =   "Add Callers..."
      Height          =   255
      Left            =   5340
      TabIndex        =   1
      Top             =   2940
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid FGridActivities 
      Height          =   1695
      Left            =   0
      TabIndex        =   30
      Top             =   8340
      Width           =   12915
      _ExtentX        =   22781
      _ExtentY        =   2990
      _Version        =   393216
      Rows            =   0
      Cols            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColorFixed  =   12632256
      ForeColorFixed  =   0
      BackColorBkg    =   16777215
      GridColor       =   16777215
      GridColorFixed  =   16777215
      AllowBigSelection=   0   'False
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker dtRemindTime 
      Height          =   315
      Left            =   2400
      TabIndex        =   37
      Top             =   300
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48824322
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtRemindDate 
      Height          =   315
      Left            =   720
      TabIndex        =   38
      Top             =   300
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48824321
      CurrentDate     =   38126
   End
   Begin VB.Label lblTimeOnHold 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Time On Hold"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6900
      TabIndex        =   55
      Top             =   3300
      Width           =   2775
   End
   Begin VB.Shape Shape5 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   315
      Left            =   9900
      Top             =   2880
      Width           =   2775
   End
   Begin VB.Label lblChangeIncident 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Change / Incident"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9900
      TabIndex        =   53
      Top             =   2640
      Width           =   2775
   End
   Begin VB.Shape Shape4 
      FillStyle       =   0  'Solid
      Height          =   75
      Left            =   6780
      Top             =   3900
      Width           =   6135
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H80000006&
      FillStyle       =   0  'Solid
      Height          =   735
      Left            =   6720
      Top             =   3240
      Width           =   75
   End
   Begin VB.Label lblCMob 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2280
      TabIndex        =   40
      Top             =   1380
      Width           =   1515
   End
   Begin VB.Label lblCTel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   420
      TabIndex        =   41
      Top             =   1380
      Width           =   1455
   End
   Begin VB.Label lblCLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2580
      TabIndex        =   42
      Top             =   1140
      Width           =   1215
   End
   Begin VB.Label lblCDept 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   540
      TabIndex        =   43
      Top             =   1140
      Width           =   1635
   End
   Begin VB.Label lblUserDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Associated Caller Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   48
      Top             =   660
      Width           =   3735
   End
   Begin VB.Label lblCName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   660
      TabIndex        =   49
      Top             =   900
      Width           =   3135
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Name:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   50
      Top             =   900
      Width           =   615
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Dept:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   47
      Top             =   1140
      Width           =   495
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2160
      TabIndex        =   46
      Top             =   1140
      Width           =   435
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Tel:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   45
      Top             =   1380
      Width           =   375
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Mob:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1860
      TabIndex        =   44
      Top             =   1380
      Width           =   435
   End
   Begin VB.Image imgWordExport 
      Height          =   240
      Left            =   120
      Picture         =   "frmSingleRequestDetails.frx":0442
      ToolTipText     =   "Click to Export this Request to Microsoft Word"
      Top             =   180
      Width           =   240
   End
   Begin VB.Label lblAttachInfo 
      Height          =   555
      Left            =   9900
      TabIndex        =   35
      Top             =   3300
      Width           =   2775
   End
   Begin VB.Label lblActivities 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activities"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   31
      Top             =   8100
      Width           =   12915
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H80000008&
      BackStyle       =   1  'Opaque
      Height          =   75
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   3240
      Width           =   6735
   End
   Begin VB.Label lblLoggedDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Logged Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9900
      TabIndex        =   23
      Top             =   1980
      Width           =   2775
   End
   Begin VB.Label lblGroup 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Group"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9900
      TabIndex        =   21
      Top             =   1320
      Width           =   2775
   End
   Begin VB.Label lblLoggedBy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Logged By"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9900
      TabIndex        =   19
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblActions 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Actions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   18
      Top             =   6060
      Width           =   12915
   End
   Begin VB.Label lblResolution 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resolution"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6480
      TabIndex        =   17
      Top             =   4020
      Width           =   6435
   End
   Begin VB.Label lblDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   15
      Top             =   4020
      Width           =   6435
   End
   Begin VB.Label lblSummary 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   3360
      Width           =   6675
   End
   Begin VB.Label lblPriority 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Priority"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6900
      TabIndex        =   11
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6900
      TabIndex        =   10
      Top             =   1320
      Width           =   2775
   End
   Begin VB.Label lblCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6900
      TabIndex        =   9
      Top             =   1980
      Width           =   2775
   End
   Begin VB.Label lblSpecialist 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialist"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6900
      TabIndex        =   8
      Top             =   2640
      Width           =   2775
   End
   Begin VB.Label lblUsers 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Associated Callers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3900
      TabIndex        =   3
      Top             =   660
      Width           =   2775
   End
   Begin VB.Image Image1 
      Height          =   1515
      Left            =   60
      Picture         =   "frmSingleRequestDetails.frx":0784
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   3750
   End
   Begin VB.Line Line1 
      X1              =   7260
      X2              =   60
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Request Details for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7440
      TabIndex        =   0
      Top             =   60
      Width           =   5415
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmSingleRequestDetails.frx":1E4BA
      Top             =   0
      Width           =   18000
   End
   Begin VB.Menu mnuActions 
      Caption         =   "Actions"
      Visible         =   0   'False
      Begin VB.Menu mnuAddAction 
         Caption         =   "Add Action"
      End
      Begin VB.Menu mnuActionTemplate 
         Caption         =   "Action from Template"
      End
   End
   Begin VB.Menu mnuActivities 
      Caption         =   "Activities"
      Visible         =   0   'False
      Begin VB.Menu mnuAddActivity 
         Caption         =   "Add Activity"
      End
   End
   Begin VB.Menu mnuUsers 
      Caption         =   "Users"
      Visible         =   0   'False
      Begin VB.Menu mnuRemove 
         Caption         =   "Remove User"
      End
   End
End
Attribute VB_Name = "frmSingleRequestDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim ActionDetails As New DynDatabaseEX 'ActionID, ActionTypeID,ActionDate,ActionByID,Description
Dim ActivityDetails As New DynDatabaseEX 'ID,ActionTypeID,ActivityDate,ActivityByID,Description,loggedbyid

Dim AssociatedUsers As New DynDatabaseEX 'Public version of associated users.

Dim ReqID As String
Dim ReqStatus As String
Dim ReqPriority As String

Private Sub cmbAddActivity_Click()
    frmAddActivity.PopulateActivityTypes
    frmAddActivity.PopulateActivityFor
    frmAddActivity.PopulateDTControl
    Call frmAddActivity.SetRequestDetails(ReqID)

    frmAddActivity.Show
End Sub

Private Sub cmbStatus_Click()
    If cmbStatus.Text = "On Hold" And frmOnHoldDetails.ReasonSet = False Then
        frmOnHoldDetails.ClearDetails
        frmOnHoldDetails.SetCallerForm ("UPDATE")
        frmOnHoldDetails.Show
    End If
End Sub

Public Function HoldReasonSet()
    cmbStatus.Text = "On Hold"
    ' Add the on hold details as an action on this request
    Call Main.AddRequestAction(ReqID, "Advice", Format(Date, "DD/MM/YYYY") + " " + Format(Time, "HH:MM:00"), "Reason call placed On Hold: " + vbCrLf + frmOnHoldDetails.GetReason, Main.GetCurrentLoggedOnUser, "On Hold", cmbPriority.Text)
    ActionAdded
End Function

Public Function HoldReasonCancelled()
    cmbStatus.Text = "Open"
End Function

Private Sub cmdActionTemplates_Click()
    Me.MousePointer = 11
    frmSelectActionTemplate.PopulateAvailTemplates
    Me.MousePointer = 0
    frmSelectActionTemplate.Show
End Sub

Private Sub cmdAddAction_Click()
    frmAddAction.PopulateActionTypes
    frmAddAction.PopulateSpecialist
    frmAddAction.PopulateDTControl
    Call frmAddAction.SetRequestDetails(ReqID, ReqStatus, ReqPriority)
    frmAddAction.PopulateTemplate 'If templates exist!
    frmAddAction.Show
End Sub

Public Function TriggerAddAction()
    Call cmdAddAction_Click
End Function

Private Sub cmdAddUsers_Click()
    Me.MousePointer = 11
    frmLocateUser.PopulateListWithSelection ("")
    Me.MousePointer = 0
    frmLocateUser.Show vbModal
    
    If Trim(frmLocateUser.GetSelectedUser) <> "" Then
        lstUsers.AddItem (frmLocateUser.GetSelectedUser)
        txtSummary.SetFocus
    End If
End Sub

Private Sub cmdAdminCategories_Click()
    frmAdminCategories.SetCaller ("EXISTINGREQUEST")
    frmAdminCategories.Show
End Sub

Private Sub cmdClose_Click()

    ' Check if the user has placed a watch on this request
    If chkWatch.Value = 1 Then
        If Main.IsRequestWatched(ReqID) = False Then
            Main.AddRequestWatch (ReqID)
        End If
    End If
    
    ' Check if the user has removed a watch on this request
    If chkWatch.Value = 0 Then
        If Main.IsRequestWatched(ReqID) = True Then
            Main.RemoveRequestWatch (ReqID)
        End If
    End If
    CheckReminder
    frmSingleRequestDetails.Hide
End Sub

Public Function PopulatePriority()
    ' Clear and Populate the priority combo
    Dim PriorityDescriptions As New DynDatabaseEX
    
    cmbPriority.Clear
    Call Main.GetPriorityDescriptions(PriorityDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbPriority, PriorityDescriptions)
    
End Function

Public Function PopulateStatus()
    ' Clear and populate the status combo
    Dim StatusDescriptions As New DynDatabaseEX
    
    cmbStatus.Clear
    
    Call Main.GetStatusDescriptions(StatusDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbStatus, StatusDescriptions)
    
End Function

Public Function PopulateCategory()
    ' Clear and populate the category combo
    Dim CategoryDescriptions As New DynDatabaseEX
    
    cmbCategory.Clear
    
    Call Main.GetCategoryDescriptions(CategoryDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbCategory, CategoryDescriptions)

End Function

Public Function PopulateSpecialist()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    
    cmbSpecialist.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbSpecialist, Specialists)
End Function

Public Function ShowRequestDetails(RequestID As String)
    Dim RequestDetails As New DynDatabaseEX
    Dim AssocUsers As New DynDatabaseEX
    Dim CurrentUser As String
    Dim NumAttachments As Single
    Dim Row As Single
    Dim DateStamp As String
    
    PopulatePriority
    PopulateStatus
    PopulateCategory
    PopulateSpecialist
    
    txtSummary.Text = ""
    txtDetails.Text = ""
    txtResolution.Text = ""
    txtLoggedBy.Text = ""
    txtLoggedDate.Text = ""
    txtGroup.Text = ""
    frmOnHoldDetails.ReasonSet = True
    optChange.Value = False
    optIncident.Value = False
    
    lblAttachInfo.Visible = False
    lblAttachInfo.Caption = ""
    
    ReqID = RequestID
    lstUsers.Clear
    
    If Trim(RequestID) <> "" Then
        Call Main.GetSingleRequestDetails(RequestID, RequestDetails, AssocUsers)
        
        If RequestDetails.GetNumCols > 0 And RequestDetails.GetNumRows > 0 Then
        
            lblTitle.Caption = "Request Details for " + Trim(RequestID)
        
            cmbPriority.Text = RequestDetails.GetElement(2, 1)
            txtGroup.Text = RequestDetails.GetElement(3, 1)
            cmbStatus.Text = RequestDetails.GetElement(4, 1)
            cmbCategory.Text = RequestDetails.GetElement(5, 1)
            cmbSpecialist.Text = RequestDetails.GetElement(6, 1)
            txtLoggedBy.Text = RequestDetails.GetElement(7, 1)
            txtLoggedDate.Text = RequestDetails.GetElement(8, 1)
            txtSummary.Text = RequestDetails.GetElement(9, 1)
            txtDetails.Text = RequestDetails.GetElement(10, 1)
            txtResolution.Text = RequestDetails.GetElement(11, 1)
            txtTimeOnHold.Text = Trim(Str(Val(RequestDetails.GetElement(15, 1)))) + " Minutes"
            ' Grab the local datestamp field - for changed item usage!
            DateStamp = RequestDetails.GetElement(12, 1)
            
            ' Get the change / incident selection
            If RequestDetails.GetElement(13, 1) = "C" Then
                optChange.Value = True
                optIncident.Value = False
            End If
            If RequestDetails.GetElement(13, 1) = "I" Then
                optIncident.Value = True
                optChange.Value = False
            End If
            
            PopulateContactInfo (AssocUsers.GetElement(2, 1))
            
            Call CMF.PopulateListWithDynArray(AssocUsers, lstUsers)
            
            ' Now clear the global associated users list, and copy the local users to it - this is for exporting to word function
            Call AssociatedUsers.SetSize(AssocUsers.GetNumCols, AssocUsers.GetNumRows)
            For Row = 1 To AssocUsers.GetNumRows
                Call AssociatedUsers.SetElement(1, Row, AssocUsers.GetElement(1, Row))
                Call AssociatedUsers.SetElement(2, Row, AssocUsers.GetElement(2, Row))
            Next
            
            ' Set the global request details - stuff for passing onto child forms!
            ReqStatus = cmbStatus.Text
            ReqPriority = cmbPriority.Text
            
            'Now check how many attachments this request has - just for info.
            NumAttachments = Main.GetNumRequestAttachments(RequestID)
            
            If NumAttachments > 0 Then
                lblAttachInfo.Visible = True
                
                If NumAttachments = 1 Then
                    lblAttachInfo.Caption = "This Request has " + Trim(Str(NumAttachments)) + " File Attachment."
                End If
                
                If NumAttachments > 1 Then
                    lblAttachInfo.Caption = "This Request has " + Trim(Str(NumAttachments)) + " File Attachments."
                End If
                
            End If
        End If
    End If
    
    'Check if a watch has been placed on this request
    'If so then make sure the watch tick box is checked
    If Main.IsRequestWatched(RequestID) = True Then
        chkWatch.Value = 1
    Else
        chkWatch.Value = 0
    End If
    
    ' Populate the actions
    Call ActionDetails.SetSize(0, 0)
    PopulateActions (RequestID)

    'Populate the activities
    Call ActivityDetails.SetSize(0, 0)
    PopulateActivities (RequestID)

    ' Check if there is a reminder set for this request
    GetReminderDetails (RequestID)
    
    ' Inform the main class that we have read this item, we only want to do this if the item is unread, and we only want to do this if we are viewing a request in the
    ' current users queue!! and we only want to do this, if the item is NOT closed
    CurrentUser = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase))
    
    If CMF.FindString(Trim(StrConv(cmbStatus.Text, vbUpperCase)), "CLOSED", True) = 0 Then
        If Trim(cmbSpecialist.Text) = CurrentUser Then
            ' Okay this is the current users request, then check and update the unread items list
            If Main.IsItemRead(RequestID) = False Then
                Main.AddReadItem (RequestID)
                Main.FList_RefreshUnreadItems
                Main.RefreshCurrentSpecialistDisplay
                
                ' Now generate an email to the person who logged this call, saying it has been read by the specialist
                If Main.SendReadReceipt = True Then
                    Call Main.GeneratedSpecialistReadEmail(RequestID, txtSummary.Text, Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)), txtLoggedBy.Text)
                End If
            End If
            
            ' Now check and update the changed items list
            If Main.IsItemChanged(RequestID, DateStamp) = False Then
                Call Main.AddChangedItem(RequestID, DateStamp)
                Main.FList_RefreshUnreadItems
                Main.RefreshCurrentSpecialistDisplay
            End If
            
        End If
    End If
    frmOnHoldDetails.ReasonSet = False
End Function
Private Function CleanStringLF(InString As String) As String
    ' This function outputs all of the characters in the input string until either a line feed or cariage return is
    ' found in the source string. This gives the action description preview a cleaner look
    
    Dim C As Single
    Dim FinalString As String
    
    'MsgBox Str(Len(InString))
    For C = 1 To Len(InString)
        
        FinalString = FinalString + Mid(InString, C, 1)
        If Asc(Mid(InString, C, 1)) = 13 Or Asc(Mid(InString, C, 1)) = 10 Then
            Exit For
        End If
        
    Next
    
    CleanStringLF = FinalString
End Function

Public Function PopulateActions(RequestID As String)

    Dim Row As Single
    Dim ActionFieldTitles As New DynDatabaseEX
    Dim CleanedActions As New DynDatabaseEX
    Dim CleanedDescription As String
    'Dim Opt1 As New DynDatabaseEX
    Dim Opt2 As New DynDatabaseEX
    Dim Opt3 As New DynDatabaseEX

    ' Now populate the list of actions for this request
    Call Main.GetActionDetails(RequestID, ActionDetails)
    
    ' Now set the field titles for the actions
    Call ActionFieldTitles.SetSize(1, 5)
    
    FGridActions.Clear
    FGridActions.Cols = 0
    FGridActions.Rows = 0
    
    If ActionDetails.GetNumCols > 0 And ActionDetails.GetNumRows > 0 Then
        Call ActionFieldTitles.SetElement(1, 1, "Action ID")
        Call ActionFieldTitles.SetElement(1, 2, "Action Type")
        Call ActionFieldTitles.SetElement(1, 3, "Action Date")
        Call ActionFieldTitles.SetElement(1, 4, "Action By")
        Call ActionFieldTitles.SetElement(1, 5, "Action Details")
        
        Call CleanedActions.SetSize(5, ActionDetails.GetNumRows)
        
        For Row = 1 To ActionDetails.GetNumRows
            Call CleanedActions.SetElement(1, Row, ActionDetails.GetElement(1, Row))
            Call CleanedActions.SetElement(2, Row, ActionDetails.GetElement(2, Row))
            Call CleanedActions.SetElement(3, Row, ActionDetails.GetElement(3, Row))
            Call CleanedActions.SetElement(4, Row, ActionDetails.GetElement(4, Row))
            
            CleanedDescription = CleanStringLF(ActionDetails.GetElement(5, Row))
            'CleanedDescription = ActionDetails.GetElement(5, Row)
            Call CleanedActions.SetElement(5, Row, CleanedDescription)
        Next
        
        'Now sort the array by date
        Call CMF.SortDynArray(CleanedActions, 3, False, "DT", ActionDetails, Opt2, Opt3)
                
        'now really populate the list
        Call CMF.CopyArrayToFGrid(CleanedActions, FGridActions)
        Call CMF.SetFGridTitles(ActionFieldTitles, FGridActions)
        
        ' Now set the sizes of each of the action cells
        FGridActions.ColWidth(0) = 825
        FGridActions.ColWidth(1) = 1395
        FGridActions.ColWidth(2) = 1920
        FGridActions.ColWidth(3) = 1275
        FGridActions.ColWidth(4) = 7350
    End If
End Function

Public Function PopulateActivities(RequestID As String)
    Dim Row As Single
    Dim ActivityFieldTitles As New DynDatabaseEX
    Dim CleanedActivities As New DynDatabaseEX
    Dim CleanedDescription As String
    'Dim Opt1 As New DynDatabaseEX
    Dim Opt2 As New DynDatabaseEX
    Dim Opt3 As New DynDatabaseEX
    'ID,ActionTypeID,ActivityDate,ActivityByID,Description,loggedbyid
    
    'Now populate the list of activities for this request
    Call Main.GetActivityDetails(RequestID, ActivityDetails)
    
    'Now set the field titles for the activities
    Call ActivityFieldTitles.SetSize(1, 6)
    
    FGridActivities.Clear
    FGridActivities.Cols = 0
    FGridActivities.Rows = 0
    
    If ActivityDetails.GetNumCols > 0 And ActivityDetails.GetNumRows > 0 Then
        Call ActivityFieldTitles.SetElement(1, 1, "Activity ID")
        Call ActivityFieldTitles.SetElement(1, 2, "Activity Type")
        Call ActivityFieldTitles.SetElement(1, 3, "Activity Date")
        Call ActivityFieldTitles.SetElement(1, 4, "Activity For")
        Call ActivityFieldTitles.SetElement(1, 5, "Description")
        Call ActivityFieldTitles.SetElement(1, 6, "Assigned By")
        
        Call CleanedActivities.SetSize(6, ActivityDetails.GetNumRows)
        
        For Row = 1 To ActivityDetails.GetNumRows
            Call CleanedActivities.SetElement(1, Row, ActivityDetails.GetElement(1, Row))
            Call CleanedActivities.SetElement(2, Row, ActivityDetails.GetElement(2, Row))
            Call CleanedActivities.SetElement(3, Row, ActivityDetails.GetElement(3, Row))
            Call CleanedActivities.SetElement(4, Row, ActivityDetails.GetElement(4, Row))
            CleanedDescription = CleanStringLF(ActivityDetails.GetElement(5, Row))
            Call CleanedActivities.SetElement(5, Row, CleanedDescription)
            Call CleanedActivities.SetElement(6, Row, ActivityDetails.GetElement(6, Row))
        Next
        
        'Now sort the array by date
        Call CMF.SortDynArray(CleanedActivities, 3, False, "DT", ActivityDetails, Opt2, Opt3)
        
        ' Now really populate the list
        Call CMF.CopyArrayToFGrid(CleanedActivities, FGridActivities)
        Call CMF.SetFGridTitles(ActivityFieldTitles, FGridActivities)
        
        ' Now set the sizes of each of the activity cells
        FGridActivities.ColWidth(0) = 825
        FGridActivities.ColWidth(1) = 1890
        FGridActivities.ColWidth(2) = 1935
        FGridActivities.ColWidth(3) = 1320
        FGridActivities.ColWidth(4) = 5670
        FGridActivities.ColWidth(5) = 1170
    End If
    
End Function

Private Sub cmdFileAttachments_Click()
    frmRequestAttach.Show
    frmRequestAttach.SetMode (False)
    frmRequestAttach.PopulateRequestAttachments (ReqID)
End Sub

Private Sub cmdRequestLog_Click()
    frmSingleRequestDetails.MousePointer = 11
    frmRequestLog.PopulateRequestLog (ReqID)
    frmRequestLog.Show
    frmSingleRequestDetails.MousePointer = 0
End Sub

Private Sub cmdUpdate_Click()
    Dim AssocUserName As New DynDatabaseEX
    Dim NewAssocUsers As New DynDatabaseEX
    Dim Row As Single
    Dim bChange As Boolean
    
    ' Quick check to see if the category is set to autologged
    If cmbCategory.Text = "Autologged" Then
        MsgBox "Please select an appropriate category for this request.", vbInformation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Me.MousePointer = 11
    cmdUpdate.Enabled = False
    
    'Populate the associated usernames locally
    Call AssocUserName.SetSize(1, AssociatedUsers.GetNumRows)
    For Row = 1 To AssociatedUsers.GetNumRows
        Call AssocUserName.SetElement(1, Row, AssociatedUsers.GetElement(2, Row))
    Next
    
    If optChange.Value = True Then bChange = True
    If optIncident.Value = True Then bChange = False
    
    Call CMF.PopulateDynArrayWithList(NewAssocUsers, lstUsers)
    Call Main.UpdateRequest(ReqID, bChange, cmbPriority.Text, cmbStatus.Text, cmbCategory.Text, cmbSpecialist.Text, txtSummary.Text, txtDetails.Text, txtResolution.Text, NewAssocUsers)
    
    ' Ask the user if they would like to generate a closed call email to the user
    If CMF.FindString(cmbStatus.Text, "Closed", False) <> 0 Then 'If the status has been changed to closed
        If Main.DisplayEmailPrompt = True Then
            If MsgBox("Would you like to Generate a Closed call Email to the Associated Users?", vbQuestion + vbYesNo, "Cede SupportDesk") = vbYes Then
                Call Main.GenerateClosedCallEmail(ReqID, AssocUserName, False)
            End If
        Else
            If Main.DontEmail = False Then
                Call Main.GenerateClosedCallEmail(ReqID, AssocUserName, True)
            End If
        End If
    End If
    
    ' Check if the user has placed a watch on this request
    If chkWatch.Value = 1 Then
        If Main.IsRequestWatched(ReqID) = False Then
            Main.AddRequestWatch (ReqID)
        End If
    End If
    
    ' Check if the user has removed a watch on this request
    If chkWatch.Value = 0 Then
        If Main.IsRequestWatched(ReqID) = True Then
            Main.RemoveRequestWatch (ReqID)
        End If
    End If
    
    ' Inform the main module of this request update
    Main.AddNewRequestToNew (ReqID)
    
    CheckReminder 'Check if the request reminder has been set
    frmSingleRequestDetails.Hide
    Me.MousePointer = 0
    cmdUpdate.Enabled = True
End Sub

Private Sub FGridActions_DblClick()
    Dim SelectedRow As Integer
    Dim ActionBy As String
    Dim ActionType As String
    Dim ActionDate As String
    Dim ActionDescription As String
    
    SelectedRow = FGridActions.RowSel
    
    If ActionDetails.GetNumCols > 0 And ActionDetails.GetNumRows > 0 Then
        ' Set the forms action id, just incase the user wants to update the action
        Me.MousePointer = 11
        frmActionDetails.SetActionID (ActionDetails.GetElement(1, (SelectedRow)))
        
        ActionBy = ActionDetails.GetElement(4, (SelectedRow))
        ActionType = ActionDetails.GetElement(2, (SelectedRow))
        ActionDate = ActionDetails.GetElement(3, (SelectedRow))
        ActionDescription = ActionDetails.GetElement(5, (SelectedRow))
                
        Call frmActionDetails.PopulateActionDetails(ActionType, ActionBy, ActionDate, ActionDescription)
        Call frmActionDetails.SetReqID(ReqID)
        
        frmActionDetails.Show
        Me.MousePointer = 0
    End If
End Sub

Public Function CheckReminder()
    Dim strDate As String
    Dim strTime As String
    Dim TempDate As Date
    
    If chkRequestReminder.Value = 1 Then
    
        strDate = Trim(Str(dtRemindDate.Day)) + "/" + Trim(Str(dtRemindDate.Month)) + "/" + Trim(Str(dtRemindDate.Year))
        TempDate = strDate
        strDate = Format(TempDate, "DD/MM/YYYY")
        
        If Len(Trim(Str(dtRemindTime.Hour))) = 1 Then
            strTime = strTime + "0"
            strTime = strTime + Trim(Str(dtRemindTime.Hour))
        Else
            strTime = strTime + Trim(Str(dtRemindTime.Hour))
        End If
        
        strTime = Trim(Str(dtRemindTime.Hour)) + ":"
        
        If Len(Trim(Str(dtRemindTime.Minute))) = 1 Then
            strTime = strTime + "0"
            strTime = strTime + Trim(Str(dtRemindTime.Minute))
        Else
            strTime = strTime + Trim(Str(dtRemindTime.Minute))
        End If
        
        strTime = Format(strTime, "HH:MM")
        
        Call Main.ReminderEnabled(ReqID, strDate, strTime)
    Else
        Call Main.CheckToDeleteReminder(ReqID)
    End If
End Function

Private Function GetReminderDetails(strReqID As String)
    Dim strDate As String
    Dim strTime As String
    
    If Main.GetReminder(strReqID, strDate, strTime) = True Then
        ' There is a reminder set for this request
        chkRequestReminder.Value = 1
        
        dtRemindDate.Day = CMF.GetDataPart(strDate, 1, "/")
        dtRemindDate.Month = CMF.GetDataPart(strDate, 2, "/")
        dtRemindDate.Year = CMF.GetDataPart(strDate, 3, "/")
        
        dtRemindTime.Hour = CMF.GetDataPart(strTime, 1, ":")
        dtRemindTime.Minute = CMF.GetDataPart(strTime, 2, ":")
    Else
        ' There is no reminder set for this request, therefore switch off the check box,
        ' and default the time to now
        
        chkRequestReminder.Value = 0
        strDate = Format(Date, "DD/MM/YYYY")
        strTime = Format(Time, "HH:MM")
        
        dtRemindDate.Day = CMF.GetDataPart(strDate, 1, "/")
        dtRemindDate.Month = CMF.GetDataPart(strDate, 2, "/")
        dtRemindDate.Year = CMF.GetDataPart(strDate, 3, "/")

        dtRemindTime.Hour = CMF.GetDataPart(strTime, 1, ":")
        dtRemindTime.Minute = CMF.GetDataPart(strTime, 2, ":")
    End If
End Function


Public Function ActionAdded()
    ' Called by the add action form to notify this form that the add action has completed
    Call ActionDetails.SetSize(0, 0)
    PopulateActions (ReqID)
End Function


Public Function ActivityAdded()
    ' Called by the update / add activity form to notify this form that the activity update or add has completed
    Call ActivityDetails.SetSize(0, 0)
    PopulateActivities (ReqID)
End Function

Private Sub FGridActions_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        PopupMenu mnuActions
    End If
End Sub

Private Sub FGridActivities_DblClick()
    'ID,ActionTypeID,ActivityDate,ActivityByID,Description,loggedbyid
    Dim SelectedRow As Integer
    Dim ActivityFor As String
    Dim ActivityType As String
    Dim ActivityDate As String
    Dim ActivityDescription As String
    Dim AssignedBy As String
    
    SelectedRow = FGridActivities.RowSel
    
    If ActivityDetails.GetNumCols > 0 And ActivityDetails.GetNumRows > 0 Then
        ' Set the forms activity id, just incase the user wants to update the action
        Me.MousePointer = 11
        frmActivityDetails.SetActivityID (ActivityDetails.GetElement(1, (SelectedRow)))
        
        ActivityFor = ActivityDetails.GetElement(4, (SelectedRow))
        ActivityType = ActivityDetails.GetElement(2, (SelectedRow))
        ActivityDate = ActivityDetails.GetElement(3, (SelectedRow))
        ActivityDescription = ActivityDetails.GetElement(5, (SelectedRow))
        AssignedBy = ActivityDetails.GetElement(6, (SelectedRow))
        
        Call frmActivityDetails.PopulateActivityDetails(ActivityType, ActivityFor, ActivityDate, ActivityDescription, AssignedBy)
        frmActivityDetails.SetReqID (ReqID)
        frmActivityDetails.Show
        Me.MousePointer = 0
    End If
    
End Sub

Private Sub FGridActivities_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        PopupMenu mnuActivities
    End If
End Sub

Private Sub Form_Activate()
    SetColours
End Sub

Private Sub imgWordExport_Click()
    ' This function contacts the word handler and writes the single
    'call details to microsoft word.
    
    Dim WDH As New WordHandler
    Dim Row As Single
    
    WDH.InitWord
    WDH.SetFontSize (18)
    WDH.SetFontBold (True)
    WDH.WriteText ("Details for Request ID " + ReqID)
    WDH.SetFontBold (False)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
    WDH.SetFontSize (12)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Logged By: ")
    WDH.SetFontBold (False)
    WDH.WriteText (txtLoggedBy.Text + vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Logged Date: ")
    WDH.SetFontBold (False)
    WDH.WriteText (txtLoggedDate.Text + vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Specialist: ")
    WDH.SetFontBold (False)
    WDH.WriteText (cmbSpecialist.Text + vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Category: ")
    WDH.SetFontBold (False)
    WDH.WriteText (cmbCategory.Text + vbCrLf)
    
    WDH.WriteText (vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Priority: ")
    WDH.SetFontBold (False)
    WDH.WriteText (cmbPriority.Text + vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Status: ")
    WDH.SetFontBold (False)
    WDH.WriteText (cmbStatus.Text + vbCrLf)
    
        
    WDH.WriteText (vbCrLf)
    WDH.SetFontBold (True)
    WDH.WriteText ("Associated Users:" + vbCrLf)
    WDH.SetFontBold (False)
    
    For Row = 1 To AssociatedUsers.GetNumRows
        WDH.WriteText (AssociatedUsers.GetElement(1, Row) + vbCrLf)
    Next
    
    WDH.WriteText (vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Summary: " + vbCrLf)
    WDH.SetFontBold (False)
    WDH.WriteText (txtSummary.Text)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Details: " + vbCrLf)
    WDH.SetFontBold (False)
    WDH.WriteText (txtDetails.Text)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
    
    WDH.SetFontBold (True)
    WDH.WriteText ("Resolution: " + vbCrLf)
    WDH.SetFontBold (False)
    WDH.WriteText (txtResolution.Text)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
        
    'Output the action details for this request
    
    WDH.SetFontSize (14)
    WDH.SetFontBold (True)
    WDH.WriteText ("Request Actions: ")
    WDH.SetFontBold (False)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
    WDH.SetFontSize (8)

    For Row = 1 To ActionDetails.GetNumRows
        WDH.SetFontBold (True)
        WDH.WriteText ("Action Type: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActionDetails.GetElement(2, Row))
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Action Date: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActionDetails.GetElement(3, Row))
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Action By: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActionDetails.GetElement(4, Row))
        WDH.WriteText (vbCrLf)
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Action Details: ")
        WDH.WriteText (vbCrLf)
        WDH.SetFontBold (False)
        WDH.WriteText (ActionDetails.GetElement(5, Row))
        WDH.WriteText (vbCrLf)
        WDH.WriteText (vbCrLf)
        
    Next
    
    'Output the activity details for this request
    
    WDH.SetFontSize (14)
    WDH.SetFontBold (True)
    WDH.WriteText ("Request Activities: ")
    WDH.SetFontBold (False)
    WDH.WriteText (vbCrLf)
    WDH.WriteText (vbCrLf)
    WDH.SetFontSize (8)
    
    For Row = 1 To ActivityDetails.GetNumRows
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Activity Type: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActivityDetails.GetElement(2, Row))
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Activity Date: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActivityDetails.GetElement(3, Row))
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Activity For: ")
        WDH.SetFontBold (False)
        WDH.WriteText (ActivityDetails.GetElement(4, Row))
        WDH.WriteText (vbCrLf)
        WDH.WriteText (vbCrLf)
        
        WDH.SetFontBold (True)
        WDH.WriteText ("Activity Details: ")
        WDH.WriteText (vbCrLf)
        WDH.SetFontBold (False)
        WDH.WriteText (ActivityDetails.GetElement(5, Row))
        WDH.WriteText (vbCrLf)
        WDH.WriteText (vbCrLf)
        
    Next

    
End Sub

Private Sub Label1_Click()

End Sub

Private Sub lstUsers_Click()
    If lstUsers.ListIndex <> -1 Then
        PopulateContactInfo (AssociatedUsers.GetElement(2, lstUsers.ListIndex + 1))
    End If
End Sub

Private Sub lstUsers_DblClick()
    Dim SelectedUser As String
    Me.MousePointer = 11
    If lstUsers.SelCount > 0 Then
        If lstUsers.ListIndex <> -1 Then
            SelectedUser = AssociatedUsers.GetElement(2, lstUsers.ListIndex + 1)
        End If
    End If
    
    ' Now check if there is something in the variable, and if so then display the associated calls dialog.
    If Trim(SelectedUser) <> "" Then
        frmAssocUserRequests.Show
        DoEvents
        frmAssocUserRequests.PopulateStatus
        frmAssocUserRequests.PopulateUserRequests (SelectedUser)
        frmAssocUserRequests.PopulateSortbyCombo
        frmAssocUserRequests.PopulateSortModeCombo
        frmAssocUserRequests.SortView
        Me.MousePointer = 0
    Else
        Me.MousePointer = 0
    End If
End Sub

Private Sub lstUsers_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        PopupMenu mnuUsers
    End If
End Sub

Private Sub mnuActionTemplate_Click()
    Call cmdActionTemplates_Click
End Sub

Private Sub mnuAddAction_Click()
    Call cmdAddAction_Click
End Sub

Private Sub mnuAddActivity_Click()
    Call cmbAddActivity_Click
End Sub

Public Function SetColours()
    lblUsers.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblUsers.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblPriority.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblPriority.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblLoggedBy.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblLoggedBy.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblStatus.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblStatus.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblGroup.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblGroup.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblCategory.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblCategory.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblLoggedDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblLoggedDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSpecialist.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSpecialist.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSummary.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSummary.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lbldetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lbldetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblResolution.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblResolution.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActions.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActions.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActivities.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActivities.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTimeOnHold.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTimeOnHold.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblChangeIncident.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblChangeIncident.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblUserDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblUserDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub mnuRemove_Click()
    Dim SelUser As String
    Dim AssocUsers As New DynDatabaseEX
    Dim RequestDetails As New DynDatabaseEX
    Dim Row As Single
    
    If lstUsers.ListCount = 1 Then
        MsgBox "Cannot remove the last user from a Request. A Request must have at least 1 Associated User.", vbInformation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If lstUsers.ListIndex <> -1 Then
        SelUser = AssociatedUsers.GetElement(2, lstUsers.ListIndex + 1)
        If Main.DeleteRequestTIE(ReqID, SelUser) = True Then
            ' Refresh the associated users, by calling the getsingle request details over again
            ' I know this is a bit wasteful, but it is much more convenient!
            Call Main.GetSingleRequestDetails(ReqID, RequestDetails, AssocUsers)
            Call CMF.PopulateListWithDynArray(AssocUsers, lstUsers)
            
            Call AssociatedUsers.SetSize(AssocUsers.GetNumCols, AssocUsers.GetNumRows)
            For Row = 1 To AssocUsers.GetNumRows
                Call AssociatedUsers.SetElement(1, Row, AssocUsers.GetElement(1, Row))
                Call AssociatedUsers.SetElement(2, Row, AssocUsers.GetElement(2, Row))
            Next
            
        Else
            MsgBox "There was a problem remove this Associated User.", vbExclamation, "Cede SupportDesk"
        End If
    End If
End Sub

Public Function PopulateContactInfo(SelectedUser As String)
    Dim DBDetails As New DynDatabaseEX
    
    If Trim(SelectedUser) <> "" Then
        Call Main.GetContactDetails(SelectedUser, DBDetails)
        
        If DBDetails.GetNumRows > 0 And DBDetails.GetNumCols > 0 Then
            ' 1 - FirstName
            ' 2 - LastName
            ' 3 - Department
            ' 4 - Location
            ' 5 - Telephone
            ' 6 - Mobile
            lblCName.Caption = DBDetails.GetElement(1, 1) + " " + DBDetails.GetElement(2, 1) + " (" + Trim(SelectedUser) + ")"
            lblCDept.Caption = DBDetails.GetElement(3, 1)
            lblCLoc.Caption = DBDetails.GetElement(4, 1)
            lblCTel.Caption = DBDetails.GetElement(5, 1)
            lblCMob.Caption = DBDetails.GetElement(6, 1)
        End If
    End If
End Function
