Attribute VB_Name = "Main"
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Option Explicit

Dim SQL As New SQLHandler
Dim CMF As New CommonFunctions
Dim PrimaryUsername As String
Public RichHandler As New RichmondHandler
Dim InvHandler As New InventoryHandler
Dim OlkHandler As New OutlookHandler

' These hold the current displayed queue (whether specialist, or user, or anything) - what is displayed on the main form
Dim CurrentQueue As New DynDatabaseEX
Dim CurrentQueueTitles As New DynDatabaseEX
Dim CurrentReadItems As New DynDatabaseEX
Dim CurrentChangedItems As New DynDatabaseEX
Dim CurrentQueueColSizes As New DynDatabaseEX
Dim CurrentUrgentItems As New DynDatabaseEX

' Database Server details
Public SDSQLServer As String
Public SDSQLPort As String
Public SDSQLCatalog As String
Public SDSQLUser As String
Public SDSQLPassword As String
Public SDSQLTrustCon As Boolean

Public IVSQLServer As String
Public IVSQLPort As String
Public IVSQLCatalog As String
Public IVSQLUser As String
Public IVSQLPassword As String
Public IVSQLTrustCon As Boolean

Public DatabaseConnectionString As String
Public UsingAccessDB As Boolean

' Username Override variable
Public UserOverride As String

' Read receipt variable
Public SendReadReceipt As Boolean

' Request popup variable
Public DisplayRequestPopup As Boolean

' Email prompt variable
Public DisplayEmailPrompt As Boolean

' Display Specialist Status variable
Public DisplayCAOptions As Boolean

'Use AutoStatus variable (Mark as absent when LSD is close, and present when open)
Public UseAutoStatus As Boolean

' Use AutoAbsence variable
Public UseAutoAbsence As Boolean
Public IdleMinutes As Integer
Public IdleValue As Single

' Preivew Pane State Variable
Public PreviewPaneVisible As Boolean

' Flag to state whether the system should always email the user without asking
Public DontEmail As Boolean

' DynDB to hold the columns customised by the user
Dim CustomisedColumns As New DynDatabaseEX

' Global DynList for storing a list of all the users in richmond
Dim UserList As New DynList

' Global Dynlist for storing a list of all the deleted users in richmond
Dim DeletedUserList As New DynList

' String storing the last queue displayed
Dim LastNodeKey As String
Dim LastUserNodeKey As String

' Global initialisation flag - Set to true when initialisation has finished
Dim ProgInitialised As Boolean

' Current Sorted Columns
Dim CurrentSortedColumn As String

' Current Sorted Mode
Dim CurrentSortMode As String

' Ascending and Descending Sort values
Dim AscStringValue As String
Dim DescStringValue As String

' Custom Added Queues - these store any queues that the user has added to their folder list
Public CustomAddedSpecialists As New DynList 'made public so that the remove form can access it - shouldn't do this really!
Public CustomAddedGroups As New DynList

' Expanded items in the folder list - so that when the program loads, it loads in the exact state it was when the user last exited.
Dim ExpandedItems As New DynList ' Stores a list of node keys which have their expanded value set to true

' This dynlist stores a list of all the current specialists requets
' This facilitates reoccuring checks, and the New Request Popup message
Dim NewRequests As New DynList
Dim RequestPopupDisplayed As Boolean

' This list stores all of the request reminders
Dim ReminderReqs As New DynList
Dim ReminderDates As New DynList
Dim ReminderTimes As New DynList


' App initialisation
Public Function InitProgram()
          
    ' Set the ascending and descending string values
    AscStringValue = "Ascending"
    DescStringValue = "Descending"
    
    ' Load the ini file
    LoadINIFile
    
    If Trim(DatabaseConnectionString) = "" Then
        'MsgBox "Please set the database connection string. This can be found Under Tools->Options.", vbInformation
        'Exit Function
        
        ' Now we have access support, if there is no connection string then we default to the Access Database contained in the
        ' same directory as the application
        DatabaseConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\CedeSupportDesk.mdb;Persist Security Info=False"
        'MsgBox DatabaseConnectionString
    End If
    
    If Trim(DatabaseConnectionString) <> "" Then
         frmLogin.ConnectionString = DatabaseConnectionString
         frmLogin.Show vbModal
        
         If Trim(frmLogin.Username) = "" Then
             End
         Else
             PrimaryUsername = StrConv(frmLogin.Username, vbUpperCase)
         End If
    End If
        
    ' Now Set the App Custom Colours
    AppColours.SetAllAppColours
    
    ' Populate the options text box
    frmOptions.txtConnectionString = DatabaseConnectionString
     
    If (CMF.FindString(DatabaseConnectionString, ".mdb", False) > 0) Then
        UsingAccessDB = True
    Else
        UsingAccessDB = False
    End If
     
    frmOptions.txtUsername = UserOverride
        
    If SendReadReceipt = True Then frmOptions.chkReadReceipt.Value = 1 Else frmOptions.chkReadReceipt.Value = 0
    If DisplayRequestPopup = True Then frmOptions.chkNewPopup.Value = 1 Else frmOptions.chkNewPopup.Value = 0
    If DisplayEmailPrompt = True Then frmOptions.chkEmailPrompt.Value = 1 Else frmOptions.chkEmailPrompt.Value = 0
    If UseAutoAbsence = True Then
        frmCAOptions.chkIdleCheck.Value = 1
        frmCAOptions.txtIdleMinutes.Text = Trim(Str(IdleMinutes))
    Else
        frmCAOptions.chkIdleCheck.Value = 0
        frmCAOptions.txtIdleMinutes.Text = "5"
    End If
    
    If DontEmail = True Then frmCAOptions.chkDontEmail.Value = 1 Else frmCAOptions.chkDontEmail.Value = 0
    If DisplayCAOptions = True Then frmCAOptions.chkDisplayCAStatus.Value = 1 Else frmCAOptions.chkDisplayCAStatus.Value = 0
    If UseAutoStatus = True Then frmCAOptions.chkAutoStatus.Value = 1 Else frmCAOptions.chkAutoStatus.Value = 0
        

    
    Call RichHandler.Initialise(DatabaseConnectionString)
    'Call InvHandler.Initialise(IVSQLServer, IVSQLPort, IVSQLCatalog, IVSQLUser, IVSQLPassword, IVSQLTrustCon)
      
   

    ' Now build the global list of all the current specialists request ids apart from status closed
    ' This facilitates the new request popup message.
    BuildNewRequests
    ' Set the popup displayed flag to false
    RequestPopupDisplayed = False
    
    ' Enable the request check interval
    frmMain.tmrNewRequests.Enabled = True
    
    ' Now set the idle checking if the user has chosen to do so
    If UseAutoAbsence = True Then
        IdleValue = IdleMinutes * 60 'To get the number of seconds we need to wait for an idle event to be triggered
        frmMain.tmrIdle.Enabled = True
    Else
        frmMain.tmrIdle.Enabled = False
    End If
    
    ' Now set the preview pane state
    If PreviewPaneVisible = False Then
        frmMain.HidePreviewPane
    Else
        frmMain.ShowPreviewPane
    End If
    
    ' Now load the reminders, if any
    LoadReminders
    
    ' Now Initialise the email templates
    EmailTemplates.Initialise
    
    ' Set the users status to present if the option has been switched on
    If UseAutoStatus = True Then
        Call InvHandler.SetSpecialistStatus(GetCurrentLoggedOnUser, True)
    End If
    
    ' Populate the display names so that the user can set their absence or presence for call allocation
    If DisplayCAOptions = True Then
        frmMain.lblStatusLabel.Visible = True
        frmMain.cmbSASpecialist.Visible = True
        frmMain.cmbSAStatus.Visible = True
        PopulateCANames
    End If
End Function
Public Function SetSpecialistAutoStatus(Presence As Boolean)
    ' Set the users status if the option has been switched on
    If UseAutoStatus = True Then
        Call InvHandler.SetSpecialistStatus(GetCurrentLoggedOnUser, Presence)
    End If
End Function

Public Function GetAscendingVal() As String
    GetAscendingVal = AscStringValue
End Function

Public Function GetDescendingVal() As String
    GetDescendingVal = DescStringValue
End Function

Public Function AddCustomAddedGroup(GroupName As String)
    CustomAddedGroups.AddItem (GroupName)
End Function

Public Function AddCustomAddedSpecialist(SpecialistID As String)
    CustomAddedSpecialists.AddItem (SpecialistID)
End Function

Public Function RichHandlerInitialised()
    ' Called by the rich handler to notify us that richhandler has finished initialising
    
    ' Now we need to add the default specialists queue
    Dim CurrentSpecialistID As String
    Dim CurrentSpecialistGroup As String
    
    CurrentSpecialistID = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    CurrentSpecialistGroup = RichHandler.GetSpecialistGroupName(CurrentSpecialistID)
    
    'FList_AddSpecialistQueue (CurrentSpecialistID)
    FList_AddSpecialistGroup (CurrentSpecialistGroup)
    
    ' Now we need to expand the default specialist queue
    Dim N As Integer
    Dim TempKey As String
    Dim Row As Single
    Dim LRow As Integer
    
    ' Load the ini file
   ' LoadINIFile
    
    For N = 1 To frmMain.treQueues.Nodes.Count
        If frmMain.treQueues.Nodes(N).Key = CurrentSpecialistID Then
            ' Expand the queue
            frmMain.treQueues.Nodes(N).Expanded = True
        End If
        
        If frmMain.treQueues.Nodes(N).Key = CurrentSpecialistGroup + "$" + CurrentSpecialistID + "$" + "Open" Then
            ' Make sure the open queue is highlighted
            frmMain.treQueues.Nodes(N).Selected = True
            
            ' Retrieve the specialists current open queue - this populates the current queue and current queue titles
            Call GetSpecialistQueue(CurrentSpecialistID, RichHandler.GetStatusID("Open"))
            
            ' Now we copy all of the available columns to the custmised columns (by default all fields are displayed)
            ' However this only happens if there is nothing in the customised columns array (nothing loaded by the ini file)
            If CustomisedColumns.GetNumRows = 0 Then
                Call CustomisedColumns.SetSize(1, CurrentQueueTitles.GetNumRows)
                For Row = 1 To CurrentQueueTitles.GetNumRows
                    Call CustomisedColumns.SetElement(1, Row, CurrentQueueTitles.GetElement(1, Row))
                Next
            End If
            
            ' Now we make sure the open queue is displayed, so that we populate the current queue dyn db
            Call FolderListClicked(CurrentSpecialistID + "$" + "Open", "Open")
            
        End If
        
    Next
    
    ' Populate the sort by combo on the main form
    PopulateSortbyCombo
    ' Set the default value
    If Trim(CurrentSortedColumn) = "" Then 'only if nothing loaded by the ini file
        frmMain.cmbSort.Text = "<None>"
    Else
        frmMain.cmbSort.Text = CurrentSortedColumn
    End If
    
    'Populate the sort mode combo on the main form
    PopulateSortModeCombo
    'Set the default value
    If Trim(CurrentSortMode) = "" Then 'only if nothing loaded by the ini file
        frmMain.cmbSortMode.Text = "<None>"
    Else
        frmMain.cmbSortMode.Text = CurrentSortMode
    End If
    
    ' Now add any custom specialists the user may have added
    For LRow = 1 To CustomAddedSpecialists.GetSize
        FList_AddSpecialistQueue (CustomAddedSpecialists.GetItem(LRow))
    Next
    
    ' Now add any custom groups the user may have added
    For LRow = 1 To CustomAddedGroups.GetSize
        FList_AddSpecialistGroup (CustomAddedGroups.GetItem(LRow))
    Next
    
    ' Now set any expanded nodes, that were set the last time the user exited
    FList_SetExpandedNodes
    
    ' Set the global flag to say that we have initialised the program - this is used by the folderlistclicked function
    ' to determine if it should retrieve the column sizes or not
    ProgInitialised = True
    
End Function

Public Function PopulateCANames()
    frmMain.cmbSASpecialist.Clear
    
    ' This is part of the upgrade that negates the need for a specialist update program for call allocation
    Dim DisplayNames As New DynList
    
    ' Populate the dynlist with all the display names
    Call InvHandler.GetAllDisplayNames(DisplayNames)
    
    ' Now populate the combo
    Call CMF.PopulateComboWithDynList(frmMain.cmbSASpecialist, DisplayNames)
    
    ' Now we set the combo box to the name of the current logged on user
    frmMain.cmbSASpecialist.Text = InvHandler.GetDisplayName(GetCurrentLoggedOnUser)
    
    ' Now we set the status combo to the status of the current displayed name in the display name combo box
    frmMain.cmbSAStatus.Text = InvHandler.GetSpecialistStatus(frmMain.cmbSASpecialist.Text)
End Function

Public Function SANameChanged()
    ' Now we set the status combo to the status of the current displayed name in the display name combo box
    frmMain.cmbSAStatus.Text = InvHandler.GetSpecialistStatus(frmMain.cmbSASpecialist.Text)
End Function

Public Function SAStatusChanged()
    ' Manually update the status of the selected user
    Call InvHandler.SetSpecialistStatusManual(frmMain.cmbSASpecialist.Text, frmMain.cmbSAStatus.Text)
End Function

Public Function PopulateSortbyCombo()
    Dim SortByFields As New DynList
    Dim Row As Single
    
    frmMain.cmbSort.Clear
    
    SortByFields.AddItem ("<None>")
    
    For Row = 1 To CustomisedColumns.GetNumRows
        SortByFields.AddItem (CustomisedColumns.GetElement(1, Row))
    Next
    
    Call CMF.PopulateComboWithDynList(frmMain.cmbSort, SortByFields)
End Function

Public Function PopulateSortModeCombo()
    Dim SortModeFields As New DynList
    
    frmMain.cmbSortMode.Clear
    
    SortModeFields.AddItem ("<None>")
    SortModeFields.AddItem (GetAscendingVal)
    SortModeFields.AddItem (GetDescendingVal)
    
    Call CMF.PopulateComboWithDynList(frmMain.cmbSortMode, SortModeFields)
    
End Function

Public Function SortDisplayedCalls(SortByField As String, SortMode As String)
    CurrentSortedColumn = SortByField
    CurrentSortMode = SortMode
    RefreshLastDisplayedQueue
End Function

Public Function ResetSortColumn()
    frmMain.cmbSort.Text = "<None>"
    CurrentSortedColumn = "<None>"
End Function

Public Function GetAllAvailColumns(DestDynDB As DynDatabaseEX)
    ' Retrieve all of the available column titles - used by the customise column form
    Dim Row As Single
    
    Call DestDynDB.SetSize(1, CurrentQueueTitles.GetNumRows)
    
    For Row = 1 To CurrentQueueTitles.GetNumRows
        Call DestDynDB.SetElement(1, Row, CurrentQueueTitles.GetElement(1, Row))
    Next
    
End Function

Public Function GetAllCustomColumns(DestDynDB As DynDatabaseEX)
    ' Retrieve all of the customised column titles - used by the customise column form
    
    Dim Row As Single
    
    Call DestDynDB.SetSize(1, CustomisedColumns.GetNumRows)
    
    For Row = 1 To CustomisedColumns.GetNumRows
        Call DestDynDB.SetElement(1, Row, CustomisedColumns.GetElement(1, Row))
    Next
End Function

Public Function SetCustomColumns(SrcDynDB As DynDatabaseEX)
    ' Used by the choose columns form to set the custom columns
    Dim Row As Single
    
    ' Set the global customised columns to that specified by the input SrcDynDB
    Call CustomisedColumns.SetSize(1, SrcDynDB.GetNumRows)
    For Row = 1 To SrcDynDB.GetNumRows
        Call CustomisedColumns.SetElement(1, Row, SrcDynDB.GetElement(1, Row))
    Next
    
    ' Now refresh the last queue that was displayed, so that changed columns take effect immediately
    RefreshLastDisplayedQueue
End Function

' Progress Indicator functions
' Used by lower level classes
Public Function UpdateMainProgressStatus(Status As String, MaxToDo As Single, CurrentProgress As Single)
    frmMain.lblProgressStatus = Status
    frmMain.progressMain.Max = MaxToDo
    frmMain.progressMain.Value = CurrentProgress
End Function

Public Function ShowMainProgress()
    frmMain.lblProgressStatus.Visible = True
    frmMain.progressMain.Visible = True
    ' Disable the folder list while an update is in progress otherwise if the user clicks on a folder list item
    ' while an index operation is in progress this will cause problems.
    frmMain.treQueues.Enabled = False
    frmMain.treUserQueues.Enabled = False
End Function

Public Function HideMainProgress()
    frmMain.lblProgressStatus.Visible = False
    frmMain.progressMain.Visible = False
    ' Reenable the folder lists
    frmMain.treQueues.Enabled = True
    frmMain.treUserQueues.Enabled = True
End Function

Public Function ShowMainStaticStatus()
    frmMain.lblStaticStatus.Visible = True
    ' Disable the folder list while an update is in progress otherwise if the user clicks on a folder list item
    ' while an index operation is in progress this will cause problems.
    frmMain.treQueues.Enabled = False
    frmMain.treUserQueues.Enabled = False
End Function

Public Function HideMainStaticStatus()
    frmMain.lblStaticStatus.Visible = False
    ' Reenable the folder lists
    frmMain.treQueues.Enabled = True
    frmMain.treUserQueues.Enabled = True
End Function

Public Function UpdateMainStaticStatus(Status As String)
    frmMain.lblStaticStatus.Caption = Status
End Function


Public Function GetSpecialistQueue(SpecialistID As String, StatusID As String)
    Dim NumUnreadItems As Integer
    Dim NumChangedItems As Integer
    Dim StatusDescription As String
    Dim CurrentUser As String
    
    CurrentUser = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    
    If Trim(SpecialistID) = CurrentUser Then 'Display unread items only if we are retrieving the current users queue!
        ' Retrieve a specialists queue, depending on status, and place the queue and titles in the global dyndb
        Call RichHandler.GetSpecialistRequestsEX(SpecialistID, StatusID, CurrentQueue, CurrentQueueTitles, CurrentReadItems, CurrentChangedItems, CurrentUrgentItems, NumUnreadItems, NumChangedItems)
        
        StatusDescription = RichHandler.GetStatusName(StatusID)
        Call FList_MarkUnread(SpecialistID, StatusDescription, NumUnreadItems)
    Else
        Call RichHandler.GetSpecialistRequests(SpecialistID, StatusID, CurrentQueue, CurrentQueueTitles)
    End If
    
End Function

Public Function GetAssocUserQueue(UserID As String, StatusID As String, AllActive As Boolean, AllClosed As Boolean)

    ' Retrieve an associated users queue. depending on the status, and place the queue and titles in the global dyndb
    Call RichHandler.GetAssociatedUserRequests(UserID, StatusID, CurrentQueue, CurrentQueueTitles, AllActive, AllClosed)
End Function

Public Function GetAssocUserRequests(UserID As String, StatusID As String, RequestDestDB As DynDatabaseEX, TitlesDestDB As DynDatabaseEX, AllActive As Boolean, AllClosed As Boolean)
    ' Interface function to retrieve an associated users queue from the database
    Call RichHandler.GetAssociatedUserRequests(UserID, StatusID, RequestDestDB, TitlesDestDB, AllActive, AllClosed)
    
End Function

Public Function GetLoggedQueue(LoggedbyID As String, StatusID As String)
    'Retrieve all calls logged by the specified specialist, with the specified status
    Call RichHandler.GetLoggedRequests(LoggedbyID, StatusID, CurrentQueue, CurrentQueueTitles)
End Function

Public Function GetActiveQueue(SpecialistID As String)
    Call RichHandler.GetActiveRequests(SpecialistID, CurrentQueue, CurrentQueueTitles)
End Function

Public Function GetGroupRequests(GroupName As String)
    Call RichHandler.GetGroupRequests(GroupName, CurrentQueue, CurrentQueueTitles)
End Function

Public Function GetAllClosedQueue(SpecialistID As String)
    Call RichHandler.GetClosedRequests(SpecialistID, CurrentQueue, CurrentQueueTitles)
End Function

Public Function GetStatusID(StatusDescription As String) As String
    ' Interface function to retrieve the status id from the database
    GetStatusID = RichHandler.GetStatusID(StatusDescription)
End Function

Public Function DisplayCurrentQueue(ShowRead As Boolean)

    ' Display the global queue dyndb's and set the titles
    ' This is where we have to filter the queue to the columns the user has chosen to display, so do it!
    Dim FilteredQueue As New DynDatabaseEX
    Dim FilteredQueueTitles As New DynDatabaseEX
    ' Vars for sorting
    Dim ColToSort As Single
    Dim Ascending As Boolean
    Dim FieldType As String
    
    'Check if the user has selected ascending or descending
    If CurrentSortMode = GetAscendingVal Then
        Ascending = True
    Else
        Ascending = False
    End If
    
    ' Get the field type of the column to sort
    FieldType = RichHandler.GetFieldType(CurrentSortedColumn)
    
    
    
    ' Check if we should sort the queue
    If CurrentSortedColumn <> "" And CurrentSortedColumn <> "<None>" Then
        
        If CurrentQueue.GetNumRows > 200 Then
            ShowMainStaticStatus
            UpdateMainStaticStatus ("Sorting Display, Please wait...")
        End If
        
        ColToSort = CMF.GetNameArrayLoc(CurrentQueueTitles, CurrentSortedColumn)
        Call CMF.SortDynArray(CurrentQueue, ColToSort, Ascending, FieldType, CurrentReadItems, CurrentChangedItems, CurrentUrgentItems)
        frmMain.cmbSort.Text = CurrentSortedColumn
        frmMain.cmbSortMode.Text = CurrentSortMode
        
        If CurrentQueue.GetNumRows > 200 Then
            HideMainStaticStatus
        End If
    Else
        frmMain.cmbSort.Text = "<None>"
        frmMain.cmbSortMode.Text = "<None>"
    End If
    
    ' Customise the queue!
    Call CMF.CustomiseDynDatabase(CurrentQueue, CurrentQueueTitles, CustomisedColumns, FilteredQueue, FilteredQueueTitles)

    If ShowRead = True Then 'We are displaying a specialists queue, therefore take into account read and unread items (making them bold)
        Call CMF.CopyArrayToFGridEX(FilteredQueue, frmMain.FGridCalls, CurrentReadItems, CurrentChangedItems, CurrentUrgentItems)
    Else
        Call CMF.CopyArrayToFGrid(FilteredQueue, frmMain.FGridCalls)
    End If
    
    ' Now retrieve the sizes of each column and place them in the CurrentQueueColSizes dyndb
    If CurrentQueueColSizes.GetNumRows = 0 Then
        GetColSizes
        SetColSizes
    Else
        SetColSizes
    End If
    
    Call CMF.SetFGridTitles(FilteredQueueTitles, frmMain.FGridCalls)
    
    frmMain.lblRequestInfo.Caption = "Requests Displayed: " + Trim(Str(CurrentQueue.GetNumRows))
End Function

Public Function GetColSizes()
    Dim Col As Integer
    ' Retrieve the sizes of the columns and place them in the global colsizes list
    Call CurrentQueueColSizes.SetSize(1, frmMain.FGridCalls.Cols)
    
    For Col = 0 To frmMain.FGridCalls.Cols - 1
        Call CurrentQueueColSizes.SetElement(1, Col + 1, Trim(Str(frmMain.FGridCalls.ColWidth(Col))))
    Next
End Function

Public Function SetColSizes()
    ' Read through the col sizes in the global list, and set the column sizes to match
    Dim Row As Single
    
    For Row = 1 To CurrentQueueColSizes.GetNumRows
        If (Row - 1) <= frmMain.FGridCalls.Cols - 1 Then
            frmMain.FGridCalls.ColWidth(Row - 1) = Val(CurrentQueueColSizes.GetElement(1, Row))
        End If
    Next
End Function

Public Function GetSingleRequestDetails(RequestID As String, DestDynDB As DynDatabaseEX, AssocUsers As DynDatabaseEX)
    Call RichHandler.GetSingleRequestDetails(RequestID, DestDynDB, AssocUsers)
End Function

Public Function GetActionDetails(RequestID As String, DestDynDB As DynDatabaseEX)
    Call RichHandler.GetActionDetails(RequestID, DestDynDB)
End Function

Public Function GetActivityDetails(RequestID As String, DestDynDB As DynDatabaseEX)
    Call RichHandler.GetActivityDetails(RequestID, DestDynDB)
End Function


Public Function FList_AddSpecialistQueue(SpecialistID As String)
    ' Function to add a specialists queue to the folder list
    Dim StatusDescriptions As New DynDatabaseEX
    Dim Row As Single
    Dim TempElement As String
    
    Call FolderList.AddRoot(SpecialistID, "Requests for " + SpecialistID)
    ' Get the status descriptions for adding to the folder list
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    
    ' Now add a node for each status description
    For Row = 1 To StatusDescriptions.GetNumRows
        TempElement = StatusDescriptions.GetElement(1, Row) 'This is the description of the status
        Call FolderList.AddNode(SpecialistID, SpecialistID + "$" + TempElement, TempElement)
    Next
    
    ' Now add a node for All Closed Requests
    Call FolderList.AddNode(SpecialistID, SpecialistID + "$" + "ALLCLOSED", "All Closed Requests")
    
    ' Now add a node for All Active Requests
    Call FolderList.AddNode(SpecialistID, SpecialistID + "$" + "ALLACTIVE", "All Active Requests")
    
    ' Now add a node for logged call, but for each different status
    Call FolderList.AddNode(SpecialistID, SpecialistID + "$" + "LOGGEDCALLS", "Logged Calls")
    
    ' Now add a node for each status under logged calls
    For Row = 1 To StatusDescriptions.GetNumRows
        TempElement = StatusDescriptions.GetElement(1, Row)  'The description of the status
        Call FolderList.AddNode(SpecialistID + "$" + "LOGGEDCALLS", SpecialistID + "$" + "LOGGEDCALLS" + "$" + TempElement, TempElement)
    Next
    
    'Now check if we've added a queue for the current user. If we have then refresh the unread items
    If SpecialistID = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase)) Then
        FList_RefreshUnreadItems
    End If
End Function

Public Function FList_AddNonClosed(SpecialistID As String)
    'Function to add the all non closed calls to the specialists folder list
    'Call FolderList.AddNode(SpecialistID, SpecialistID + "$" + "ALLACTIVE", "All Active Requests")
End Function

Public Function FList_AddSpecialistGroup(GroupName As String)
    ' Function to add a specialist entire group to the folder list
    Dim StatusDescriptions As New DynDatabaseEX
    Dim Row As Single
    Dim StatusRow As Single
    Dim TempStatus As String
    Dim TempElement As String
    Dim GroupSpecialists As New DynDatabaseEX
    Dim GroupID As String
    
    ' Get the specialists assigned to this group from the database
     GroupID = RichHandler.GetGroupID(GroupName)
    Call RichHandler.GetGroupSpecialists(GroupID, GroupSpecialists)
    
    ' Get the status descriptions for adding to the folder list
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    
    ' Now construct the nodes for the folder list
    Call FolderList.AddRoot(GroupName, GroupName + " Group")
    
    ' Now add a node for each specialist, and for each specialist add a status node
    For Row = 1 To GroupSpecialists.GetNumRows
        TempElement = GroupSpecialists.GetElement(1, Row)
        Call FolderList.AddNode(GroupName, GroupName + "$" + TempElement, "Requests for " + TempElement)
        ' Now add a node for each description under this specialist
        For StatusRow = 1 To StatusDescriptions.GetNumRows
            TempStatus = StatusDescriptions.GetElement(1, StatusRow)
            Call FolderList.AddNode(GroupName + "$" + TempElement, GroupName + "$" + TempElement + "$" + TempStatus, TempStatus)
        Next
    
        ' Now add a node for the Closed Requests
        Call FolderList.AddNode(GroupName + "$" + TempElement, GroupName + "$" + TempElement + "$" + "ALLCLOSED", "All Closed Requests")
    
        'Now add a node for the Active Requests
        Call FolderList.AddNode(GroupName + "$" + TempElement, GroupName + "$" + TempElement + "$" + "ALLACTIVE", "All Active Requests")
    
        'Now add a node for the logged calls for this specialist
        Call FolderList.AddNode(GroupName + "$" + TempElement, GroupName + "$" + TempElement + "$" + "LOGGEDCALLS", "Logged Calls")
        
        'Now add a node for each status under logged calls
        For StatusRow = 1 To StatusDescriptions.GetNumRows
            TempStatus = StatusDescriptions.GetElement(1, StatusRow)
            Call FolderList.AddNode(GroupName + "$" + TempElement + "$" + "LOGGEDCALLS", GroupName + "$" + TempElement + "$" + "LOGGEDCALLS" + "$" + TempStatus, TempStatus)
        Next
    Next
    
    ' Now check if this is the group for the current user, if it is then refresh the unread items
    If RichHandler.GetSpecialistGroupName(Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))) = GroupName Then
        FList_RefreshUnreadItems
    End If
End Function

Public Function FList_AddUserQueue(UserID As String)
    ' Function to add a user queue to the user folder list
    Dim StatusDescriptions As New DynDatabaseEX
    Dim Row As Single
    Dim TempElement As String

    Call FolderList.AddUserRoot(UserID, "Associated with " + UserID)
    ' Get the status descriptions for adding to the folder list
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
        
    ' Now add a node for each status descriptions associated with the userid
    For Row = 1 To StatusDescriptions.GetNumRows
        TempElement = StatusDescriptions.GetElement(1, Row)  ' This is the description of the status
        Call FolderList.AddUserNode(UserID, UserID + "$" + TempElement, TempElement)
    Next
    
    ' Now add a node for the Active Requests
    Call FolderList.AddUserNode(UserID, UserID + "$" + "ALLACTIVE", "All Active Requests")
    
    ' Now add a node for the Closed Requests
    Call FolderList.AddUserNode(UserID, UserID + "$" + "ALLCLOSED", "All Closed Requests")
End Function

Public Function FList_AddDQueue(UserID As String)
    ' Function to add a user queue to the deleted folder list
    Dim StatusDescriptions As New DynDatabaseEX
    Dim Row As Single
    Dim TempElement As String

    Call FolderList.AddDRoot(UserID, "Associated with " + UserID)
    ' Get the status descriptions for adding to the folder list
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
        
    ' Now add a node for each status descriptions associated with the userid
    For Row = 1 To StatusDescriptions.GetNumRows
        TempElement = StatusDescriptions.GetElement(1, Row)  ' This is the description of the status
        Call FolderList.AddDNode(UserID, UserID + "$" + TempElement, TempElement)
    Next
    
    ' Now add a node for the Active Requests
    Call FolderList.AddDNode(UserID, UserID + "$" + "ALLACTIVE", "All Active Requests")
End Function

Public Function FList_MarkUnread(SpecialistID As String, StatusDescription As String, NumUnRead As Integer)
    Dim N As Integer
    Dim SpecialistGroup As String
    Dim SearchKey1 As String
    Dim SearchKey2 As String
    
    ' Get the specialists group to put in the second search key
    SpecialistGroup = Trim(RichHandler.GetSpecialistGroupName(SpecialistID))
    
    ' Define the search keys given the parameters
    SearchKey1 = SpecialistGroup + "$" + Trim(SpecialistID) + "$" + Trim(StatusDescription)
    SearchKey2 = Trim(SpecialistID) + "$" + Trim(StatusDescription)
    
    For N = 1 To frmMain.treQueues.Nodes.Count
        If NumUnRead > 0 Then
            If frmMain.treQueues.Nodes(N).Key = SearchKey1 Then
                frmMain.treQueues.Nodes(N).Text = StatusDescription + " (" + Trim(Str(NumUnRead)) + ")"
                frmMain.treQueues.Nodes(N).Bold = True
            End If
            
            If frmMain.treQueues.Nodes(N).Key = SearchKey2 Then
                frmMain.treQueues.Nodes(N).Text = StatusDescription + " (" + Trim(Str(NumUnRead)) + ")"
                frmMain.treQueues.Nodes(N).Bold = True
            End If
        Else
            If frmMain.treQueues.Nodes(N).Key = SearchKey1 Then
                frmMain.treQueues.Nodes(N).Text = StatusDescription
                frmMain.treQueues.Nodes(N).Bold = False
            End If
            
            If frmMain.treQueues.Nodes(N).Key = SearchKey2 Then
                frmMain.treQueues.Nodes(N).Text = StatusDescription
                frmMain.treQueues.Nodes(N).Bold = False
            End If
        End If
    Next
End Function

Public Function FList_RefreshUnreadItems()
    ' Function to read the current specialists queue for each status, and if any unread items are found then the folderlist unread
    ' Indicator is updated accordingly
    
    Dim Row As Single
    Dim TempStatus As String
    Dim StatusDescriptions As New DynDatabaseEX
    Dim SpecialistID As String
    Dim StatusID As String
    Dim TempRequests As New DynDatabaseEX
    Dim TempTitles As New DynDatabaseEX
    Dim TempReadItems As New DynDatabaseEX
    Dim TempUnreadItems As Integer
    
    Dim TempChangedItems As New DynDatabaseEX
    Dim TempNumChangedItems As Integer
    
    Dim TempUrgentItems As New DynDatabaseEX
    
    SpecialistID = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    
    For Row = 1 To StatusDescriptions.GetNumRows
        TempStatus = StatusDescriptions.GetElement(1, Row)
        StatusID = RichHandler.GetStatusID(TempStatus)
        
        If CMF.FindString(Trim(StrConv(TempStatus, vbUpperCase)), "CLOSED", True) = 0 Then
            Call RichHandler.GetSpecialistRequestsEX(SpecialistID, StatusID, TempRequests, TempTitles, TempReadItems, TempChangedItems, TempUrgentItems, TempUnreadItems, TempNumChangedItems)
            Call FList_MarkUnread(SpecialistID, TempStatus, TempUnreadItems)
        End If
        
    Next
End Function

Public Function FList_GetExpandedNodes()
    ' This function builds a list of all the items in the specialists treeview control that are expanded.
    
    Dim N As Integer
    
    ExpandedItems.ClearList
    
    For N = 1 To frmMain.treQueues.Nodes.Count
        If frmMain.treQueues.Nodes(N).Expanded = True Then
            ExpandedItems.AddItem (frmMain.treQueues.Nodes(N).Key)
        End If
    Next
    
End Function

Public Function FList_SetExpandedNodes()
    'This function reads through the expanded items list and expands any nodes that are in the list
    
    Dim N As Integer
    Dim LRow As Integer
    Dim CurrentExpandedKey As String
    
    For LRow = 1 To ExpandedItems.GetSize
        CurrentExpandedKey = ExpandedItems.GetItem(LRow)
        For N = 1 To frmMain.treQueues.Nodes.Count
            If frmMain.treQueues.Nodes(N).Key = CurrentExpandedKey Then
                ' Set this node to expanded
                frmMain.treQueues.Nodes(N).Expanded = True
            End If
        Next
    Next
    
End Function

Public Function AddReadItem(RequestID As String)
    Call RichHandler.AddReadItem(RequestID)
End Function

Public Function AddChangedItem(RequestID As String, DateStamp As String)
    Call RichHandler.AddChangedItem(RequestID, DateStamp)
End Function

Public Function IsItemRead(RequestID As String) As Boolean
    IsItemRead = RichHandler.IsItemRead(RequestID)
End Function

Public Function IsItemChanged(RequestID As String, DateStamp As String) As Boolean
    IsItemChanged = RichHandler.IsItemChanged(RequestID, DateStamp)
End Function

Public Function SaveReadItems()
    Call RichHandler.SaveReadItemsFile
End Function

Public Function SaveWatchedItems()
    Call RichHandler.SaveWatchedItems
End Function


Public Function TestFunction()
    'Dim AssocUsers As New DynDatabaseEX
    
    'Call AssocUsers.SetSize(1, 1)
    'Call AssocUsers.SetElement(1, 1, "CAYTONC")
    
    'Call RichHandler.AddRequest("Medium - 4 Hours", "CORP CST", "Open", "Monitor", "DRAPERD", "DRAPERD", "Test Call", "Test Details", "Test Resolution", AssocUsers)
    
    'MsgBox "Call Added"
    
    'MsgBox RichHandler.GetNumUserRequestsClosedTotal(Main.GetCurrentLoggedOnUser)
    MsgBox InvHandler.GetDisplayName("SCARLETJ")
End Function

Public Function AddRequest(bChange As Boolean, Priority As String, GroupName As String, StatusDescription As String, CategoryDescription As String, Specialist As String, LoggedBy As String, LoggedDate As String, _
                                                Summary As String, Details As String, Resolution As String, AssocUsers As DynDatabaseEX) As String
                                                
    AddRequest = RichHandler.AddRequest(bChange, Priority, GroupName, StatusDescription, CategoryDescription, Specialist, LoggedBy, LoggedDate, Summary, Details, Resolution, AssocUsers)
    
End Function

Public Function AddRequestTemplate(Priority As String, Status As String, Category As String, Summary As String, Details As String, Resolution As String, TemplateName As String, TemplateComments As String)
    ' First check if the template name already exists in the database
    If RichHandler.DoesRequestTemplateExist(TemplateName) = True Then
        MsgBox "Template Name already exists. Please enter another Template Name.", vbInformation, "Cede SupportDesk"
        Exit Function
    End If
    
    ' Now attempt to add the template
    If RichHandler.AddRequestTemplate(Priority, Status, Category, Summary, Details, Resolution, TemplateName, TemplateComments) = True Then
        MsgBox "Template Added Successfully.", vbInformation, "Cede SupportDesk"
    End If
End Function

Public Function GetAvailableRequestTemplates(DestDynDB As DynDatabaseEX)
    ' Retrieve the available templates from the database
    Call RichHandler.GetAvailableTemplates(DestDynDB)
End Function

Public Function GetRequestTemplate(TemplateName As String, DestDynDB As DynDatabaseEX)
    ' function to retrieve the details of a request template
    Call RichHandler.GetTemplateDetails(TemplateName, DestDynDB)
End Function

Public Function DeleteRequestTemplate(TemplateName As String)
    Call RichHandler.DeleteRequestTemplate(TemplateName)
End Function

Public Function RenameRequestTemplate(ExistingTemplateName As String, NewTemplateName As String, NewDescription As String)
    ' First check if the new template name already exists in the database
    If ExistingTemplateName <> NewTemplateName Then
        If RichHandler.DoesRequestTemplateExist(NewTemplateName) = True Then
            MsgBox "Requested Template Name already exists. Please enter another Template Name.", vbInformation, "Cede SupportDesk"
            Exit Function
        End If
    
        Call RichHandler.RenameRequestTemplate(ExistingTemplateName, NewTemplateName, NewDescription)
    Else
        Call RichHandler.RenameRequestTemplate(ExistingTemplateName, NewTemplateName, NewDescription)
    End If
End Function

Public Function AddActionTemplate(ActionType As String, ActionDetails As String, TemplateName As String, TemplateComments As String)
    ' First check if the template name already exists in the database
    If RichHandler.DoesActionTemplateExist(TemplateName) = True Then
        MsgBox "Template Name already exists. Please enter another Template Name.", vbInformation, "Cede SupportDesk"
        Exit Function
    End If
        
    ' Now attempt to add the template
    If RichHandler.AddActionTemplate(ActionType, ActionDetails, TemplateName, TemplateComments) = True Then
        MsgBox "Template Added Successfully.", vbInformation, "Cede SupportDesk"
    End If
End Function

Public Function GetAvailableActionTemplates(DestDynDB As DynDatabaseEX)
    ' Retrieve the available templates from the database
    Call RichHandler.GetAvailableActionTemplates(DestDynDB)
End Function

Public Function GetActionTemplate(TemplateName As String, DestDynDB As DynDatabaseEX)
    ' function to retrieve the details of a request template
    Call RichHandler.GetActionTemplateDetails(TemplateName, DestDynDB)
End Function

Public Function DeleteActionTemplate(TemplateName As String)
    Call RichHandler.DeleteActionTemplate(TemplateName)
End Function

Public Function RenameActionTemplate(ExistingTemplateName As String, NewTemplateName As String, NewDescription As String)
    ' First check if the new template name already exists in the database
    If ExistingTemplateName <> NewTemplateName Then
        If RichHandler.DoesActionTemplateExist(NewTemplateName) = True Then
            MsgBox "Requested Template Name already exists. Please enter another Template Name.", vbInformation, "Cede SupportDesk"
            Exit Function
        End If
    
        Call RichHandler.RenameActionTemplate(ExistingTemplateName, NewTemplateName, NewDescription)
    Else
        Call RichHandler.RenameActionTemplate(ExistingTemplateName, NewTemplateName, NewDescription)
    End If
End Function

Public Function DeleteRequestTIE(ReqID As String, UserID As String) As Boolean
    DeleteRequestTIE = RichHandler.RemoveRequestTIE(ReqID, UserID)
End Function

Public Function UpdateRequest(RequestID As String, bChange As Boolean, NewPriority As String, NewStatus As String, NewCategory As String, NewSpecialist As String, NewSummary As String, NewDetails As String, NewResolution As String, NewAssocUsers As DynDatabaseEX)

    Call RichHandler.UpdateRequest(RequestID, bChange, NewPriority, NewStatus, NewCategory, NewSpecialist, NewSummary, NewDetails, NewResolution, NewAssocUsers)

End Function

Public Function UpdateRequestDateStamp(RequestID As String)
    Call RichHandler.UpdateDateStamp(RequestID)
End Function

Public Function ReAssignRequest(RequestID As String, NewSpecialist As String)
    Call RichHandler.ReAssignRequest(RequestID, NewSpecialist)
    
    If Trim(NewSpecialist) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
        AddNewRequestToNew (RequestID) 'This is so that we don't get a popup for a request we may have assigned to ourselves!
        AddReadItem (RequestID)
    Else
        Call RichHandler.RemoveReadItem(RequestID)
    End If
End Function

Public Function AddRequestAction(RequestID As String, ActionType As String, ActionDate As String, Description As String, ActionBy As String, RequestStatus As String, RequestPriority As String)
    Call RichHandler.AddAction(RequestID, ActionType, ActionBy, ActionDate, Description, RequestStatus, RequestPriority)
End Function
Public Function AddRequestActivity(RequestID As String, ActionType As String, ActivityFor As String, ActivityDate As String, Description As String, AssignedBy As String)
    Call RichHandler.AddActivity(RequestID, ActionType, ActivityFor, ActivityDate, AssignedBy, Description)
End Function
Public Function UpdateRequestAction(ActionID As String, NewActionBy As String, NewActionType As String, NewActionDate As String, NewDescription As String)
    Call RichHandler.UpdateSingleAction(ActionID, NewActionBy, NewActionType, NewActionDate, NewDescription)
End Function
Public Function GetRequestLog(RequestID As String, DestDynDB As DynDatabaseEX)
    Call RichHandler.GetRequestLog(RequestID, DestDynDB)
End Function
Public Function UpdateRequestActivity(ActivityID As String, NewActivityType As String, NewActivityDate As String, NewActivityFor As String, NewActivityDetails As String)
    Call RichHandler.UpdateSingleActivity(ActivityID, NewActivityType, NewActivityFor, NewActivityDate, NewActivityDetails)
End Function


Public Function GetCallDetails(ReqID As String) As String
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim DBResults As New DynDatabaseEX
    Dim ChosenFieldString As String
    
    ChosenFieldString = "Details"
    
    QueryFields.AddItem ("ID")
    QueryValues.AddItem (ReqID)
    
    Call SQL.MULTReadQuery("SupportCalls", QueryFields, QueryValues, ChosenFieldString, DBResults, False)
    
    If DBResults.GetNumCols > 0 And DBResults.GetNumRows > 0 Then
        GetCallDetails = DBResults.GetElement(1, 1)
        
    End If
    
    If Trim(GetCallDetails) = "" Then
        GetCallDetails = "<No Details>"
    End If
End Function
Public Function GetCallResolution(ReqID As String) As String
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim DBResults As New DynDatabaseEX
    Dim ChosenFieldString As String
    
    ChosenFieldString = "Resolution"
    
    QueryFields.AddItem ("ID")
    QueryValues.AddItem (ReqID)
    
    Call SQL.MULTReadQuery("SupportCalls", QueryFields, QueryValues, ChosenFieldString, DBResults, False)
    
    If DBResults.GetNumCols > 0 And DBResults.GetNumRows > 0 Then
        GetCallResolution = DBResults.GetElement(1, 1)
    End If
    
    If Trim(GetCallResolution) = "" Then
        GetCallResolution = "<No Resolution>"
    End If
End Function

Public Function FolderListClicked(NodeKey As String, NodeName As String)
    
    ' This is where we have to parse the nodekey and determine what to retrieve
    Dim NumKeyItems As Integer
    Dim SpecialistID As String
    Dim StatusDescription As String
    Dim GroupName As String
    Dim StatusID As String
    Dim TempElement As String
    
    NumKeyItems = CMF.GetNumDataParts(NodeKey, "$")
    
    ' Set the mouse pointer to busy
    frmMain.MousePointer = 11
    
    'get the col sizes if the program has been intialised
    If ProgInitialised = True Then
        GetColSizes
    End If
    
    ' Place the nodekey in the global variable
    LastNodeKey = NodeKey
    
    If NumKeyItems = 1 Then 'One item, then this must ne in the form <GROUP> or <SPECIALIST>
        ' We must find out if this is a valid group before proceeding
        If RichHandler.IsValidGroup(NodeKey) = True Then
            GetGroupRequests (NodeKey)
            DisplayCurrentQueue (False)
            frmMain.lblTitle.Caption = "All Requests for " + NodeKey + "."
        End If
    End If
    
    If NumKeyItems = 2 Then 'Two items, then this must be in the form <SPECIALIST><STATUSDESCRIPTION>
            SpecialistID = CMF.GetDataPart(NodeKey, 1, "$")
            StatusDescription = CMF.GetDataPart(NodeKey, 2, "$")
            
            If RichHandler.IsValidSpecialist(SpecialistID) = True Then
                
                'Check if the all closed requests has been selected
                If StatusDescription = "ALLCLOSED" Then
                    'MsgBox "Woohoo All Closed - Specialist"
                    GetAllClosedQueue (SpecialistID)
                    DisplayCurrentQueue (False)
                    frmMain.lblTitle.Caption = "All Closed Requests for " + SpecialistID + "."
                End If
                
                ' Check if the all active requests has been selected
                If StatusDescription = "ALLACTIVE" Then
                    'MsgBox "Woohoo single!"
                    'call richhandler.GetActiveRequests (specialistid,
                    GetActiveQueue (SpecialistID)
                    DisplayCurrentQueue (False)
                    frmMain.lblTitle.Caption = "All Active Requests for " + SpecialistID + "."
                End If
                
                If RichHandler.IsValidStatus(StatusDescription) = True Then
                    ' We now have the specialist and status description, now we need the status ID for the status description
                    ' For this we need to ask the Richmond Handler for this value
                    StatusID = RichHandler.GetStatusID(StatusDescription)
                    
                    'Now we have all the info we need to get and display the queue for the selected item
                    Call GetSpecialistQueue(SpecialistID, StatusID)
                    
                    ' Check if we are displaying the logged on users queue, if yes then show unread items
                    If Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase)) = SpecialistID Then
                        DisplayCurrentQueue (True)
                    Else
                        DisplayCurrentQueue (False)
                    End If
                    frmMain.lblTitle.Caption = "Requests for " + SpecialistID + " with Status " + StatusDescription + "."
                End If
            
            End If
            
    End If
    
    If NumKeyItems = 3 Then 'Three Items, then this might be in the form <GROUP><SPECIALIST><STATUSDESCRIPTION> or <SPECIALIST><LOGGEDCALLS><STATUSDESCRIPTION>
        ' Check the second item, which is the one that will tell the key type apart
        TempElement = CMF.GetDataPart(NodeKey, 2, "$")
        
        If TempElement = "LOGGEDCALLS" Then
            SpecialistID = CMF.GetDataPart(NodeKey, 1, "$")
            StatusDescription = CMF.GetDataPart(NodeKey, 3, "$")
            
            If RichHandler.IsValidSpecialist(SpecialistID) = True Then
                If RichHandler.IsValidStatus(StatusDescription) = True Then
                    ' We now have the specialist and status description, now we need the status ID for the status description
                    ' For this we need to ask the Richmond Handler for this value
                    StatusID = RichHandler.GetStatusID(StatusDescription)
                                        
                    'Now we have all the info we need to get and display the queue for the selected item
                    Call GetLoggedQueue(SpecialistID, StatusID)
                    DisplayCurrentQueue (False)
                    frmMain.lblTitle.Caption = "Calls Logged by " + SpecialistID + " with Status " + StatusDescription + "."
                End If
            End If
        Else
            
            GroupName = CMF.GetDataPart(NodeKey, 1, "$")
            SpecialistID = CMF.GetDataPart(NodeKey, 2, "$")
            StatusDescription = CMF.GetDataPart(NodeKey, 3, "$")
            
            ' Check here if the selected option is the all closed requests option
            If StatusDescription = "ALLCLOSED" Then
                GetAllClosedQueue (SpecialistID)
                DisplayCurrentQueue (False)
                frmMain.lblTitle.Caption = "All Closed Requests for " + SpecialistID + "."
            End If
            
            'Check here if the selected option is the all active status requests
            If StatusDescription = "ALLACTIVE" Then
                'MsgBox "Woohoo!"
                GetActiveQueue (SpecialistID)
                DisplayCurrentQueue (False)
                frmMain.lblTitle.Caption = "All Active Requests for " + SpecialistID + "."
            End If
            
            If RichHandler.IsValidGroup(GroupName) = True Then
                If RichHandler.IsValidSpecialist(SpecialistID) = True Then
                    If RichHandler.IsValidStatus(StatusDescription) = True Then
                        ' We now have the specialist and status description, now we need the status ID for the status description
                        ' For this we need to ask the Richmond Handler for this value
                        StatusID = RichHandler.GetStatusID(StatusDescription)
                                               
                        'Now we have all the info we need to get and display the queue for the selected item
                        Call GetSpecialistQueue(SpecialistID, StatusID)
                        
                        ' Check if we are displaying the logged on users queue, if yes then show unread items
                        If Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase)) = SpecialistID Then
                            DisplayCurrentQueue (True)
                        Else
                            DisplayCurrentQueue (False)
                        End If
                        frmMain.lblTitle.Caption = "Requests for " + SpecialistID + " with Status " + StatusDescription + "."
                    End If
                End If
            End If

        End If
        
    End If
    
    If NumKeyItems = 4 Then 'Four items, then this must be in the form <GROUP><SPECIALIST><LOGGEDCALLS><STATUSDESCRIPTION>
        GroupName = CMF.GetDataPart(NodeKey, 1, "$")
        SpecialistID = CMF.GetDataPart(NodeKey, 2, "$")
        StatusDescription = CMF.GetDataPart(NodeKey, 4, "$")

        If RichHandler.IsValidGroup(GroupName) = True Then
            If RichHandler.IsValidSpecialist(SpecialistID) = True Then
                If RichHandler.IsValidStatus(StatusDescription) = True Then
                    ' We now have the specialist and status description, now we need the status ID for the status description
                    ' For this we need to ask the Richmond Handler for this value
                    StatusID = RichHandler.GetStatusID(StatusDescription)
                    
                    'Now we have all the info we need to get and display the queue for the selected item
                    Call GetLoggedQueue(SpecialistID, StatusID)
                    DisplayCurrentQueue (False)
                    frmMain.lblTitle.Caption = "Calls Logged by " + SpecialistID + " with Status " + StatusDescription + "."
                End If
            End If
        End If
    End If
    
    'Set the mouse pointer to not busy
    frmMain.MousePointer = 0
End Function
Public Function UserFolderListClicked(NodeKey As String, NodeName As String)
    ' Parse the node key and determine the user ID and selected status
    Dim NumKeyItems As Integer
    Dim UserID As String
    Dim StatusDescription As String
    Dim StatusID As String
    Dim ContactDetails As New DynDatabaseEX
    
    Dim Firstname As String
    Dim Lastname As String
    Dim Department As String
    Dim Location As String
    Dim Telephone As String
    Dim Mobile As String
    
    LastUserNodeKey = NodeKey
    
    NumKeyItems = CMF.GetNumDataParts(NodeKey, "$")
    
    If CMF.FindString(NodeKey, "$", False) = 0 Then 'One item, this must be in the form <USERID>
        Call RichHandler.GetContactDetails(NodeKey, ContactDetails)
        
        Firstname = ContactDetails.GetElement(1, 1)
        Lastname = ContactDetails.GetElement(2, 1)
        Department = ContactDetails.GetElement(3, 1)
        Location = ContactDetails.GetElement(4, 1)
        Telephone = ContactDetails.GetElement(5, 1)
        Mobile = ContactDetails.GetElement(6, 1)
        
        frmMain.StatusBar1.SimpleText = NodeKey + " - " + Firstname + " " + Lastname + "." + "     Dept: " + Department + "     Location: " + Location + "     Telephone: " + Telephone + "     Mobile: " + Mobile
    End If
    
    If NumKeyItems = 2 Then 'Two items, then this must be in the form <USERID><STATUSDESCRIPTION>
        UserID = CMF.GetDataPart(NodeKey, 1, "$")
        StatusDescription = CMF.GetDataPart(NodeKey, 2, "$")
                
        If StatusDescription = "ALLACTIVE" Then
            ' We don't really need the status id, but just for completion of the function parameters...
            StatusID = RichHandler.GetStatusID(StatusDescription)
            
            ' Now we retrieve the associated user queue with the allactive flag set
            Call GetAssocUserQueue(UserID, StatusID, True, False)
            DisplayCurrentQueue (False)
            frmMain.lblTitle.Caption = "All Active Requests associated with " + UserID + "."
        Else
        
            If StatusDescription = "ALLCLOSED" Then
                ' We don't really need the status id, but just for completion of the function parameters...
                StatusID = RichHandler.GetStatusID(StatusDescription)
            
                ' Now we retrieve the associated user queue with the allactive flag set
                Call GetAssocUserQueue(UserID, StatusID, False, True)
                DisplayCurrentQueue (False)
                frmMain.lblTitle.Caption = "All Closed Requests associated with " + UserID + "."
            Else
                ' We now need the StatusID for this description
                StatusID = RichHandler.GetStatusID(StatusDescription)
        
                ' Now we have all the info we need to get and display the associated users requests
                Call GetAssocUserQueue(UserID, StatusID, False, False)
                DisplayCurrentQueue (False)
                frmMain.lblTitle.Caption = "Requests associated with " + UserID + " with Status " + StatusDescription + "."
            End If
            
        End If
    End If
End Function

Public Function GetGroupDescriptions(DestDynDB As DynDatabaseEX)
    ' This is used by the Add Queue form so that the user can choose a specialist queue to add
    Call RichHandler.GetGroupDescriptions(DestDynDB)
End Function
Public Function GetStatusDescriptions(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetStatusDescriptions(DestDynDB)
End Function
Public Function GetCategoryDescriptions(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetCategoryDescriptions(DestDynDB)
End Function
Public Function GetActionTypeDescriptions(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetActionTypeDescriptions(DestDynDB)
End Function

Public Function GetPriorityDescriptions(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetPriorityDescriptions(DestDynDB)
End Function
Public Function GetGroupID(GroupDescription As String) As String
    ' Used by the Add queue form
    GetGroupID = RichHandler.GetGroupID(GroupDescription)
End Function
Public Function GetSpecialistGroupName(SpecialistID As String) As String
    GetSpecialistGroupName = RichHandler.GetSpecialistGroupName(SpecialistID)
End Function
Public Function GetGroupSpecialists(GroupID As String, DestDynDB As DynDatabaseEX)
    ' Used by the Add queue form
    Call RichHandler.GetGroupSpecialists(GroupID, DestDynDB)
End Function
Public Function GetAllSpecialists(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetAllSpecialists(DestDynDB)
End Function
Public Function GetAssocUserDetails(DestDynDB As DynDatabaseEX)
    Call RichHandler.GetAssocUserDetails(DestDynDB)
End Function

Public Function GetAttachPath() As String
    ' Retrieve the path where attachments are stored
    GetAttachPath = RichHandler.GetAttachmentPath
End Function

Public Function GetRequestAttachDetails(RequestID As String, DestDynDB As DynDatabaseEX) As String
    ' Retrieve attachment details for a request
    'Call RichHandler.GetRequestAttachDetails(RequestID, DestDynDB) 'LiveSupDesk method of attachments
    
    Call RichHandler.GetRequestAttachDetailsEX(RequestID, DestDynDB)
End Function

Public Function OpenRichAttachment(AttachID As String, FileName As String)
    Call RichHandler.OpenRichAttach(AttachID, FileName)
End Function

Public Function SaveRichAttachment(AttachID As String, DestFileName As String)
    Call RichHandler.SaveRichAttach(AttachID, DestFileName)
End Function

Public Function AddRequestAttachment(RequestID As String, FileName As String, SourcePath As String)
    ' Add a file attachment to a request
    'Call RichHandler.AddRequestAttachment(RequestID, FileName, SourcePath) 'New LiveSupDesk attachment style
    
    Call RichHandler.AddRequestAttachmentEX(RequestID, FileName, SourcePath) ' old richmond style
    
End Function

Public Function DeleteRequestAttachment(RequestID As String, FileName As String)
    ' Remove a request attachment from the database, this also attempts to delete the file from the file store.
    Call RichHandler.RemoveRequestAttachment(RequestID, FileName)
End Function

Public Function GetNumRequestAttachments(RequestID As String) As Single
    GetNumRequestAttachments = RichHandler.GetNumRequestAttachments(RequestID)
End Function

Public Function GetUserList()
    Dim Row As Single
    
    ' Get the list of the users
    ' If the user has selected all users
    Call RichHandler.GetAssocUsernames(UserList, 0)

    ' Now we have the list of the users, we need to add a queue for each user in the user queues folder list.
    ShowMainStaticStatus
    UpdateMainStaticStatus ("Populating User Queues...")
    DoEvents
    ' Cycle through each user in the list adding a queue for each in the user folder list.
    For Row = 1 To UserList.GetSize
        FList_AddUserQueue (UserList.GetItem((Row)))
        'Call UpdateMainProgressStatus("Populating User Queues...", UserList.GetSize, Row)
        'DoEvents
    Next
    DoEvents
    HideMainStaticStatus
End Function

Public Function GetDeletedUserList()
    Dim Row As Single
    
    ' Get the list of the deleted users
    Call RichHandler.GetAssocUsernames(DeletedUserList, 1)

    ' Now we have the list of the users, we need to add a queue for each user in the user queues folder list.
    ShowMainStaticStatus
    UpdateMainStaticStatus ("Populating Deleted User Queues...")
    DoEvents
    ' Cycle through each user in the list adding a queue for each in the user folder list.
    For Row = 1 To DeletedUserList.GetSize
        FList_AddDQueue (DeletedUserList.GetItem((Row)))
        'Call UpdateMainProgressStatus("Populating User Queues...", UserList.GetSize, Row)
        'DoEvents
    Next
    DoEvents
    HideMainStaticStatus
End Function

Public Function GetContactDetails(Username As String, DestDynDB As DynDatabaseEX)
    ' Interface function to retrieve contact details
    Call RichHandler.GetContactDetails(Username, DestDynDB)
End Function

Public Function FilterUserList(SearchUserString As String)
    Dim N As Integer
    Dim TempString As String
    
    'This function highlights the item in the user list which best matches what the
    ' user has typed in the search string
       
    For N = 1 To frmMain.treUserQueues.Nodes.Count
        TempString = frmMain.treUserQueues.Nodes(N).Key
        If StrConv(Mid(TempString, 1, Len(SearchUserString)), vbUpperCase) = StrConv(SearchUserString, vbUpperCase) Then
            frmMain.treUserQueues.Nodes(N).Selected = True
            Exit For
        End If
    Next
End Function

Public Function FilterDeletedUserList(SearchUserString As String)
    Dim N As Integer
    Dim TempString As String
    
    'This function highlights the item in the user list which best matches what the
    ' user has typed in the search string
       
    For N = 1 To frmMain.treDQueues.Nodes.Count
        TempString = frmMain.treDQueues.Nodes(N).Key
        If StrConv(Mid(TempString, 1, Len(SearchUserString)), vbUpperCase) = StrConv(SearchUserString, vbUpperCase) Then
            frmMain.treDQueues.Nodes(N).Selected = True
            Exit For
        End If
    Next
End Function

Public Function CallGridClicked(RowSelected As Integer)
    ' The call grid was clicked, we now need to display a preview of the call in the preview pane
    ' First we need the request ID of the call, and we need to know which column the request id is in
    ' so look in the field titles, this will tell us!
        
    Dim ReqIDColumn As Integer
    Dim RequestID As String
    Dim RequestDetails As New DynDatabaseEX
    Dim AssocUsers As New DynDatabaseEX
    
    
    
    ' Only do this function is there is something that we can do it with!
    If CurrentQueueTitles.GetNumCols = 0 And CurrentQueueTitles.GetNumRows = 0 Then
        Exit Function
    End If
    
    ReqIDColumn = CMF.GetNameArrayLoc(CurrentQueueTitles, RichHandler.GetFTRequestID)
    RequestID = CurrentQueue.GetElement((ReqIDColumn), (RowSelected))
    
    'MsgBox RequestID
    
    If Trim(RequestID) <> "" Then
        Call RichHandler.GetSingleRequestDetails(RequestID, RequestDetails, AssocUsers)
    End If
    
    If RequestDetails.GetNumRows > 0 And RequestDetails.GetNumCols > 0 Then
        frmMain.txtDetails.Text = RequestDetails.GetElement(10, 1) 'Details are stored in column 10 of the returned row
    End If
End Function
Public Function CallGridDblClicked(RowSelected As Integer)
    ' The call grid was clicked, we now need to display a preview of the call in the preview pane
    ' First we need the request ID of the call, and we need to know which column the request id is in
    ' so look in the field titles, this will tell us!
    Dim ReqIDColumn As Integer
    Dim RequestID As String
    Dim RequestDetails As New DynDatabaseEX
    Dim AssocUsers As New DynDatabaseEX
    
    ' Only do this function is there is something that we can do it with!
    If CurrentQueueTitles.GetNumCols = 0 And CurrentQueueTitles.GetNumRows = 0 Then
        Exit Function
    End If
    
    frmMain.MousePointer = 11
    
    ReqIDColumn = CMF.GetNameArrayLoc(CurrentQueueTitles, RichHandler.GetFTRequestID)
    RequestID = CurrentQueue.GetElement((ReqIDColumn), (RowSelected))
    
    frmSingleRequestDetails.ShowRequestDetails (RequestID)
    frmSingleRequestDetails.Show
    
    frmMain.MousePointer = 0
    
End Function

Public Function GetSelectedRequestID() As String
    ' This function returns the selected Request ID highlighted in the call grid
    ' Used by the reassign request button
    
    Dim SelectedRow As Single
    Dim ReqIDColumn As Integer
    Dim RequestID As String
        
    SelectedRow = frmMain.FGridCalls.RowSel
    
    ' Only do this function if there is something that we can do it with - ie the grid has something in it!
    If CurrentQueueTitles.GetNumCols = 0 And CurrentQueueTitles.GetNumRows = 0 Then
        Exit Function
    End If
        
    ' Now find the request ID column, and retrieve that value from the current queue array
    ReqIDColumn = CMF.GetNameArrayLoc(CurrentQueueTitles, RichHandler.GetFTRequestID)
    RequestID = CurrentQueue.GetElement((ReqIDColumn), SelectedRow)
    
    GetSelectedRequestID = RequestID
    
End Function

Public Function RefreshCurrentSpecialistDisplay()
    ' Function to redisplay the list of calls displayed in the flexgrid. We only want to do this if the last queue displayed is any one of the
    ' current users queues apart from closed. This is so that the user doesn't have to manually refresh the display to see any unread items
    
    Dim CurrentSpecialistID As String
    Dim SpecialistGroup As String
    Dim StatusDescriptions As New DynDatabaseEX
    Dim Row As Single
    Dim TempStatus As String
    Dim TempStatusID As String
    Dim TempSearchKey As String
    
    GetColSizes
    CurrentSpecialistID = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    SpecialistGroup = RichHandler.GetSpecialistGroupName(CurrentSpecialistID)
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    
    For Row = 1 To StatusDescriptions.GetNumRows
        TempStatus = StatusDescriptions.GetElement(1, Row)
        TempStatusID = RichHandler.GetStatusID(TempStatus)
        
        If CMF.FindString(Trim(StrConv(TempStatus, vbUpperCase)), "CLOSED", True) = 0 Then
            TempSearchKey = SpecialistGroup + "$" + CurrentSpecialistID + "$" + Trim(TempStatus)
            
            If LastNodeKey = TempSearchKey Then
                Call GetSpecialistQueue(CurrentSpecialistID, TempStatusID)
                DisplayCurrentQueue (True)
            End If
            
            TempSearchKey = CurrentSpecialistID + "$" + Trim(TempStatus)
            
            If LastNodeKey = TempSearchKey Then
                Call GetSpecialistQueue(CurrentSpecialistID, TempStatusID)
                DisplayCurrentQueue (True)
            End If
            
        End If
    
    Next
    
End Function

Public Function RefreshLastDisplayedQueue()
    GetColSizes
    If frmMain.treUserQueues.Visible = False Then
        Call FolderListClicked(LastNodeKey, CMF.GetDataPart(LastNodeKey, CMF.GetNumDataParts(LastNodeKey, "$"), "$"))
    Else
        Call UserFolderListClicked(LastUserNodeKey, CMF.GetDataPart(LastUserNodeKey, CMF.GetNumDataParts(LastUserNodeKey, "$"), "$"))
    End If
End Function

Public Function GetCurrentLoggedOnUser() As String
    If Trim(UserOverride) = "" Then
        GetCurrentLoggedOnUser = Trim(PrimaryUsername)
    Else
        GetCurrentLoggedOnUser = Trim(UserOverride)
    End If
End Function

Private Function GetNumCalls(SpecialistID As String, StatusID As String) As Integer
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    Dim DBResults As New DynDatabaseEX
    Dim ChosenFieldString As String
    
    ChosenFieldString = "ID,SpecialistID,LoggedbyID,LoggedDate,Summary"
    
    QueryFields.AddItem ("SpecialistID")
    QueryValues.AddItem (SpecialistID)
    QueryFields.AddItem ("StatusID")
    QueryValues.AddItem (StatusID)
    
    Call SQL.MULTReadQuery("SupportCalls", QueryFields, QueryValues, ChosenFieldString, DBResults, False)
    GetNumCalls = DBResults.GetNumRows
End Function
Public Function GetNumOpenCalls(SpecialistID As String) As Integer
    GetNumOpenCalls = GetNumCalls(SpecialistID, "1")
End Function
Public Function GetNumClosedCalls(SpecialistID As String) As Integer
    GetNumClosedCalls = GetNumCalls(SpecialistID, "2")
End Function

Public Function GetFTRequestID() As String
    ' Get the name of the Request ID field
    GetFTRequestID = RichHandler.GetFTRequestID
End Function


Private Function GetFriendlyFieldTitle(FieldName As String) As String
    If FieldName = "ID" Then GetFriendlyFieldTitle = "ID"
    If FieldName = "SpecialistID" Then GetFriendlyFieldTitle = "Specialist"
    If FieldName = "LoggedbyID" Then GetFriendlyFieldTitle = "Logged By"
    If FieldName = "LoggedDate" Then GetFriendlyFieldTitle = "Logged Date"
    If FieldName = "Summary" Then GetFriendlyFieldTitle = "Call Summary"
End Function

Public Function LoadINIFile()
    Dim Path As String
    Dim Inline As String
    Dim CurrentSection As String
    Dim TempString As String
    Dim ChosenCols As New DynList
    Dim ColSizes As New DynList
    Dim Row As Single
    Dim PreviewSettingFound As Boolean
    Dim UseAutoStatusFound As Boolean
    
    PreviewSettingFound = False
    UseAutoStatusFound = False
    Path = App.Path + "\CedeSupportDesk.ini"
    If Dir(Path) <> "" Then
            Open Path For Input As #1
                While Not EOF(1)
                    Line Input #1, Inline
                    
                    If CMF.FindString(Inline, "[", True) <> 0 Then CurrentSection = Trim(Inline)
                    
                    If CMF.FindString(Inline, "SORTCOL", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SORTCOL=", True)
                        CurrentSortedColumn = Trim(TempString)
                    End If
        
                    If CMF.FindString(Inline, "SORTMODE", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SORTMODE=", True)
                        CurrentSortMode = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLServer", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLServer=", True)
                        SDSQLServer = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLPort", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLPort=", True)
                        SDSQLPort = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLCatalog", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLCatalog=", True)
                        SDSQLCatalog = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "DBConnectionString", True) <> 0 Then
                       TempString = CMF.FilterWord(Inline, "DBConnectionString=", True)
                       DatabaseConnectionString = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLUser", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLUser=", True)
                        SDSQLUser = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLPassword", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLPassword=", True)
                        SDSQLPassword = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "SDSQLTrustCon", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SDSQLTrustCon=", True)
                        If TempString = "TRUE" Then SDSQLTrustCon = True Else SDSQLTrustCon = False
                    End If
                    
                    If CMF.FindString(Inline, "IVSQLServer", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLServer=", True)
                        IVSQLServer = Trim(TempString)
                    End If
                       
                    If CMF.FindString(Inline, "IVSQLPort", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLPort=", True)
                        IVSQLPort = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "IVSQLCatalog", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLCatalog=", True)
                        IVSQLCatalog = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "IVSQLUser", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLUser=", True)
                        IVSQLUser = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "IVSQLPassword", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLPassword=", True)
                        IVSQLPassword = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "IVSQLTrustCon", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IVSQLTrustCon=", True)
                        If TempString = "TRUE" Then IVSQLTrustCon = True Else IVSQLTrustCon = False
                    End If
                    
                    If CMF.FindString(Inline, "DisplayRequestPopup", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "DisplayRequestPopup=", True)
                        If TempString = "TRUE" Then DisplayRequestPopup = True Else DisplayRequestPopup = False
                    End If
                    
                    If CMF.FindString(Inline, "SendReadReceipt", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "SendReadReceipt=", True)
                        If TempString = "TRUE" Then SendReadReceipt = True Else SendReadReceipt = False
                    End If
                               
                    If CMF.FindString(Inline, "DisplayEmailPrompt", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "DisplayEmailPrompt=", True)
                        If TempString = "TRUE" Then DisplayEmailPrompt = True Else DisplayEmailPrompt = False
                    End If
                    
                    If CMF.FindString(Inline, "UseAutoAbsence", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "UseAutoAbsence=", True)
                        If TempString = "TRUE" Then UseAutoAbsence = True Else UseAutoAbsence = False
                    End If
                    
                    If CMF.FindString(Inline, "PreviewPaneVisible", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "PreviewPaneVisible=", True)
                        If TempString = "TRUE" Then PreviewPaneVisible = True Else PreviewPaneVisible = False
                        PreviewSettingFound = True
                    End If
                    
                    If CMF.FindString(Inline, "DisplayCAOptions", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "DisplayCAOptions=", True)
                        If TempString = "TRUE" Then DisplayCAOptions = True Else DisplayCAOptions = False
                    End If
                    
                    If CMF.FindString(Inline, "DontEmail", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "DontEmail=", True)
                        If TempString = "TRUE" Then DontEmail = True Else DontEmail = False
                    End If
                    
                    If CMF.FindString(Inline, "UseAutoStatus", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "UseAutoStatus=", True)
                        If TempString = "TRUE" Then UseAutoStatus = True Else UseAutoStatus = False
                        UseAutoStatusFound = True
                    End If
                    
                    If CMF.FindString(Inline, "IdleMinutes", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "IdleMinutes=", True)
                        IdleMinutes = Val(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "UserOverride", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "UserOverride=", True)
                        UserOverride = Trim(TempString)
                    End If
                    
                    If CMF.FindString(Inline, "RQLISTBG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "RQLISTBG=", True)
                        AppColours.Set_FrmMain_FGridCalls_BackGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "RQLISTFG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "RQLISTFG=", True)
                        AppColours.Set_FrmMain_FGridCalls_ForeGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "PVPANEBG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "PVPANEBG=", True)
                        AppColours.Set_FrmMain_txtDetails_BackGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "PVPANEFG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "PVPANEFG=", True)
                        AppColours.Set_FrmMain_txtDetails_ForeGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "FDHEADBG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "FDHEADBG=", True)
                        AppColours.Set_FieldHeadings_BackGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "FDHEADFG", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "FDHEADFG=", True)
                        AppColours.Set_FieldHeadings_ForeGround (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "RQCHANGED", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "RQCHANGED=", True)
                        AppColours.SetChangedCalls (Val(TempString))
                    End If
                    
                    If CMF.FindString(Inline, "RQURGENT", True) <> 0 Then
                        TempString = CMF.FilterWord(Inline, "RQURGENT=", True)
                        AppColours.SetUrgentCalls (Val(TempString))
                    End If
                    
                    If CurrentSection = "[CHOSENCOLUMNS]" Then
                        If CMF.FindString(Inline, "[", True) = 0 Then
                            ChosenCols.AddItem (Trim(Inline))
                        End If
                    End If
        
                    If CurrentSection = "[CUSTOMCOLSIZES]" Then
                        If CMF.FindString(Inline, "[", True) = 0 Then
                            ColSizes.AddItem (Trim(Inline))
                        End If
                    End If
                     
                    If CurrentSection = "[ADDEDGROUPS]" Then
                        If CMF.FindString(Inline, "[", True) = 0 Then
                            CustomAddedGroups.AddItem (Trim(Inline))
                        End If
                    End If
                     
                    If CurrentSection = "[ADDEDSPECIALISTS]" Then
                        If CMF.FindString(Inline, "[", True) = 0 Then
                            CustomAddedSpecialists.AddItem (Trim(Inline))
                        End If
                    End If
                    
                    If CurrentSection = "[EXPANDEDNODES]" Then
                        If CMF.FindString(Inline, "[", True) = 0 Then
                            ExpandedItems.AddItem (Trim(Inline))
                        End If
                    End If
                    
                Wend
            Close #1
            
            '**** NOW, transfer the loaded lists into the appropriate dyn arrays.
            Call CustomisedColumns.SetSize(1, ChosenCols.GetSize)
            For Row = 1 To ChosenCols.GetSize
                Call CustomisedColumns.SetElement(1, Row, ChosenCols.GetItem((Row)))
            Next
            
            Call CurrentQueueColSizes.SetSize(1, ColSizes.GetSize)
            For Row = 1 To ColSizes.GetSize
                Call CurrentQueueColSizes.SetElement(1, Row, ColSizes.GetItem((Row)))
            Next
                 
            If PreviewSettingFound = False Then PreviewPaneVisible = True
            If UseAutoStatusFound = False Then UseAutoStatus = True
    End If
End Function

Public Function PadHexColourValue(InHexString As String) As String
    ' This function takes an unpadded hex value, and pads out the hex value with leading zeros
    Dim TempString As String
    
    If Len(InHexString) = 6 Then
        PadHexColourValue = InHexString
        Exit Function
    End If
    
    If Len(InHexString) = 5 Then
        TempString = "&H0" + CMF.FilterWord(InHexString, "&H", True)
        PadHexColourValue = TempString
        Exit Function
    End If
    
    If Len(InHexString) = 4 Then
        TempString = "&H00" + CMF.FilterWord(InHexString, "&H", True)
        PadHexColourValue = TempString
        Exit Function
    End If
    
    If Len(InHexString) = 3 Then
        TempString = "&H000" + CMF.FilterWord(InHexString, "&H", True)
        PadHexColourValue = TempString
        Exit Function
    End If
    
    If Len(InHexString) = 2 Then
        TempString = "&H0000" + CMF.FilterWord(InHexString, "&H", True)
        PadHexColourValue = TempString
        Exit Function
    End If
    
    If Len(InHexString) = 1 Then
        TempString = "&H00000" + CMF.FilterWord(InHexString, "&H", True)
        PadHexColourValue = TempString
        Exit Function
    End If
End Function

Public Function SaveINIFile()
    Dim Path As String
    Dim Row As Single
    Dim FieldOpt As Integer
    
    Path = App.Path + "\CedeSupportDesk.ini"

    Open Path For Output As #1
        
        Print #1, "SORTCOL=" + CurrentSortedColumn
        Print #1, "SORTMODE=" + CurrentSortMode
        
        Print #1, "DBConnectionString=" + DatabaseConnectionString
        
        Print #1, "SDSQLServer=" + SDSQLServer
        Print #1, "SDSQLPort=" + SDSQLPort
        Print #1, "SDSQLCatalog=" + SDSQLCatalog
        Print #1, "SDSQLUser=" + SDSQLUser
        Print #1, "SDSQLPassword=" + SDSQLPassword
        
        If SDSQLTrustCon = True Then
            Print #1, "SDSQLTrustCon=TRUE"
        Else
            Print #1, "SDSQLTrustCon=FALSE"
        End If
        
        Print #1, "IVSQLServer=" + IVSQLServer
        Print #1, "IVSQLPort=" + IVSQLPort
        Print #1, "IVSQLCatalog=" + IVSQLCatalog
        Print #1, "IVSQLUser=" + IVSQLUser
        Print #1, "IVSQLPassword=" + IVSQLPassword
        
        If IVSQLTrustCon = True Then
            Print #1, "IVSQLTrustCon=TRUE"
        Else
            Print #1, "IVSQLTrustCon=FALSE"
        End If
        
        Print #1, "UserOverride=" + UserOverride
        
        If SendReadReceipt = True Then
            Print #1, "SendReadReceipt=TRUE"
        Else
            Print #1, "SendReadReceipt=FALSE"
        End If
        
        If DisplayRequestPopup = True Then
            Print #1, "DisplayRequestPopup=TRUE"
        Else
            Print #1, "DisplayRequestPopup=FALSE"
        End If
        
        If DisplayEmailPrompt = True Then
            Print #1, "DisplayEmailPrompt=TRUE"
        Else
            Print #1, "DisplayEmailPrompt=FALSE"
        End If
        
        If UseAutoAbsence = True Then
            Print #1, "UseAutoAbsence=TRUE"
        Else
            Print #1, "UseAutoAbsence=FALSE"
        End If
        
        If PreviewPaneVisible = True Then
            Print #1, "PreviewPaneVisible=TRUE"
        Else
            Print #1, "PreviewPaneVisible=FALSE"
        End If
        
        If DisplayCAOptions = True Then
            Print #1, "DisplayCAOptions=TRUE"
        Else
            Print #1, "DisplayCAOptions=FALSE"
        End If
        
        If UseAutoStatus = True Then
            Print #1, "UseAutoStatus=TRUE"
        Else
            Print #1, "UseAutoStatus=FALSE"
        End If
        
        If DontEmail = True Then
            Print #1, "DontEmail=TRUE"
        Else
            Print #1, "DontEmail=FALSE"
        End If
        
        Print #1, "IdleMinutes=" + Trim(Str(IdleMinutes))
        Print #1, "RQLISTBG=" + Trim(Str((AppColours.Get_FrmMain_FGridCalls_BackGround)))
        Print #1, "RQLISTFG=" + Trim(Str((AppColours.Get_FrmMain_FGridCalls_ForeGround)))
        Print #1, "PVPANEBG=" + Trim(Str((AppColours.Get_FrmMain_txtDetails_BackGround)))
        Print #1, "PVPANEFG=" + Trim(Str((AppColours.Get_FrmMain_txtDetails_ForeGround)))
        Print #1, "FDHEADBG=" + Trim(Str((AppColours.Get_FieldHeadings_BackGround)))
        Print #1, "FDHEADFG=" + Trim(Str((AppColours.Get_FieldHeadings_ForeGround)))
        Print #1, "RQCHANGED=" + Trim(Str((AppColours.GetChangedCalls)))
        Print #1, "RQURGENT=" + Trim(Str((AppColours.GetUrgentCalls)))
        
        Print #1, "[CHOSENCOLUMNS]"
        For Row = 1 To CustomisedColumns.GetNumRows
            Print #1, CustomisedColumns.GetElement(1, Row)
        Next
                
        Print #1, "[CUSTOMCOLSIZES]"
        For Row = 1 To CurrentQueueColSizes.GetNumRows
            Print #1, CurrentQueueColSizes.GetElement(1, Row)
        Next
        
        Print #1, "[ADDEDGROUPS]"
        For Row = 1 To CustomAddedGroups.GetSize
            Print #1, CustomAddedGroups.GetItem((Row))
        Next
        
        Print #1, "[ADDEDSPECIALISTS]"
        For Row = 1 To CustomAddedSpecialists.GetSize
            Print #1, CustomAddedSpecialists.GetItem((Row))
        Next
        
        Print #1, "[EXPANDEDNODES]"
        For Row = 1 To ExpandedItems.GetSize
            Print #1, ExpandedItems.GetItem((Row))
        Next
        
    Close #1

End Function

Public Function ExportCurrentQueue()
    ' Function to export the current queue to excel
    Dim EXH As New ExcelHandler
    Call EXH.ExportToExcel(CurrentQueue, CurrentQueueTitles)
    
End Function

Public Function GetPCDetails(Username As String, DestDynDB As DynDatabaseEX)
    ' Inventory linking - Function to retrieve the details of a PC in the inventory given a username
    Call InvHandler.GetPCDetails(Username, DestDynDB)
End Function

Public Function BuildNewRequests()
    ' This function facilitates the reoccuring automatic checking of the current specialists queue.
    ' To facilitate the popup message "A New Request has been assigned to you, do you want to read it now?" message.
    ' In order to do this we need a list of all of the current specialists requests that are not closed, so when we run a check of all the specialists
    ' requests, we have a list to compare it with. This means we can determine which requests are new.
    
    Dim StatusDescriptions As New DynDatabaseEX
    Dim CurrentStatus As String
    Dim CurrentStatusID As String
    Dim CurrentReqID As String
    Dim CurrentUser As String
    Dim Row As Single
    Dim RequestRow As Single
    
    Dim TempRequests As New DynDatabaseEX
    Dim TempRequestTitles As New DynDatabaseEX
    
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    CurrentUser = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    
    'Now we build the list of the current specialists request id's, by working through all of the status descriptions apart from the
    ' ones matching "CLOSED"
    
    For Row = 1 To StatusDescriptions.GetNumRows
        CurrentStatus = StatusDescriptions.GetElement(1, Row)
        CurrentStatusID = RichHandler.GetStatusID(CurrentStatus)
        If CMF.FindString(CurrentStatus, "Closed", False) = 0 Then 'If the status is matching anything other than Closed
            ' Get the specialists requests for this status
            Call RichHandler.GetSpecialistRequests(CurrentUser, CurrentStatusID, TempRequests, TempRequestTitles)
            
            'Now work through the request id's and add them to the newrequests global list
            For RequestRow = 1 To TempRequests.GetNumRows
                CurrentReqID = TempRequests.GetElement(1, RequestRow)
                'Add this request to the global list
                NewRequests.AddItem (CurrentReqID)
            Next
            
        End If
    Next
End Function

Public Function CheckNewRequests()
    ' This function is executed on a regular interval to check the current specialists queue for any new requests.
    ' This function checks all status queues apart from any matching closed. If new calls are found, then a system modal
    ' Message Box is displayed asking the user if they would like to view the new request.
    ' If more than 1 request is found to be new, then the message box changes to, "Would you like to view the new requests?"
    
    Dim StatusDescriptions As New DynDatabaseEX
    Dim CurrentStatus As String
    Dim CurrentStatusID As String
    Dim CurrentReqID As String
    Dim CurrentUser As String
    Dim Row As Single
    Dim URow As Integer
    Dim RequestRow As Single
    Dim UnReadRequests As New DynList
    Dim CurrentUnReadRequest As String
    Dim N As Integer
    Dim CurrentSpecialistGroup As String
    
    Dim TempRequests As New DynDatabaseEX
    Dim TempRequestTitles As New DynDatabaseEX
    
    Call RichHandler.GetStatusDescriptions(StatusDescriptions)
    CurrentUser = Trim(StrConv(GetCurrentLoggedOnUser, vbUpperCase))
    
    'Now we go through and check all of the specialists request ids, with the request id's stored in the global list
    'apart from ones matching "CLOSED"
    
    For Row = 1 To StatusDescriptions.GetNumRows
        CurrentStatus = StatusDescriptions.GetElement(1, Row)
        CurrentStatusID = RichHandler.GetStatusID(CurrentStatus)
        If CMF.FindString(CurrentStatus, "Closed", False) = 0 Then 'If the status is matching anything other than Closed
            
            ' Get the specialists requests for this status
            Call RichHandler.GetSpecialistRequests(CurrentUser, CurrentStatusID, TempRequests, TempRequestTitles)
            
            'Now work through the request ID's, and any ones that we don't know of, add them to a local list
            For RequestRow = 1 To TempRequests.GetNumRows
                CurrentReqID = TempRequests.GetElement(1, RequestRow)
                ' Check if we know of this request
                If NewRequests.DoesItemExist(CurrentReqID, False) = 0 Then
                    ' This is a new unread request, add it to the new local list
                    UnReadRequests.AddItem (CurrentReqID)
                End If
            Next
            
        End If
    Next
    
    ' Now we should have a list of all unread new request ids. Decide how we should display the MessageBox dialog.
    
    If UnReadRequests.GetSize = 1 Then
        ' There is one new item, ask the user if they would like to read it now
        ' We must also add this request to the global list. This is so that subsequent checks, do not trigger the message again!
        
        CurrentUnReadRequest = UnReadRequests.GetItem(1)
        NewRequests.AddItem (CurrentUnReadRequest)
        
        
        If Main.DisplayRequestPopup = True Then 'Check if the option has been set before doing this
            If RequestPopupDisplayed = False Then
                RequestPopupDisplayed = True
                If MsgBox("You have a New Request. Would you like to read it now?", vbYesNo + vbQuestion + vbSystemModal, "Cede SupportDesk") = vbYes Then
                    RequestPopupDisplayed = False
                    frmSingleRequestDetails.ShowRequestDetails (CurrentUnReadRequest)
                    frmSingleRequestDetails.Show
                Else
                    RequestPopupDisplayed = False
                End If
            End If
        End If
        
    End If
    
    If UnReadRequests.GetSize > 1 Then
        ' There is more than 1 item, we must display a different message.
        ' We must also add these requests to the global list of new requests
        For URow = 1 To UnReadRequests.GetSize
            NewRequests.AddItem (UnReadRequests.GetItem(URow))
        Next
        
        If Main.DisplayRequestPopup = True Then 'Check if the option has been set before doing this
            If RequestPopupDisplayed = False Then
                 RequestPopupDisplayed = True
                 If MsgBox("You have " + Trim(Str(UnReadRequests.GetSize)) + " New Requests. Would you like to view them now?", vbYesNo + vbQuestion + vbSystemModal, "Cede SupportDesk") = vbYes Then
                         RequestPopupDisplayed = False
                         
                         ' Simulate the user clicking their open queue.
                         CurrentSpecialistGroup = RichHandler.GetSpecialistGroupName(CurrentUser)
            
                         For N = 1 To frmMain.treQueues.Nodes.Count
                             If frmMain.treQueues.Nodes(N).Key = CurrentUser Then
                                 ' Expand the queue
                                 frmMain.treQueues.Nodes(N).Expanded = True
                             End If
                             
                             If frmMain.treQueues.Nodes(N).Key = CurrentSpecialistGroup + "$" + CurrentUser + "$" + "Open" Then
                                 ' Make sure the open queue is highlighted
                                 frmMain.treQueues.Nodes(N).Selected = True
                                 
                                 ' Retrieve the specialists current open queue - this populates the current queue and current queue titles
                                 Call GetSpecialistQueue(CurrentUser, RichHandler.GetStatusID("Open"))
                                                        
                                 ' Now we make sure the open queue is displayed, so that we populate the current queue dyn db
                                 Call FolderListClicked(CurrentUser + "$" + "Open", "Open")
                                 
                             End If
                         Next
                 Else
                    RequestPopupDisplayed = False
                 End If
                 
            End If
        End If
        
    End If
End Function

Public Function AddNewRequestToNew(RequestID As String)
    ' This function adds the specified request id, to the newrequests table
    ' This is called from UI forms so that when a request is added or modified by the user, a request popup is not
    ' displayed notifying the user of the new request
    
    NewRequests.AddItem (RequestID)
End Function

Public Function GenerateEmail(Recipient As String, Subject As String, Body As String)
    ' Interface function to handle the constructing of new mail using Microsoft Outlook
    Call OlkHandler.NewMail(Recipient, Subject, Body)
End Function

Public Function GenerateHTMLEmail(Recipient As String, Subject As String, Body As String, SendNow As Boolean)
    ' Interface function to handle the constructing of new html mail using microsoft outlook
    Call OlkHandler.NewHTMLMail(Recipient, Subject, Body, SendNow)
End Function

Public Function GenerateSendMail(Recipient As String, Subject As String, Body As String)
    ' INterface function to handle the sending of email
    Call OlkHandler.NewSendMail(Recipient, Subject, Body)
End Function

Public Function GenerateOpenCallEmail(RequestID As String, Summary As String, AssocUsers As DynDatabaseEX, SendNow As Boolean)
    ' This function will generate an email to the associated users informing them of the current open call status.
    
    Dim RecipientString As String
    Dim CurrentRecipient As String
    Dim EmailSubject As String
    Dim EmailBody As String
    Dim Row As Single
    Dim NameString As String
    Dim ContactDetails As New DynDatabaseEX
    Dim SenderString As String
    
    ' This function generates the recipient string so that the associated users are in the TO field separated by a semicolon.
    For Row = 1 To AssocUsers.GetNumRows
        CurrentRecipient = AssocUsers.GetElement(1, Row)
        Call RichHandler.GetContactDetails(CurrentRecipient, ContactDetails)
        If Trim(RecipientString) = "" Then
            RecipientString = CurrentRecipient
        Else
            RecipientString = RecipientString + "; " + CurrentRecipient
        End If
        
        If Trim(NameString) = "" Then
            If ContactDetails.GetNumRows > 0 Then
                NameString = ContactDetails.GetElement(1, 1)
            End If
        Else
            If ContactDetails.GetNumRows > 0 Then
                NameString = NameString + ", " + ContactDetails.GetElement(1, 1)
            End If
        End If
    Next
    
    Call RichHandler.GetContactDetails(GetCurrentLoggedOnUser, ContactDetails)
    If ContactDetails.GetNumRows > 0 Then
        SenderString = ContactDetails.GetElement(1, 1) + " " + ContactDetails.GetElement(2, 1)
    End If
       
    EmailSubject = "SupportDesk Request number " + RequestID
    
    EmailBody = EmailTemplates.OpenTemplateEX
    
    EmailBody = CMF.ReplaceString(EmailBody, "[NAME]", NameString)
    EmailBody = CMF.ReplaceString(EmailBody, "[SENDER]", SenderString)
    EmailBody = CMF.ReplaceString(EmailBody, "######", RequestID)
    EmailBody = CMF.ReplaceString(EmailBody, "[SUMMARY]", Summary)
    
    Call GenerateHTMLEmail(RecipientString, EmailSubject, EmailBody, SendNow)
    
End Function

Public Function GenerateOpenClosedCallEmail(RequestID As String, Summary As String, LoggedDateTime As String, AssocUsers As DynDatabaseEX, SendNow As Boolean)
    ' This function will generate an email to the associated users informing them of the current open call status.
    
    Dim RecipientString As String
    Dim CurrentRecipient As String
    Dim EmailSubject As String
    Dim EmailBody As String
    Dim Row As Single
    Dim NameString As String
    Dim ContactDetails As New DynDatabaseEX
    Dim SenderString As String
    Dim strTime As String
    Dim strDate As String
    
    ' This function generates the recipient string so that the associated users are in the TO field separated by a semicolon.
    For Row = 1 To AssocUsers.GetNumRows
        CurrentRecipient = AssocUsers.GetElement(1, Row)
        Call RichHandler.GetContactDetails(CurrentRecipient, ContactDetails)
        If Trim(RecipientString) = "" Then
            RecipientString = CurrentRecipient
        Else
            RecipientString = RecipientString + "; " + CurrentRecipient
        End If
        
        If Trim(NameString) = "" Then
            If ContactDetails.GetNumRows > 0 Then
                NameString = ContactDetails.GetElement(1, 1)
            End If
        Else
            If ContactDetails.GetNumRows > 0 Then
                NameString = NameString + ", " + ContactDetails.GetElement(1, 1)
            End If
        End If
    Next
    
    Call RichHandler.GetContactDetails(GetCurrentLoggedOnUser, ContactDetails)
    If ContactDetails.GetNumRows > 0 Then
        SenderString = ContactDetails.GetElement(1, 1) + " " + ContactDetails.GetElement(2, 1)
    End If
    
    strDate = CMF.GetDataPart(LoggedDateTime, 1, " ")
    strTime = CMF.GetDataPart(LoggedDateTime, 2, " ")
    
    EmailSubject = "SupportDesk Request number " + RequestID
    
    EmailBody = EmailTemplates.OpenCloseTemplateEX
    
    EmailBody = CMF.ReplaceString(EmailBody, "[NAME]", NameString)
    EmailBody = CMF.ReplaceString(EmailBody, "[SENDER]", SenderString)
    EmailBody = CMF.ReplaceString(EmailBody, "[TIME]", strTime)
    EmailBody = CMF.ReplaceString(EmailBody, "[DATE]", strDate)
    EmailBody = CMF.ReplaceString(EmailBody, "#####", RequestID)
    EmailBody = CMF.ReplaceString(EmailBody, "[SUMMARY]", Summary)
    
    Call GenerateHTMLEmail(RecipientString, EmailSubject, EmailBody, SendNow)
    
End Function

Public Function GenerateClosedCallEmail(RequestID As String, AssocUsers As DynDatabaseEX, SendNow As Boolean)
    ' This function will generate an email to the associated users informing them of the current open call status.
    
    Dim RecipientString As String
    Dim CurrentRecipient As String
    Dim EmailSubject As String
    Dim EmailBody As String
    Dim Row As Single
    Dim NameString As String
    Dim ContactDetails As New DynDatabaseEX
    Dim SenderString As String
    
    Dim ResultDB As New DynDatabaseEX
    Dim ReqAssocUsers As New DynDatabaseEX
    Dim SummaryString As String
    
    ' This function generates the recipient string so that the associated users are in the TO field separated by a semicolon.
    For Row = 1 To AssocUsers.GetNumRows
        CurrentRecipient = AssocUsers.GetElement(1, Row)
        Call RichHandler.GetContactDetails(CurrentRecipient, ContactDetails)
        If Trim(RecipientString) = "" Then
            RecipientString = CurrentRecipient
        Else
            RecipientString = RecipientString + "; " + CurrentRecipient
        End If
        
        If Trim(NameString) = "" Then
            If ContactDetails.GetNumRows > 0 Then
                NameString = ContactDetails.GetElement(1, 1)
            End If
        Else
            If ContactDetails.GetNumRows > 0 Then
                NameString = NameString + ", " + ContactDetails.GetElement(1, 1)
            End If
        End If
    Next
    
    Call RichHandler.GetContactDetails(GetCurrentLoggedOnUser, ContactDetails)
    If ContactDetails.GetNumRows > 0 Then
        SenderString = ContactDetails.GetElement(1, 1) + " " + ContactDetails.GetElement(2, 1)
    End If
    
    Call RichHandler.GetSingleRequestDetails(RequestID, ResultDB, ReqAssocUsers)
    
    If ResultDB.GetNumRows > 0 Then
        SummaryString = ResultDB.GetElement(9, 1)
    End If
    
    EmailSubject = "SupportDesk Request number " + RequestID + " - Closed"
    
    EmailBody = EmailTemplates.ClosedTemplateEX
    
    EmailBody = CMF.ReplaceString(EmailBody, "[NAME]", NameString)
    EmailBody = CMF.ReplaceString(EmailBody, "[SENDER]", SenderString)
    EmailBody = CMF.ReplaceString(EmailBody, "#####", RequestID)
    EmailBody = CMF.ReplaceString(EmailBody, "[SUMMARY]", SummaryString)
    
    Call GenerateHTMLEmail(RecipientString, EmailSubject, EmailBody, SendNow)
    
End Function

Public Function GeneratedSpecialistReadEmail(RequestID As String, Summary As String, ReadSpecialist As String, LoggedBy As String)
    ' This function will generate an email to the associated users informing them of the current open call status.
     
    Dim RecipientString As String
    Dim EmailSubject As String
    Dim EmailBody As String
    Dim Row As Single
    
    RecipientString = LoggedBy
    
    EmailSubject = "Request ID " + RequestID + " read by " + ReadSpecialist
    
        
    EmailBody = EmailBody + "Request ID " + RequestID + " with Summary '" + Summary + "' has been read by " + ReadSpecialist + "." + vbCrLf + vbCrLf
    EmailBody = EmailBody + "Cede SupportDesk." + vbCrLf + vbCrLf
    
    'Call GenerateEmail(RecipientString, EmailSubject, EmailBody)
    Call GenerateSendMail(RecipientString, EmailSubject, EmailBody)
End Function

Public Function SearchRequests(QueryFields As DynList, QueryValues As DynList, DestDynDB As DynDatabaseEX, DestDBTitles As DynDatabaseEX)
    ' Interface function to search the database
    Call RichHandler.SearchRequests(QueryFields, QueryValues, DestDynDB, DestDBTitles)
End Function

Public Function GetWindowsUsername() As String
    Dim Username As String  ' receives name of the user
    Dim slength As Long  ' length of the string
    Dim retval As Long  ' return value
    
    Username = Space(255)
    slength = 255
    retval = GetUserName(Username, slength)
    
    ' Return the windows username
    GetWindowsUsername = Trim(StrConv(Username, vbUpperCase))
End Function

Public Function AddRequestWatch(RequestID As String)
    ' Interface function to keep a watch on a request
    Call RichHandler.AddWatchedRequest(RequestID)
End Function

Public Function IsRequestWatched(RequestID As String) As Boolean
    ' Interface function for checking if a request has a watch
    IsRequestWatched = RichHandler.DoesRequestHaveWatch(RequestID)
End Function

Public Function RemoveRequestWatch(RequestID As String) As String
    ' Interface function for removing a watch from a request
    Call RichHandler.RemoveWatchedRequest(RequestID)
End Function

Public Function CheckWatchedRequests()
    ' Function to check if any of the users watched requests have been changed
    ' This is actually done by the richhandler class, and this is a high level implementation of it
    Dim UpdatedRequests As New DynList
    Dim ReqID As String
    Dim AssocUser As String
    Dim Summary As String
    
    Call RichHandler.CheckWatchedRequests(UpdatedRequests)

    If UpdatedRequests.GetSize = 1 Then
        ReqID = CMF.GetDataPart(UpdatedRequests.GetItem(1), 1, "�")
        AssocUser = CMF.GetDataPart(UpdatedRequests.GetItem(1), 2, "�")
        Summary = CMF.GetDataPart(UpdatedRequests.GetItem(1), 3, "�")
        
        If MsgBox("Request ID " + ReqID + " (" + Summary + ") " + "for " + AssocUser + " has been updated. Would you like to read it now?", vbYesNo + vbQuestion + vbSystemModal, "Cede SupportDesk") = vbYes Then
            frmSingleRequestDetails.ShowRequestDetails (ReqID)
            frmSingleRequestDetails.Show
        End If
    End If
    
    If UpdatedRequests.GetSize > 1 Then
        
        If MsgBox(Trim(Str(UpdatedRequests.GetSize)) + " of your watched Requests have been updated. Do you want to view them now?", vbYesNo + vbQuestion + vbSystemModal, "Cede SupportDesk") = vbYes Then
            Call frmWatchedRequests.PopulateWatchedRequests(UpdatedRequests)
            frmWatchedRequests.Show
        End If
        
    End If
    
End Function

Public Function SystemIdle()
    frmMain.imgAway.Visible = True
    frmMain.lblAway.Visible = True
    Call InvHandler.SetSpecialistStatus(GetCurrentLoggedOnUser, False)
    DoEvents
End Function

Public Function SystemNotIdle()
    frmMain.imgAway.Visible = False
    frmMain.lblAway.Visible = False
    Call InvHandler.SetSpecialistStatus(GetCurrentLoggedOnUser, True)
    DoEvents
End Function

Public Function ShowStats()
    frmStats.Show
    DoEvents
    
    frmStats.lblNum1.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum1.Caption = Trim(Str(RichHandler.GetNumRequestsLoggedToday))
    DoEvents
    
    frmStats.lblNum2.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum2.Caption = Trim(Str(RichHandler.GetNumRequestsClosedToday))
    DoEvents
    
    frmStats.lblNum3.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum3.Caption = Trim(Str(RichHandler.GetNumRequestsLoggedWeek))
    DoEvents
    
    frmStats.lblNum4.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum4.Caption = Trim(Str(RichHandler.GetNumRequestsClosedWeek))
    DoEvents
    
    frmStats.lblNum5.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum5.Caption = Trim(Str(RichHandler.GetNumRequestsLoggedMonth))
    DoEvents
    
    frmStats.lblNum6.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum6.Caption = Trim(Str(RichHandler.GetNumRequestsClosedMonth))
    DoEvents
    
    frmStats.lblNum7.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum7.Caption = Trim(Str(RichHandler.GetAvgNumLoggedPerDay))
    DoEvents
    
    frmStats.lblNum8.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum8.Caption = Trim(Str(RichHandler.GetAvgNumClosedPerDay))
    DoEvents
    
    frmStats.lblNum9.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum9.Caption = Trim(Str(RichHandler.GetAvgNumLoggedPerWeek))
    DoEvents
    
    frmStats.lblNum10.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum10.Caption = Trim(Str(RichHandler.GetAvgNumClosedPerWeek))
    DoEvents
    
    frmStats.lblNum11.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum11.Caption = Trim(Str(RichHandler.GetAvgNumLoggedPerMonth))
    DoEvents
    
    frmStats.lblNum12.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum12.Caption = Trim(Str(RichHandler.GetAvgNumClosedPerMonth))
    DoEvents
    
    frmStats.lblNum13.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum13.Caption = Trim(RichHandler.GetTodayPopularCategory)
    DoEvents
    
    frmStats.lblNum14.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum14.Caption = Trim(RichHandler.GetWeekPopularCategory)
    DoEvents
    
    frmStats.lblNum15.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum15.Caption = Trim(RichHandler.GetMonthPopularCategory)
    DoEvents
    
    frmStats.lblNum16.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum16.Caption = Trim(Str(RichHandler.GetNumUserRequestsLoggedToday(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum17.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum17.Caption = Trim(Str(RichHandler.GetNumUserRequestsLoggedWeek(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum18.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum18.Caption = Trim(Str(RichHandler.GetNumUserRequestsLoggedMonth(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum19.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum19.Caption = Trim(Str(RichHandler.GetNumUserRequestsLoggedTotal(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum20.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum20.Caption = Trim(Str(RichHandler.GetNumUserRequestsClosedToday(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum21.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum21.Caption = Trim(Str(RichHandler.GetNumUserRequestsClosedWeek(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum22.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum22.Caption = Trim(Str(RichHandler.GetNumUserRequestsClosedMonth(GetCurrentLoggedOnUser)))
    DoEvents
    
    frmStats.lblNum23.Caption = "Calculating..."
    DoEvents
    frmStats.lblNum23.Caption = Trim(Str(RichHandler.GetNumUserRequestsClosedTotal(GetCurrentLoggedOnUser)))
    DoEvents
    
End Function

Public Function ReminderEnabled(RequestID, strDate As String, strTime As String)
    ' This function is called when a user chooses the request reminder option
    ' This function will check if it already exists in the reminders list, and if it does update it, if not then add it
    
    'MsgBox RequestID
    'MsgBox strDate
    'MsgBox strTime
    
    Dim ReminderLoc As Integer
    
    ReminderLoc = ReminderReqs.DoesItemExist(Trim(RequestID), False)
    
    If ReminderLoc <> 0 Then
        ' We found the reminder, so update the date and time that it is set for with the new paramters
        Call ReminderDates.SetItem(ReminderLoc, Trim(strDate))
        Call ReminderTimes.SetItem(ReminderLoc, Trim(strTime))
    Else
        ReminderReqs.AddItem (Trim(RequestID))
        ReminderDates.AddItem (Trim(strDate))
        ReminderTimes.AddItem (Trim(strTime))
    End If

    ' Now we need to save the list back to disk
    SaveReminders

End Function

Public Function SaveReminders()
On Error GoTo err

    Dim R As Integer
    Dim strLine As String
    Dim Path As String
    
    Path = App.Path + "\" + Trim(GetCurrentLoggedOnUser) + "-Reminders.dat"
    
    ' Before saving the reminders, cleanup the list - remove any old reminders
    CleanupReminders
    
    If ReminderReqs.GetSize > 0 Then
        Open Path For Output As #1
            For R = 1 To ReminderReqs.GetSize
                strLine = ReminderReqs.GetItem(R) + "," + ReminderDates.GetItem(R) + "," + ReminderTimes.GetItem(R)
                Print #1, strLine
            Next
        Close #1
    Else
        ' The reminder list if empty, so check if a file exists on the hard disk
        If Dir(Path) <> "" Then
            ' There is a reminder file, so delete it
            Call CMF.SimpleDelete(Path)
        End If
    End If
    Exit Function
err:
    MsgBox "Unable to save reminders. Check that you have write permission to " + App.Path + ".", vbExclamation, "Cede SupportDesk"
End Function

Public Function LoadReminders()
    Dim strReq As String
    Dim strDate As String
    Dim strTime As String
    Dim Path As String
    Dim Inline As String
    
    Call ReminderReqs.ClearList
    Call ReminderDates.ClearList
    Call ReminderTimes.ClearList
    
    Path = App.Path + "\" + Trim(GetCurrentLoggedOnUser) + "-Reminders.dat"
    
    If Dir(Path) <> "" Then
        Open Path For Input As #1
            While Not EOF(1)
                Line Input #1, Inline
                strReq = CMF.GetDataPart(Inline, 1, ",")
                strDate = CMF.GetDataPart(Inline, 2, ",")
                strTime = CMF.GetDataPart(Inline, 3, ",")
                ReminderReqs.AddItem (Trim(strReq))
                ReminderDates.AddItem (Trim(strDate))
                ReminderTimes.AddItem (Trim(strTime))
            Wend
        Close #1
    End If
End Function

Public Function DeleteReminder(ReminderIndex As Integer)
    Dim TempReminderReqs As New DynList
    Dim TempReminderDates As New DynList
    Dim TempReminderTimes As New DynList
    Dim R As Integer
    
    If ReminderIndex <= ReminderReqs.GetSize Then
        
        For R = 1 To ReminderReqs.GetSize
            If R <> ReminderIndex Then
                TempReminderReqs.AddItem (ReminderReqs.GetItem(R))
                TempReminderDates.AddItem (ReminderDates.GetItem(R))
                TempReminderTimes.AddItem (ReminderTimes.GetItem(R))
            End If
        Next
        
        ' Now transfer the temp reminder data into the main reminder lists
        
        ReminderReqs.ClearList
        ReminderDates.ClearList
        ReminderTimes.ClearList
        
        For R = 1 To TempReminderReqs.GetSize
            ReminderReqs.AddItem (TempReminderReqs.GetItem(R))
            ReminderDates.AddItem (TempReminderDates.GetItem(R))
            ReminderTimes.AddItem (TempReminderTimes.GetItem(R))
        Next
    End If
End Function

Public Function CheckToDeleteReminder(ReqID As String)
    ' This function will remove a reminder given by the request id
    ' Only used if an item has been unticked or is not ticked in the single request details
    
    Dim ReminderLoc As Integer
    
    ReminderLoc = ReminderReqs.DoesItemExist(ReqID, False)
    
    If ReminderLoc <> 0 Then
        DeleteReminder (ReminderLoc)
    End If
End Function

Public Function PollReminders()
    ' This function must be executed once every 30 seconds at least to check if the user has
    ' any reminders set for the time
    
    Dim strNowDate As String
    Dim strNowTime As String
    Dim R As Integer
    Dim remDate As String
    Dim remTime As String
    Dim remReq As String
    
    
    strNowDate = Format(Date, "DD/MM/YYYY")
    strNowTime = Format(Time, "HH:MM")
    
    For R = 1 To ReminderReqs.GetSize
        remReq = ReminderReqs.GetItem(R)
        remDate = ReminderDates.GetItem(R)
        remTime = ReminderTimes.GetItem(R)
        
        If strNowDate = remDate Then
            If strNowTime = remTime Then
                DeleteReminder (R)
                frmMain.tmrReminders.Enabled = False
                Call SMSRemindSpecialist(remReq, Trim(GetCurrentLoggedOnUser))
                If MsgBox("This is a reminder for Request ID " + Trim(remReq) + ". Do you want to read this Request?", vbInformation + vbYesNo + vbSystemModal, "Cede SupportDesk Reminder") = vbYes Then
                    ' Open the request, then exit the function
                    frmMain.tmrReminders.Enabled = True
                    frmSingleRequestDetails.ShowRequestDetails (remReq)
                    frmSingleRequestDetails.Show
                    Exit Function
                Else
                    frmMain.tmrReminders.Enabled = True
                    Exit Function
                End If
            End If
        End If
    Next
End Function

Public Function GetReminder(strReqID As String, strDestDate As String, strDestTime As String) As Boolean
    Dim ReminderLoc As Integer
    
    ReminderLoc = ReminderReqs.DoesItemExist(strReqID, False)
    If ReminderLoc <> 0 Then
        GetReminder = True
        strDestDate = ReminderDates.GetItem(ReminderLoc)
        strDestTime = ReminderTimes.GetItem(ReminderLoc)
    Else
        GetReminder = False
    End If
End Function

Public Function CleanupReminders()
    ' This function looks at the reminders list and looks for any reminders that have expired
    ' This would occur if Live Support Desk has not been run during the time a reminder was due to occur
    
    Dim R As Integer
    Dim strDate As String
    Dim dteDate As Date
    
    For R = 1 To ReminderReqs.GetSize
        strDate = ReminderDates.GetItem(R)
        dteDate = strDate
        
        If dteDate < Date Then
            'This reminder is old, so delete it
            DeleteReminder (R)
        End If
    Next
    
End Function

Public Function SMSRemindSpecialist(RequestID As String, Specialist As String)

    ' Function to notify the specialist by SMS of a newly assigned call.
    Dim ResultDB As New DynDatabaseEX
    Dim AssocUsers As New DynDatabaseEX
    Dim ContactDetails As New DynDatabaseEX
    Dim Summary As String
    Dim UserDetails As String
    Dim UserTel As String
    Dim UserID As String
    Dim SpecMobile As String
        
    Dim RecipientString As String
    Dim EmailBody As String
    
    SpecMobile = InvHandler.GetSpecialistMobile(Specialist)
    
    If Trim(SpecMobile) <> "" Then
    
        Call RichHandler.GetSingleRequestDetails(RequestID, ResultDB, AssocUsers)
        
        Summary = ResultDB.GetElement(9, 1)
        UserDetails = AssocUsers.GetElement(1, 1)
        UserID = AssocUsers.GetElement(2, 1)
        Call RichHandler.GetContactDetails(UserID, ContactDetails)
        
        UserTel = ContactDetails.GetElement(5, 1)
        
        RecipientString = SpecMobile + "@sms.roche.com"
        
        EmailBody = "*REQUEST REMINDER* -: " + UserDetails + "," + UserTel + "," + Summary
        
        Call OlkHandler.NewSendMail(RecipientString, "", EmailBody)
        
    End If
End Function
