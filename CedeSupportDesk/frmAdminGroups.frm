VERSION 5.00
Begin VB.Form frmAdminGroups 
   Caption         =   "Manage Groups"
   ClientHeight    =   7170
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   3675
   Icon            =   "frmAdminGroups.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7170
   ScaleWidth      =   3675
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstGroups 
      Height          =   5325
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   3435
   End
   Begin VB.TextBox txtGroup 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   6300
      Width           =   3435
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add Group"
      Height          =   315
      Left            =   1980
      TabIndex        =   1
      Top             =   6720
      Width           =   1575
   End
   Begin VB.CommandButton cmdNewGroup 
      Caption         =   "New Group"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   6720
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H8000000C&
      Caption         =   "Manage Groups"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   14115
   End
   Begin VB.Label lblGroups 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Existing Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   420
      Width           =   3435
   End
   Begin VB.Label lblGroup 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "New Group"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   6060
      Width           =   3435
   End
End
Attribute VB_Name = "frmAdminGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CMF As New CommonFunctions
Dim EditMode As Boolean
Dim LastGroup As String
Dim FormCaller As String

Private Sub cmdAdd_Click()
    If EditMode = True Then
        If Trim(txtGroup.Text) <> "" Then
            If Main.RichHandler.DoesSupportGroupExist(txtGroup.Text) = True Then
                MsgBox "This group already exists.", vbExclamation
            Else
                If Main.RichHandler.UpdateSingleSupportGroup(LastGroup, txtGroup.Text) = False Then
                    MsgBox "There was a problem updating this group.", vbExclamation
                Else
                    Main.RichHandler.BuildIndexedGroupTable
                    PopulateExistingGroups
                    EditMode = False
                    ToggleEditMode (False)
                End If
            End If
        End If
    Else
        If Trim(txtGroup.Text) <> "" Then
            If Main.RichHandler.DoesSupportGroupExist(txtGroup.Text) = True Then
                MsgBox "This group already exists.", vbExclamation
            Else
                If Main.RichHandler.AddSingleSupportGroup(txtGroup.Text) = False Then
                    MsgBox "There was a problem adding this group.", vbExclamation
                Else
                    Main.RichHandler.BuildIndexedGroupTable
                    PopulateExistingGroups
                    EditMode = False
                    ToggleEditMode (False)
                End If
            End If
        End If
    End If
End Sub

Private Sub cmdNewGroup_Click()
    ToggleEditMode (False)
End Sub

Private Sub Form_Activate()
    PopulateExistingGroups
    EditMode = False
End Sub

Public Function PopulateExistingGroups()
    ' Clear and populate the category combo
    Dim GroupDescriptions As New DynDatabaseEX
    
    lstGroups.Clear
    
    Call Main.GetGroupDescriptions(GroupDescriptions)
    Call CMF.PopulateListWithDynArray(GroupDescriptions, lstGroups)
End Function

Private Function ToggleEditMode(bEditMode As Boolean)
    If bEditMode = True Then
        cmdAdd.Caption = "Update Group"
        lblGroup.Caption = "Update Group"
        txtGroup.SetFocus
        EditMode = True
        cmdNewGroup.Visible = True
    Else
        cmdAdd.Caption = "Add Group"
        lblGroup.Caption = "New Group"
        txtGroup.Text = ""
        txtGroup.SetFocus
        EditMode = False
        cmdNewGroup.Visible = False
    End If
End Function

Private Sub lstGroups_Click()
    ToggleEditMode (True)
    txtGroup.Text = lstGroups.List(lstGroups.ListIndex)
    LastGroup = lstGroups.List(lstGroups.ListIndex)
End Sub

