VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmMain 
   Caption         =   "CedeSupportDesk v2.18 (Beta) CedeSoft Ltd"
   ClientHeight    =   9855
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   14190
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9855
   ScaleWidth      =   14190
   StartUpPosition =   2  'CenterScreen
   Begin CedeSupportDesk.XPStyle XPStyle1 
      Left            =   6840
      Top             =   5040
      _ExtentX        =   1429
      _ExtentY        =   1429
   End
   Begin VB.ComboBox cmbSAStatus 
      Height          =   315
      Left            =   9660
      TabIndex        =   32
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.ComboBox cmbSASpecialist 
      Height          =   315
      Left            =   11340
      TabIndex        =   31
      Top             =   60
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Timer tmrReminders 
      Interval        =   25000
      Left            =   8760
      Top             =   5220
   End
   Begin VB.CommandButton cmdStats 
      Caption         =   "Statistics"
      Height          =   375
      Left            =   12720
      TabIndex        =   30
      Top             =   60
      Width           =   915
   End
   Begin VB.Timer tmrIdle 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   10080
      Top             =   5220
   End
   Begin VB.TextBox txtDeletedSearch 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   0
      TabIndex        =   28
      Top             =   8820
      Width           =   3435
   End
   Begin MSComctlLib.TreeView treDQueues 
      Height          =   7455
      Left            =   0
      TabIndex        =   27
      Top             =   1380
      Width           =   3435
      _ExtentX        =   6059
      _ExtentY        =   13150
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   413
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.OptionButton optDQueue 
      Caption         =   "Deleted Users"
      Height          =   195
      Left            =   2040
      TabIndex        =   26
      Top             =   1140
      Value           =   -1  'True
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   375
      Left            =   6000
      TabIndex        =   25
      Top             =   60
      Width           =   855
   End
   Begin VB.CommandButton cmdSearchRequests 
      Caption         =   "Search Requests"
      Height          =   375
      Left            =   4440
      TabIndex        =   24
      Top             =   60
      Width           =   1515
   End
   Begin VB.Timer tmrNewRequests 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   6780
      Top             =   9120
   End
   Begin VB.CommandButton cmdReqTemplates 
      Caption         =   "Request Templates"
      Height          =   375
      Left            =   1260
      TabIndex        =   23
      Top             =   60
      Width           =   1575
   End
   Begin VB.CommandButton cmdReAssign 
      Caption         =   "Reassign Request"
      Height          =   375
      Left            =   2880
      TabIndex        =   22
      Top             =   60
      Width           =   1515
   End
   Begin VB.ComboBox cmbSortMode 
      Height          =   315
      Left            =   10740
      TabIndex        =   19
      Top             =   480
      Width           =   1635
   End
   Begin VB.ComboBox cmbSort 
      Height          =   315
      Left            =   7200
      TabIndex        =   18
      Top             =   480
      Width           =   2235
   End
   Begin VB.CommandButton cmdChooseCols 
      Caption         =   "Customise Columns"
      Height          =   255
      Left            =   12480
      TabIndex        =   17
      Top             =   540
      Width           =   1635
   End
   Begin VB.TextBox txtUserSearch 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   0
      TabIndex        =   16
      Top             =   8820
      Width           =   3435
   End
   Begin VB.CommandButton cmdTest2 
      Caption         =   "Test 2"
      Height          =   375
      Left            =   12780
      TabIndex        =   13
      Top             =   5400
      Visible         =   0   'False
      Width           =   1335
   End
   Begin MSComctlLib.ProgressBar progressMain 
      Height          =   195
      Left            =   10500
      TabIndex        =   10
      Top             =   9240
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   344
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "Test"
      Height          =   375
      Left            =   11400
      TabIndex        =   8
      Top             =   5400
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.OptionButton optSQueue 
      Caption         =   "Show Specialist Queues"
      Height          =   195
      Left            =   0
      TabIndex        =   6
      Top             =   840
      Width           =   2055
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "New Request"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   60
      Width           =   1215
   End
   Begin VB.TextBox txtDetails 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   3435
      Left            =   3540
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   5700
      Width           =   10575
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   9540
      Width           =   14190
      _ExtentX        =   25030
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FGridCalls 
      Height          =   4335
      Left            =   3540
      TabIndex        =   1
      Top             =   840
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   7646
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   12640511
      ForeColor       =   14737632
      BackColorFixed  =   16777215
      ForeColorFixed  =   14737632
      GridColor       =   16761024
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin MSComctlLib.TreeView treQueues 
      Height          =   7755
      Left            =   0
      TabIndex        =   0
      Top             =   1380
      Width           =   3435
      _ExtentX        =   6059
      _ExtentY        =   13679
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   413
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.TreeView treUserQueues 
      Height          =   7455
      Left            =   0
      TabIndex        =   14
      Top             =   1380
      Width           =   3435
      _ExtentX        =   6059
      _ExtentY        =   13150
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   413
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.OptionButton optUQueue 
      Caption         =   "Show Caller Queues"
      Height          =   195
      Left            =   0
      TabIndex        =   7
      Top             =   1140
      Width           =   1755
   End
   Begin VB.Label lblStatusLabel 
      BackColor       =   &H00808080&
      Caption         =   "Specialist Status:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   8400
      TabIndex        =   33
      Top             =   120
      Visible         =   0   'False
      Width           =   4275
   End
   Begin VB.Image imgAway 
      Height          =   345
      Left            =   7800
      Picture         =   "Form1.frx":0E42
      Stretch         =   -1  'True
      Top             =   60
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label lblAway 
      BackColor       =   &H8000000C&
      Caption         =   "...Away..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6900
      TabIndex        =   29
      Top             =   120
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblSortMode 
      BackColor       =   &H00808080&
      Caption         =   "Sort Mode:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   9900
      TabIndex        =   21
      Top             =   540
      Width           =   795
   End
   Begin VB.Label lblSort 
      BackColor       =   &H00808080&
      Caption         =   "Sort by:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   6540
      TabIndex        =   20
      Top             =   540
      Width           =   615
   End
   Begin VB.Label lblRequestInfo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Requests Displayed: 0"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   10800
      TabIndex        =   15
      Top             =   5160
      Width           =   3315
   End
   Begin VB.Label lblStaticStatus 
      Caption         =   "<Static Status>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   60
      TabIndex        =   12
      Top             =   9240
      Width           =   6255
   End
   Begin VB.Label lblProgressStatus 
      Alignment       =   1  'Right Justify
      Caption         =   "<Progress Status>"
      Height          =   195
      Left            =   7980
      TabIndex        =   11
      Top             =   9240
      Width           =   2415
   End
   Begin VB.Label lbldetails 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3540
      TabIndex        =   9
      Top             =   5460
      Width           =   5355
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H8000000C&
      Caption         =   "Welcome to CedeSupportDesk"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   0
      TabIndex        =   4
      Top             =   480
      Width           =   14115
   End
   Begin VB.Image imgExcelExport 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   13740
      MouseIcon       =   "Form1.frx":114C
      Picture         =   "Form1.frx":1456
      ToolTipText     =   "Click to Export the Current Queue to Microsoft Excel"
      Top             =   120
      Width           =   285
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "View"
      Begin VB.Menu mnuRefresh 
         Caption         =   "Refresh"
      End
      Begin VB.Menu mnuTogglePreview 
         Caption         =   "Toggle Preview Pane"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "Tools"
      Begin VB.Menu mnuOptions 
         Caption         =   "Options"
      End
      Begin VB.Menu mnuAppearance 
         Caption         =   "Appearance Settings"
      End
      Begin VB.Menu mnuManageSpecialists 
         Caption         =   "Add / Remove Specialists"
      End
      Begin VB.Menu mnuManageCategories 
         Caption         =   "Add / Remove Categories"
      End
      Begin VB.Menu mnuAdminGroups 
         Caption         =   "Add / Update Groups"
      End
   End
   Begin VB.Menu mnuFolderList 
      Caption         =   "FolderList"
      Begin VB.Menu mnuAddGroup 
         Caption         =   "Add Group Queue"
      End
      Begin VB.Menu mnuAddQueue 
         Caption         =   "Add Specialist Queue"
      End
      Begin VB.Menu mnuRemoveQueue 
         Caption         =   "Remove Queue"
      End
   End
   Begin VB.Menu mnuRequestOptions 
      Caption         =   "RequestOptions"
      Visible         =   0   'False
      Begin VB.Menu mnuOpenRequest 
         Caption         =   "Open Request"
      End
      Begin VB.Menu mnuReAssignRequest 
         Caption         =   "ReAssign Request"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim CallView As String
Dim AppInitialised As Boolean
Dim UserFolderPopulated As Boolean
Dim DeletedFolderPopulated As Boolean

Public RequestFormShown As Boolean
Dim PreviewPaneDisplayed As Boolean
Dim SplashScreenDisplayed As Boolean
Dim DeveloperMode As Boolean

Dim RefreshMinutes As Integer
Dim IdleCounter As Single
Dim IdleEventTriggered As Boolean

' v1.02 - Fixed a bug with Actions whereby when double clicking an Action it would display the wrong action in the dialog
' v1.03 - Fixed a bug with new requests displayed in the main list, new requests would not display the associated user
'            - implemented a suggestion from Hannah so that when you click on one of the usernames for an associated user, it displays their full details. Done in the status bar
'            - implemented the option to sort a queue larger than 200 rows, however defaults to none on closed call queues.
' v1.04  - File attachments attached to the database instead of to a file on a separate file share - so now compatible with old richmond
'             - Request Update Notification - now have the ability to keep track of requests regardless of where they are
'             - SetFocus on user search box when clicking on Show User Queues
' v1.05   - Added the ability to quickly log Non IT Calls using ALT-F9
' v1.06   - Added the option to be able to turn off read receipts, and turn off the new request popup. Now in tools->options
' v1.07   - Added the username at the bottom status display when clicking on the user queues
'             - Added the username in the contact details when double clicking on a user in a request
'             - Implemented the show "All Active Requests" option which shows all Non Closed Calls.
' v1.08  - Changed the Add Single Request function to accomodate a new field added by Richmond 5.3 in the database.
' v1.09  - Tick box to log a call per associated user - DONE
'             - Active Requests under User Queues - DONE
'             - Deleted Users category for User Queues - DONE
'             - Remove Duplications of Action / Activity Types - DONE
' v1.10   - Added a view to list All Closed Requests
'             - Added the option to Save a file attachment as well as open it.
' v1.11  - Added the ability to customise colours
'             - Tims Suggestion - When logging a new request, pressing the down arrow will allow you to highlight a user, and pressing enter will select it
'             - Viewing all calls for a group
'             - Removed the Col and Row index out of range error msgbox - Thanks Tom!
'             - To delete or unassign users from calls
' v1.11  - Fixed Bug found by Suz whereby sorting a user queue defaults the queue selection to a specialist queue.
' v1.12  - Fixed an issue whereby logging requests for new users would show the list for deleted users aswell.
'v1.13 - Rota system application which recognises our system as being idle and marks us as busy for the rota.
'           - now have an option under tools > options to turn this on or off and set the time that lsd will set you as absent after
'           - Preview pane option to be recorded if turned off/on.
'           - Option to turn off and on the email request box when adding / closing calls
'           - Option to enable reminders for requests, a reminder will send the person a text message
'           - reminding them of a call.
'           - Email templates to use the new format as suggested by SupportDesk (in Email)
'           - All Active Requests and All Closed Requests when checking for Duplicate calls
'           - and to have a sort option for this list and the default to be date logged
'           - Limit the Summary Field to 50 chars
'           - Function to Produce Stats
' v1.14 - Remove add and close button (Set as invisible)
'           - Contact details viewable when adding new requests
'           - Option to set user call allocation status to absent when closed, and present when open
'           - Request reminders bug fixed (didn't work before hours of 10:00) because of non leading zero
'           - Remove error "There was a problem retrieving the query"
'           - Seperate field for computer details when adding new requests (computer details then added as separate action when saving the call)
'           - Email template changed to include 7pm helpdesk close time
'           - Check for "Autologged" when updating a request and prompt the user to change this.
' v1.15 - Additions made to open and closed call templates to include summary on open call templates, and format has changed slightly.
'           - Additional option in more settings so that the user can override the default setting of sending silent confirmation emails back to the user.
'           - templates can now be renamed
'           - templates list are now sorted by name
'           - contact details on single request screen now same as new request screen
'           - double clicking on associated user on single request screen now shows that users associated calls (used to show contact details).
' v1.16 - Altered the current templates to incorporate the new location in Hexagon Place, and reflect the new address
'           - Added a new template so if a call is added and closed immediately a seperate "OpenClose" template is used and populated
'           - Fixed a bug with attachments, whereby if the user tried to add a file that was in use it would generate an error rather than quit with a runtime error.
' v1.17 - Added an on hold accumalator
'           - Change / Incident field added
'           - New dialog raised when a call is placed on hold, and reasons entered as a new action in the call
'           - Username displayed on contact details
'           - Removed various Closed status types from the database to leave only Closed and Closed FCR.


Private Sub cmbSort_Click()
    If cmbSortMode.Text <> Main.GetAscendingVal And cmbSortMode.Text <> Main.GetDescendingVal Then
        cmbSortMode.Text = Main.GetAscendingVal
    End If
    
    Call Main.SortDisplayedCalls(cmbSort.Text, cmbSortMode.Text)
End Sub

Private Sub cmbSortMode_Click()
    If cmbSortMode.Text <> Main.GetAscendingVal And cmbSortMode.Text <> Main.GetDescendingVal Then
        cmbSortMode.Text = Main.GetAscendingVal
    End If
    
    Call Main.SortDisplayedCalls(cmbSort.Text, cmbSortMode.Text)
End Sub

Private Sub cmdChooseCols_Click()
    frmChooseCols.Show
    frmChooseCols.PopulateColumnLists
End Sub

Private Sub cmbSASpecialist_Click()
    ' Inform the main module, that the combo has changed
    Main.SANameChanged
End Sub

Private Sub cmbSAStatus_Click()
    ' Inform the main module that the combo has changed
    Main.SAStatusChanged
End Sub

Private Sub cmdExcelExport_Click()
    Main.ExportCurrentQueue
End Sub

Private Sub cmdNew_Click()
    frmRequestDetails.ShowUsers = True
    frmRequestDetails.ResetForm = True
    frmRequestDetails.txtSummary = ""
    frmRequestDetails.txtDetails = ""
    frmRequestDetails.txtResolution = ""
    frmRequestDetails.Show
    frmLocateUser.SetInitialSelection ("")
    RequestFormShown = True
End Sub

Public Function TriggerNewRequest()
    Call cmdNew_Click
End Function

Private Sub cmdReAssign_Click()
    Dim Specialist As String
    
    frmReAssignRequest.SelectedRequestID = Main.GetSelectedRequestID
    
    frmReAssignRequest.Show vbModal
    Specialist = frmReAssignRequest.SelectedSpecialist
    If Trim(Specialist) <> "" Then
        'Now reassign the selected request ID to the selected specialist
        Call Main.ReAssignRequest(Main.GetSelectedRequestID, Specialist)
    End If
End Sub

Private Sub cmdRefresh_Click()
    Call mnuRefresh_Click
End Sub

Private Sub cmdReqTemplates_Click()
    frmSelectTemplate.PopulateAvailTemplates
    frmSelectTemplate.Show
End Sub

Private Sub cmdSearchRequests_Click()
    frmSearchRequests.Show
    frmSearchRequests.PopulateForm
    
End Sub

Private Sub cmdStats_Click()
    Call Main.ShowStats
End Sub

Private Sub cmdTest_Click()
'    Call Main.RenameRequestTemplate("Broken PC", "Blue Screening PC", "Use this when something has broken")
End Sub

Private Sub cmdTest2_Click()
    'Main.FList_AddSpecialistQueue ("DRAPERD")
    'Main.GetUserList
    'frmLocateUser.Show
    'MsgBox Main.GetSpecialistGroupName("DRAPERD")
    
   ' MsgBox Str(CMF.FindString("fdgdfCLOSEDdfrgdfg", "CLOSED", True))
   Main.CheckNewRequests
End Sub


Private Sub FGridCalls_Click()
    'MsgBox FGridCalls.TextMatrix(FGridCalls.Row, 0)
    Main.CallGridClicked (FGridCalls.RowSel)
End Sub

Private Sub FGridCalls_DblClick()
    If FGridCalls.Rows > 1 And FGridCalls.Cols > 0 Then
        Main.CallGridDblClicked (FGridCalls.RowSel)
    End If
End Sub

Private Sub FGridCalls_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If FGridCalls.Rows > 1 And FGridCalls.Cols > 0 Then
            FGridCalls.Row = FGridCalls.MouseRow
            FGridCalls.ColSel = FGridCalls.Cols - 1
            
            PopupMenu mnuRequestOptions
        End If
    End If
    
End Sub

Private Sub Form_Activate()
    ' Do the app initialised at form activate so the form gets displayed before the preparation is done
    If SplashScreenDisplayed = False Then
        SplashScreenDisplayed = True
        frmSplash.Show
    End If
    
    If AppInitialised = False Then
        AppInitialised = True
        Me.MousePointer = 11
        frmSplash.MousePointer = 11
        Main.InitProgram
        frmSplash.Hide
        Me.MousePointer = 0
        frmSplash.MousePointer = 0
    End If
    
    'If we have other forms open then show them, otherwise they will be hidden behind the main window
    If RequestFormShown = True Then
        frmRequestDetails.Show
    End If
    

    
End Sub

Private Sub Form_Load()
    'Main.InitProgram
    'Main.OpenDatabase
    'CallView = "OPEN"
    Dim ExpDate As Date
    
    Dim strExpDate As String
    
    strExpDate = "01/12/2008"
    
    ExpDate = strExpDate
    
    If Date > ExpDate Then
        MsgBox "This Beta version of Cede SupportDesk has now expired. Please contact CedeSoft for the latest version.", vbExclamation
        End
    End If
    
    AppInitialised = False
    UserFolderPopulated = False
    DeletedFolderPopulated = False
    SplashScreenDisplayed = False
    DeveloperMode = True
    
    Main.HideMainProgress
    Main.HideMainStaticStatus
    
    ' Set the initial queue selection
    optSQueue.Value = 1
    optUQueue.Value = 0
    treQueues.Visible = True
    treUserQueues.Visible = False
    txtUserSearch.Visible = False
    
    ' Set the preview pane mode
    PreviewPaneDisplayed = True
    
    ' Set the refresh minutes
    RefreshMinutes = 0
    
    ' Set the app colours to the defaults
    AppColours.SetAllAppDefaultColours
    
    ' Add the default specialist status to the combo boxes
    cmbSAStatus.AddItem ("Present")
    cmbSAStatus.AddItem ("Lunch")
    cmbSAStatus.AddItem ("Absent")
    
    ' Register the New Request hotkey.
    If DeveloperMode = False Then
        If RegisterHotKey(hWnd, HOTKEY_ID, MOD_ALT, VK_F10) = 0 Then
            MsgBox "Error registering New Request System HotKey. Cede SupportDesk may already be running.", vbExclamation, "Cede SupportDesk"
        Else
            ' Subclass the TextBox to watch for
            ' WM_HOTKEY messages.
            OldWindowProc = SetWindowLong(hWnd, GWL_WNDPROC, AddressOf NewWindowProc)
        End If
        
        If RegisterHotKey(hWnd, HOTKEYTWO_ID, MOD_ALT, VK_F9) = 0 Then
            MsgBox "Error registering Quick Request System HotKey. Cede SupportDesk may already be running.", vbExclamation, "Cede SupportDesk"
        End If
            ' Subclass the TextBox to watch for
            ' WM_HOTKEY messages.
        '    OldWindowProc = SetWindowLong(hWnd, GWL_WNDPROC, AddressOf NewWindowProc)
        'End If
    End If
End Sub

' This function is executed when the Hot Key event has been triggered.
Public Sub Hotkey()
    'MsgBox "Hot Key Pressed!", vbInformation + vbSystemModal, "Cede SupportDesk"
    'If Me.WindowState = vbMinimized Then Me.WindowState = vbNormal
    'Me.SetFocus
    'Me.WindowState = vbNormal
    Me.SetFocus
    
    Call cmdNew_Click
End Sub

Public Sub HotkeyTwo()
    Me.SetFocus
    'MsgBox "Second HotKey!"
    
    Call frmRequestDetails.SetTemplateDetails("Low - 8 Hours", "Closed", "Other", "Non IT Call", "Non IT Call", "Assisted Person")
    Call cmdNew_Click
    frmLocateUser.SetInitialSelection ("_G")
End Sub

Private Sub Form_Resize()
    ' Now we must resize all of the controls on the window
        
On Error Resume Next 'Just incase the user resizes the form to some rediculous size!

    ' The flexgrid
    FGridCalls.Width = frmMain.Width - FGridCalls.Left - 200
    
    'The title label
    lblTitle.Width = frmMain.Width - 200
    
    'The requests info label
    lblRequestInfo.Left = frmMain.Width - lblRequestInfo.Width - 200
    
    'The details text box
    txtDetails.Top = (frmMain.Height / 2) + 430
    txtDetails.Height = (frmMain.Height / 2) - 1850
    txtDetails.Width = frmMain.Width - txtDetails.Left - 200
    
    'The details label
    lblDetails.Top = txtDetails.Top - lblDetails.Height + 30
    
    'The flexgrid calls height, this is dependant whether the preview pane is on or off
    If PreviewPaneDisplayed = True Then
        FGridCalls.Height = (frmMain.Height / 2) - 900
    Else
        FGridCalls.Height = frmMain.Height - 2500
    End If
    
    'The requests info label vertical position, this depends on the height of the calls flex grid
    lblRequestInfo.Top = FGridCalls.Top + FGridCalls.Height - 20
    
    'The User Treeview control
    treUserQueues.Height = frmMain.Height - 3000
    
    ' The deleted user treeview control
    treDQueues.Height = frmMain.Height - 3000
    
    ' The deleted user search text box
    txtDeletedSearch.Top = treDQueues.Top + treDQueues.Height - 10
    
    'The User Search Text Box
    txtUserSearch.Top = treUserQueues.Top + treUserQueues.Height - 10
    
    ' The specialist treeview control
    treQueues.Height = frmMain.Height - 2750
    
    ' Now the status indicators
    lblStaticStatus.Top = frmMain.Height - 1305
    
    ' The main progress status bar, and text indicator
    progressMain.Top = frmMain.Height - 1305
    progressMain.Left = frmMain.Width - progressMain.Width - 200
    lblProgressStatus.Top = frmMain.Height - 1305
    lblProgressStatus.Left = progressMain.Left - lblProgressStatus.Width - 100
    
    ' Now the choose columns button
    cmdChooseCols.Left = (lblTitle.Left + lblTitle.Width) - cmdChooseCols.Width
    
    'Now the stats button
    cmdStats.Left = frmMain.Width - cmdStats.Width - 570
    
    ' Now the specialist update combo's
    cmbSASpecialist.Left = cmdStats.Left - cmbSASpecialist.Width - 80
    cmbSAStatus.Left = cmbSASpecialist.Left - cmbSAStatus.Width - 80
    lblStatusLabel.Left = cmdStats.Left - lblStatusLabel.Width - 80
    
    'Now the sort combo boxes
    cmbSortMode.Left = cmdChooseCols.Left - cmbSortMode.Width - 50
    lblSortMode.Left = cmbSortMode.Left - lblSortMode.Width - 50
    
    cmbSort.Left = lblSortMode.Left - cmbSort.Width - 200
    lblSort.Left = cmbSort.Left - lblSort.Width - 50
    
    ' The excel export image
    imgExcelExport.Left = (lblTitle.Left + lblTitle.Width) - imgExcelExport.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main.SaveReadItems
    Main.SaveWatchedItems
    Main.GetColSizes
    Main.FList_GetExpandedNodes
    Main.SaveINIFile
    Main.SaveReminders
    ' Set the users status to absent if the option has been switched on
    Call Main.SetSpecialistAutoStatus(False)
    End
End Sub

Private Sub imgExcelExport_Click()
    Main.ExportCurrentQueue
End Sub

Private Sub mnuAbout_Click()
    frmSplash.AllowClick = True
    frmSplash.Show
End Sub

Private Sub mnuAddGroup_Click()
    Dim Group As String
    frmAddGroup.Show vbModal
    Group = frmAddGroup.SelectedGroup
    If Trim(Group) <> "" Then
        Main.FList_AddSpecialistGroup (Group)
        Main.AddCustomAddedGroup (Group)
    End If
End Sub

Private Sub mnuAddQueue_Click()
    Dim Specialist As String
    
    frmAddQueue.Show vbModal
    Specialist = frmAddQueue.SelectedSpecialist
    If Trim(Specialist) <> "" Then
        Main.FList_AddSpecialistQueue (Specialist)
        Main.AddCustomAddedSpecialist (Specialist)
    End If
End Sub

Private Sub mnuAdminGroups_Click()
    frmAdminGroups.Show
End Sub

Private Sub mnuAppearance_Click()
    frmAppearance.InitialiseColourSettings
    frmAppearance.Show
End Sub

Private Sub mnuExit_Click()
    Main.SaveReadItems
    Main.SaveWatchedItems
    Main.GetColSizes
    Main.FList_GetExpandedNodes
    Main.SaveINIFile
    Main.SaveReminders
    ' Set the users status to absent if the option has been switched on
    Call Main.SetSpecialistAutoStatus(False)
    End
End Sub

Private Sub mnuManageCategories_Click()
    frmAdminCategories.Show
End Sub

Private Sub mnuManageSpecialists_Click()
    frmAdminSpecialists.Show
End Sub

Private Sub mnuOpenRequest_Click()
    Call FGridCalls_DblClick
End Sub

Private Sub mnuOptions_Click()
    frmOptions.Show vbModal
End Sub

Private Sub mnuReAssignRequest_Click()
    Call cmdReAssign_Click
End Sub

Private Sub mnuRefresh_Click()
    Main.FList_RefreshUnreadItems
    'Main.RefreshCurrentSpecialistDisplay
    Main.RefreshLastDisplayedQueue
End Sub

Private Sub mnuRemoveQueue_Click()
    frmRemoveQueue.Show
    frmRemoveQueue.PopulateCustomGroups
    frmRemoveQueue.PopulateCustomSpecialists
End Sub

Private Sub mnuTogglePreview_Click()
    If PreviewPaneDisplayed = True Then
        ' We need to make the preview pane dissappear
        txtDetails.Visible = False
        lblDetails.Visible = False
        
        ' Now we need to resize the flex grid so it fills the space previously held by the preview pane
        FGridCalls.Height = frmMain.Height - 2500
        
        'The requests info label vertical position
        lblRequestInfo.Top = FGridCalls.Top + FGridCalls.Height - 20
    
        PreviewPaneDisplayed = False
        Main.PreviewPaneVisible = False
        Exit Sub
    End If
    
    If PreviewPaneDisplayed = False Then
        ' Now we need to make the preview pane appear!
        txtDetails.Visible = True
        lblDetails.Visible = True
        
        ' Put the flex grid back to it's normal size
        FGridCalls.Height = (frmMain.Height / 2) - 900
        
        'The requests info label vertical position
        lblRequestInfo.Top = FGridCalls.Top + FGridCalls.Height - 20
    
        
        PreviewPaneDisplayed = True
        Main.PreviewPaneVisible = True
        Exit Sub
    End If
    
End Sub

Public Function HidePreviewPane()
        ' We need to make the preview pane dissappear
        txtDetails.Visible = False
        lblDetails.Visible = False
        
        ' Now we need to resize the flex grid so it fills the space previously held by the preview pane
        FGridCalls.Height = frmMain.Height - 2500
        
        'The requests info label vertical position
        lblRequestInfo.Top = FGridCalls.Top + FGridCalls.Height - 20
    
        PreviewPaneDisplayed = False
End Function

Public Function ShowPreviewPane()
        ' Now we need to make the preview pane appear!
        txtDetails.Visible = True
        lblDetails.Visible = True
        
        ' Put the flex grid back to it's normal size
        FGridCalls.Height = (frmMain.Height / 2) - 900
        
        'The requests info label vertical position
        lblRequestInfo.Top = FGridCalls.Top + FGridCalls.Height - 20
    
        PreviewPaneDisplayed = True
End Function

Private Sub Option1_Click()

End Sub

Private Sub optDQueue_Click()
    If DeletedFolderPopulated = False Then
        DeletedFolderPopulated = True
        Main.GetDeletedUserList
    End If

    treUserQueues.Visible = False
    txtUserSearch.Visible = False
    treQueues.Visible = False
    txtDeletedSearch.Visible = True
    treDQueues.Visible = True
End Sub

Private Sub optSQueue_Click()
    treQueues.Visible = True
    treUserQueues.Visible = False
    txtUserSearch.Visible = False
    txtDeletedSearch.Visible = False
    treDQueues.Visible = False
End Sub

Private Sub optUQueue_Click()
    If UserFolderPopulated = False Then
        UserFolderPopulated = True
        Main.GetUserList
    End If

    treQueues.Visible = False
    treUserQueues.Visible = True
    txtUserSearch.Visible = True
    txtDeletedSearch.Visible = False
    treDQueues.Visible = False
    txtUserSearch.SetFocus
End Sub

Private Sub tmrIdle_Timer()
    If IdleHandler.CheckIdleState = True Then
        IdleCounter = IdleCounter + 1
        
        ' If the system as gone idle, then check if we've triggered an event
        ' If we haven't then trigger the system idle event
        If IdleCounter >= Main.IdleValue Then
            If IdleEventTriggered = False Then
                IdleEventTriggered = True
                Call Main.SystemIdle
            End If
        End If
        
        ' If the system idle counter is less than the idle value, but the trigger event is true (we are set as away)
        ' Then trigger the SystemNotIdle event (user is back)
        If IdleCounter < Main.IdleValue Then
            If IdleEventTriggered = True Then
                IdleEventTriggered = False
                Call Main.SystemNotIdle
            End If
        End If
    Else
        IdleCounter = 0
        
        ' If the system is NOT idle, and the user is set as away, then trigger the NOT idle event
        ' to ensure user is back
        If IdleEventTriggered = True Then
            IdleEventTriggered = False
            Call Main.SystemNotIdle
        End If
    End If
End Sub

Private Sub tmrNewRequests_Timer()
    If RefreshMinutes = 3 Then
        RefreshMinutes = 0
        Main.CheckNewRequests
        Main.RefreshLastDisplayedQueue
        Main.CheckWatchedRequests
    Else
        RefreshMinutes = RefreshMinutes + 1
    End If
End Sub

Private Sub tmrReminders_Timer()
    Call Main.PollReminders
End Sub

Private Sub treDQueues_Click()
On Error GoTo err
    ' Inform the main module that the folder list was clicked.
    ' We only need to inform the module of the key and the name as the full path of the node is stored as part of the key (much like OS paths)

    Dim NodeKey As String
    Dim NodeName As String
        
    NodeKey = treDQueues.SelectedItem.Key
    NodeName = treDQueues.SelectedItem.Text
    
    ' Inform the main module
    Call Main.UserFolderListClicked(NodeKey, NodeName)
err:
End Sub

Private Sub treQueues_Click()
On Error GoTo err
    ' Inform the main module that the folder list was clicked.
    ' We only need to inform the module of the key and the name as the full path of the node is stored as part of the key (much like OS paths)

    Dim NodeKey As String
    Dim NodeName As String
    
    NodeKey = treQueues.SelectedItem.Key
    NodeName = treQueues.SelectedItem.Text
    
    ' If the user has clicked a closed call display, then default the sort column to <None>
    If CMF.FindString(NodeName, "Closed", False) <> 0 Then Main.ResetSortColumn
        
    ' Inform the main module
    Call Main.FolderListClicked(NodeKey, NodeName)
err:

End Sub

Private Sub treUserQueues_Click()
On Error GoTo err
    ' Inform the main module that the folder list was clicked.
    ' We only need to inform the module of the key and the name as the full path of the node is stored as part of the key (much like OS paths)

    Dim NodeKey As String
    Dim NodeName As String
        
    NodeKey = treUserQueues.SelectedItem.Key
    NodeName = treUserQueues.SelectedItem.Text
    
    ' Inform the main module
    Call Main.UserFolderListClicked(NodeKey, NodeName)
err:

End Sub


Private Sub treQueues_Expand(ByVal Node As MSComctlLib.Node)
    'Dim UserKey As String
    'Dim ND As Integer
    'Dim NDCount As Integer
    
    'NDCount = treQueues.Nodes.Count
    
    'UserKey = treQueues.SelectedItem.Key
    
    'For ND = 1 To NDCount
    '    If treQueues.Nodes.Item(ND).Key = UserKey + "$" + "OPEN" Then
    '        treQueues.Nodes.Item(ND).Text = "Open Calls (" + Trim(Str(Main.GetNumOpenCalls(UserKey))) + ")"
            'MsgBox "Open Node at " + Str(ND)
    '    End If
    '    If treQueues.Nodes.Item(ND).Key = UserKey + "$" + "CLOSED" Then
    '        treQueues.Nodes.Item(ND).Text = "Closed Calls (" + Trim(Str(Main.GetNumClosedCalls(UserKey))) + ")"
            'MsgBox "Closed Node at " + Str(ND)
    '    End If
    'Next
End Sub

Private Sub treQueues_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        PopupMenu mnuFolderList
    End If
End Sub

Private Sub txtDeletedSearch_Change()
    Main.FilterDeletedUserList (txtDeletedSearch.Text)
End Sub

Private Sub txtUserSearch_Change()
    Main.FilterUserList (txtUserSearch.Text)
End Sub

Public Function SetColours()
    ' FGrid Control
    FGridCalls.BackColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridCalls.BackColorBkg = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridCalls.GridColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridCalls.ForeColor = AppColours.Get_FrmMain_FGridCalls_ForeGround
    FGridCalls.ForeColorFixed = AppColours.Get_FrmMain_FGridCalls_ForeGround
    
    ' txtDetails Control
    txtDetails.BackColor = AppColours.Get_FrmMain_txtDetails_BackGround
    txtDetails.ForeColor = AppColours.Get_FrmMain_txtDetails_ForeGround
    
    
End Function
