VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAddAction 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Action"
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8805
   Icon            =   "frmAddAction.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   8805
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdChooseTemplate 
      Caption         =   "Choose Template"
      Height          =   255
      Left            =   3180
      TabIndex        =   14
      Top             =   180
      Width           =   1695
   End
   Begin VB.CommandButton cmdSaveTemplate 
      Caption         =   "Save as Template"
      Height          =   255
      Left            =   4980
      TabIndex        =   13
      Top             =   180
      Width           =   1695
   End
   Begin VB.ComboBox cmbActionBy 
      Height          =   315
      Left            =   3000
      Sorted          =   -1  'True
      TabIndex        =   7
      Top             =   900
      Width           =   2775
   End
   Begin VB.CommandButton cmdAddAction 
      Caption         =   "Add Action"
      Height          =   375
      Left            =   5340
      TabIndex        =   6
      Top             =   4440
      Width           =   1635
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   7080
      TabIndex        =   5
      Top             =   4440
      Width           =   1635
   End
   Begin VB.ComboBox cmbActionType 
      Height          =   315
      Left            =   5940
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   900
      Width           =   2775
   End
   Begin VB.TextBox txtActionDetails 
      Appearance      =   0  'Flat
      Height          =   2775
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1560
      Width           =   8655
   End
   Begin MSComCtl2.DTPicker dtTime 
      Height          =   315
      Left            =   1740
      TabIndex        =   9
      Top             =   900
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562178
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtDate 
      Height          =   315
      Left            =   60
      TabIndex        =   10
      Top             =   900
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562177
      CurrentDate     =   38126
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   12
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1740
      TabIndex        =   11
      Top             =   660
      Width           =   1095
   End
   Begin VB.Label lblActionby 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action By"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   8
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Add Action"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6900
      TabIndex        =   4
      Top             =   120
      Width           =   1815
   End
   Begin VB.Line Line1 
      X1              =   6780
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblActionType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5940
      TabIndex        =   3
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblActionDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   1
      Top             =   1320
      Width           =   8655
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmAddAction.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmAddAction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim ReqID As String
Dim ReqStatus As String
Dim ReqPriority As String
Dim ActionTypeTemplate As String
Dim ActionDetailsTemplate As String

Private Sub cmdAddAction_Click()
    
    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    
    If Trim(cmbActionType.Text) = "" Then
        MsgBox "Please select an action type.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(txtActionDetails.Text) = "" Then
        MsgBox "Please enter some details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActionBy.Text) = "" Then
        MsgBox "Please select a specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Me.MousePointer = 11
    cmdAddAction.Enabled = False
    ' Okay, we've got this far so time to add the action
    
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
    
    Call Main.AddRequestAction(ReqID, cmbActionType.Text, LoggedDateTime, txtActionDetails.Text, cmbActionBy.Text, ReqStatus, ReqPriority)
    Call Main.UpdateRequestDateStamp(ReqID)
    
    frmAddAction.Hide
    ClearTemplateDetails
    Me.MousePointer = 0
    cmdAddAction.Enabled = True
    ' Notify the parent form
    frmSingleRequestDetails.ActionAdded
End Sub

Private Sub cmdCancel_Click()
    ClearTemplateDetails
    frmAddAction.Hide
End Sub

Public Function PopulateActionTypes()
    Dim ActionTypes As New DynDatabaseEX
    
    cmbActionType.Clear
    txtActionDetails.Text = ""
    
    Call Main.GetActionTypeDescriptions(ActionTypes)
    Call CMF.PopulateComboWithDynArray(cmbActionType, ActionTypes)
    
    ' Now set the action type to the default of "Investigate"
    cmbActionType.Text = "Investigate"
End Function

Public Function PopulateSpecialist()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    Dim CurrentUser As String
    
    cmbActionBy.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbActionBy, Specialists)
    
    ' Now set the combo box to default to the current specialist
    CurrentUser = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase))
    cmbActionBy.Text = CurrentUser
End Function

Public Function PopulateTemplate()
    ' This function is called by the parent to enable this form to check
    ' If template values have been set, if they have then the template values are displayed
    
    If Trim(ActionTypeTemplate) <> "" Then
        cmbActionType.Text = ActionTypeTemplate
    End If
    
    If Trim(ActionDetailsTemplate) <> "" Then
        txtActionDetails.Text = ActionDetailsTemplate
    End If
End Function

Public Function SetRequestDetails(RequestID As String, Status As String, Priority As String)
    ReqID = RequestID
    ReqStatus = Status
    ReqPriority = Priority
End Function

Public Function SetTemplateDetails(ActionType As String, ActionDetails As String)
    ActionTypeTemplate = ActionType
    ActionDetailsTemplate = ActionDetails
End Function

Public Function PopulateDTControl()
    ' Private function to populate the date and time controls with the current date and time of the system
    Dim CurrentTime As String
    Dim CurMin As String
    Dim CurHour As String
    
    CurrentTime = Format(Time, "HH:MM:SS")

    CurMin = CMF.GetDataPart(CurrentTime, 2, ":")
    CurHour = CMF.GetDataPart(CurrentTime, 1, ":")

    dtTime.Hour = CurHour
    dtTime.Minute = CurMin
    dtTime.Second = "00"
    
    dtDate.Month = Format(Date, "MM")
    dtDate.Day = Format(Date, "DD")
    dtDate.Year = Format(Date, "YYYY")
End Function

Private Sub cmdChooseTemplate_Click()
    frmSelectActionTemplate.PopulateAvailTemplates
    frmSelectActionTemplate.Show vbModal
    If Trim(frmSelectActionTemplate.GetSelectedTemplate) <> "" Then
        cmbActionType.Text = frmSelectActionTemplate.GetActionType
        txtActionDetails.Text = frmSelectActionTemplate.GetActionDetails
    End If
End Sub

Private Sub cmdSaveTemplate_Click()
    ' Initial checks to see if we can add the template
    If Trim(cmbActionType.Text) = "" Then
        MsgBox "Please select an Action Type.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
       
    If Trim(txtActionDetails.Text) = "" Then
        MsgBox "Please enter some Action Details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
        
    ' Now set the templates details, and give these to the add template form
    Call frmAddActionTemplate.SetTemplateDetails(cmbActionType.Text, txtActionDetails.Text)

    ' Now display the form
    frmAddActionTemplate.txtTemplateName = ""
    frmAddActionTemplate.txtTemplateComments = ""
    frmAddActionTemplate.Show
End Sub

Public Function ClearTemplateDetails()
    ActionTypeTemplate = ""
    ActionDetailsTemplate = ""
End Function

Public Function SetColours()
    lblDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTime.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTime.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionby.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionby.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionType.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionType.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

