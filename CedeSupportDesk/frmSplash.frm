VERSION 5.00
Begin VB.Form frmSplash 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4830
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9285
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4830
   ScaleWidth      =   9285
   StartUpPosition =   2  'CenterScreen
   Begin VB.Image Image1 
      Height          =   4860
      Left            =   0
      Picture         =   "frmSplash.frx":0E42
      Top             =   0
      Width           =   9330
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public AllowClick As Boolean

Private Sub Image1_Click()
    If AllowClick = True Then
        frmSplash.Hide
    End If
End Sub
