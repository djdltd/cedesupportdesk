VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmWatchedRequests 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Watched Requests"
   ClientHeight    =   4710
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9225
   Icon            =   "frmWatchedRequests.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   9225
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   7500
      TabIndex        =   2
      Top             =   4260
      Width           =   1695
   End
   Begin MSFlexGridLib.MSFlexGrid FGridWatchedCalls 
      Height          =   2895
      Left            =   0
      TabIndex        =   1
      ToolTipText     =   "Double Click to view Request Details"
      Top             =   1260
      Width           =   9195
      _ExtentX        =   16219
      _ExtentY        =   5106
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16761024
      BackColorFixed  =   16777215
      BackColorBkg    =   16761024
      GridColor       =   16761024
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Line Line1 
      X1              =   5880
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Watched Requests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   0
      Top             =   120
      Width           =   2955
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmWatchedRequests.frx":0442
      Top             =   0
      Width           =   18000
   End
   Begin VB.Image Image1 
      Height          =   795
      Left            =   0
      Picture         =   "frmWatchedRequests.frx":24516
      Stretch         =   -1  'True
      Top             =   420
      Width           =   9210
   End
End
Attribute VB_Name = "frmWatchedRequests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Private Sub cmdClose_Click()
    frmWatchedRequests.Hide
End Sub

Public Function PopulateWatchedRequests(WatchedRequestList As DynList)

    Dim RequestList As New DynDatabaseEX
    Dim RequestListTitles As New DynDatabaseEX
    Dim DRow As Integer
    Dim ReqID As String
    Dim AssocUser As String
    Dim Summary As String
    
    Call RequestList.SetSize(3, WatchedRequestList.GetSize)
    
    ' Set the titles
    Call RequestListTitles.SetSize(1, 3)
    Call RequestListTitles.SetElement(1, 1, "Request ID")
    Call RequestListTitles.SetElement(1, 2, "Associated User")
    Call RequestListTitles.SetElement(1, 3, "Summary")
    
    ' Transfer the list to a dynarray
    For DRow = 1 To WatchedRequestList.GetSize
        ReqID = CMF.GetDataPart(WatchedRequestList.GetItem(DRow), 1, "�")
        AssocUser = CMF.GetDataPart(WatchedRequestList.GetItem(DRow), 2, "�")
        Summary = CMF.GetDataPart(WatchedRequestList.GetItem(DRow), 3, "�")
        
        Call RequestList.SetElement(1, (DRow), ReqID)
        Call RequestList.SetElement(2, (DRow), AssocUser)
        Call RequestList.SetElement(3, (DRow), Summary)
    Next
    
    'Now populate the grid with the dynarray
    Call CMF.CopyArrayToFGrid(RequestList, FGridWatchedCalls)
    
    ' Now populate the titles
    Call CMF.SetFGridTitles(RequestListTitles, FGridWatchedCalls)
    
    'Set the column sizes
    SetGridSizes
    
End Function

Private Function SetGridSizes()
    On Error GoTo err
        
    FGridWatchedCalls.ColWidth(0) = 960
    FGridWatchedCalls.ColWidth(1) = 3270
    FGridWatchedCalls.ColWidth(2) = 4905
err:

End Function

Private Sub FGridWatchedCalls_DblClick()
   Dim SelectedReqID As String
    
    If FGridWatchedCalls.Rows > 1 And FGridWatchedCalls.Cols > 1 Then
        Me.MousePointer = 11
        SelectedReqID = Trim(Str(FGridWatchedCalls.TextMatrix(FGridWatchedCalls.RowSel, 0)))
    End If
    
    If Trim(SelectedReqID) <> "" Then
        frmSingleRequestDetails.ShowRequestDetails (SelectedReqID)
        frmSingleRequestDetails.Show
        Me.MousePointer = 0
    End If
    
End Sub

Public Function SetColours()
    FGridWatchedCalls.BackColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridWatchedCalls.GridColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridWatchedCalls.BackColorBkg = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridWatchedCalls.ForeColor = AppColours.Get_FrmMain_FGridCalls_ForeGround
    FGridWatchedCalls.ForeColorFixed = AppColours.Get_FrmMain_FGridCalls_ForeGround
End Function
 
Private Sub Form_Activate()
    SetColours
End Sub

