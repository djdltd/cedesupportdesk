VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmColourChooser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Change Colour"
   ClientHeight    =   3330
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7500
   Icon            =   "frmColourChooser.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3330
   ScaleWidth      =   7500
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   435
      Left            =   5940
      TabIndex        =   13
      Top             =   2760
      Width           =   1395
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   435
      Left            =   4380
      TabIndex        =   12
      Top             =   2760
      Width           =   1395
   End
   Begin MSComctlLib.Slider sldRed 
      Height          =   435
      Left            =   2340
      TabIndex        =   3
      Top             =   1020
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   767
      _Version        =   393216
      Max             =   255
   End
   Begin VB.CommandButton cmdPreview 
      BackColor       =   &H00C0C0C0&
      Height          =   1155
      Left            =   300
      MaskColor       =   &H80000004&
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1260
      Width           =   1275
   End
   Begin MSComctlLib.Slider sldGreen 
      Height          =   435
      Left            =   2340
      TabIndex        =   4
      Top             =   1560
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   767
      _Version        =   393216
      Max             =   255
   End
   Begin MSComctlLib.Slider sldBlue 
      Height          =   435
      Left            =   2340
      TabIndex        =   5
      Top             =   2100
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   767
      _Version        =   393216
      Max             =   255
   End
   Begin VB.Label lblBlue 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6540
      TabIndex        =   11
      Top             =   2160
      Width           =   675
   End
   Begin VB.Label lblGreen 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6540
      TabIndex        =   10
      Top             =   1620
      Width           =   675
   End
   Begin VB.Label lblRed 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6540
      TabIndex        =   9
      Top             =   1080
      Width           =   675
   End
   Begin VB.Label Label5 
      Caption         =   "Blue:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1800
      TabIndex        =   8
      Top             =   2220
      Width           =   435
   End
   Begin VB.Label Label4 
      Caption         =   "Green:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1680
      TabIndex        =   7
      Top             =   1680
      Width           =   555
   End
   Begin VB.Label Label3 
      Caption         =   "Red:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1860
      TabIndex        =   6
      Top             =   1140
      Width           =   435
   End
   Begin VB.Label Label2 
      Caption         =   "Preview:"
      Height          =   195
      Left            =   360
      TabIndex        =   1
      Top             =   1020
      Width           =   915
   End
   Begin VB.Line Line1 
      X1              =   5160
      X2              =   180
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Colour Selection"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5340
      TabIndex        =   0
      Top             =   60
      Width           =   2055
   End
   Begin VB.Image Image1 
      Height          =   435
      Left            =   0
      Picture         =   "frmColourChooser.frx":0442
      Stretch         =   -1  'True
      Top             =   360
      Width           =   7590
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   -60
      Picture         =   "frmColourChooser.frx":1442C
      Top             =   -180
      Width           =   18000
   End
End
Attribute VB_Name = "frmColourChooser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CurrentColour As Long
Dim CurrentRed As Integer
Dim CurrentGreen As Integer
Dim CurrentBlue As Integer
Dim ColourChanged As Boolean

Public Function SetInitialColour(InitialColour As Long)
    Dim HexRep As String
    Dim HBlue As String
    Dim HGreen As String
    Dim HRed As String
    Dim IRed As Integer
    Dim IGreen As Integer
    Dim IBlue As Integer
    Dim TempString As String
    
    
    HexRep = Hex(InitialColour)
    
    ' If the length of the hex representation is less than 6, then
    ' pad the string out with zeros first
    If Len(HexRep) = 5 Then
        TempString = "0" + HexRep
        HexRep = TempString
    End If
    
    If Len(HexRep) = 4 Then
        TempString = "00" + HexRep
        HexRep = TempString
    End If
    
    If Len(HexRep) = 3 Then
        TempString = "000" + HexRep
        HexRep = TempString
    End If
    
    If Len(HexRep) = 2 Then
        TempString = "0000" + HexRep
        HexRep = TempString
    End If
    
    If Len(HexRep) = 1 Then
        TempString = "00000" + HexRep
        HexRep = TempString
    End If
    
    
    If Len(HexRep) = 6 Then
        ' As we have been given a long, we need to separate the red, green and blue components
        HRed = "&H" + Mid(HexRep, 5, 2)
        HGreen = "&H" + Mid(HexRep, 3, 2)
        HBlue = "&H" + Mid(HexRep, 1, 2)
                
        IRed = Val(HRed)
        IGreen = Val(HGreen)
        IBlue = Val(HBlue)
        
        CurrentRed = IRed
        CurrentGreen = IGreen
        CurrentBlue = IBlue
        
        ' Now we set the control values
        lblRed.Caption = Trim(Str(IRed))
        lblGreen.Caption = Trim(Str(IGreen))
        lblBlue.Caption = Trim(Str(IBlue))
        
        sldRed.Value = IRed
        sldGreen.Value = IGreen
        sldBlue.Value = IBlue
        
        cmdPreview.BackColor = RGB(IRed, IGreen, IBlue)
        
        CurrentColour = InitialColour
    End If
End Function

Private Sub cmdCancel_Click()
    ColourChanged = False
    frmColourChooser.Hide
End Sub

Private Sub cmdOk_Click()
    ColourChanged = True
    CurrentColour = RGB(CurrentRed, CurrentGreen, CurrentBlue)
    frmColourChooser.Hide
End Sub

Private Sub sldBlue_Scroll()
    lblBlue.Caption = Trim(Str(sldBlue.Value))
    CurrentBlue = sldBlue.Value
    cmdPreview.BackColor = RGB(CurrentRed, CurrentGreen, CurrentBlue)
End Sub

Private Sub sldGreen_Scroll()
    lblGreen.Caption = Trim(Str(sldGreen.Value))
    CurrentGreen = sldGreen.Value
    cmdPreview.BackColor = RGB(CurrentRed, CurrentGreen, CurrentBlue)
End Sub

Private Sub sldRed_Scroll()
    lblRed.Caption = Trim(Str(sldRed.Value))
    CurrentRed = sldRed.Value
    cmdPreview.BackColor = RGB(CurrentRed, CurrentGreen, CurrentBlue)
End Sub

Public Function GetCurrentColour() As Long
    GetCurrentColour = CurrentColour
End Function

Public Function SetColourChanged(Changed As Boolean)
    ColourChanged = Changed
End Function
Public Function GetColourChanged() As Boolean
    GetColourChanged = ColourChanged
End Function
