VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ExcelHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Function ExportToExcel(SrcDynArray As DynDatabaseEX, FieldTitles As DynDatabaseEX)
On Error GoTo err
    Dim C As Single
    Dim R As Single
    Dim X As Excel.Application
    Dim XB As Excel.Workbook
    Dim XS As Excel.Worksheet
    Dim TempElement As String

    Set X = CreateObject("Excel.Application")
    Set XB = X.Workbooks.Add
    Set XS = XB.Worksheets(1)
    
    Main.ShowMainProgress
            
    ' Set the Field Titles
    
    For C = 1 To FieldTitles.GetNumRows
        TempElement = FieldTitles.GetElement(1, C)
        XS.Cells(1, C).Value = TempElement
    Next
    
    For R = 1 To SrcDynArray.GetNumRows
        For C = 1 To SrcDynArray.GetNumCols
            TempElement = SrcDynArray.GetElement(C, R)
            XS.Cells(R + 2, C).Value = TempElement
        Next
        DoEvents
        Call Main.UpdateMainProgressStatus("Exporting, Please Wait...", SrcDynArray.GetNumRows, R)
    Next
    
    Main.HideMainProgress
    X.Visible = True
    
    Set X = Nothing
    Set XB = Nothing
    Set XS = Nothing
    
    Exit Function
err:
    MsgBox "There was a exporting to Excel.", vbExclamation, "Cede SupportDesk"
    X.Quit
    Set X = Nothing
    Set XB = Nothing
    Set XS = Nothing
End Function
