VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAddActivity 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Request Activity"
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8790
   Icon            =   "frmAddActivity.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   8790
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtActivityDetails 
      Appearance      =   0  'Flat
      Height          =   2775
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1560
      Width           =   8655
   End
   Begin VB.ComboBox cmbActivityType 
      Height          =   315
      Left            =   5940
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   900
      Width           =   2775
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   7080
      TabIndex        =   2
      Top             =   4440
      Width           =   1635
   End
   Begin VB.CommandButton cmdAddActivity 
      Caption         =   "Add Activity"
      Height          =   375
      Left            =   5340
      TabIndex        =   1
      Top             =   4440
      Width           =   1635
   End
   Begin VB.ComboBox cmbActivityFor 
      Height          =   315
      Left            =   3000
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   900
      Width           =   2775
   End
   Begin MSComCtl2.DTPicker dtTime 
      Height          =   315
      Left            =   1740
      TabIndex        =   5
      Top             =   900
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562178
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtDate 
      Height          =   315
      Left            =   60
      TabIndex        =   6
      Top             =   900
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562177
      CurrentDate     =   38126
   End
   Begin VB.Label lblActivityDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   12
      Top             =   1320
      Width           =   8655
   End
   Begin VB.Label lblActionType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5940
      TabIndex        =   11
      Top             =   660
      Width           =   2775
   End
   Begin VB.Line Line1 
      X1              =   6540
      X2              =   180
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Add Activity"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6720
      TabIndex        =   10
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblActionby 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity For"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   9
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1740
      TabIndex        =   8
      Top             =   660
      Width           =   1095
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   7
      Top             =   660
      Width           =   1695
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmAddActivity.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmAddActivity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim ReqID As String

Private Sub cmdAddActivity_Click()
    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    
    If Trim(cmbActivityType.Text) = "" Then
        MsgBox "Please select an activity type.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(txtActivityDetails.Text) = "" Then
        MsgBox "Please enter some details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActivityFor.Text) = "" Then
        MsgBox "Please select a activity for specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Okay, we've got this far so time to add the action
    
    Me.MousePointer = 11
    cmdAddActivity.Enabled = False
    
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
        
    Call Main.AddRequestActivity(ReqID, cmbActivityType.Text, cmbActivityFor.Text, LoggedDateTime, txtActivityDetails.Text, Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)))
    Call Main.UpdateRequestDateStamp(ReqID)
    
    frmAddActivity.Hide
    
    Me.MousePointer = 0
    cmdAddActivity.Enabled = True
    
    ' Notify the parent form
    frmSingleRequestDetails.ActivityAdded
End Sub

Public Function PopulateActivityTypes()
    Dim ActionTypes As New DynDatabaseEX
    
    cmbActivityType.Clear
    txtActivityDetails.Text = ""
    
    Call Main.GetActionTypeDescriptions(ActionTypes)
    Call CMF.PopulateComboWithDynArray(cmbActivityType, ActionTypes)
    
    ' Now set the action type to the default of "Investigate"
    cmbActivityType.Text = "Investigate"
End Function

Public Function PopulateActivityFor()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    Dim CurrentUser As String
    
    cmbActivityFor.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbActivityFor, Specialists)
    
    ' Now set the combo box to default to the current specialist
    CurrentUser = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase))
    cmbActivityFor.Text = CurrentUser
End Function

Public Function SetRequestDetails(RequestID As String)
    ReqID = RequestID
End Function

Public Function PopulateDTControl()
    ' Private function to populate the date and time controls with the current date and time of the system
    Dim CurrentTime As String
    Dim CurMin As String
    Dim CurHour As String
    
    CurrentTime = Format(Time, "HH:MM:SS")

    CurMin = CMF.GetDataPart(CurrentTime, 2, ":")
    CurHour = CMF.GetDataPart(CurrentTime, 1, ":")

    dtTime.Hour = CurHour
    dtTime.Minute = CurMin
    dtTime.Second = "00"
    
    dtDate.Month = Format(Date, "MM")
    dtDate.Day = Format(Date, "DD")
    dtDate.Year = Format(Date, "YYYY")
End Function

Private Sub cmdCancel_Click()
    frmAddActivity.Hide
End Sub

Public Function SetColours()
    lblDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTime.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTime.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionby.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionby.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionType.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionType.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActivityDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActivityDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    
End Function

Private Sub Form_Activate()
    SetColours
End Sub

