VERSION 5.00
Begin VB.Form frmAdminSpecialists 
   Caption         =   "Manage Specialists"
   ClientHeight    =   6060
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   9390
   Icon            =   "frmAdminSpecialists.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6060
   ScaleWidth      =   9390
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAdd 
      Caption         =   "< Add Specialist"
      Height          =   375
      Left            =   6060
      TabIndex        =   19
      Top             =   5040
      Width           =   1755
   End
   Begin VB.TextBox txtPassword 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   6060
      PasswordChar    =   "*"
      TabIndex        =   16
      Top             =   4500
      Width           =   2775
   End
   Begin VB.TextBox txtTelephone 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6060
      TabIndex        =   14
      Top             =   3720
      Width           =   2775
   End
   Begin VB.TextBox txtEmail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6060
      TabIndex        =   12
      Top             =   3060
      Width           =   2775
   End
   Begin VB.TextBox txtLastname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6060
      TabIndex        =   10
      Top             =   2400
      Width           =   2775
   End
   Begin VB.TextBox txtFirstname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6060
      TabIndex        =   8
      Top             =   1740
      Width           =   2775
   End
   Begin VB.TextBox txtUsername 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6060
      TabIndex        =   6
      Top             =   1080
      Width           =   2775
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Specialist"
      Height          =   375
      Left            =   60
      TabIndex        =   5
      Top             =   5580
      Width           =   1755
   End
   Begin VB.ListBox lstSpecialists 
      Height          =   4155
      Left            =   60
      TabIndex        =   3
      Top             =   1320
      Width           =   5415
   End
   Begin VB.ComboBox cmbGroups 
      Height          =   315
      Left            =   60
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   660
      Width           =   5415
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "New Specialist Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5640
      TabIndex        =   18
      Top             =   420
      Width           =   3615
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Login Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   17
      Top             =   4260
      Width           =   2775
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Telephone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   15
      Top             =   3480
      Width           =   2775
   End
   Begin VB.Label lblEmail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   13
      Top             =   2820
      Width           =   2775
   End
   Begin VB.Label lblLastname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lastname"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   11
      Top             =   2160
      Width           =   2775
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Firstname"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   9
      Top             =   1500
      Width           =   2775
   End
   Begin VB.Label lblUsername 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Username"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6060
      TabIndex        =   7
      Top             =   840
      Width           =   2775
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialists"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   1080
      Width           =   5415
   End
   Begin VB.Label lblCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   5415
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H8000000C&
      Caption         =   "Manage Specialists"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14115
   End
End
Attribute VB_Name = "frmAdminSpecialists"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbGroups_Change()
    Call PopulateSpecialists
End Sub

Private Sub ClearForm()
    txtUsername.Text = ""
    txtFirstname.Text = ""
    txtLastname.Text = ""
    txtEmail.Text = ""
    txtTelephone.Text = ""
    txtPassword.Text = ""
End Sub

Private Sub cmbGroups_Click()
    Call PopulateSpecialists
End Sub

Private Sub cmdAdd_Click()
    Dim CMF As New CommonFunctions
    Dim DynArraySpecialists As New DynDatabaseEX
    Dim SelectedItem As String
    Dim GroupID As String
    
    SelectedItem = cmbGroups.Text
    GroupID = Main.GetGroupID(SelectedItem)
    
    Dim NumErrorFields As Integer
    NumErrorFields = 0
    
    NumErrorFields = NumErrorFields + CheckField(txtUsername.Text, "Username")
    NumErrorFields = NumErrorFields + CheckField(txtLastname.Text, "Lastname")
    NumErrorFields = NumErrorFields + CheckField(txtFirstname.Text, "Firstname")
    NumErrorFields = NumErrorFields + CheckField(txtEmail.Text, "Email")
    NumErrorFields = NumErrorFields + CheckField(txtTelephone.Text, "Telephone")
    NumErrorFields = NumErrorFields + CheckField(txtPassword.Text, "Login Password")
    
    If NumErrorFields > 0 Then Exit Sub
    
    ' If we get here then we're ok to go
    If Main.RichHandler.DoesSpecialistExist(txtUsername.Text) = True Then
        MsgBox "This specialist already exists in the database. Please enter an alternate username.", vbExclamation
        Exit Sub
    End If
    
    If Main.RichHandler.AddSingleSpecialist(txtUsername.Text, txtFirstname.Text, txtLastname.Text, txtEmail.Text, GroupID, txtTelephone.Text, txtPassword.Text) = False Then
        MsgBox "There was a problem adding the new specialist.", vbExclamation
    Else
        PopulateSpecialists
        ClearForm
    End If
    
End Sub

Private Sub cmdRemove_Click()
    
    Dim SelectedUser As String
    
    SelectedUser = lstSpecialists.List(lstSpecialists.ListIndex)
    
    If Trim(SelectedUser) <> "" Then
        If MsgBox("Are you sure you want to remove " + SelectedUser + "?", vbQuestion + vbYesNo) = vbYes Then
            Call Main.RichHandler.RemoveSingleSpecialist(SelectedUser)
            Call PopulateSpecialists
        End If
    End If
    
End Sub

Private Sub Form_Activate()
    Dim CMF As New CommonFunctions
    Dim DynArrayGroups As New DynDatabaseEX
    Call Main.GetGroupDescriptions(DynArrayGroups)
    Call CMF.PopulateComboWithDynArray(cmbGroups, DynArrayGroups)
    cmbGroups.ListIndex = 0
    
    Call PopulateSpecialists
    
End Sub

Private Sub PopulateSpecialists()
    Dim CMF As New CommonFunctions
    Dim DynArraySpecialists As New DynDatabaseEX
    Dim SelectedItem As String
    Dim GroupID As String
    
    SelectedItem = cmbGroups.Text
    GroupID = Main.GetGroupID(SelectedItem)
    Call Main.GetGroupSpecialists(GroupID, DynArraySpecialists)
    
    Call CMF.PopulateListWithDynArray(DynArraySpecialists, lstSpecialists)
End Sub

Public Function CheckField(InputString As String, FieldName As String) As Integer
    If Trim(InputString) = "" Then
        MsgBox "Please enter a " + FieldName + ".", vbExclamation
        CheckField = 1
    Else
        CheckField = 0
    End If
End Function
