VERSION 5.00
Begin VB.Form frmAddActionTemplate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Action Template"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6810
   Icon            =   "frmAddActionTemplate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtTemplateName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   3
      Top             =   900
      Width           =   6675
   End
   Begin VB.TextBox txtTemplateComments 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   1560
      Width           =   6675
   End
   Begin VB.CommandButton cmdAddTemplate 
      Caption         =   "Add Template"
      Height          =   375
      Left            =   3360
      TabIndex        =   1
      Top             =   1980
      Width           =   1635
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5100
      TabIndex        =   0
      Top             =   1980
      Width           =   1635
   End
   Begin VB.Line Line1 
      X1              =   3540
      X2              =   300
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Add ActionTemplate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3660
      TabIndex        =   6
      Top             =   120
      Width           =   3135
   End
   Begin VB.Label lblName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   660
      Width           =   6675
   End
   Begin VB.Label lblComments 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template Comments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   1320
      Width           =   6675
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmAddActionTemplate.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmAddActionTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim ActionTypeDesc As String
Dim ActionDetailsDesc As String

Private Sub cmdAddTemplate_Click()
    ' Check if the user entered something
    If Trim(txtTemplateName.Text) = "" Then
        MsgBox "Please enter a template name.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Now add the template
    Call Main.AddActionTemplate(ActionTypeDesc, ActionDetailsDesc, txtTemplateName.Text, txtTemplateComments.Text)
    
    ' Hide the form
    frmAddActionTemplate.Hide
End Sub

Private Sub cmdCancel_Click()
    frmAddActionTemplate.Hide
End Sub

Public Function SetTemplateDetails(ActionType As String, ActionDetails As String)
    ActionTypeDesc = ActionType
    ActionDetailsDesc = ActionDetails
End Function

Public Function SetColours()
    lblName.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblName.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblComments.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblComments.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

