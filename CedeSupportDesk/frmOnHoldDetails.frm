VERSION 5.00
Begin VB.Form frmOnHoldDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "On Hold Details"
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7215
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   7215
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5520
      TabIndex        =   4
      Top             =   2820
      Width           =   1635
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   3780
      TabIndex        =   3
      Top             =   2820
      Width           =   1635
   End
   Begin VB.TextBox txtReasons 
      Appearance      =   0  'Flat
      Height          =   1935
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   780
      Width           =   7215
   End
   Begin VB.Label lblReasonDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Please enter reasons why this request is being placed On Hold:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   540
      Width           =   7215
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "On Hold Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4740
      TabIndex        =   0
      Top             =   120
      Width           =   2415
   End
   Begin VB.Line Line1 
      X1              =   4500
      X2              =   180
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmOnHoldDetails.frx":0000
      Top             =   -60
      Width           =   18000
   End
End
Attribute VB_Name = "frmOnHoldDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim OnHoldReason As String
Public ReasonSet As Boolean
Dim CallerForm As String

Private Sub cmdCancel_Click()
    ReasonSet = False
    frmOnHoldDetails.Hide
    If CallerForm = "NEW" Then
            frmRequestDetails.HoldReasonCancelled
    End If
    If CallerForm = "UPDATE" Then
            frmSingleRequestDetails.HoldReasonCancelled
    End If
End Sub

Private Sub cmdOk_Click()
    If Trim(txtReasons.Text) <> "" Then
        ReasonSet = True
        OnHoldReason = txtReasons.Text
        frmOnHoldDetails.Hide
        If CallerForm = "NEW" Then
            frmRequestDetails.HoldReasonSet
        End If
        If CallerForm = "UPDATE" Then
            frmSingleRequestDetails.HoldReasonSet
        End If
    End If
End Sub

Public Function ClearDetails()
    'OnHoldReason = ""
    txtReasons.Text = ""
    ReasonSet = False
End Function

Public Function GetReason() As String
    GetReason = OnHoldReason
End Function

Public Function SetCallerForm(CallForm As String)
    CallerForm = CallForm
End Function

