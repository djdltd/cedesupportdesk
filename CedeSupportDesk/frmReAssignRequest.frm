VERSION 5.00
Begin VB.Form frmReAssignRequest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reassign Request"
   ClientHeight    =   4545
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5430
   Icon            =   "frmReAssignRequest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   5430
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   1620
      TabIndex        =   6
      Top             =   4080
      Width           =   1815
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3600
      TabIndex        =   5
      Top             =   4080
      Width           =   1755
   End
   Begin VB.ListBox lstSpecialists 
      Appearance      =   0  'Flat
      Height          =   3150
      Left            =   2760
      TabIndex        =   2
      Top             =   840
      Width           =   2595
   End
   Begin VB.ListBox lstGroups 
      Appearance      =   0  'Flat
      Height          =   3150
      Left            =   60
      TabIndex        =   1
      Top             =   840
      Width           =   2595
   End
   Begin VB.Label lblGroups 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   600
      Width           =   2595
   End
   Begin VB.Label lblSpecialists 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialists"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2760
      TabIndex        =   3
      Top             =   600
      Width           =   2595
   End
   Begin VB.Line Line1 
      X1              =   1020
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Reassign Request"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1140
      TabIndex        =   0
      Top             =   120
      Width           =   4215
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmReAssignRequest.frx":0442
      Top             =   -60
      Width           =   18000
   End
End
Attribute VB_Name = "frmReAssignRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SelectedSpecialist As String
Public SelectedRequestID As String

Private Sub cmdCancel_Click()
    SelectedSpecialist = ""
    frmReAssignRequest.Hide
End Sub

Private Sub cmdOk_Click()
    Dim SelectedItem As String
    SelectedItem = lstSpecialists.List(lstSpecialists.ListIndex)
    If Trim(SelectedItem) <> "" Then
        SelectedSpecialist = SelectedItem
        frmReAssignRequest.Hide
    Else
        MsgBox "Please select a specialist.", vbExclamation, "Cede SupportDesk"
    End If
End Sub

Private Sub Form_Activate()
    Dim CMF As New CommonFunctions
    Dim DynArrayGroups As New DynDatabaseEX
    Call Main.GetGroupDescriptions(DynArrayGroups)
    Call CMF.PopulateListWithDynArray(DynArrayGroups, lstGroups)
    lblTitle.Caption = "Reassign Request " + SelectedRequestID
    SetColours
End Sub

Private Sub lstGroups_Click()
    Dim CMF As New CommonFunctions
    Dim DynArraySpecialists As New DynDatabaseEX
    Dim SelectedItem As String
    Dim GroupID As String
    
    SelectedItem = lstGroups.List(lstGroups.ListIndex)
    GroupID = Main.GetGroupID(SelectedItem)
    Call Main.GetGroupSpecialists(GroupID, DynArraySpecialists)
    Call CMF.PopulateListWithDynArray(DynArraySpecialists, lstSpecialists)
End Sub

Public Function SetColours()
    lblGroups.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblGroups.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSpecialists.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSpecialists.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function
