VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmRequestAttach 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Request Attachments"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5895
   Icon            =   "frmRequestAttach.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   5895
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSaveFile 
      Caption         =   "Save File"
      Height          =   375
      Left            =   2820
      TabIndex        =   6
      Top             =   3540
      Width           =   1275
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3900
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   3540
      Width           =   1275
   End
   Begin VB.CommandButton cmdRemoveFile 
      Caption         =   "Remove File"
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   3540
      Width           =   1275
   End
   Begin VB.CommandButton cmdAddFile 
      Caption         =   "Add File"
      Height          =   375
      Left            =   60
      TabIndex        =   3
      Top             =   3540
      Width           =   1275
   End
   Begin VB.ListBox lstFiles 
      Appearance      =   0  'Flat
      Height          =   2565
      Left            =   60
      TabIndex        =   1
      Top             =   900
      Width           =   5775
   End
   Begin VB.Label lblFiles 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Attached Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   660
      Width           =   5775
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "File Attachments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3180
      TabIndex        =   0
      Top             =   120
      Width           =   2595
   End
   Begin VB.Line Line1 
      X1              =   3060
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmRequestAttach.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmRequestAttach"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Dim CMF As New CommonFunctions
Dim FileAttachments As New DynDatabaseEX
Dim AddedFiles As New DynList
Dim ReqID As String
Dim FileAddMode As Boolean
Private Sub cmdAddFile_Click()
    Dim ChosenFile As String
    Dim ChosenPath As String
    
    CommonDialog1.DialogTitle = "Locate File Attachment"
    CommonDialog1.Filter = "All Files (*.*)|*.*"
    CommonDialog1.ShowOpen
    
    If CommonDialog1.FileName <> "" Then
        ChosenFile = CommonDialog1.FileTitle
        ChosenPath = CommonDialog1.FileName
        
        If FileAddMode = True Then
            If AddedFiles.DoesItemExist(ChosenFile + "|" + ChosenPath, False) = 0 Then
                AddedFiles.AddItem (ChosenFile + "|" + ChosenPath)
                lstFiles.AddItem (ChosenFile)
            Else
                MsgBox "This file is already attached.", vbInformation, "Cede SupportDesk"
            End If
        Else
            Call AttachFileNow(ReqID, ChosenFile, ChosenPath)
            PopulateRequestAttachments (ReqID)
        End If
        
    End If

End Sub

Public Function PopulateRequestAttachments(RequestID As String)
    ' Clear the attachment details
    Call FileAttachments.SetSize(0, 0)
    
    ' Get the attachment details
    Call Main.GetRequestAttachDetails(RequestID, FileAttachments)
    
    ' Now populate the list
    Call CMF.PopulateListWithDynArray(FileAttachments, lstFiles)
    
    ' Set the request id
    ReqID = RequestID
End Function

Public Function SetMode(AddMode As Boolean)
    ' The parent form sets the mode of this form
    ' If true then we only add the files to the database file calling the AttachFiles function (for new requests)
    ' If false then we add file to the database immediately after they are selected (for existing requests)
    AddedFiles.ClearList
    lstFiles.Clear
    FileAddMode = AddMode
End Function

Private Sub cmdClose_Click()
    frmRequestAttach.Hide
End Sub

Public Function AttachFiles(RequestID As String)
    ' This is for add mode only, if we are only adding files to a new request
    ' This function goes through the list of added files and actually adds them to the database
    
    Dim Row As Integer
    
    Dim FileTitle As String
    Dim FilePath As String
    
    For Row = 1 To AddedFiles.GetSize
        FileTitle = CMF.GetDataPart(AddedFiles.GetItem(Row), 1, "|")
        FilePath = CMF.GetDataPart(AddedFiles.GetItem(Row), 2, "|")
        
        Call Main.AddRequestAttachment(RequestID, FileTitle, FilePath)
    Next
End Function

Public Function AttachFileNow(RequestID As String, FileTitle As String, FilePath As String)
    Call Main.AddRequestAttachment(RequestID, FileTitle, FilePath)
    Call Main.UpdateRequestDateStamp(RequestID)
End Function

Private Sub cmdRemoveFile_Click()
    Dim FileToRemove As String
    Dim CurrentCheckFile As String
    Dim NewAddedFiles As New DynList
    Dim Row As Integer
    
    If FileAddMode = True Then
        If lstFiles.ListCount > 0 Then
            If lstFiles.ListIndex <> -1 Then 'If the user has selected something
                FileToRemove = lstFiles.List(lstFiles.ListIndex)
                
                ' Cycle through the added files list, and add all the ones to a new list that don't match the file to remove
                For Row = 1 To AddedFiles.GetSize
                    CurrentCheckFile = CMF.GetDataPart(AddedFiles.GetItem(Row), 1, "|")
                    
                    If FileToRemove <> CurrentCheckFile Then
                        NewAddedFiles.AddItem (AddedFiles.GetItem(Row))
                    End If
                Next
                
                ' Now clear the public list, and copy the local one to it
                AddedFiles.ClearList
                For Row = 1 To NewAddedFiles.GetSize
                    AddedFiles.AddItem (NewAddedFiles.GetItem(Row))
                Next
                
                ' Remove the item from the list
                lstFiles.RemoveItem (lstFiles.ListIndex)
            End If
        End If
    Else
        If lstFiles.ListCount > 0 Then
            If lstFiles.ListIndex <> -1 Then 'If the user has selected something
                FileToRemove = lstFiles.List(lstFiles.ListIndex)
                If MsgBox("Are you sure you wish to remove the file " + FileToRemove + " from this Request?", vbYesNo + vbQuestion, "Cede SupportDesk") = vbYes Then
                    Call Main.DeleteRequestAttachment(ReqID, FileToRemove)
                    Call Main.UpdateRequestDateStamp(ReqID)
                    PopulateRequestAttachments (ReqID)
                End If
            End If
        End If
    End If
End Sub

Private Sub cmdSaveFile_Click()
    
    If lstFiles.ListCount > 0 Then
        If FileAddMode = False Then ' We only want to be able to do this for existing requests
        '    ActualPath = FileAttachments.GetElement(2, lstFiles.ListIndex + 1)
        '    If Dir(ActualPath) <> "" Then
        '        ShellExecute Me.hWnd, "", ActualPath, "", "", 1
        '    Else
        '        MsgBox "File attachment has been removed from the file store!", vbExclamation, "Cede SupportDesk"
        '    End If
            'Call Main.OpenRichAttachment(FileAttachments.GetElement(2, lstFiles.ListIndex + 1), FileAttachments.GetElement(1, lstFiles.ListIndex + 1))
            CommonDialog1.DialogTitle = "Save Request Attachment..."
            CommonDialog1.FileName = FileAttachments.GetElement(1, lstFiles.ListIndex + 1)
            CommonDialog1.Filter = "All Files (*.*)|*.*"
            CommonDialog1.ShowSave
            If Trim(CommonDialog1.FileName) <> "" Then
                Call Main.SaveRichAttachment(FileAttachments.GetElement(2, lstFiles.ListIndex + 1), CommonDialog1.FileName)
            End If
        End If
    End If
    
End Sub

Private Sub Form_Activate()
    SetColours
End Sub

Private Sub lstFiles_DblClick()
    Dim ActualPath As String
    
    If lstFiles.ListCount > 0 Then
        If FileAddMode = False Then ' We only want to be able to do this for existing requests
        '    ActualPath = FileAttachments.GetElement(2, lstFiles.ListIndex + 1)
        '    If Dir(ActualPath) <> "" Then
        '        ShellExecute Me.hWnd, "", ActualPath, "", "", 1
        '    Else
        '        MsgBox "File attachment has been removed from the file store!", vbExclamation, "Cede SupportDesk"
        '    End If
            Call Main.OpenRichAttachment(FileAttachments.GetElement(2, lstFiles.ListIndex + 1), FileAttachments.GetElement(1, lstFiles.ListIndex + 1))
        End If
    End If
    
End Sub

Public Function SetColours()
    lblFiles.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblFiles.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function
