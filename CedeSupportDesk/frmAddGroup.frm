VERSION 5.00
Begin VB.Form frmAddGroup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Group"
   ClientHeight    =   3855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4515
   Icon            =   "frmAddGroup.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   4515
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3000
      TabIndex        =   2
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Top             =   3360
      Width           =   1515
   End
   Begin VB.ListBox lstGroups 
      Appearance      =   0  'Flat
      Height          =   2370
      Left            =   60
      TabIndex        =   0
      Top             =   900
      Width           =   4395
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Add Group"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2700
      TabIndex        =   4
      Top             =   120
      Width           =   2355
   End
   Begin VB.Line Line1 
      X1              =   2580
      X2              =   60
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblGroups 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Available Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   3
      Top             =   660
      Width           =   4395
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmAddGroup.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmAddGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SelectedGroup As String

Private Sub cmdCancel_Click()
    SelectedGroup = ""
    frmAddGroup.Hide
End Sub

Private Sub cmdOk_Click()
   SelectedGroup = lstGroups.List(lstGroups.ListIndex)
    If Trim(SelectedGroup) <> "" Then
        frmAddGroup.Hide
    Else
        MsgBox "Please select a group.", vbExclamation, "Cede SupportDesk"
    End If
End Sub

Private Sub Form_Activate()
    Dim CMF As New CommonFunctions
    Dim DynArrayGroups As New DynDatabaseEX
    Call Main.GetGroupDescriptions(DynArrayGroups)
    Call CMF.PopulateListWithDynArray(DynArrayGroups, lstGroups)
    SetColours
End Sub

Public Function SetColours()
    lblGroups.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblGroups.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function
