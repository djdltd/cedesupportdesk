VERSION 5.00
Begin VB.Form frmAppearance 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Appearance Settings"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5565
   Icon            =   "frmAppearance.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   5565
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDefaults 
      Caption         =   "Set Defaults"
      Height          =   435
      Left            =   120
      TabIndex        =   19
      Top             =   5280
      Width           =   1395
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   435
      Left            =   4080
      TabIndex        =   9
      Top             =   5280
      Width           =   1395
   End
   Begin VB.CommandButton cmdUrgentCalls 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   4680
      Width           =   1335
   End
   Begin VB.CommandButton cmdChangedCalls 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   4200
      Width           =   1335
   End
   Begin VB.CommandButton cmdFieldHeadingsFG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3720
      Width           =   1335
   End
   Begin VB.CommandButton cmdFieldHeadingsBG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton cmdPreviewPaneFG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2760
      Width           =   1335
   End
   Begin VB.CommandButton cmdPreviewPaneBG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2280
      Width           =   1335
   End
   Begin VB.CommandButton cmdRequestListFG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1800
      Width           =   1335
   End
   Begin VB.CommandButton cmdRequestListBG 
      Height          =   375
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1320
      Width           =   1335
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H00C0FFFF&
      Caption         =   "Click on the Colour Preview to customise the colour."
      Height          =   195
      Left            =   0
      TabIndex        =   18
      Top             =   840
      Width           =   5535
   End
   Begin VB.Label Label9 
      Caption         =   "Urgent Requests"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   17
      Top             =   4740
      Width           =   5055
   End
   Begin VB.Label Label8 
      Caption         =   "Changed Requests"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   16
      Top             =   4260
      Width           =   5055
   End
   Begin VB.Label Label7 
      Caption         =   "Field Headings - ForeGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   15
      Top             =   3780
      Width           =   5055
   End
   Begin VB.Label Label6 
      Caption         =   "Field Headings - BackGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   14
      Top             =   3300
      Width           =   5055
   End
   Begin VB.Label Label5 
      Caption         =   "Preview Pane - ForeGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   13
      Top             =   2820
      Width           =   5055
   End
   Begin VB.Label Label4 
      Caption         =   "Preview Pane - BackGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   12
      Top             =   2340
      Width           =   5055
   End
   Begin VB.Label Label3 
      Caption         =   "Request List - ForeGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   11
      Top             =   1860
      Width           =   5055
   End
   Begin VB.Label Label2 
      Caption         =   "Request List - BackGround"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1980
      TabIndex        =   10
      Top             =   1380
      Width           =   5055
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Appearance Settings"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2880
      TabIndex        =   0
      Top             =   60
      Width           =   2775
   End
   Begin VB.Line Line1 
      X1              =   2700
      X2              =   240
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Image Image1 
      Height          =   435
      Left            =   -60
      Picture         =   "frmAppearance.frx":0442
      Stretch         =   -1  'True
      Top             =   360
      Width           =   5670
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   -60
      Picture         =   "frmAppearance.frx":1442C
      Top             =   -240
      Width           =   18000
   End
End
Attribute VB_Name = "frmAppearance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Function InitialiseColourSettings()
    cmdRequestListBG.BackColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    cmdRequestListFG.BackColor = AppColours.Get_FrmMain_FGridCalls_ForeGround
    cmdPreviewPaneBG.BackColor = AppColours.Get_FrmMain_txtDetails_BackGround
    cmdPreviewPaneFG.BackColor = AppColours.Get_FrmMain_txtDetails_ForeGround
    cmdFieldHeadingsBG.BackColor = AppColours.Get_FieldHeadings_BackGround
    cmdFieldHeadingsFG.BackColor = AppColours.Get_FieldHeadings_ForeGround
    cmdChangedCalls.BackColor = AppColours.GetChangedCalls
    cmdUrgentCalls.BackColor = AppColours.GetUrgentCalls
End Function

Private Sub cmdChangedCalls_Click()
    frmColourChooser.SetInitialColour (AppColours.GetChangedCalls)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.SetChangedCalls (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdClose_Click()
    frmAppearance.Hide
End Sub

Private Sub cmdDefaults_Click()
    AppColours.SetAllAppDefaultColours
    InitialiseColourSettings
End Sub

Private Sub cmdFieldHeadingsBG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FieldHeadings_BackGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FieldHeadings_BackGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdFieldHeadingsFG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FieldHeadings_ForeGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FieldHeadings_ForeGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdPreviewPaneBG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FrmMain_txtDetails_BackGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FrmMain_txtDetails_BackGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdPreviewPaneFG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FrmMain_txtDetails_ForeGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FrmMain_txtDetails_ForeGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdRequestListBG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FrmMain_FGridCalls_BackGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FrmMain_FGridCalls_BackGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdRequestListFG_Click()
    frmColourChooser.SetInitialColour (AppColours.Get_FrmMain_FGridCalls_ForeGround)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.Set_FrmMain_FGridCalls_ForeGround (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

Private Sub cmdUrgentCalls_Click()
    frmColourChooser.SetInitialColour (AppColours.GetUrgentCalls)
    frmColourChooser.Show vbModal
    If frmColourChooser.GetColourChanged = True Then
        AppColours.SetUrgentCalls (frmColourChooser.GetCurrentColour)
        InitialiseColourSettings
        AppColours.SetAllAppColours
    End If
End Sub

