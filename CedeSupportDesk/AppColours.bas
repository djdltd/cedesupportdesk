Attribute VB_Name = "AppColours"
Option Explicit

' FrmMain FgridCalls BackGround Settings
Dim FrmMain_FGridCalls_BackGround As Long
Const FrmMain_FGridCalls_BackGround_DEFAULT = &HD9E5E7
' FrmMain FGridCalls_ForeGround Settings
Dim FrmMain_FGridCalls_ForeGround As Long
Const FrmMain_FGridCalls_ForeGround_DEFAULT = &H0&

' FrmMain txtDetails BackGround
Dim FrmMain_txtDetails_BackGround As Long
Const FrmMain_txtDetails_BackGround_DEFAULT = &HE0E0E0
' FrmMain txtDetails ForeGround
Dim FrmMain_txtDetails_ForeGround As Long
Const FrmMain_txtDetails_ForeGround_DEFAULT = &H0&

' All FieldHeadings Background
Dim FrmMain_FieldHeadings_BackGround As Long
Const FrmMain_FieldHeadings_BackGround_DEFAULT = &HFFC0C0
' All FieldHeadings ForeGround
Dim FrmMain_FieldHeadings_ForeGround As Long
Const FrmMain_FieldHeadings_ForeGround_DEFAULT = &H0&

' Urgent Calls Colour
Dim UrgentCalls As Long
Const UrgentCalls_DEFAULT = vbRed

' Changed Calls Colour
Dim ChangedCalls As Long
Const ChangedCalls_DEFAULT = vbBlue

Public Function Set_FrmMain_FGridCalls_BackGround(ColourSetting As Long)
    FrmMain_FGridCalls_BackGround = ColourSetting
End Function
Public Function Get_FrmMain_FGridCalls_BackGround() As Long
    Get_FrmMain_FGridCalls_BackGround = FrmMain_FGridCalls_BackGround
End Function
Public Function Set_FrmMain_FGridCalls_ForeGround(ColourSetting As Long)
    FrmMain_FGridCalls_ForeGround = ColourSetting
End Function
Public Function Get_FrmMain_FGridCalls_ForeGround() As Long
    Get_FrmMain_FGridCalls_ForeGround = FrmMain_FGridCalls_ForeGround
End Function

Public Function Set_FrmMain_txtDetails_BackGround(ColourSetting As Long)
    FrmMain_txtDetails_BackGround = ColourSetting
End Function
Public Function Get_FrmMain_txtDetails_BackGround() As Long
    Get_FrmMain_txtDetails_BackGround = FrmMain_txtDetails_BackGround
End Function
Public Function Set_FrmMain_txtDetails_ForeGround(ColourSetting As Long)
    FrmMain_txtDetails_ForeGround = ColourSetting
End Function
Public Function Get_FrmMain_txtDetails_ForeGround() As Long
    Get_FrmMain_txtDetails_ForeGround = FrmMain_txtDetails_ForeGround
End Function

Public Function Set_FieldHeadings_BackGround(ColourSetting As Long)
    FrmMain_FieldHeadings_BackGround = ColourSetting
End Function
Public Function Get_FieldHeadings_BackGround() As Long
    Get_FieldHeadings_BackGround = FrmMain_FieldHeadings_BackGround
End Function
Public Function Set_FieldHeadings_ForeGround(ColourSetting As Long)
    FrmMain_FieldHeadings_ForeGround = ColourSetting
End Function
Public Function Get_FieldHeadings_ForeGround() As Long
    Get_FieldHeadings_ForeGround = FrmMain_FieldHeadings_ForeGround
End Function

Public Function SetUrgentCalls(ColourSetting As Long)
    UrgentCalls = ColourSetting
End Function
Public Function GetUrgentCalls() As Long
    GetUrgentCalls = UrgentCalls
End Function
Public Function SetChangedCalls(ColourSetting As Long)
    ChangedCalls = ColourSetting
End Function
Public Function GetChangedCalls() As Long
    GetChangedCalls = ChangedCalls
End Function


Public Function SetAllAppColours()
    'Call Set_FrmMain_FGridCalls_BackGround(RGB(0, 100, 200))
    'Call Set_FrmMain_FGridCalls_ForeGround(RGB(255, 100, 200))
    'Call Set_FrmMain_txtDetails_BackGround(RGB(100, 0, 0))
    'Call Set_FrmMain_txtDetails_ForeGround(RGB(100, 255, 0))
    'Call Set_FieldHeadings_BackGround(RGB(0, 0, 100))
    'Call Set_FieldHeadings_ForeGround(RGB(0, 255, 0))
    'Call SetUrgentCalls(RGB(255, 255, 100))
    'Call SetChangedCalls(RGB(0, 255, 255))

    frmMain.SetColours
    frmActionDetails.SetColours
    frmActivityDetails.SetColours
    frmAddAction.SetColours
    frmAddActionTemplate.SetColours
    frmAddActivity.SetColours
    frmAddGroup.SetColours
    frmAddQueue.SetColours
    frmAddReqTemplate.SetColours
    frmAssocUserRequests.SetColours
    frmChooseCols.SetColours
    frmContactDetails.SetColours
    frmLocateUser.SetColours
    frmReAssignRequest.SetColours
    frmRemoveQueue.SetColours
    frmRequestAttach.SetColours
    frmRequestDetails.SetColours
    frmRequestLog.SetColours
    frmSearchRequests.SetColours
    frmSelectActionTemplate.SetColours
    frmSelectTemplate.SetColours
    frmSingleRequestDetails.SetColours
    frmWatchedRequests.SetColours
    
    
End Function

Public Function SetAllAppDefaultColours()
    Call Set_FrmMain_FGridCalls_BackGround(FrmMain_FGridCalls_BackGround_DEFAULT)
    Call Set_FrmMain_FGridCalls_ForeGround(FrmMain_FGridCalls_ForeGround_DEFAULT)
    Call Set_FrmMain_txtDetails_BackGround(FrmMain_txtDetails_BackGround_DEFAULT)
    Call Set_FrmMain_txtDetails_ForeGround(FrmMain_txtDetails_ForeGround_DEFAULT)
    Call Set_FieldHeadings_BackGround(FrmMain_FieldHeadings_BackGround_DEFAULT)
    Call Set_FieldHeadings_ForeGround(FrmMain_FieldHeadings_ForeGround_DEFAULT)
    Call SetUrgentCalls(UrgentCalls_DEFAULT)
    Call SetChangedCalls(ChangedCalls_DEFAULT)
    
    frmMain.SetColours
    frmActionDetails.SetColours
    frmActivityDetails.SetColours
    frmAddAction.SetColours
    frmAddActionTemplate.SetColours
    frmAddActivity.SetColours
    frmAddGroup.SetColours
    frmAddQueue.SetColours
    frmAddReqTemplate.SetColours
    frmAssocUserRequests.SetColours
    frmChooseCols.SetColours
    frmContactDetails.SetColours
    frmLocateUser.SetColours
    frmReAssignRequest.SetColours
    frmRemoveQueue.SetColours
    frmRequestAttach.SetColours
    frmRequestDetails.SetColours
    frmRequestLog.SetColours
    frmSearchRequests.SetColours
    frmSelectActionTemplate.SetColours
    frmSelectTemplate.SetColours
    frmSingleRequestDetails.SetColours
    frmWatchedRequests.SetColours
End Function
