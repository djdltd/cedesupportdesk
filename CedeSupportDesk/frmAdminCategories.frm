VERSION 5.00
Begin VB.Form frmAdminCategories 
   Caption         =   "Manage Categories"
   ClientHeight    =   7545
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   3585
   Icon            =   "frmAdminCategories.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7545
   ScaleWidth      =   3585
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdNewCategory 
      Caption         =   "New Category"
      Height          =   315
      Left            =   60
      TabIndex        =   7
      Top             =   7140
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add Category"
      Height          =   315
      Left            =   1920
      TabIndex        =   6
      Top             =   7140
      Width           =   1575
   End
   Begin VB.TextBox txtCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   4
      Top             =   6720
      Width           =   3435
   End
   Begin VB.ListBox lstCategories 
      Height          =   5325
      Left            =   60
      TabIndex        =   2
      Top             =   660
      Width           =   3435
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Category"
      Height          =   315
      Left            =   1920
      TabIndex        =   1
      Top             =   6060
      Width           =   1575
   End
   Begin VB.Label lblCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "New Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   6480
      Width           =   3435
   End
   Begin VB.Label lblCategories 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Existing Categories"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   3
      Top             =   420
      Width           =   3435
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H8000000C&
      Caption         =   "Manage Categories"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14115
   End
End
Attribute VB_Name = "frmAdminCategories"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CMF As New CommonFunctions
Dim EditMode As Boolean
Dim LastCategory As String
Dim FormCaller As String

Private Sub cmdAdd_Click()
    If EditMode = True Then
        If Trim(txtCategory.Text) <> "" Then
            If Main.RichHandler.DoesCategoryExist(txtCategory.Text) = True Then
                MsgBox "This category name already exists.", vbExclamation
            Else
                If Main.RichHandler.UpdateSingleCategory(LastCategory, txtCategory.Text) = False Then
                    MsgBox "There was a problem updating this category.", vbExclamation
                Else
                    Main.RichHandler.BuildIndexedCategoryTable
                    PopulateExistingCategories
                    EditMode = False
                    ToggleEditMode (False)
                End If
            End If
        End If
    Else
        If Trim(txtCategory.Text) <> "" Then
            If Main.RichHandler.DoesCategoryExist(txtCategory.Text) = True Then
                MsgBox "This category name already exists.", vbExclamation
            Else
                If Main.RichHandler.AddSingleCategory(txtCategory.Text) = False Then
                    MsgBox "There was a problem adding this category.", vbExclamation
                Else
                    Main.RichHandler.BuildIndexedCategoryTable
                    PopulateExistingCategories
                    EditMode = False
                    ToggleEditMode (False)
                End If
            End If
        End If
    End If
End Sub

Private Sub cmdNewCategory_Click()
    ToggleEditMode (False)
End Sub

Private Sub cmdRemove_Click()
    Dim SelectedCategory As String
    
    SelectedCategory = lstCategories.List(lstCategories.ListIndex)
    
    If Trim(SelectedCategory) <> "" Then
        
        If Main.RichHandler.IsCategoryInUse(SelectedCategory) = True Then
            MsgBox "Cannot remove this category. It is currently in use by one or more request records. You can rename this category, however if you wish to remove it then all requests that refer to this category will have to be updated.", vbExclamation
            Exit Sub
        Else
            If MsgBox("Remove " + SelectedCategory + "?", vbQuestion + vbYesNo) = vbYes Then
                If Main.RichHandler.RemoveSingleCategory(SelectedCategory) = False Then
                    MsgBox "There was a problem removing this category", vbExclamation
                Else
                    Main.RichHandler.BuildIndexedCategoryTable
                    PopulateExistingCategories
                    EditMode = False
                    ToggleEditMode (False)
                End If
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    PopulateExistingCategories
    EditMode = False
End Sub

Private Function ToggleEditMode(bEditMode As Boolean)
    If bEditMode = True Then
        cmdAdd.Caption = "Update Category"
        lblCategory.Caption = "Update Category"
        txtCategory.SetFocus
        EditMode = True
        cmdNewCategory.Visible = True
    Else
        cmdAdd.Caption = "Add Category"
        lblCategory.Caption = "New Category"
        txtCategory.Text = ""
        txtCategory.SetFocus
        EditMode = False
        cmdNewCategory.Visible = False
    End If
End Function

Public Function PopulateExistingCategories()
    ' Clear and populate the category combo
    Dim CategoryDescriptions As New DynDatabaseEX
    
    lstCategories.Clear
    
    Call Main.GetCategoryDescriptions(CategoryDescriptions)
    Call CMF.PopulateListWithDynArray(CategoryDescriptions, lstCategories)
End Function

Private Sub lstCategories_Click()
    ToggleEditMode (True)
    txtCategory.Text = lstCategories.List(lstCategories.ListIndex)
    LastCategory = lstCategories.List(lstCategories.ListIndex)
End Sub

Public Function SetCaller(CallerName As String)
    FormCaller = CallerName
End Function

Private Sub lstCategories_DblClick()
    If FormCaller = "NEWREQUEST" Then
        frmRequestDetails.PopulateCategory
        frmRequestDetails.cmbCategory.Text = lstCategories.List(lstCategories.ListIndex)
    End If
    
    If FormCaller = "EXISTINGREQUEST" Then
        frmSingleRequestDetails.PopulateCategory
        frmSingleRequestDetails.cmbCategory.Text = lstCategories.List(lstCategories.ListIndex)
    End If
    
    Hide
End Sub
