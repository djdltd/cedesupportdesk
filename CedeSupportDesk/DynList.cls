VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DynList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Rebust dynamic list class, basically capable of storing and resizing as items are added to it.
Dim DynList() As String
Dim DynListSize As Integer
Public Function AddItem(ItemToAdd As String)
    If DynListSize = 0 Then
        ReDim Preserve DynList(1) As String
        DynListSize = 1
        DynList(DynListSize) = ItemToAdd
    Else
        DynListSize = DynListSize + 1
        ReDim Preserve DynList(DynListSize) As String
        DynList(DynListSize) = ItemToAdd
    End If
End Function
Public Function GetSize() As Integer
    GetSize = DynListSize
End Function
Public Function GetItem(ListIndex As Integer) As String
    If ListIndex > 0 And ListIndex <= DynListSize Then
        GetItem = DynList(ListIndex)
    Else
        MsgBox "ListIndex Out Of Range!", vbCritical, "GetItem in DynList"
    End If
End Function
Public Function SetItem(ListIndex As Integer, NewItem As String)
    If ListIndex > 0 And ListIndex <= DynListSize Then
        DynList(ListIndex) = NewItem
    Else
        MsgBox "ListIndex Out Of Range!", vbCritical, "SetItem in DynList"
    End If
End Function
Public Function DoesItemExist(ItemToFind As String, CaseSens As Boolean) As Integer
    Dim Indx As Integer
    DoesItemExist = 0
    For Indx = 1 To DynListSize
        If CaseSens = True Then
            If DynList(Indx) = ItemToFind Then
                DoesItemExist = Indx
                Exit Function
            End If
        Else
            If StrConv(DynList(Indx), vbUpperCase) = StrConv(ItemToFind, vbUpperCase) Then
                DoesItemExist = Indx
                Exit Function
            End If
        End If
        DoEvents
    Next
End Function
Public Function ClearList()
    Erase DynList
    DynListSize = 0
End Function

Public Function RemoveItemNo(ListIndex As Integer)
    Dim NewDynList() As String
    Dim R As Integer
    Dim NewR As Integer
    Dim TempString As String
    
    If ListIndex > 0 And ListIndex <= DynListSize Then
        ReDim NewDynList(DynListSize - 1) As String
        
        NewR = 1
        
        For R = 1 To DynListSize
            If R <> ListIndex Then
                TempString = DynList(R)
                NewDynList(NewR) = TempString
                NewR = NewR + 1
            End If
        Next
        
        ' Now we copy this local new dynlist to the main one
        ReDim DynList(DynListSize - 1) As String
        
        For R = 1 To DynListSize - 1
            TempString = NewDynList(R)
            DynList(R) = TempString
        Next
        
        ' Now we correct the size of the global dynlist
        DynListSize = DynListSize - 1
    Else
        MsgBox "ListIndex Out Of Range!", vbCritical, "RemoveItemNo in DynList"
    End If
End Function

Public Function RemoveItemString(StrToRemove As String)
    ' This is a error vulnerable function!! - If the string does not exist in the list, this function will fail!
    ' The calling function must use DoesItemExist to check if the item to remove actually exists first!!
    
    Dim NewDynList() As String
    Dim R As Integer
    Dim NewR As Integer
    Dim TempString As String
        
    ReDim NewDynList(DynListSize - 1) As String
    
    NewR = 1
    
    For R = 1 To DynListSize
        TempString = DynList(R)
        If TempString <> StrToRemove Then
            NewDynList(NewR) = TempString
            NewR = NewR + 1
        End If
    Next
    
    ' Now we copy this local new dynlist to the main one
    ReDim DynList(DynListSize - 1) As String
    
    For R = 1 To DynListSize - 1
        TempString = NewDynList(R)
        DynList(R) = TempString
    Next
    
    ' Now we correct the size of the global dynlist
    DynListSize = DynListSize - 1
    
End Function
