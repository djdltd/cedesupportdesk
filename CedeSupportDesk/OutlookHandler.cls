VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OutlookHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Class to handle contruction of emails using Microsoft Outlook

Option Explicit

Public Function NewMail(Recipient As String, Subject As String, Body As String)
On Error GoTo err

    'Exit Function ' This is for developer mode!

    Dim objOLApp As Outlook.Application
    Dim NewMailMessage As Outlook.MailItem

    Set objOLApp = New Outlook.Application
    Set NewMailMessage = objOLApp.CreateItem(olMailItem)
    
    ' set the email details
    NewMailMessage.Subject = Subject
    NewMailMessage.To = Recipient
    NewMailMessage.Body = Body
    
    ' Show the email
    NewMailMessage.Display
    Exit Function
err:
    Exit Function
End Function

Public Function NewHTMLMail(Recipient As String, Subject As String, Body As String, SendNow As Boolean)
On Error GoTo err

    'Exit Function ' This is for developer mode!

    Dim objOLApp As Outlook.Application
    Dim NewMailMessage As Outlook.MailItem

    Set objOLApp = New Outlook.Application
    Set NewMailMessage = objOLApp.CreateItem(olMailItem)
    
    ' set the email details
    NewMailMessage.Subject = Subject
    NewMailMessage.To = Recipient
    NewMailMessage.HTMLBody = Body
    
    ' Show the email
    If SendNow = False Then
        NewMailMessage.Display
    Else
        NewMailMessage.Send
    End If
    Exit Function
err:
    Exit Function
End Function

Public Function NewSendMail(Recipient As String, Subject As String, Body As String)
On Error GoTo err

    'Exit Function ' This is for developer mode

    Dim objOLApp As Outlook.Application
    Dim NewMailMessage As Outlook.MailItem

    Set objOLApp = New Outlook.Application
    Set NewMailMessage = objOLApp.CreateItem(olMailItem)
    
    ' set the email details
    NewMailMessage.Subject = Subject
    NewMailMessage.To = Recipient
    NewMailMessage.Body = Body
    
    ' send the mail!
    NewMailMessage.Send
    Exit Function
err:
    Exit Function
    'NewMailMessage.Display
End Function
