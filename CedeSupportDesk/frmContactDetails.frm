VERSION 5.00
Begin VB.Form frmContactDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contact Details"
   ClientHeight    =   6465
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5670
   Icon            =   "frmContactDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   5670
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   435
      Left            =   3420
      TabIndex        =   11
      Top             =   5940
      Width           =   2175
   End
   Begin VB.TextBox txtMobile 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   5280
      Width           =   5475
   End
   Begin VB.TextBox txtTelephone 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   4380
      Width           =   5475
   End
   Begin VB.TextBox txtLocation 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   3480
      Width           =   5475
   End
   Begin VB.TextBox txtDepartment 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   2580
      Width           =   5475
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1680
      Width           =   5475
   End
   Begin VB.Label lblMobile 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Mobile"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   5040
      Width           =   5475
   End
   Begin VB.Label lblTelephone 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Telephone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   4140
      Width           =   5475
   End
   Begin VB.Label lblLocation 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   3240
      Width           =   5475
   End
   Begin VB.Label lblDepartment 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Department"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2340
      Width           =   5475
   End
   Begin VB.Label lblName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1440
      Width           =   5475
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "User Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   0
      Top             =   120
      Width           =   1875
   End
   Begin VB.Line Line1 
      X1              =   3600
      X2              =   300
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmContactDetails.frx":0442
      Top             =   0
      Width           =   18000
   End
   Begin VB.Image Image1 
      Height          =   795
      Left            =   -60
      Picture         =   "frmContactDetails.frx":24516
      Stretch         =   -1  'True
      Top             =   540
      Width           =   5670
   End
End
Attribute VB_Name = "frmContactDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub lblLoggedDate_Click()

End Sub

Private Sub cmdClose_Click()
    frmContactDetails.Hide
End Sub

Public Function SetColours()
    lblName.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblName.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblDepartment.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDepartment.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblLocation.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblLocation.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTelephone.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTelephone.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblMobile.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblMobile.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

