VERSION 5.00
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Options"
   ClientHeight    =   5325
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8130
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5325
   ScaleWidth      =   8130
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmConnectionString 
      Caption         =   "Database Connection String"
      Height          =   1395
      Left            =   2460
      TabIndex        =   10
      Top             =   960
      Width           =   5535
      Begin VB.TextBox txtConnectionString 
         Height          =   795
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   11
         Top             =   360
         Width           =   4995
      End
   End
   Begin VB.CommandButton cmdCASettings 
      Caption         =   "More Settings..."
      Height          =   375
      Left            =   60
      TabIndex        =   9
      Top             =   4800
      Width           =   1335
   End
   Begin VB.Frame Frame4 
      Caption         =   "Notifications"
      Height          =   1455
      Left            =   2460
      TabIndex        =   5
      Top             =   3240
      Width           =   5535
      Begin VB.CheckBox chkEmailPrompt 
         Caption         =   "Ask me if I want to send an Email to Associated User when Raising and Closing Requests."
         Height          =   435
         Left            =   120
         TabIndex        =   8
         Top             =   900
         Width           =   5115
      End
      Begin VB.CheckBox chkNewPopup 
         Caption         =   "Display a New Request Popup for new Requests"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   600
         Width           =   5115
      End
      Begin VB.CheckBox chkReadReceipt 
         Caption         =   "When opening new Requests, send a read receipt to the helpdesk "
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   300
         Width           =   5115
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Username Override"
      Height          =   675
      Left            =   2460
      TabIndex        =   2
      Top             =   2460
      Width           =   5535
      Begin VB.TextBox txtUsername 
         Height          =   285
         Left            =   2040
         TabIndex        =   3
         Top             =   240
         Width           =   2235
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6660
      TabIndex        =   1
      Top             =   4800
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   5220
      TabIndex        =   0
      Top             =   4800
      Width           =   1335
   End
   Begin VB.Label Label6 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Application Settings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   4
      Top             =   120
      Width           =   3075
   End
   Begin VB.Line Line1 
      X1              =   4740
      X2              =   300
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image1 
      Height          =   3795
      Left            =   60
      Picture         =   "frmOptions.frx":0442
      Stretch         =   -1  'True
      Top             =   900
      Width           =   2340
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmOptions.frx":A26C
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
    frmOptions.Hide
End Sub

Private Sub cmdCASettings_Click()
    frmCAOptions.Show vbModal
End Sub

Private Sub cmdOk_Click()

    Main.DatabaseConnectionString = txtConnectionString.Text

    Main.UserOverride = txtUsername.Text
    
    If chkReadReceipt.Value = 1 Then Main.SendReadReceipt = True Else Main.SendReadReceipt = False
    If chkNewPopup.Value = 1 Then Main.DisplayRequestPopup = True Else Main.DisplayRequestPopup = False
    If chkEmailPrompt.Value = 1 Then Main.DisplayEmailPrompt = True Else Main.DisplayEmailPrompt = False
       
    Main.SaveINIFile
    
    frmOptions.Hide
End Sub



