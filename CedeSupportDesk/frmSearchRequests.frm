VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSearchRequests 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Search Database"
   ClientHeight    =   9180
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13470
   Icon            =   "frmSearchRequests.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9180
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   435
      Left            =   11400
      TabIndex        =   18
      Top             =   8640
      Width           =   1995
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8640
      TabIndex        =   16
      Top             =   720
      Width           =   1755
   End
   Begin VB.ComboBox cmbLoggedBy 
      Height          =   315
      Left            =   7620
      Sorted          =   -1  'True
      TabIndex        =   14
      Top             =   1740
      Width           =   2775
   End
   Begin VB.ComboBox cmbSpecialist 
      Height          =   315
      Left            =   3900
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   1740
      Width           =   2775
   End
   Begin VB.ComboBox cmbCategory 
      Height          =   315
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   1740
      Width           =   2775
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      Left            =   7620
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   2580
      Width           =   2775
   End
   Begin VB.ComboBox cmbGroup 
      Height          =   315
      Left            =   3900
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   2580
      Width           =   2775
   End
   Begin VB.ComboBox cmbPriority 
      Height          =   315
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   2580
      Width           =   2775
   End
   Begin VB.TextBox txtRequestID 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   900
      Width           =   2775
   End
   Begin MSFlexGridLib.MSFlexGrid FGridResults 
      Height          =   5475
      Left            =   60
      TabIndex        =   1
      ToolTipText     =   "Double Click to Display Request Details"
      Top             =   3060
      Width           =   13335
      _ExtentX        =   23521
      _ExtentY        =   9657
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16761024
      BackColorFixed  =   16777215
      BackColorBkg    =   16761024
      GridColor       =   16761024
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Image imgExcelExport 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   300
      MouseIcon       =   "frmSearchRequests.frx":0442
      Picture         =   "frmSearchRequests.frx":074C
      ToolTipText     =   "Click to Export the Current Queue to Microsoft Excel"
      Top             =   180
      Width           =   285
   End
   Begin VB.Label lblNumRequests 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "0 Requests Displayed"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   8700
      Width           =   4755
   End
   Begin VB.Line Line2 
      X1              =   10860
      X2              =   4380
      Y1              =   8880
      Y2              =   8880
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      Caption         =   $"frmSearchRequests.frx":0C02
      ForeColor       =   &H80000008&
      Height          =   675
      Left            =   3180
      TabIndex        =   17
      Top             =   660
      Width           =   5235
   End
   Begin VB.Label lblLoggedBy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Logged By"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7620
      TabIndex        =   15
      Top             =   1500
      Width           =   2775
   End
   Begin VB.Label lblSpecialist 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialist"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3900
      TabIndex        =   13
      Top             =   1500
      Width           =   2775
   End
   Begin VB.Label lblCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1500
      Width           =   2775
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7620
      TabIndex        =   9
      Top             =   2340
      Width           =   2775
   End
   Begin VB.Label lblGroup 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Group"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3900
      TabIndex        =   7
      Top             =   2340
      Width           =   2775
   End
   Begin VB.Label lblPriority 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Priority"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2340
      Width           =   2775
   End
   Begin VB.Label lblRequestID 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Request ID"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   2775
   End
   Begin VB.Image Image1 
      Height          =   2250
      Left            =   10560
      Picture         =   "frmSearchRequests.frx":0CCF
      Top             =   660
      Width           =   2850
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Database Search"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10680
      TabIndex        =   0
      Top             =   120
      Width           =   2715
   End
   Begin VB.Line Line1 
      X1              =   10500
      X2              =   180
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmSearchRequests.frx":2C36
      Top             =   0
      Width           =   18000
   End
   Begin VB.Image Image3 
      Height          =   615
      Left            =   -6840
      Picture         =   "frmSearchRequests.frx":26D0A
      Top             =   8580
      Width           =   18000
   End
End
Attribute VB_Name = "frmSearchRequests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim SearchResults As New DynDatabaseEX
Dim SearchTitles As New DynDatabaseEX

Public Function PopulatePriority()
    Dim PriorityDescriptions As New DynDatabaseEX
    
    cmbPriority.Clear
    Call Main.GetPriorityDescriptions(PriorityDescriptions)
    
    Call CMF.PopulateComboWithDynArray(cmbPriority, PriorityDescriptions)
    
    ' Add the default option of none
    cmbPriority.AddItem ("<None>")
    cmbPriority.Text = "<None>"
End Function

Public Function PopulateCategory()
    Dim CategoryDescriptions As New DynDatabaseEX
    
    cmbCategory.Clear
    Call Main.GetCategoryDescriptions(CategoryDescriptions)
    
    Call CMF.PopulateComboWithDynArray(cmbCategory, CategoryDescriptions)
    
    ' Add the default option of none
    cmbCategory.AddItem ("<None>")
    cmbCategory.Text = "<None>"
End Function

Public Function PopulateGroup()
    Dim GroupDescriptions As New DynDatabaseEX
    
    cmbGroup.Clear
    Call Main.GetGroupDescriptions(GroupDescriptions)
    
    Call CMF.PopulateComboWithDynArray(cmbGroup, GroupDescriptions)
    
    ' Add the default option of none
    cmbGroup.AddItem ("<None>")
    cmbGroup.Text = "<None>"
    
End Function

Public Function PopulateStatus()
    Dim StatusDescriptions As New DynDatabaseEX
    
    cmbStatus.Clear
    Call Main.GetStatusDescriptions(StatusDescriptions)
    
    Call CMF.PopulateComboWithDynArray(cmbStatus, StatusDescriptions)
    
    ' Add the default option of none
    cmbStatus.AddItem ("<None>")
    cmbStatus.Text = "<None>"
    
End Function

Public Function PopulateSpecialist()
    Dim Specialists As New DynDatabaseEX
    
    cmbSpecialist.Clear
    Call Main.GetAllSpecialists(Specialists)
    
    Call CMF.PopulateComboWithDynArray(cmbSpecialist, Specialists)
    
    ' Add the default option of none
    cmbSpecialist.AddItem ("<None>")
    cmbSpecialist.Text = "<None>"
    
End Function

Public Function PopulateLoggedBy()
    Dim LoggedBy As New DynDatabaseEX
    
    cmbLoggedBy.Clear
    Call Main.GetAllSpecialists(LoggedBy)
    
    Call CMF.PopulateComboWithDynArray(cmbLoggedBy, LoggedBy)
    
    'Add the default option of none
    cmbLoggedBy.AddItem ("<None>")
    cmbLoggedBy.Text = "<None>"
    
End Function

Public Function PopulateForm()
    PopulatePriority
    PopulateGroup
    PopulateStatus
    PopulateCategory
    PopulateSpecialist
    PopulateLoggedBy
End Function

Private Sub cmbCategory_Click()
    ResetRequestField
End Sub

Private Sub cmbGroup_Click()
    ResetRequestField
End Sub

Private Sub cmbLoggedBy_Click()
    ResetRequestField
End Sub

Private Sub cmbPriority_Click()
    ResetRequestField
End Sub

Private Sub cmbSpecialist_Click()
    ResetRequestField
End Sub

Private Sub cmbStatus_Click()
    ResetRequestField
End Sub

Private Sub cmdClose_Click()
    frmSearchRequests.Hide
End Sub

Private Sub cmdSearch_Click()
    Dim QueryFields As New DynList
    Dim QueryValues As New DynList
    
    If Trim(txtRequestID.Text) = "" Then ' The user has NOT specified a request Id
    
        If Trim(cmbPriority.Text) <> "" And Trim(cmbPriority.Text) <> "<None>" Then
            QueryFields.AddItem ("Priority")
            QueryValues.AddItem (cmbPriority.Text)
        End If
        
        If Trim(cmbGroup.Text) <> "" And Trim(cmbGroup.Text) <> "<None>" Then
            QueryFields.AddItem ("Group")
            QueryValues.AddItem (cmbGroup.Text)
        End If
        
        If Trim(cmbStatus.Text) <> "" And Trim(cmbStatus.Text) <> "<None>" Then
            QueryFields.AddItem ("Status")
            QueryValues.AddItem (cmbStatus.Text)
        End If
        
        If Trim(cmbCategory.Text) <> "" And Trim(cmbCategory.Text) <> "<None>" Then
            QueryFields.AddItem ("Category")
            QueryValues.AddItem (cmbCategory.Text)
        End If
        
        If Trim(cmbSpecialist.Text) <> "" And Trim(cmbSpecialist.Text) <> "<None>" Then
            QueryFields.AddItem ("Specialist")
            QueryValues.AddItem (cmbSpecialist.Text)
        End If
        
        If Trim(cmbLoggedBy.Text) <> "" And Trim(cmbLoggedBy.Text) <> "<None>" Then
            QueryFields.AddItem ("LoggedBy")
            QueryValues.AddItem (cmbLoggedBy.Text)
        End If
    
    Else ' The user has specified a request id
            
        QueryFields.AddItem ("Request")
        QueryValues.AddItem (Trim(txtRequestID.Text))
        
    End If
    
    Me.MousePointer = 11
    
    ' Now execute the search query!
    Call Main.SearchRequests(QueryFields, QueryValues, SearchResults, SearchTitles)
    
    ' Now populate the search results grid!
    FGridResults.Cols = 0
    FGridResults.Rows = 0
    FGridResults.Clear
    lblNumRequests.Caption = "0 Requests Displayed"
        
    If SearchResults.GetNumCols > 0 And SearchResults.GetNumRows > 0 Then
        Call CMF.CopyArrayToFGrid(SearchResults, FGridResults)
        ' Populate the titles
        Call CMF.SetFGridTitles(SearchTitles, FGridResults)
        
        ' Set the column sizes
        FGridResults.ColWidth(0) = 960
        FGridResults.ColWidth(1) = 3180
        FGridResults.ColWidth(2) = 1710
        FGridResults.ColWidth(3) = 1200
        FGridResults.ColWidth(4) = 1500
        FGridResults.ColWidth(5) = 2265
        FGridResults.ColWidth(6) = 1065
        FGridResults.ColWidth(7) = 1035
        FGridResults.ColWidth(8) = 1680
        FGridResults.ColWidth(9) = 5415
        FGridResults.ColWidth(10) = 1725
        
        ' Set the status caption
        lblNumRequests.Caption = Trim(Str(SearchResults.GetNumRows)) + " Requests Displayed"
    End If
    
    Me.MousePointer = 0
    
End Sub

Private Sub FGridResults_DblClick()
    Dim SelectedReqID As String
    
    If FGridResults.Rows > 1 And FGridResults.Cols > 1 Then
        Me.MousePointer = 11
        SelectedReqID = Trim(Str(FGridResults.TextMatrix(FGridResults.RowSel, 0)))
    End If
        
    If Trim(SelectedReqID) <> "" Then
        frmSingleRequestDetails.ShowRequestDetails (SelectedReqID)
        frmSingleRequestDetails.Show
        Me.MousePointer = 0
    End If
End Sub

Private Sub Form_Activate()
    SetColours
End Sub

Private Sub imgExcelExport_Click()
    ' Function to export the current queue to excel
    Dim EXH As New ExcelHandler
    Call EXH.ExportToExcel(SearchResults, SearchTitles)
    
End Sub

Private Sub txtRequestID_Change()
    ResetComboBoxes
End Sub

Private Function ResetComboBoxes()
    
    cmbPriority.Text = "<None>"
    cmbStatus.Text = "<None>"
    cmbCategory.Text = "<None>"
    cmbGroup.Text = "<None>"
    cmbSpecialist.Text = "<None>"
    cmbLoggedBy.Text = "<None>"
    
End Function

Private Function ResetRequestField()
    txtRequestID.Text = ""
End Function

Public Function SetColours()
    lblRequestID.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblRequestID.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblCategory.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblCategory.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSpecialist.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSpecialist.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblLoggedBy.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblLoggedBy.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblPriority.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblPriority.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblGroup.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblGroup.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblStatus.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblStatus.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    FGridResults.BackColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridResults.GridColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridResults.BackColorBkg = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridResults.ForeColor = AppColours.Get_FrmMain_FGridCalls_ForeGround
    FGridResults.ForeColorFixed = AppColours.Get_FrmMain_FGridCalls_ForeGround
    
End Function
