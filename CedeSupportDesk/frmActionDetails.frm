VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmActionDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Action Details"
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8805
   Icon            =   "frmActionDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   8805
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbActionType 
      Height          =   315
      Left            =   5940
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   900
      Width           =   2775
   End
   Begin VB.ComboBox cmbActionBy 
      Height          =   315
      Left            =   3000
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   900
      Width           =   2775
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update Action"
      Height          =   375
      Left            =   5340
      TabIndex        =   4
      Top             =   4440
      Width           =   1635
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   7080
      TabIndex        =   3
      Top             =   4440
      Width           =   1635
   End
   Begin VB.TextBox txtActionDetails 
      Appearance      =   0  'Flat
      Height          =   2775
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   1560
      Width           =   8655
   End
   Begin MSComCtl2.DTPicker dtTime 
      Height          =   315
      Left            =   1740
      TabIndex        =   7
      Top             =   900
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562178
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtDate 
      Height          =   315
      Left            =   60
      TabIndex        =   8
      Top             =   900
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562177
      CurrentDate     =   38126
   End
   Begin VB.Label lblActionType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5940
      TabIndex        =   12
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblActionby 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action By"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   11
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1740
      TabIndex        =   10
      Top             =   660
      Width           =   1095
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   9
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblActionDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Action Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   1320
      Width           =   8655
   End
   Begin VB.Line Line1 
      X1              =   6480
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Action Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   6600
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmActionDetails.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmActionDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ActID As String
Dim CMF As New CommonFunctions
Dim ReqID As String

Private Sub cmdClose_Click()
    frmActionDetails.Hide
End Sub

Public Function PopulateActionDetails(ActionType As String, ActionBy As String, ActionDate As String, ActionDetails As String)
    
    ' Now set the datetime control to the one specified in the action date
    SetDTControl (ActionDate)
    
    ' Now populate the specialists
    PopulateSpecialist
        
    'Now populate the action types
    PopulateActionTypes
    
    'Set the action details
    txtActionDetails.Text = ActionDetails
    cmbActionType.Text = ActionType
    cmbActionBy.Text = ActionBy
End Function

Public Function SetActionID(ActionID As String)
    ActID = ActionID
End Function

Public Function SetReqID(RequestID As String)
    ReqID = RequestID
End Function

Private Sub cmdUpdate_Click()

    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    
    If Trim(txtActionDetails.Text) = "" Then
        MsgBox "Please enter some Action Details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActionType.Text) = "" Then
        MsgBox "Please select an Action Type.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActionBy.Text) = "" Then
        MsgBox "Please select a specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Me.MousePointer = 11
    
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
    
    Call Main.UpdateRequestAction(ActID, cmbActionBy.Text, cmbActionType.Text, LoggedDateTime, txtActionDetails.Text)
    Call Main.UpdateRequestDateStamp(ReqID)
    frmActionDetails.Hide
    
    Me.MousePointer = 0
    
    ' Force the parent form to refresh the actions
    frmSingleRequestDetails.ActionAdded
End Sub

Public Function PopulateActionTypes()
    Dim ActionTypes As New DynDatabaseEX
    
    cmbActionType.Clear
    txtActionDetails.Text = ""
    
    Call Main.GetActionTypeDescriptions(ActionTypes)
    Call CMF.PopulateComboWithDynArray(cmbActionType, ActionTypes)
    
    ' Now set the action type to the default of "Investigate"
    cmbActionType.Text = "Investigate"
End Function

Public Function PopulateSpecialist()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    Dim CurrentUser As String
    
    cmbActionBy.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbActionBy, Specialists)
    
    ' Now set the combo box to default to the current specialist
    CurrentUser = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase))
    cmbActionBy.Text = CurrentUser
End Function

Public Function SetDTControl(DateTime As String)
    ' Private function to populate the date and time controls with the date and time specified
    On Error GoTo err
    
    Dim strDate As String
    Dim strTime As String
    Dim strDay As String
    Dim strMonth As String
    Dim strYear As String
    Dim strHour As String
    Dim strMinute As String
    Dim strSecond As String
    
    strDate = Trim(Mid(DateTime, 1, 10))
    strTime = Trim(Mid(DateTime, 12, 8))
    
    strDay = CMF.GetDataPart(strDate, 1, "/")
    strMonth = CMF.GetDataPart(strDate, 2, "/")
    strYear = CMF.GetDataPart(strDate, 3, "/")
    
    strHour = CMF.GetDataPart(strTime, 1, ":")
    strMinute = CMF.GetDataPart(strTime, 2, ":")
    strSecond = CMF.GetDataPart(strTime, 3, ":")
        
    dtTime.Hour = strHour
    dtTime.Minute = strMinute
    dtTime.Second = strSecond
        
    ' Reset the date control back to default
    dtDate.Day = 1
    dtDate.Month = 1
    dtDate.Year = 1980
    
    dtDate.Day = strDay
    dtDate.Month = strMonth
    dtDate.Year = strYear
    
    Exit Function
err:
    MsgBox "There was a problem setting the Date/Time control.", vbExclamation, "Cede SupportDesk Internal Error"
End Function

Public Function SetColours()
    lblDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTime.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTime.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionby.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionby.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionType.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionType.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActionDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

