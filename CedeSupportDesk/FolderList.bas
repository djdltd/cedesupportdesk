Attribute VB_Name = "FolderList"
Option Explicit

Public Function AddSpecialist(Username As String)
    frmMain.treQueues.Nodes.Add , , Username, "Queue for " + Username
    frmMain.treQueues.Nodes.Add Username, tvwChild, Username + "$" + "OPEN", "Open Calls"
    frmMain.treQueues.Nodes.Add Username, tvwChild, Username + "$" + "CLOSED", "Closed Calls"
End Function

Public Function AddUser(Username As String)
On Error GoTo err
    frmMain.treQueues.Nodes.Add , , Username, Username
    frmMain.treQueues.Nodes.Add Username, tvwChild, Username + "$" + "OPEN", "Open Calls"
    frmMain.treQueues.Nodes.Add Username, tvwChild, Username + "$" + "CLOSED", "Closed Calls"
err:
End Function

Public Function AddBranch(BranchKey As String, BranchName As String)
    frmMain.treQueues.Nodes.Add , , BranchKey, BranchName
End Function
Public Function AddBranchNode(BranchKey As String, NodeKey As String, NodeName As String)
    frmMain.treQueues.Nodes.Add BranchKey, tvwChild, NodeKey, NodeName
End Function


Public Function AddRoot(RootKey As String, RootName As String)
    frmMain.treQueues.Nodes.Add , , RootKey, RootName
End Function

Public Function AddNode(ParentKey As String, NodeKey As String, NodeName As String)
    'MsgBox NodeKey
    frmMain.treQueues.Nodes.Add ParentKey, tvwChild, NodeKey, NodeName
    
End Function

Public Function AddUserRoot(RootKey As String, RootName As String)
    frmMain.treUserQueues.Nodes.Add , , RootKey, RootName
End Function

Public Function AddUserNode(ParentKey As String, NodeKey As String, NodeName As String)
    frmMain.treUserQueues.Nodes.Add ParentKey, tvwChild, NodeKey, NodeName
End Function

Public Function AddDRoot(RootKey As String, RootName As String)
    frmMain.treDQueues.Nodes.Add , , RootKey, RootName
End Function

Public Function AddDNode(ParentKey As String, NodeKey As String, NodeName As String)
    frmMain.treDQueues.Nodes.Add ParentKey, tvwChild, NodeKey, NodeName
End Function
