VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRequestLog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Request Log"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "frmRequestLog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   8700
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   7020
      TabIndex        =   3
      Top             =   4080
      Width           =   1635
   End
   Begin MSFlexGridLib.MSFlexGrid FGridLog 
      Height          =   3135
      Left            =   0
      TabIndex        =   1
      Top             =   900
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   0
      Cols            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColorFixed  =   12632256
      ForeColorFixed  =   0
      BackColorBkg    =   16777215
      GridColor       =   16777215
      GridColorFixed  =   16777215
      AllowBigSelection=   0   'False
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Label lblActionDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Request Log Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   660
      Width           =   8655
   End
   Begin VB.Line Line1 
      X1              =   6480
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Request Log"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmRequestLog.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmRequestLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions

Private Sub cmdClose_Click()
    frmRequestLog.Hide
End Sub

Public Function PopulateRequestLog(RequestID As String)
    Dim RequestLog As New DynDatabaseEX
    Dim RequestLogTitles As New DynDatabaseEX
    
    Dim Opt1 As New DynDatabaseEX
    Dim Opt2 As New DynDatabaseEX
    Dim Opt3 As New DynDatabaseEX
    
    FGridLog.Clear
    
    Call Main.GetRequestLog(RequestID, RequestLog)
    
    'DateTime , UserID, Cause, Status, Priority
    
    Call RequestLogTitles.SetSize(1, 5)
    Call RequestLogTitles.SetElement(1, 1, "Date / Time")
    Call RequestLogTitles.SetElement(1, 2, "User ID")
    Call RequestLogTitles.SetElement(1, 3, "Cause")
    Call RequestLogTitles.SetElement(1, 4, "Status")
    Call RequestLogTitles.SetElement(1, 5, "Priority")
    
    Call CMF.SortDynArray(RequestLog, 1, False, "DT", Opt1, Opt2, Opt3)
    
    Call CMF.CopyArrayToFGrid(RequestLog, FGridLog)
    Call CMF.SetFGridTitles(RequestLogTitles, FGridLog)
    
    If RequestLog.GetNumCols = 5 Then
        FGridLog.ColWidth(0) = 1920
        FGridLog.ColWidth(1) = 1245
        FGridLog.ColWidth(2) = 2370
        FGridLog.ColWidth(3) = 960
        FGridLog.ColWidth(4) = 2085
    End If
    
End Function

Public Function SetColours()
    lblActionDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActionDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Load()
    SetColours
End Sub
