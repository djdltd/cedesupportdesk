VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAssocUserRequests 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Associated User Requests"
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11955
   Icon            =   "frmAssocUserRequests.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6285
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbSortMode 
      Height          =   315
      Left            =   7500
      TabIndex        =   5
      Top             =   780
      Width           =   1635
   End
   Begin VB.ComboBox cmbSort 
      Height          =   315
      Left            =   3960
      TabIndex        =   6
      Top             =   780
      Width           =   2235
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   10260
      TabIndex        =   4
      Top             =   5820
      Width           =   1635
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   900
      Width           =   2775
   End
   Begin MSFlexGridLib.MSFlexGrid FGridUserCalls 
      Height          =   4335
      Left            =   60
      TabIndex        =   1
      ToolTipText     =   "Double Click to view Request Details"
      Top             =   1380
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   7646
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16761024
      BackColorFixed  =   16777215
      BackColorBkg    =   16761024
      GridColor       =   16761024
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Label lblSort 
      BackColor       =   &H00808080&
      Caption         =   "Sort by:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3300
      TabIndex        =   8
      Top             =   840
      Width           =   615
   End
   Begin VB.Label lblSortMode 
      BackColor       =   &H00808080&
      Caption         =   "Sort Mode:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   6660
      TabIndex        =   7
      Top             =   840
      Width           =   795
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000C&
      Height          =   315
      Left            =   3120
      TabIndex        =   9
      Top             =   780
      Width           =   6255
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Associated Requests for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5580
      TabIndex        =   0
      Top             =   120
      Width           =   6315
   End
   Begin VB.Line Line1 
      X1              =   5460
      X2              =   360
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmAssocUserRequests.frx":0442
      Top             =   0
      Width           =   18000
   End
   Begin VB.Image Image1 
      Height          =   795
      Left            =   2940
      Picture         =   "frmAssocUserRequests.frx":24516
      Stretch         =   -1  'True
      Top             =   480
      Width           =   8970
   End
End
Attribute VB_Name = "frmAssocUserRequests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim PubUserID As String
Dim UserRequests As New DynDatabaseEX
Dim UserRequestTitles As New DynDatabaseEX

Private Sub cmbSort_Click()
    SortView
End Sub

Private Sub cmbSortMode_Click()
    SortView
End Sub

Private Sub cmbStatus_Click()
    PopulateUserRequests (PubUserID)
End Sub

Private Sub cmdClose_Click()
    frmAssocUserRequests.Hide
End Sub

Public Function PopulateStatus()
    ' Clear and populate the status combo
    Dim StatusDescriptions As New DynDatabaseEX
    
    cmbStatus.Clear
    
    'Call Main.GetStatusDescriptions(StatusDescriptions)
    Call StatusDescriptions.SetSize(1, 2)
    Call StatusDescriptions.SetElement(1, 1, "All Active Requests")
    Call StatusDescriptions.SetElement(1, 2, "All Closed Requests")
    
    Call CMF.PopulateComboWithDynArray(cmbStatus, StatusDescriptions)
    
    ' Set the default status description
    'cmbStatus.Text = "Open"
    cmbStatus.Text = StatusDescriptions.GetElement(1, 1)
End Function

Public Function PopulateUserRequests(UserID As String)


    Dim StatusID As String
    Dim StatusDesc As String

    Call UserRequests.SetSize(0, 0)
    Call UserRequestTitles.SetSize(0, 0)

    If Trim(cmbStatus.Text) = "" Then
        MsgBox "Please select a Status.", vbExclamation, "Cede SupportDesk"
        Exit Function
    End If
    
    ' Set the mouse pointer to busy
    frmAssocUserRequests.MousePointer = 11
    
    ' Make the user id global
    PubUserID = UserID

    'Ammend the title of the form
    lblTitle.Caption = "Associated Requests for " + UserID

    ' Convert the status description into a status id
    'StatusID = Main.GetStatusID(cmbStatus.Text)
    StatusID = "0"
    StatusDesc = cmbStatus.Text
        
    ' Clear the flexgrid
    FGridUserCalls.Cols = 0
    FGridUserCalls.Rows = 0
    FGridUserCalls.Clear
        
    ' Now retrieve the requests from the database
    If StatusDesc = "All Active Requests" Then
        Call Main.GetAssocUserRequests(UserID, StatusID, UserRequests, UserRequestTitles, True, False)
    End If
    
    If StatusDesc = "All Closed Requests" Then
        Call Main.GetAssocUserRequests(UserID, StatusID, UserRequests, UserRequestTitles, False, True)
    End If
    
    ' Now we display the requests
    Call CMF.CopyArrayToFGrid(UserRequests, FGridUserCalls)
    Call CMF.SetFGridTitles(UserRequestTitles, FGridUserCalls)
    
    ' Now have to resize each of the columns
    FGridUserCalls.ColWidth(0) = 960
    FGridUserCalls.ColWidth(1) = 2850
    FGridUserCalls.ColWidth(2) = 1515
    FGridUserCalls.ColWidth(3) = 1095
    FGridUserCalls.ColWidth(4) = 1365
    FGridUserCalls.ColWidth(5) = 1710
    FGridUserCalls.ColWidth(6) = 1140
    FGridUserCalls.ColWidth(7) = 1080
    FGridUserCalls.ColWidth(8) = 1755
    FGridUserCalls.ColWidth(9) = 5955
    FGridUserCalls.ColWidth(10) = 1755
    
    ' Set the mouse pointer back to normal
    frmAssocUserRequests.MousePointer = 0
    
End Function

Private Sub FGridUserCalls_DblClick()
    Dim SelectedReqID As String
    
    If FGridUserCalls.Rows > 1 And FGridUserCalls.Cols > 1 Then
        Me.MousePointer = 11
        SelectedReqID = Trim(Str(FGridUserCalls.TextMatrix(FGridUserCalls.RowSel, 0)))
    End If
    
    If Trim(SelectedReqID) <> "" Then
        frmSingleRequestDetails.ShowRequestDetails (SelectedReqID)
        frmSingleRequestDetails.Show
        Me.MousePointer = 0
    End If
End Sub

Public Function SetColours()
    lblStatus.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblStatus.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    FGridUserCalls.BackColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridUserCalls.GridColor = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridUserCalls.BackColorBkg = AppColours.Get_FrmMain_FGridCalls_BackGround
    FGridUserCalls.ForeColor = AppColours.Get_FrmMain_FGridCalls_ForeGround
    FGridUserCalls.ForeColorFixed = AppColours.Get_FrmMain_FGridCalls_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

Public Function PopulateSortbyCombo()
    Dim SortByFields As New DynList
    Dim Row As Single
    
    frmMain.cmbSort.Clear
    
    SortByFields.AddItem ("<None>")
    
    For Row = 1 To UserRequestTitles.GetNumRows
        SortByFields.AddItem (UserRequestTitles.GetElement(1, Row))
    Next
       
    Call CMF.PopulateComboWithDynList(frmAssocUserRequests.cmbSort, SortByFields)
    
    frmAssocUserRequests.cmbSort.Text = "Logged Date"
End Function

Public Function PopulateSortModeCombo()
    Dim SortModeFields As New DynList
    
    frmMain.cmbSortMode.Clear
    
    SortModeFields.AddItem ("<None>")
    SortModeFields.AddItem (Main.GetAscendingVal)
    SortModeFields.AddItem (Main.GetDescendingVal)
    
    Call CMF.PopulateComboWithDynList(frmAssocUserRequests.cmbSortMode, SortModeFields)
    
    frmAssocUserRequests.cmbSortMode.Text = Main.GetDescendingVal
    
End Function

Public Function SortView()
    Dim OptSort1 As New DynDatabaseEX
    Dim OptSort2 As New DynDatabaseEX
    Dim OptSort3 As New DynDatabaseEX
    
    Dim ColToSort As Single
    Dim AscendingVal As Boolean
    Dim ColType As String
    
    ColToSort = CMF.DoesElementExist(UserRequestTitles, cmbSort.Text, 1, False)
    
    If ColToSort <> 0 Then
        ' We found the column number that we need to sort, so sort it!
        If cmbSortMode.Text = Main.GetAscendingVal Then
            AscendingVal = True
        Else
            AscendingVal = False
        End If
        
        If cmbSort.Text = "Logged Date" Or cmbSort.Text = "DateStamp" Then
            ColType = "DT"
        Else
            ColType = "STR"
        End If
        ' Set the mouse pointer to busy
        frmAssocUserRequests.MousePointer = 11
        
        Call CMF.SortDynArray(UserRequests, ColToSort, AscendingVal, ColType, OptSort1, OptSort2, OptSort3)
    
        ' Now we re-display the requests
        Call CMF.CopyArrayToFGrid(UserRequests, FGridUserCalls)
        Call CMF.SetFGridTitles(UserRequestTitles, FGridUserCalls)
        
        ' Set the mouse pointer back to normal
        frmAssocUserRequests.MousePointer = 0
        
    End If
    
End Function
