VERSION 5.00
Begin VB.Form frmRemoveQueue 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Remove Queue"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5190
   Icon            =   "frmRemoveQueue.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   5190
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   3600
      TabIndex        =   7
      Top             =   5580
      Width           =   1515
   End
   Begin VB.CommandButton cmdRemoveSpecialist 
      Caption         =   "Remove Specialist"
      Height          =   315
      Left            =   60
      TabIndex        =   6
      Top             =   5580
      Width           =   1515
   End
   Begin VB.CommandButton cmdRemoveGroup 
      Caption         =   "Remove Group"
      Height          =   315
      Left            =   60
      TabIndex        =   5
      Top             =   2940
      Width           =   1515
   End
   Begin VB.ListBox lstSpecialists 
      Appearance      =   0  'Flat
      Height          =   1980
      Left            =   60
      TabIndex        =   3
      Top             =   3540
      Width           =   5055
   End
   Begin VB.ListBox lstGroups 
      Appearance      =   0  'Flat
      Height          =   1980
      Left            =   60
      TabIndex        =   1
      Top             =   900
      Width           =   5055
   End
   Begin VB.Label lblSpecialists 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialists in your Folder List"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   3300
      Width           =   5055
   End
   Begin VB.Label lblGroups 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Groups in your Folder List"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   660
      Width           =   5055
   End
   Begin VB.Line Line1 
      X1              =   2640
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Remove Queue"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      TabIndex        =   0
      Top             =   120
      Width           =   2355
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmRemoveQueue.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmRemoveQueue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Private Sub Label1_Click()

End Sub

Public Function PopulateCustomGroups()
    lstGroups.Clear
    
    Call CMF.PopulateListWithDynList(Main.CustomAddedGroups, lstGroups)
End Function

Public Function PopulateCustomSpecialists()
    lstSpecialists.Clear
    
    Call CMF.PopulateListWithDynList(Main.CustomAddedSpecialists, lstSpecialists)
End Function

Private Sub cmdClose_Click()
    frmRemoveQueue.Hide
End Sub

Private Sub cmdRemoveGroup_Click()
    Dim Row As Single
    Dim N As Integer
    Dim GroupsRemaining As New DynDatabaseEX
    Dim GroupToRemove As String
    
    If lstGroups.SelCount > 0 Then
        ' Remove the item from the list
        GroupToRemove = lstGroups.List(lstGroups.ListIndex)
        lstGroups.RemoveItem (lstGroups.ListIndex)
        
        ' Populate a new dynarray with the list
        Call CMF.PopulateDynArrayWithList(GroupsRemaining, lstGroups)
        
        ' Now set the main custom groups with the groups remaining
        Main.CustomAddedGroups.ClearList
        For Row = 1 To GroupsRemaining.GetNumRows
            Main.CustomAddedGroups.AddItem (GroupsRemaining.GetElement(1, Row))
        Next
        
        ' Now remove the group from the tree control
        For N = 1 To frmMain.treQueues.Nodes.Count
            If frmMain.treQueues.Nodes(N).Key = GroupToRemove Then
                frmMain.treQueues.Nodes.Remove (N)
                Exit For
            End If
        Next
        
    End If
End Sub

Private Sub cmdRemoveSpecialist_Click()
    Dim Row As Single
    Dim N As Integer
    Dim SpecialistsRemaining As New DynDatabaseEX
    Dim SpecialistToRemove As String
    
    
    If lstSpecialists.SelCount > 0 Then
        ' Remove the item from the list
        SpecialistToRemove = lstSpecialists.List(lstSpecialists.ListIndex)
        lstSpecialists.RemoveItem (lstSpecialists.ListIndex)
       
        ' Populate a new dynarray with the list
        Call CMF.PopulateDynArrayWithList(SpecialistsRemaining, lstSpecialists)
        
        ' Now set the main custom specialists with the specialists remaining
        Main.CustomAddedSpecialists.ClearList
        For Row = 1 To SpecialistsRemaining.GetNumRows
            Main.CustomAddedSpecialists.AddItem (SpecialistsRemaining.GetElement(1, Row))
        Next
        
        ' Now remove the group from the tree control
        For N = 1 To frmMain.treQueues.Nodes.Count
            If frmMain.treQueues.Nodes(N).Key = SpecialistToRemove Then
                frmMain.treQueues.Nodes.Remove (N)
                Exit For
            End If
        Next
    
    End If
    
End Sub

Public Function SetColours()
    lblGroups.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblGroups.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSpecialists.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSpecialists.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

