VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "WordHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim oApp As Word.Application
Dim oDoc As Word.Document
Dim oSel As Word.Selection
Dim oFont As Word.Font

Public Function InitWord()
    ' This function initialises the word application and creates a new document
        
    Set oApp = CreateObject("Word.Application")
    oApp.Visible = True

    Set oDoc = oApp.Documents.Add
    Set oSel = oApp.Selection
    
    Set oFont = oSel.Font
    
End Function

Public Function SetFontName(FontName As String)
    oFont.Name = FontName
End Function

Public Function SetFontSize(FontSize As String)
    oFont.Size = FontSize
End Function

Public Function SetFontBold(FontBold As Boolean)
    oFont.Bold = FontBold
End Function

Public Function WriteText(TextToWrite As String)
    oSel.TypeText (TextToWrite)
End Function

