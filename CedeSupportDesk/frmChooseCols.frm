VERSION 5.00
Begin VB.Form frmChooseCols 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customise Displayed Columns"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   Icon            =   "frmChooseCols.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   6060
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   4380
      TabIndex        =   7
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton cmdAtoD 
      Caption         =   ">>"
      Height          =   315
      Left            =   2760
      TabIndex        =   6
      Top             =   2340
      Width           =   555
   End
   Begin VB.CommandButton cmdDtoA 
      Caption         =   "<<"
      Height          =   315
      Left            =   2760
      TabIndex        =   5
      Top             =   1920
      Width           =   555
   End
   Begin VB.ListBox lstDisplayed 
      Appearance      =   0  'Flat
      Height          =   3345
      Left            =   3300
      TabIndex        =   2
      Top             =   840
      Width           =   2535
   End
   Begin VB.ListBox lstAvail 
      Appearance      =   0  'Flat
      Height          =   3345
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Width           =   2535
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Columns not Displayed"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   600
      Width           =   2535
   End
   Begin VB.Label lblDisplayed 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Columns Displayed"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3300
      TabIndex        =   3
      Top             =   600
      Width           =   2535
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   0
      Picture         =   "frmChooseCols.frx":0442
      Stretch         =   -1  'True
      Top             =   0
      Width           =   1050
   End
   Begin VB.Line Line1 
      X1              =   2760
      X2              =   1200
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Customise Columns"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   0
      Top             =   120
      Width           =   2895
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmChooseCols.frx":4DD4
      Top             =   -60
      Width           =   18000
   End
End
Attribute VB_Name = "frmChooseCols"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions

Public Function PopulateColumnLists()
    
    ' Determine from the available columns and chosen columns, what the user has not chosen,
    ' and populate the not shown column appropriately
    
    Dim NotShown As New DynList
    Dim AvailCols As New DynDatabaseEX
    Dim CustomCols As New DynDatabaseEX
    Dim Row As Single
    Dim TempElement As String
    
    Call Main.GetAllAvailColumns(AvailCols)
    Call Main.GetAllCustomColumns(CustomCols)
    
    ' Read through all the avail columns, and attempt to find each element in the custom columns.
    ' For each one it could not find, add it to the NotShown List
    
    For Row = 1 To AvailCols.GetNumRows
        TempElement = AvailCols.GetElement(1, Row)
        
        If CMF.GetNameArrayLoc(CustomCols, TempElement) = 0 Then
            NotShown.AddItem (TempElement)
        End If
        
    Next
    
    'Now we have the shown/not shown info that we need to populate the lists
    Call CMF.PopulateListWithDynArray(CustomCols, lstDisplayed)
    Call CMF.PopulateListWithDynList(NotShown, lstAvail)
    
End Function


Private Sub cmdAtoD_Click()
    If lstAvail.SelCount > 0 Then
        lstDisplayed.AddItem (lstAvail.List(lstAvail.ListIndex))
        lstAvail.RemoveItem (lstAvail.ListIndex)
    End If
End Sub

Private Sub cmdClose_Click()
    Dim NewChosenColumns As New DynDatabaseEX
    
    If lstDisplayed.ListCount = 0 Then
        MsgBox "Please put 1 or more items in the displayed columns.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Populate the local list with the list of displayed columns from the lstdisplayed control
    Call CMF.PopulateDynArrayWithList(NewChosenColumns, lstDisplayed)
    
    Call Main.SetCustomColumns(NewChosenColumns)
    Call Main.PopulateSortbyCombo
    
    frmChooseCols.Hide
End Sub

Private Sub cmdDtoA_Click()
    If lstDisplayed.SelCount > 0 Then
        If lstDisplayed.List(lstDisplayed.ListIndex) = Main.GetFTRequestID Then
            MsgBox "Sorry, I need the " + Main.GetFTRequestID + " field displayed for reference.", vbExclamation, "Cede SupportDesk"
            Exit Sub
        End If
        
        lstAvail.AddItem (lstDisplayed.List(lstDisplayed.ListIndex))
        lstDisplayed.RemoveItem (lstDisplayed.ListIndex)
    End If
End Sub

Public Function SetColours()
    Label1.BackColor = AppColours.Get_FieldHeadings_BackGround
    Label1.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblDisplayed.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDisplayed.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    
End Function

Private Sub Form_Activate()
    SetColours
End Sub
