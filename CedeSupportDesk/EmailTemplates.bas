Attribute VB_Name = "EmailTemplates"
Option Explicit
Dim CMF As New CommonFunctions
Public ClosedTemplate As String
Public ClosedTemplateEX As String
Public OpenTemplate As String
Public OpenTemplateEX As String
Public OpenCloseTemplate As String
Public OpenCloseTemplateEX As String

Public Function Initialise()

ClosedTemplate = ""

ClosedTemplateEX = CMF.ReplaceString(ClosedTemplate, "^", Chr(34))

OpenTemplate = ""


OpenTemplateEX = CMF.ReplaceString(OpenTemplate, "^", Chr(34))

OpenCloseTemplate = ""

OpenCloseTemplateEX = CMF.ReplaceString(OpenCloseTemplate, "^", Chr(34))

End Function
