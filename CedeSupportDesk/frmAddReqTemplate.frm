VERSION 5.00
Begin VB.Form frmAddReqTemplate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Request Template"
   ClientHeight    =   2460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6825
   Icon            =   "frmAddReqTemplate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   6825
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5100
      TabIndex        =   4
      Top             =   1980
      Width           =   1635
   End
   Begin VB.CommandButton cmdAddTemplate 
      Caption         =   "Add Template"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   1980
      Width           =   1635
   End
   Begin VB.TextBox txtTemplateComments 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   1560
      Width           =   6675
   End
   Begin VB.TextBox txtTemplateName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   900
      Width           =   6675
   End
   Begin VB.Label lblComments 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template Comments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   6
      Top             =   1320
      Width           =   6675
   End
   Begin VB.Label lblName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Template Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   660
      Width           =   6675
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Add Request Template"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   0
      Top             =   120
      Width           =   3495
   End
   Begin VB.Line Line1 
      X1              =   3060
      X2              =   300
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmAddReqTemplate.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmAddReqTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim StatusDescription As String
Dim PriorityDescription As String
Dim CategoryDescription As String
Dim SummaryDesc As String
Dim DetailsDesc As String
Dim ResolutionDesc As String

Private Sub cmdAddTemplate_Click()
    ' Check if the user entered something
    If Trim(txtTemplateName.Text) = "" Then
        MsgBox "Please enter a template name.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Now add the template
    Call Main.AddRequestTemplate(PriorityDescription, StatusDescription, CategoryDescription, SummaryDesc, DetailsDesc, ResolutionDesc, txtTemplateName.Text, txtTemplateComments.Text)
    
    ' Hide the form
    frmAddReqTemplate.Hide
End Sub

Private Sub cmdCancel_Click()
    frmAddReqTemplate.Hide
End Sub

Public Function SetTemplateDetails(Priority As String, Status As String, Category As String, Summary As String, Details As String, Resolution As String)
    PriorityDescription = Priority
    StatusDescription = Status
    CategoryDescription = Category
    SummaryDesc = Summary
    DetailsDesc = Details
    ResolutionDesc = Resolution
End Function

Public Function SetColours()
    lblName.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblName.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblComments.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblComments.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

