CEDESUPPORTDESK LICENSE AGREEMENT

This license agreement is a legal agreement between you (either an individual or a single entity) and CedeSoft Limited for the software product identified above, which includes computer software and online or electronic documentation ("SOFTWARE PRODUCT" or "SOFTWARE"). By installing, copying, or otherwise using the SOFTWARE PRODUCT, you agree to be bound by the terms of this license agreement. If you do not agree to these terms and conditions, you are not authorized to install this software. This SOFTWARE PRODUCT is licensed not sold.

1. EVALUATION LICENSE

CedeSoft Limited grants you a limited, non-exclusive and non-transferable evaluation license which confers the right to install one copy of the unregistered, evaluation version of the SOFTWARE PRODUCT, solely for use at one location, for evaluation purposes only; the period of evaluation shall be as determined at the point of Install from the date of its original installation and use. The SOFTWARE PRODUCT shall not be copied or used for any other purpose. Furthermore, you agree to uninstall the SOFTWARE PRODUCT and destroy or erase all unregistered copies upon the expiration of the evaluation period. Use beyond the evaluation period requires registration.

2. LICENSE

CedeSoft Limited grants every registered user of the SOFTWARE PRODUCT the following rights:

The SOFTWARE PRODUCT may either be used by a single person who uses the software personally on one or more computers, or installed on a single workstation used nonsimultaneously by multiple people, but not both. You may access the registered version of the SOFTWARE PRODUCT through a network, provided that you have obtained individual licenses for the software covering all workstations that will access the software through the network. Each workstation must have its own license for the SOFTWARE PRODUCT, regardless of whether they use the SOFTWARE PRODUCT at different times or concurrently. 

3. RIGHTS & LIMITATIONS

The following rights and limitations apply:

(a) You may not reverse engineer, decompile, or disassemble the SOFTWARE PRODUCT, or create derivative works based on the SOFTWARE or the documentation in whole or in part.
(b) You may not rent or lease the SOFTWARE PRODUCT.
(c) You may not transfer the SOFTWARE PRODUCT or license to another person.,br> (d) You may distribute complete unregistered evaluation copies of the SOFTWARE PRODUCT to third-parties, for the sole purpose of evaluation by them, provided such parties shall agree to be bound by the terms of this Agreement. You may not distribute the SOFTWARE without clearly informing such third-parties that the product has not been registered with CedeSoft Limited and is provided for the limited purpose of evaluation in accordance with the terms of this Agreement to which said third-parties will be bound. Under no circumstances will you use or distribute only a portion of the SOFTWARE; you must provide a full and complete copy of the original software and documentation package.
(e) You may not, for any purpose, distribute copies of registered versions of the software, registration codes, documentation or related materials to any third-parties.

4. TERMINATION.

Without prejudice to any other rights, CedeSoft Limited may terminate this EULA if you fail to comply with the terms and conditions of this license agreement. In such event, you must destroy all copies of the SOFTWARE PRODUCT and all of its component parts.

5. DISCLAIMER OF WARRANTY.

The software product and the accompanying files are provided "as is" and without warranties as to performance of merchantability or any other warranties whether expressed or implied. because of the various hardware and software environments into which the software product may be put, no warranty of fitness for a particular purpose is offered.

Good data processing procedure dictates that any program be thoroughly tested with non-critical data before relying on it. the user must assume the entire risk of using the program. any liability of the seller will be limited exclusively to product replacement or refund of purchase price.

No liability for consequential damages. to the maximum extent permitted by applicable law, in no event shall CedeSoft limited or its suppliers be liable for any special, incidental, indirect, or consequential damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss) arising out of the use of or inability to use the software product, even if CedeSoft limited has been advised of the possibility of such damages. because some states and jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you.

6. PARTIAL VALIDITY.

If any of the above paragraphs is or becomes invalid, this shall not affect the validity of the rest of this license agreement.