;NSIS Modern User Interface
;Start Menu Folder Selection Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI.nsh"

;--------------------------------
;General

  ;Name and file
  Name "CedeSupportDesk"
  OutFile "SupportDeskSetup.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\CedeSoft\CedeSupportDesk"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\CedeSoft\CedeSupportDesk" ""

;--------------------------------
;Variables

  Var MUI_TEMP
  Var STARTMENU_FOLDER

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "D:\Work\Programming\VB6\CedeSupportDesk\Installer\CSDLicense.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\CedeSoft\CedeSupportDesk" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "CedeSupportDesk" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\CedeSupportDesk.exe"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\CedeSupportDesk.ini"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\CedeSupportDesk.mdb"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\Readme.rtf"

  SetOutPath "$SYSDIR"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\comdlg32.ocx"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\mscomct2.ocx"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\MSCOMCTL.OCX"
  File "D:\Work\Programming\VB6\CedeSupportDesk\Installer\msflxgrd.ocx"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\CedeSoft\CedeSupportDesk" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    SetShellVarContext current
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\CedeSupportDesk.lnk" "$INSTDIR\CedeSupportDesk.exe"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Readme.lnk" "$INSTDIR\Readme.rtf"
    CreateShortCut "$DESKTOP\CedeSupportDesk.lnk" "$INSTDIR\CedeSupportDesk.exe"
  !insertmacro MUI_STARTMENU_WRITE_END

  ; Register the Shell COM Object
  Exec '"regsvr32" /s "$SYSDIR\comdlg32.ocx"'
  Exec '"regsvr32" /s "$SYSDIR\mscomct2.ocx"'
  Exec '"regsvr32" /s "$SYSDIR\MSCOMCTL.OCX"'
  Exec '"regsvr32" /s "$SYSDIR\msflxgrd.ocx"'

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "CedeSupportDesk - Manage your helpdesk the smarter way."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  Delete "$INSTDIR\CedeSupportDesk.exe"
  Delete "$INSTDIR\CedeSupportDesk.ini"
  Delete "$INSTDIR\CedeSupportDesk.mdb"
  Delete "$INSTDIR\Readme.rtf"
  
  RMDir "$INSTDIR"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
SetShellVarContext current

  Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\CedeSupportDesk.lnk"
  Delete "$SMSTARTUP\CedeSupportDesk.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\Readme.lnk"
  Delete "$DESKTOP\CedeSupportDesk.lnk"
  
  ;Delete empty start menu parent diretories
  StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"
 
  startMenuDeleteLoop:
	ClearErrors
    RMDir $MUI_TEMP
    GetFullPathName $MUI_TEMP "$MUI_TEMP\.."
    
    IfErrors startMenuDeleteLoopDone
  
    StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
  startMenuDeleteLoopDone:

  DeleteRegKey /ifempty HKCU "Software\CedeSoft\CedeSupportDesk"

SectionEnd