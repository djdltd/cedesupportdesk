VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmActivityDetails 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Activity Details"
   ClientHeight    =   4905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11730
   Icon            =   "frmActivityDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   11730
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   10020
      TabIndex        =   14
      Top             =   4440
      Width           =   1635
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update Activity"
      Height          =   375
      Left            =   8280
      TabIndex        =   13
      Top             =   4440
      Width           =   1635
   End
   Begin VB.TextBox txtAssignedBy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   8880
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   900
      Width           =   2775
   End
   Begin VB.TextBox txtActivityDetails 
      Appearance      =   0  'Flat
      Height          =   2775
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1560
      Width           =   11595
   End
   Begin VB.ComboBox cmbActivityFor 
      Height          =   315
      Left            =   3000
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   900
      Width           =   2775
   End
   Begin VB.ComboBox cmbActivityType 
      Height          =   315
      Left            =   5940
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   900
      Width           =   2775
   End
   Begin MSComCtl2.DTPicker dtTime 
      Height          =   315
      Left            =   1740
      TabIndex        =   3
      Top             =   900
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562178
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtDate 
      Height          =   315
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48562177
      CurrentDate     =   38126
   End
   Begin VB.Label lblAssignedBy 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Assigned By"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8880
      TabIndex        =   11
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Activity Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9360
      TabIndex        =   10
      Top             =   120
      Width           =   2295
   End
   Begin VB.Line Line1 
      X1              =   9180
      X2              =   180
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label lblActivityDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   9
      Top             =   1320
      Width           =   11595
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   8
      Top             =   660
      Width           =   1695
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1740
      TabIndex        =   7
      Top             =   660
      Width           =   1095
   End
   Begin VB.Label lblActivityFor 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity For"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   6
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblActivityType 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Activity Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5940
      TabIndex        =   5
      Top             =   660
      Width           =   2775
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmActivityDetails.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmActivityDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim ActID As String
Dim ReqID As String

Private Sub cmdClose_Click()
    frmActivityDetails.Hide
End Sub

Private Sub Label1_Click()

End Sub

Public Function SetActivityID(ActivityID As String)
    ActID = ActivityID
End Function

Public Function PopulateActivityDetails(ActivityType As String, ActivityFor As String, ActivityDate As String, ActivityDetails As String, AssignedBy As String)
    
    ' Now set the datetime control to the one specified in the action date
    SetDTControl (ActivityDate)
    
    ' Now populate the specialists
    PopulateActivityFor
    
    'Now populate the activity types
    PopulateActivityTypes
    
    'Set the action details
    txtActivityDetails.Text = ActivityDetails
    cmbActivityType.Text = ActivityType
    cmbActivityFor.Text = ActivityFor
    txtAssignedBy.Text = AssignedBy
    
End Function

Public Function PopulateActivityTypes()
    Dim ActionTypes As New DynDatabaseEX
    
    cmbActivityType.Clear
    txtActivityDetails.Text = ""
    
    Call Main.GetActionTypeDescriptions(ActionTypes)
    Call CMF.PopulateComboWithDynArray(cmbActivityType, ActionTypes)
    
    ' Now set the action type to the default of "Investigate"
    cmbActivityType.Text = "Investigate"
End Function

Public Function PopulateActivityFor()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    Dim CurrentUser As String
    
    cmbActivityFor.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbActivityFor, Specialists)
    
    ' Now set the combo box to default to the current specialist
    CurrentUser = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase))
    cmbActivityFor.Text = CurrentUser
End Function

Public Function SetDTControl(DateTime As String)
    ' Private function to populate the date and time controls with the date and time specified
    On Error GoTo err
    Dim strDate As String
    Dim strTime As String
    Dim strDay As String
    Dim strMonth As String
    Dim strYear As String
    Dim strHour As String
    Dim strMinute As String
    Dim strSecond As String
    
    strDate = Trim(Mid(DateTime, 1, 10))
    strTime = Trim(Mid(DateTime, 12, 8))
    
    strDay = CMF.GetDataPart(strDate, 1, "/")
    strMonth = CMF.GetDataPart(strDate, 2, "/")
    strYear = CMF.GetDataPart(strDate, 3, "/")
    
    strHour = CMF.GetDataPart(strTime, 1, ":")
    strMinute = CMF.GetDataPart(strTime, 2, ":")
    strSecond = CMF.GetDataPart(strTime, 3, ":")
        
    dtTime.Hour = strHour
    dtTime.Minute = strMinute
    dtTime.Second = strSecond
    
    ' Reset the date control back to default
    dtDate.Day = 1
    dtDate.Month = 1
    dtDate.Year = 1980
    
    dtDate.Month = strMonth
    dtDate.Day = strDay
    dtDate.Year = strYear
    
    Exit Function
err:
    MsgBox "There was a problem setting the Date/Time control.", vbExclamation, "Cede SupportDesk Internal Error"
    
End Function

Public Function SetReqID(RequestID As String)
    ReqID = RequestID
End Function

Private Sub cmdUpdate_Click()
    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    
    If Trim(txtActivityDetails.Text) = "" Then
        MsgBox "Please enter some Activity Details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActivityFor.Text) = "" Then
        MsgBox "Please select an Activity For specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbActivityType.Text) = "" Then
        MsgBox "Please select an Activity Type.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Me.MousePointer = 11
    
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
    
    Call Main.UpdateRequestActivity(ActID, cmbActivityType.Text, LoggedDateTime, cmbActivityFor.Text, txtActivityDetails.Text)
    Call Main.UpdateRequestDateStamp(ReqID)
    frmActivityDetails.Hide
    
    Me.MousePointer = 0
    
    ' Force the parent form to refresh the activities
    frmSingleRequestDetails.ActivityAdded
End Sub

Public Function SetColours()
    lblDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTime.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTime.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActivityFor.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActivityFor.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActivityType.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActivityType.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblAssignedBy.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblAssignedBy.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblActivityDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblActivityDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

