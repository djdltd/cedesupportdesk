VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelectTemplate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Select Template"
   ClientHeight    =   3810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11835
   Icon            =   "frmSelectTemplate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3810
   ScaleWidth      =   11835
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRenameTemplate 
      Caption         =   "Rename Template"
      Height          =   375
      Left            =   1800
      TabIndex        =   6
      Top             =   3360
      Width           =   1635
   End
   Begin VB.CommandButton cmdDeleteTemplate 
      Caption         =   "Delete Template"
      Height          =   375
      Left            =   60
      TabIndex        =   5
      Top             =   3360
      Width           =   1635
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   10140
      TabIndex        =   4
      Top             =   3360
      Width           =   1635
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select Template"
      Height          =   375
      Left            =   8400
      TabIndex        =   3
      Top             =   3360
      Width           =   1635
   End
   Begin MSFlexGridLib.MSFlexGrid FGridTemplates 
      Height          =   2355
      Left            =   60
      TabIndex        =   1
      Top             =   900
      Width           =   11715
      _ExtentX        =   20664
      _ExtentY        =   4154
      _Version        =   393216
      Rows            =   0
      Cols            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColorFixed  =   12632256
      ForeColorFixed  =   0
      BackColorBkg    =   16777215
      GridColor       =   16777215
      GridColorFixed  =   16777215
      AllowBigSelection=   0   'False
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Label lblTemplates 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Available Templates"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   660
      Width           =   11715
   End
   Begin VB.Label lblTitle 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Request Templates"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8820
      TabIndex        =   0
      Top             =   120
      Width           =   2955
   End
   Begin VB.Line Line1 
      X1              =   8700
      X2              =   300
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmSelectTemplate.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmSelectTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim Priority As String
Dim Status As String
Dim Category As String
Dim Summary As String
Dim Details As String
Dim Resolution As String
Dim SelectedTemplate As String

Private Sub cmdCancel_Click()
    SelectedTemplate = ""
    frmSelectTemplate.Hide
End Sub

Public Function PopulateAvailTemplates()
    Dim AvailTemplates As New DynDatabaseEX
    Dim TemplateTitles As New DynDatabaseEX
    
    Dim OptionalSort1 As New DynDatabaseEX
    Dim OptionalSort2 As New DynDatabaseEX
    Dim OptionalSort3 As New DynDatabaseEX
    
    Call TemplateTitles.SetSize(1, 2)
    Call TemplateTitles.SetElement(1, 1, "Template Name")
    Call TemplateTitles.SetElement(1, 2, "Template Comments")
    
    FGridTemplates.Clear
    FGridTemplates.Cols = 0
    FGridTemplates.Rows = 0
    
    Call Main.GetAvailableRequestTemplates(AvailTemplates)
    
    If AvailTemplates.GetNumRows > 0 Then
        ' Time to sort the templates list alphabetically by name
        Call CMF.SortDynArray(AvailTemplates, 1, True, "", OptionalSort1, OptionalSort2, OptionalSort3)
    End If
    
    If AvailTemplates.GetNumCols > 0 And AvailTemplates.GetNumRows > 0 Then
        ' Display the templates
        Call CMF.CopyArrayToFGrid(AvailTemplates, FGridTemplates)
        ' Display the column titles
        Call CMF.SetFGridTitles(TemplateTitles, FGridTemplates)
        
        ' Set the column sizes
        FGridTemplates.ColWidth(0) = 4830
        FGridTemplates.ColWidth(1) = 6795
    End If
        
End Function

Private Sub cmdDeleteTemplate_Click()
    Dim TemplateToDelete As String
    
    If FGridTemplates.Cols > 0 And FGridTemplates.Rows > 0 Then
        TemplateToDelete = FGridTemplates.TextMatrix(FGridTemplates.RowSel, 0)
    End If
    
    If Trim(TemplateToDelete) <> "" Then
        If MsgBox("Are you sure you want to delete the template " + TemplateToDelete + "?", vbYesNo, "Cede SupportDesk") = vbYes Then
            Call Main.DeleteRequestTemplate(TemplateToDelete)
            PopulateAvailTemplates
        End If
    End If
End Sub

Private Sub cmdRenameTemplate_Click()
    Dim TemplateToRename As String
    Dim TemplateDescription As String
    
    If FGridTemplates.Cols > 0 And FGridTemplates.Rows > 0 Then
        TemplateToRename = FGridTemplates.TextMatrix(FGridTemplates.RowSel, 0)
        TemplateDescription = FGridTemplates.TextMatrix(FGridTemplates.RowSel, 1)
    End If
    
    If Trim(TemplateToRename) <> "" Then
            Call frmRenameTemplate.SetOldTemplateName(TemplateToRename, "REQUEST")
            frmRenameTemplate.txtTemplateName.Text = TemplateToRename
            frmRenameTemplate.txtTemplateComments.Text = TemplateDescription
            frmRenameTemplate.Show
    End If
End Sub

Private Sub cmdSelect_Click()
    'Priority,Status,Category,Summary,Details,Resolution
    Dim TemplateDetails As New DynDatabaseEX
    
    If FGridTemplates.Cols > 0 And FGridTemplates.Rows > 0 Then
        SelectedTemplate = FGridTemplates.TextMatrix(FGridTemplates.RowSel, 0)
    End If
    
    If Trim(SelectedTemplate) <> "" Then
        Call Main.GetRequestTemplate(SelectedTemplate, TemplateDetails)
    
        If TemplateDetails.GetNumCols > 0 And TemplateDetails.GetNumRows > 0 Then
            Priority = TemplateDetails.GetElement(1, 1)
            Status = TemplateDetails.GetElement(2, 1)
            Category = TemplateDetails.GetElement(3, 1)
            Summary = TemplateDetails.GetElement(4, 1)
            Details = TemplateDetails.GetElement(5, 1)
            Resolution = TemplateDetails.GetElement(6, 1)
        End If
        
        frmSelectTemplate.Hide
        
        If Trim(SelectedTemplate) <> "" Then
            Call frmRequestDetails.SetTemplateDetails(Priority, Status, Category, Summary, Details, Resolution)
            Call frmMain.TriggerNewRequest
        End If
    End If
End Sub
Private Sub FGridTemplates_DblClick()
    Call cmdSelect_Click
End Sub

Public Function GetStatus() As String
    GetStatus = Status
End Function
Public Function GetPriority() As String
    GetPriority = Priority
End Function
Public Function GetCategory() As String
    GetCategory = Category
End Function
Public Function GetSummary() As String
    GetSummary = Summary
End Function
Public Function GetDetails() As String
    GetDetails = Details
End Function
Public Function GetResolution() As String
    GetResolution = Resolution
End Function
Public Function GetSelectedTemplate() As String
    GetSelectedTemplate = SelectedTemplate
End Function

Public Function SetColours()
    lblTemplates.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTemplates.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub Form_Activate()
    SetColours
End Sub

