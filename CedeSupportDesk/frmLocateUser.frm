VERSION 5.00
Begin VB.Form frmLocateUser 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Locate Caller"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8940
   Icon            =   "frmLocateUser.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrNewUsername 
      Enabled         =   0   'False
      Interval        =   200
      Left            =   8160
      Top             =   7320
   End
   Begin VB.CommandButton cmdAddCaller 
      Caption         =   "< Add Caller"
      Height          =   435
      Left            =   4560
      TabIndex        =   25
      Top             =   6780
      Width           =   1215
   End
   Begin VB.TextBox txtNewAddress 
      Appearance      =   0  'Flat
      Height          =   2115
      Left            =   4560
      MultiLine       =   -1  'True
      TabIndex        =   23
      Top             =   4560
      Width           =   4095
   End
   Begin VB.TextBox txtNewLocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6720
      TabIndex        =   20
      Top             =   3240
      Width           =   1935
   End
   Begin VB.TextBox txtNewDepartment 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   4560
      TabIndex        =   18
      Top             =   3240
      Width           =   1935
   End
   Begin VB.TextBox txtNewEmail 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   4560
      TabIndex        =   22
      Top             =   3900
      Width           =   4095
   End
   Begin VB.TextBox txtNewMobile 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6720
      TabIndex        =   15
      Top             =   2580
      Width           =   1935
   End
   Begin VB.TextBox txtNewTelephone 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   4560
      TabIndex        =   13
      Top             =   2580
      Width           =   1935
   End
   Begin VB.TextBox txtNewLastname 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6720
      TabIndex        =   11
      Top             =   1920
      Width           =   1935
   End
   Begin VB.TextBox txtNewFirstname 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   4560
      TabIndex        =   9
      Top             =   1920
      Width           =   1935
   End
   Begin VB.TextBox txtNewUsername 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   315
      Left            =   4560
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1260
      Width           =   4095
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   435
      Left            =   60
      TabIndex        =   5
      Top             =   7320
      Width           =   1215
   End
   Begin VB.ListBox lstUsers 
      Appearance      =   0  'Flat
      Height          =   5685
      Left            =   60
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   1500
      Width           =   4275
   End
   Begin VB.TextBox txtUsername 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   840
      Width           =   4275
   End
   Begin VB.Label lblNewAddress 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   24
      Top             =   4320
      Width           =   4095
   End
   Begin VB.Label lblNewLocation 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   21
      Top             =   3000
      Width           =   1935
   End
   Begin VB.Label lblNewDepartment 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Department"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   19
      Top             =   3000
      Width           =   1935
   End
   Begin VB.Label lblNewEmail 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   17
      Top             =   3660
      Width           =   4095
   End
   Begin VB.Label lblNewMobile 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Mobile"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   16
      Top             =   2340
      Width           =   1935
   End
   Begin VB.Label lblNewTelephone 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Telephone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   14
      Top             =   2340
      Width           =   1935
   End
   Begin VB.Label lblNewLastname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lastname"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   12
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label lblNewFirstname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Firstname"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   10
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label lblNewUsername 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Username"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   8
      Top             =   1020
      Width           =   4095
   End
   Begin VB.Label lblNewCaller 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "New Caller Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   7
      Top             =   600
      Width           =   4095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Existing Callers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   4
      Top             =   1260
      Width           =   4275
   End
   Begin VB.Label lblUsername 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Search by Username"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   1
      Top             =   600
      Width           =   4275
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   0
      Picture         =   "frmLocateUser.frx":0442
      Stretch         =   -1  'True
      Top             =   0
      Width           =   1050
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Caller Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7020
      TabIndex        =   0
      Top             =   60
      Width           =   1875
   End
   Begin VB.Line Line1 
      X1              =   1200
      X2              =   6900
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmLocateUser.frx":4DD4
      Top             =   -120
      Width           =   18000
   End
End
Attribute VB_Name = "frmLocateUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim SelectedUser As String
Dim CMF As New CommonFunctions
Dim FilteredUserList As New DynList
Dim InitUser As String
Dim AutoUsernameCountdown As Integer

Public Function SetInitialSelection(Selection As String)
    InitUser = Selection
End Function

Public Function ClearNewCallerDetails()
    txtNewUsername.Text = ""
    txtNewLastname.Text = ""
    txtNewFirstname.Text = ""
    txtNewTelephone.Text = ""
    txtNewMobile.Text = ""
    txtNewDepartment.Text = ""
    txtNewLocation.Text = ""
    txtNewEmail.Text = ""
    txtNewAddress.Text = ""
End Function

Private Sub cmdAddCaller_Click()
    Dim NumErrorFields As Integer
    NumErrorFields = 0

    NumErrorFields = NumErrorFields + CheckField(txtNewLastname.Text, "Lastname")
    NumErrorFields = NumErrorFields + CheckField(txtNewFirstname.Text, "Firstname")
    NumErrorFields = NumErrorFields + CheckField(txtNewTelephone.Text, "Telephone")
    NumErrorFields = NumErrorFields + CheckField(txtNewMobile.Text, "Mobile")
    NumErrorFields = NumErrorFields + CheckField(txtNewDepartment.Text, "Department")
    NumErrorFields = NumErrorFields + CheckField(txtNewLocation.Text, "Location")
    
    If NumErrorFields > 0 Then Exit Sub
    
    If Main.RichHandler.AddSingleCaller(txtNewUsername.Text, txtNewFirstname.Text, txtNewLastname.Text, txtNewEmail.Text, txtNewMobile.Text, txtNewTelephone.Text, txtNewDepartment.Text, txtNewLocation.Text, txtNewAddress.Text) = False Then
        MsgBox "There was a problem adding the new caller details.", vbExclamation
    Else
        Main.RichHandler.BuildIndexedCIIDTable
        PopulateListWithSelection ("")
        ClearNewCallerDetails
    End If
End Sub

Public Function CheckField(InputString As String, FieldName As String) As Integer
    If Trim(InputString) = "" Then
        MsgBox "Please enter a " + FieldName + ".", vbExclamation
        CheckField = 1
    Else
        CheckField = 0
    End If
End Function


Private Sub cmdCancel_Click()
    SelectedUser = ""
    frmLocateUser.Hide
End Sub

Private Sub Form_Activate()
    SetColours
    txtUsername.Text = ""
    'PopulateListWithSelection ("")
    txtUsername.SetFocus
    
    If Trim(InitUser) <> "" Then
        txtUsername.Text = InitUser
        PopulateListWithSelection (InitUser)
    End If
End Sub

Public Function PopulateListWithSelection(UserNameSearchString As String)
' Function to narrow down the list of user details to the users partly matching what the user has typed
' as the search string

    Dim AssocUserDetails As New DynDatabaseEX
    Dim Row As Single
    Dim SearchString As String
    Dim TempElement As String
    
    FilteredUserList.ClearList
    Call Main.GetAssocUserDetails(AssocUserDetails)
    SearchString = StrConv(UserNameSearchString, vbUpperCase)
    
    If Trim(UserNameSearchString) = "" Then
        For Row = 1 To AssocUserDetails.GetNumRows
            FilteredUserList.AddItem (AssocUserDetails.GetElement(1, Row) + " - " + AssocUserDetails.GetElement(2, Row))
        Next
        
        Call CMF.PopulateListWithDynList(FilteredUserList, lstUsers)
    End If
    
    If Trim(UserNameSearchString) <> "" Then
        For Row = 1 To AssocUserDetails.GetNumRows
            TempElement = AssocUserDetails.GetElement(1, Row)
            
            If Mid(TempElement, 1, Len(UserNameSearchString)) = SearchString Then
                FilteredUserList.AddItem (AssocUserDetails.GetElement(1, Row) + " - " + AssocUserDetails.GetElement(2, Row))
            End If
        Next
        
        Call CMF.PopulateListWithDynList(FilteredUserList, lstUsers)
    End If
End Function

Private Sub Form_Load()
    AutoUsernameCountdown = 10
End Sub

Private Sub lstUsers_DblClick()
    Dim Username As String
    
    Username = Trim(CMF.GetDataPart(lstUsers.Text, 1, "-"))
    SelectedUser = Username
    frmLocateUser.Hide
End Sub

Private Sub lstUsers_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        Call lstUsers_DblClick
    End If
End Sub

Private Sub tmrNewUsername_Timer()
    ' This timer function attempts to autogenerate a username based on the matching usernames
    ' in the database and the firstname and lastname of the entered caller
    Dim ProposedUsername As String
    Dim NumMatchingUsernames As Integer
    
    AutoUsernameCountdown = AutoUsernameCountdown - 1
    
    
    If AutoUsernameCountdown = 0 Then
        tmrNewUsername.Enabled = False
        AutoUsernameCountdown = 10
        
        If Trim(txtNewLastname.Text) <> "" And Trim(txtNewFirstname.Text) <> "" Then
            ProposedUsername = Trim(StrConv(txtNewLastname.Text + Mid(txtNewFirstname.Text, 1, 1), vbUpperCase))
            
            NumMatchingUsernames = Main.RichHandler.CountProposedUsernames(ProposedUsername)
            
            If NumMatchingUsernames = 0 Then
                txtNewUsername.Text = ProposedUsername
            Else
                txtNewUsername.Text = ProposedUsername + Trim(Str(NumMatchingUsernames + 1))
            End If
        End If
    End If
End Sub

Private Sub txtNewFirstname_Change()
    tmrNewUsername.Enabled = True
    AutoUsernameCountdown = 10
End Sub

Private Sub txtNewLastname_Change()
    tmrNewUsername.Enabled = True
    AutoUsernameCountdown = 10
End Sub

Private Sub txtUsername_Change()
    PopulateListWithSelection (txtUsername.Text)
End Sub

Private Sub txtUsername_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim Username As String
        
    If KeyCode = 13 Then
        If FilteredUserList.GetSize = 1 Then
            ' Only one item is remaining in the list, therefore that must be the one!
            ' We need to separate the username from the dash that preceeds it
            Username = Trim(CMF.GetDataPart(FilteredUserList.GetItem(1), 1, "-"))
            SelectedUser = Username
            frmLocateUser.Hide
        End If
    End If
    
    If KeyCode = 40 Then
        lstUsers.SetFocus
    End If
End Sub

Public Function GetSelectedUser() As String
    GetSelectedUser = SelectedUser
End Function

Public Function SetColours()
    Label2.BackColor = AppColours.Get_FieldHeadings_BackGround
    Label2.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblUsername.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblUsername.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewCaller.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewCaller.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewUsername.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewUsername.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewFirstname.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewFirstname.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewLastname.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewLastname.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewTelephone.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewTelephone.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewMobile.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewMobile.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewDepartment.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewDepartment.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewLocation.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewLocation.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewEmail.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewEmail.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblNewAddress.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblNewAddress.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function
