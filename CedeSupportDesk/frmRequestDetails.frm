VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRequestDetails 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create New Request"
   ClientHeight    =   10335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9570
   Icon            =   "frmRequestDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10335
   ScaleWidth      =   9570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAdminCategories 
      Caption         =   "..."
      Height          =   315
      Left            =   9180
      TabIndex        =   47
      Top             =   2220
      Width           =   315
   End
   Begin VB.TextBox txtComputer 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   43
      Top             =   9180
      Width           =   9435
   End
   Begin VB.CheckBox chkLogPerUser 
      Caption         =   "Create New Request per User"
      Height          =   195
      Left            =   3840
      TabIndex        =   30
      Top             =   3300
      Width           =   2775
   End
   Begin VB.CheckBox chkWatch 
      Caption         =   "Keep a watch on this Request"
      Height          =   255
      Left            =   5280
      TabIndex        =   29
      Top             =   9960
      Width           =   2475
   End
   Begin VB.CommandButton cmdSaveShow 
      Caption         =   "Add && Show Request"
      Height          =   435
      Left            =   1560
      TabIndex        =   28
      Top             =   9840
      Width           =   1815
   End
   Begin VB.CommandButton cmdFileAttachments 
      Caption         =   "File Attachments.."
      Height          =   315
      Left            =   3780
      TabIndex        =   27
      Top             =   3600
      Width           =   1455
   End
   Begin VB.CommandButton cmdChooseTemplate 
      Caption         =   "Choose Template"
      Height          =   255
      Left            =   3060
      TabIndex        =   26
      Top             =   180
      Width           =   1695
   End
   Begin VB.CommandButton cmdSaveTemplate 
      Caption         =   "Save as Template"
      Height          =   255
      Left            =   4860
      TabIndex        =   25
      Top             =   180
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker dtTime 
      Height          =   315
      Left            =   8400
      TabIndex        =   24
      Top             =   3540
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48824322
      CurrentDate     =   38126
   End
   Begin MSComCtl2.DTPicker dtDate 
      Height          =   315
      Left            =   6720
      TabIndex        =   23
      Top             =   3540
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   48824321
      CurrentDate     =   38126
   End
   Begin VB.CommandButton cmdSaveClose 
      Caption         =   "Add && Close Call"
      Height          =   435
      Left            =   3480
      TabIndex        =   9
      Top             =   9840
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Add Request"
      Height          =   435
      Left            =   60
      TabIndex        =   10
      Top             =   9840
      Width           =   1395
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   435
      Left            =   8040
      TabIndex        =   11
      Top             =   9840
      Width           =   1455
   End
   Begin VB.TextBox txtResolution 
      Appearance      =   0  'Flat
      Height          =   1815
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   8
      Top             =   7080
      Width           =   9435
   End
   Begin VB.TextBox txtSummary 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   60
      MaxLength       =   50
      TabIndex        =   6
      Top             =   4380
      Width           =   9435
   End
   Begin VB.ComboBox cmbSpecialist 
      Height          =   315
      Left            =   6720
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   2880
      Width           =   2775
   End
   Begin VB.CommandButton cmdAddUsers 
      Caption         =   "Add Callers..."
      Height          =   315
      Left            =   5280
      TabIndex        =   2
      Top             =   3600
      Width           =   1395
   End
   Begin VB.ComboBox cmbCategory 
      Height          =   315
      Left            =   6720
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   2220
      Width           =   2475
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      Left            =   6720
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   1560
      Width           =   2775
   End
   Begin VB.TextBox txtDetails 
      Appearance      =   0  'Flat
      Height          =   1815
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   4980
      Width           =   9435
   End
   Begin VB.ComboBox cmbPriority 
      Height          =   315
      Left            =   6720
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   900
      Width           =   2775
   End
   Begin VB.ListBox lstUsers 
      Appearance      =   0  'Flat
      Height          =   1590
      Left            =   3840
      TabIndex        =   1
      ToolTipText     =   "Double Click to view Associated Requests"
      Top             =   900
      Width           =   2775
   End
   Begin VB.OptionButton optIncident 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Incident"
      Height          =   195
      Left            =   5400
      TabIndex        =   46
      Top             =   2940
      Width           =   915
   End
   Begin VB.OptionButton optChange 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Change"
      Height          =   195
      Left            =   4140
      TabIndex        =   45
      Top             =   2940
      Width           =   915
   End
   Begin VB.Label lblChangeIncident 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Change / Incident"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3840
      TabIndex        =   44
      Top             =   2640
      Width           =   2775
   End
   Begin VB.Shape Shape3 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   315
      Left            =   3840
      Top             =   2880
      Width           =   2775
   End
   Begin VB.Label lblComputer 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Computer Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   42
      Top             =   8940
      Width           =   9435
   End
   Begin VB.Label lblCMob 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2280
      TabIndex        =   41
      Top             =   1380
      Width           =   1515
   End
   Begin VB.Label lblCTel 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   420
      TabIndex        =   40
      Top             =   1380
      Width           =   1455
   End
   Begin VB.Label lblCLoc 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2580
      TabIndex        =   39
      Top             =   1140
      Width           =   1215
   End
   Begin VB.Label lblCDept 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   540
      TabIndex        =   38
      Top             =   1140
      Width           =   1635
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Mob:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1860
      TabIndex        =   36
      Top             =   1380
      Width           =   435
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Tel:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   35
      Top             =   1380
      Width           =   375
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2160
      TabIndex        =   34
      Top             =   1140
      Width           =   435
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Dept:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   33
      Top             =   1140
      Width           =   495
   End
   Begin VB.Label lblUserDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Associated Caller Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   31
      Top             =   660
      Width           =   3735
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8400
      TabIndex        =   22
      Top             =   3300
      Width           =   1095
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   21
      Top             =   3300
      Width           =   1695
   End
   Begin VB.Label lblResolution 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Resolution"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   20
      Top             =   6840
      Width           =   9435
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H80000008&
      BackStyle       =   1  'Opaque
      Height          =   75
      Left            =   0
      Shape           =   4  'Rounded Rectangle
      Top             =   4020
      Width           =   9495
   End
   Begin VB.Label lblSummary 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   19
      Top             =   4140
      Width           =   9435
   End
   Begin VB.Label lblDetails 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   18
      Top             =   4740
      Width           =   9435
   End
   Begin VB.Label lblSpecialist 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Specialist"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   17
      Top             =   2640
      Width           =   2775
   End
   Begin VB.Label lblCategory 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Category"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   16
      Top             =   1980
      Width           =   2775
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   15
      Top             =   1320
      Width           =   2775
   End
   Begin VB.Label lblPriority 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Priority"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6720
      TabIndex        =   14
      Top             =   660
      Width           =   2775
   End
   Begin VB.Label lblUsers 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Associated Caller"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3840
      TabIndex        =   13
      Top             =   660
      Width           =   2775
   End
   Begin VB.Image Image1 
      Height          =   2265
      Left            =   0
      Picture         =   "frmRequestDetails.frx":0442
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   3735
   End
   Begin VB.Line Line1 
      X1              =   6600
      X2              =   180
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Request Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6720
      TabIndex        =   0
      Top             =   60
      Width           =   2775
   End
   Begin VB.Label lblCName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   660
      TabIndex        =   37
      Top             =   900
      Width           =   3135
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Name:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   32
      Top             =   900
      Width           =   615
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmRequestDetails.frx":1ED54
      Top             =   0
      Width           =   18000
   End
   Begin VB.Menu mnuUsers 
      Caption         =   "Users"
      Visible         =   0   'False
      Begin VB.Menu mnuRemove 
         Caption         =   "Remove User"
      End
   End
End
Attribute VB_Name = "frmRequestDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CMF As New CommonFunctions
Dim AssocUsers As New DynList
Public ShowUsers As Boolean
Public ResetForm As Boolean
Dim PriorityTemplate As String
Dim StatusTemplate As String
Dim CategoryTemplate As String
Dim SummaryTemplate As String
Dim DetailsTemplate As String
Dim ResolutionTemplate As String
Dim AddedRequestID As String 'So that if the user chooses, Add and SHow request, we know which request they are talking about!

Private Sub chkLogPerUser_Click()
    If chkLogPerUser.Value = 1 Then
        cmdSaveShow.Enabled = False
        cmdSaveClose.Enabled = False
        chkWatch.Enabled = False
        cmdFileAttachments.Enabled = False
    Else
        cmdSaveShow.Enabled = True
        cmdSaveClose.Enabled = True
        chkWatch.Enabled = True
        cmdFileAttachments.Enabled = True
    End If
End Sub

Private Sub cmbStatus_Click()
    If cmbStatus.Text = "On Hold" And frmOnHoldDetails.ReasonSet = False Then
        frmOnHoldDetails.ClearDetails
        frmOnHoldDetails.SetCallerForm ("NEW")
        frmOnHoldDetails.Show
    End If
End Sub

Public Function HoldReasonSet()
    cmbStatus.Text = "On Hold"
End Function

Public Function HoldReasonCancelled()
    cmbStatus.Text = "Open"
End Function

Private Sub cmdAddUsers_Click()
            
    Me.MousePointer = 11
    frmLocateUser.PopulateListWithSelection ("")
    Me.MousePointer = 0
    frmLocateUser.Show vbModal
    
    If Trim(frmLocateUser.GetSelectedUser) <> "" Then
        AssocUsers.AddItem (frmLocateUser.GetSelectedUser)
        PopulatePCDetails (frmLocateUser.GetSelectedUser)
        Call CMF.PopulateListWithDynList(AssocUsers, lstUsers)
        PopulateContactInfo (frmLocateUser.GetSelectedUser)
        txtSummary.SetFocus
    End If
    
End Sub

Private Sub cmdAdminCategories_Click()
    frmAdminCategories.SetCaller ("NEWREQUEST")
    frmAdminCategories.Show
End Sub

Private Sub cmdCancel_Click()
    frmRequestDetails.Hide
    ClearTemplateDetails
    frmMain.RequestFormShown = False
End Sub

Private Sub cmdChooseTemplate_Click()
    frmSelectTemplate.PopulateAvailTemplates
    frmSelectTemplate.Show vbModal
    If Trim(frmSelectTemplate.GetSelectedTemplate) <> "" Then
        cmbPriority.Text = frmSelectTemplate.GetPriority
        cmbStatus.Text = frmSelectTemplate.GetStatus
        cmbCategory.Text = frmSelectTemplate.GetCategory
        txtSummary.Text = frmSelectTemplate.GetSummary
        txtDetails.Text = frmSelectTemplate.GetDetails
        txtResolution.Text = frmSelectTemplate.GetResolution
    End If
End Sub

Private Sub cmdFileAttachments_Click()
    frmRequestAttach.Show

End Sub

Private Function CheckIfWatched(RequestID As String)
    ' Check if the user has clicked the add request watch option
    If chkWatch.Value = 1 Then
        Main.AddRequestWatch (RequestID)
    End If
    
End Function

Private Sub cmdSave_Click()
    
    Dim Category As String
    Dim Priority As String
    Dim Specialist As String
    Dim Status As String
    Dim Summary As String
    Dim Details As String
    Dim Resolution As String
    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    Dim NewRequestID As String
    Dim bChange As Boolean
    Dim bIncident As Boolean
    
    Dim LoggedBy As String
    Dim Group As String
    
    Dim AssocUsers As New DynDatabaseEX
    Dim SingleAssocUser As New DynDatabaseEX
    Dim Row As Single
    
    Dim ActionType As String
    Dim ActionBy As String
    
    Category = cmbCategory.Text
    Priority = cmbPriority.Text
    Specialist = cmbSpecialist.Text
    Status = cmbStatus.Text
    Summary = txtSummary.Text
    Details = txtDetails.Text
    Resolution = txtResolution.Text
    bChange = optChange.Value
    bIncident = optIncident.Value
    
    
    Call CMF.PopulateDynArrayWithList(AssocUsers, lstUsers)
    
    ' Check if there are associated users, otherwise display an error message and quit the function
    If AssocUsers.GetNumRows = 0 Then
        MsgBox "Please associate 1 or more users with this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Now check if the other fields have been populated
    If Trim(Category) = "" Then
        MsgBox "Please select a category.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Priority) = "" Then
        MsgBox "Please select a priority.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Specialist) = "" Then
        MsgBox "Please select a specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
            
    If Trim(Status) = "" Then
        MsgBox "Please select a status.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Summary) = "" Then
        MsgBox "Please enter a summary for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Details) = "" Then
        MsgBox "Please enter details for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If

    If bChange = False And bIncident = False Then
        MsgBox "Please select either Change or Incident for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If

    Me.MousePointer = 11
    cmdSave.Enabled = False
    cmdSaveShow.Enabled = False
            
    'Okay if we've got here then we've passed all the initial checks, now go ahead and add the call to the database
    Group = Main.GetSpecialistGroupName(Specialist)
    LoggedBy = Main.GetCurrentLoggedOnUser
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
        
    If chkLogPerUser.Value = 0 Then
        
        NewRequestID = Main.AddRequest(bChange, Priority, Group, Status, Category, Specialist, LoggedBy, LoggedDateTime, Summary, Details, Resolution, AssocUsers)
        AddedRequestID = NewRequestID 'Set the global requestid to the new request id - incase the user chooses add and show
        
        ' Inform the main module of the new request (This prevents us getting a popup for this request)
        If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
            Main.AddNewRequestToNew (NewRequestID)
        End If
        
        ' Attach any files that the user may have added
        frmRequestAttach.AttachFiles (NewRequestID)
        
        If Trim(txtComputer.Text) <> "" Then
            ' Add the computer details as a single action for this request
            Call Main.AddRequestAction(NewRequestID, "User/PC Configuration", LoggedDateTime, txtComputer.Text, Main.GetCurrentLoggedOnUser, Status, Priority)
        End If
        
        If Trim(cmbStatus.Text) = "On Hold" Then
            ' Add the on hold details as an action on this request
            Call Main.AddRequestAction(NewRequestID, "Advice", LoggedDateTime, "Reason call placed On Hold: " + vbCrLf + frmOnHoldDetails.GetReason, Main.GetCurrentLoggedOnUser, Status, Priority)
        End If
        
        'Add this request id to the read items list, as we do not want it showing as unread, since we created it! - Only do this if the specialist of the request is the current user
        If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
            Main.AddReadItem (NewRequestID)
        End If
        
        ' Now check if the user has added a watch for this request
        CheckIfWatched (NewRequestID)
    
    Else 'The user has chosen to add a separate request for each associated user
        frmProgress.lblTitle.Caption = "Adding Requests..."
        frmProgress.ProgressBar1.Max = AssocUsers.GetNumRows
        frmProgress.Show
        
        For Row = 1 To AssocUsers.GetNumRows
            Call SingleAssocUser.SetSize(1, 1)
            Call SingleAssocUser.SetElement(1, 1, AssocUsers.GetElement(1, Row))
            
            NewRequestID = Main.AddRequest(bChange, Priority, Group, Status, Category, Specialist, LoggedBy, LoggedDateTime, Summary, Details, Resolution, SingleAssocUser)
            
            ' Inform the main module of the new request
            If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
                Main.AddNewRequestToNew (NewRequestID)
            End If
            
            ' Add the computer details as a single action for this request
            Call Main.AddRequestAction(NewRequestID, "User/PC Configuration", LoggedDateTime, txtComputer.Text, Main.GetCurrentLoggedOnUser, Status, Priority)
            
            If Trim(cmbStatus.Text) = "On Hold" Then
                ' Add the on hold details as an action on this request
                Call Main.AddRequestAction(NewRequestID, "Advice", LoggedDateTime, "Reason call placed On Hold: " + vbCrLf + frmOnHoldDetails.GetReason, Main.GetCurrentLoggedOnUser, Status, Priority)
            End If
            
            'Add this request id to the read items list, as we do not want it showing as unread, since we created it! - Only do this if the specialist of the request is the current user
            If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
                Main.AddReadItem (NewRequestID)
            End If
            frmProgress.ProgressBar1.Value = Row
        Next
        
        frmProgress.Hide
        
    End If
    
    'Now ask the user if they would like to generate an email to the user
    If Main.DisplayEmailPrompt = True Then
        If MsgBox("Would you like to Generate an Email to the Associated Users?", vbYesNo + vbQuestion, "Cede SupportDesk") = vbYes Then
            If CMF.FindString(Status, "Closed", False) <> 0 Then
                Call Main.GenerateOpenClosedCallEmail(NewRequestID, Summary, LoggedDateTime, AssocUsers, False)
            Else
                Call Main.GenerateOpenCallEmail(NewRequestID, Summary, AssocUsers, False)
            End If
        End If
    Else
        If Main.DontEmail = False Then
            If CMF.FindString(Status, "Closed", False) <> 0 Then
                Call Main.GenerateOpenClosedCallEmail(NewRequestID, Summary, LoggedDateTime, AssocUsers, True)
            Else
                Call Main.GenerateOpenCallEmail(NewRequestID, Summary, AssocUsers, True)
            End If
        End If
    End If
    
    frmRequestDetails.Hide
    ClearTemplateDetails
    frmMain.RequestFormShown = False
    
    Me.MousePointer = 0
    cmdSave.Enabled = True
    cmdSaveShow.Enabled = True
    
End Sub

Private Sub cmdSaveClose_Click()

    Dim Category As String
    Dim Priority As String
    Dim Specialist As String
    Dim Status As String
    Dim Summary As String
    Dim Details As String
    Dim Resolution As String
    
    Dim LoggedDate As String
    Dim LoggedTime As String
    Dim LoggedDateTime As String
    Dim NewRequestID As String
    
    Dim LoggedBy As String
    Dim Group As String
    
    Dim bChange As Boolean
    Dim bIncident As Boolean
    
    Dim AssocUsers As New DynDatabaseEX
    
    Category = cmbCategory.Text
    Priority = cmbPriority.Text
    Specialist = cmbSpecialist.Text
    Status = cmbStatus.Text
    Summary = txtSummary.Text
    Details = txtDetails.Text
    Resolution = txtResolution.Text
    bChange = optChange.Value
    bIncident = optIncident.Value
    
    Call CMF.PopulateDynArrayWithList(AssocUsers, lstUsers)
    
    ' Check if there are associated users, otherwise display an error message and quit the function
    If lstUsers.ListCount = 0 Then
        MsgBox "Please associate 1 or more users with this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Now check if the other fields have been populated
    If Trim(Category) = "" Then
        MsgBox "Please select a category.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Priority) = "" Then
        MsgBox "Please select a priority.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Specialist) = "" Then
        MsgBox "Please select a specialist.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
            
    If Trim(Status) = "" Then
        MsgBox "Please select a status.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Summary) = "" Then
        MsgBox "Please enter a summary for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(Details) = "" Then
        MsgBox "Please enter details for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If bChange = False And bIncident = False Then
        MsgBox "Please select either Change or Incident for this request.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Me.MousePointer = 11
    
    'Okay if we've got here then we've passed all the initial checks, now go ahead and add the call to the database
    Group = Main.GetSpecialistGroupName(Specialist)
    LoggedBy = Main.GetCurrentLoggedOnUser
    
    LoggedDate = Trim(Str(dtDate.Day)) + "/" + Trim(Str(dtDate.Month)) + "/" + Trim(Str(dtDate.Year))
    LoggedTime = Trim(Str(dtTime.Hour)) + ":" + Trim(Str(dtTime.Minute)) + ":00"
    LoggedDateTime = Format(LoggedDate, "DD/MM/YYYY") + " " + Format(LoggedTime, "HH:MM:00")
    
    NewRequestID = Main.AddRequest(bChange, Priority, Group, "Closed", Category, Specialist, LoggedBy, LoggedDateTime, Summary, Details, Resolution, AssocUsers)
    
    'Inform the main module of the new request id - This prevents us getting a popup
    If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
        Main.AddNewRequestToNew (NewRequestID)
    End If
    
    ' Attach any files that the user may have added
    frmRequestAttach.AttachFiles (NewRequestID)
    
    'Add this request id to the read items list, as we do not want it showing as unread, since we created it! - Only do this if the specialist of the request is the current user
    If Trim(cmbSpecialist.Text) = Trim(StrConv(Main.GetCurrentLoggedOnUser, vbUpperCase)) Then
        Main.AddReadItem (NewRequestID)
    End If
    
    'Now ask the user if they would like to generate an email to the user
    If Main.DisplayEmailPrompt = True Then
        If MsgBox("Would you like to Generate an Email to the Associated Users?", vbYesNo + vbQuestion, "Cede SupportDesk") = vbYes Then
            Call Main.GenerateOpenClosedCallEmail(NewRequestID, Summary, LoggedDateTime, AssocUsers, False)
        End If
    Else
        If Main.DontEmail = False Then
            Call Main.GenerateOpenClosedCallEmail(NewRequestID, Summary, LoggedDateTime, AssocUsers, True)
        End If
    End If
    
    frmRequestDetails.Hide
    ClearTemplateDetails
    frmMain.RequestFormShown = False
    
    Me.MousePointer = 0
    
End Sub

Private Sub cmdSaveShow_Click()
    ' Add the request, and show the request immediately after - for conveniance.
    Call cmdSave_Click
    Me.MousePointer = 11
    frmSingleRequestDetails.ShowRequestDetails (AddedRequestID)
    frmSingleRequestDetails.Show
    Me.MousePointer = 0
End Sub

Private Sub cmdSaveTemplate_Click()
    ' Initial checks to see if we can add the template
    If Trim(cmbPriority.Text) = "" Then
        MsgBox "Please select a priority.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbStatus.Text) = "" Then
        MsgBox "Please select a status.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(cmbCategory.Text) = "" Then
        MsgBox "Please select a Category.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(txtSummary.Text) = "" Then
        MsgBox "Please enter a summary.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    If Trim(txtDetails.Text) = "" Then
        MsgBox "Please enter some Details.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    ' Now set the templates details, and give these to the add template form
    Call frmAddReqTemplate.SetTemplateDetails(cmbPriority.Text, cmbStatus.Text, cmbCategory.Text, txtSummary.Text, txtDetails.Text, txtResolution.Text)
    
    ' Now display the form
    frmAddReqTemplate.txtTemplateName = ""
    frmAddReqTemplate.txtTemplateComments = ""
    frmAddReqTemplate.Show

End Sub

Private Sub Form_Activate()
    SetColours
    If ResetForm = True Then
        PopulatePriority
        PopulateStatus
        PopulateCategory
        PopulateSpecialist
        PopulateDTControl
    End If
    
    If ShowUsers = True Then
        AssocUsers.ClearList
        ShowUsers = False
        cmdAddUsers_Click
    End If
    
    If ResetForm = True Then
        SetFormDefaults
        
        ' Check and set the form with template values if these are set! - ie if the user has selected a request template
        If Trim(PriorityTemplate) <> "" Then
            cmbPriority.Text = PriorityTemplate
        End If
        
        If Trim(StatusTemplate) <> "" Then
            cmbStatus.Text = StatusTemplate
        End If
        
        If Trim(CategoryTemplate) <> "" Then
            cmbCategory.Text = CategoryTemplate
        End If
        
        If Trim(SummaryTemplate) <> "" Then
            txtSummary.Text = SummaryTemplate
        End If
        
        If Trim(DetailsTemplate) <> "" Then
            txtDetails.Text = DetailsTemplate
        End If
        
        If Trim(ResolutionTemplate) <> "" Then
            txtResolution.Text = ResolutionTemplate
        End If
        
        ' Set the file attachments form to Add mode. This means that when files are added by the user, the files
        ' aren't actually added to the database until the request has been added. ' This also clears the form of any previous data
        frmRequestAttach.SetMode (True)
        
        ' Reset the check box (log per request) and enable the relevant controls
        chkLogPerUser.Value = 0
        cmdSaveShow.Enabled = True
        cmdSave.Enabled = True
        cmdSaveShow.Enabled = True
        cmdSaveClose.Enabled = True
        chkWatch.Enabled = True
        cmdFileAttachments.Enabled = True
        
        ResetForm = False
        frmOnHoldDetails.ReasonSet = False
    End If
End Sub

Public Function SetFormDefaults()
    txtSummary.Text = ""
    txtDetails.Text = ""
    txtResolution.Text = ""
    txtComputer.Text = ""
    
    ' Set the default combo options
    cmbPriority.Text = "Medium - 4 Hours"
    cmbCategory.Text = ""
    cmbStatus.Text = "Open"
    cmbSpecialist.Text = Main.GetCurrentLoggedOnUser
    
End Function

Public Function PopulatePriority()
    ' Clear and Populate the priority combo
    Dim PriorityDescriptions As New DynDatabaseEX
    
    cmbPriority.Clear
    Call Main.GetPriorityDescriptions(PriorityDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbPriority, PriorityDescriptions)
    
End Function

Public Function PopulateStatus()
    ' Clear and populate the status combo
    Dim StatusDescriptions As New DynDatabaseEX
    
    cmbStatus.Clear
    
    Call Main.GetStatusDescriptions(StatusDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbStatus, StatusDescriptions)
    
End Function

Public Function PopulateCategory()
    ' Clear and populate the category combo
    Dim CategoryDescriptions As New DynDatabaseEX
    
    cmbCategory.Clear
    
    Call Main.GetCategoryDescriptions(CategoryDescriptions)
    Call CMF.PopulateComboWithDynArray(cmbCategory, CategoryDescriptions)

End Function

Public Function PopulateSpecialist()
    ' Clear and populate the specialist combo
    Dim Specialists As New DynDatabaseEX
    
    cmbSpecialist.Clear
    
    Call Main.GetAllSpecialists(Specialists)
    Call CMF.PopulateComboWithDynArray(cmbSpecialist, Specialists)
End Function

Private Sub Form_Unload(Cancel As Integer)
    frmMain.RequestFormShown = False
End Sub

Private Function PopulateDTControl()
    ' Private function to populate the date and time controls with the current date and time of the system
    Dim CurrentTime As String
    Dim CurMin As String
    Dim CurHour As String
    
    CurrentTime = Format(Time, "HH:MM:SS")

    CurMin = CMF.GetDataPart(CurrentTime, 2, ":")
    CurHour = CMF.GetDataPart(CurrentTime, 1, ":")

    dtTime.Hour = CurHour
    dtTime.Minute = CurMin
    dtTime.Second = "00"
    
    dtDate.Month = Format(Date, "MM")
    dtDate.Day = Format(Date, "DD")
    dtDate.Year = Format(Date, "YYYY")
End Function

Public Function SetTemplateDetails(Priority As String, Status As String, Category As String, Summary As String, Details As String, Resolution As String)
    ' Function to set this forms template details, when the form is displayed the template details are checked and displayed if the template details
    ' contains something
    PriorityTemplate = Priority
    StatusTemplate = Status
    CategoryTemplate = Category
    SummaryTemplate = Summary
    DetailsTemplate = Details
    ResolutionTemplate = Resolution
End Function

Public Function ClearTemplateDetails()
    PriorityTemplate = ""
    StatusTemplate = ""
    CategoryTemplate = ""
    SummaryTemplate = ""
    DetailsTemplate = ""
    ResolutionTemplate = ""
End Function

Public Function PopulatePCDetails(Username As String)
    Dim PCDetails As New DynDatabaseEX
    Dim ComputerName As String
    Dim OS As String
    Dim COEVersion As String
    Dim Model As String
    Dim CPUSpeed As String
    Dim RAM As String
    
    Dim PCInfo As String
    Dim Row As Single
    
    'ComputerName,OS,COEVersion,Model,CPUSpeed,RAM
    Call Main.GetPCDetails(Username, PCDetails)
    
    If PCDetails.GetNumRows > 0 Then
        If Trim(txtComputer.Text) <> "" Then
            ' If there is something already in the details field, then insert some carriage returns.
            txtComputer.Text = txtComputer.Text + vbCrLf
        End If
    End If
    
    For Row = 1 To PCDetails.GetNumRows
        ComputerName = PCDetails.GetElement(1, Row)
        OS = PCDetails.GetElement(2, Row)
        COEVersion = PCDetails.GetElement(3, Row)
        Model = PCDetails.GetElement(4, Row)
        CPUSpeed = PCDetails.GetElement(5, Row)
        RAM = PCDetails.GetElement(6, Row)
        
        PCInfo = Username + ": " + ComputerName + " - " + Model + " (" + COEVersion + ")" + ", " + CPUSpeed + " CPU, " + RAM + " RAM."
        
        If CMF.FindString(OS, "Windows XP", False) <> 0 Then
            PCInfo = Username + ": " + ComputerName + " - " + Model + " - " + "Windows XP " + "(" + COEVersion + ")" + ", " + CPUSpeed + " CPU, " + RAM + " RAM."
        End If
        
        If CMF.FindString(OS, "Windows NT", False) <> 0 Then
            PCInfo = Username + ": " + ComputerName + " - " + Model + " - " + "Windows NT" + ", " + CPUSpeed + " CPU, " + RAM + " RAM."
        End If
        
        txtComputer.Text = txtComputer.Text + PCInfo
        If Row < PCDetails.GetNumRows Then txtComputer.Text = txtComputer.Text + vbCrLf
    Next
    
End Function

Private Sub lstUsers_Click()
    Dim SelectedUser As String
    Dim DBDetails As New DynDatabaseEX
    
    If lstUsers.SelCount > 0 Then
        If lstUsers.ListIndex <> -1 Then
            SelectedUser = lstUsers.List(lstUsers.ListIndex)
        End If
    End If
    
    PopulateContactInfo (SelectedUser)
End Sub
Public Function PopulateContactInfo(SelectedUser As String)
    Dim DBDetails As New DynDatabaseEX
    
    If Trim(SelectedUser) <> "" Then
        Call Main.GetContactDetails(SelectedUser, DBDetails)
        
        If DBDetails.GetNumRows > 0 And DBDetails.GetNumCols > 0 Then
            ' 1 - FirstName
            ' 2 - LastName
            ' 3 - Department
            ' 4 - Location
            ' 5 - Telephone
            ' 6 - Mobile
            lblCName.Caption = DBDetails.GetElement(1, 1) + " " + DBDetails.GetElement(2, 1) + " (" + Trim(SelectedUser) + ")"
            lblCDept.Caption = DBDetails.GetElement(3, 1)
            lblCLoc.Caption = DBDetails.GetElement(4, 1)
            lblCTel.Caption = DBDetails.GetElement(5, 1)
            lblCMob.Caption = DBDetails.GetElement(6, 1)
        End If
    End If
End Function

Private Sub lstUsers_DblClick()
    Dim SelectedUser As String
    Me.MousePointer = 11
    If lstUsers.SelCount > 0 Then
        If lstUsers.ListIndex <> -1 Then
            SelectedUser = lstUsers.List(lstUsers.ListIndex)
        End If
    End If
    
    ' Now check if there is something in the variable, and if so then display the associated calls dialog.
    If Trim(SelectedUser) <> "" Then
        frmAssocUserRequests.Show
        DoEvents
        frmAssocUserRequests.PopulateStatus
        frmAssocUserRequests.PopulateUserRequests (SelectedUser)
        frmAssocUserRequests.PopulateSortbyCombo
        frmAssocUserRequests.PopulateSortModeCombo
        frmAssocUserRequests.SortView
        Me.MousePointer = 0
    End If
    
End Sub

Public Function SetColours()
    lblUsers.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblUsers.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblPriority.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblPriority.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblStatus.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblStatus.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblCategory.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblCategory.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSpecialist.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSpecialist.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblDate.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblDate.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblTime.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblTime.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblSummary.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblSummary.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lbldetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lbldetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblResolution.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblResolution.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblUserDetails.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblUserDetails.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblComputer.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblComputer.ForeColor = AppColours.Get_FieldHeadings_ForeGround
    lblChangeIncident.BackColor = AppColours.Get_FieldHeadings_BackGround
    lblChangeIncident.ForeColor = AppColours.Get_FieldHeadings_ForeGround
End Function

Private Sub lstUsers_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        PopupMenu mnuUsers
    End If
End Sub

Private Sub mnuRemove_Click()
    Dim SelectedUser As String

    If lstUsers.ListCount = 1 Then
        MsgBox "Cannot Remove the Last Associated User. A Request must have at least 1 Associated User.", vbInformation, "Cede SupportDesk"
        Exit Sub
    End If

    If lstUsers.SelCount > 0 Then
        If lstUsers.ListIndex <> -1 Then
            SelectedUser = lstUsers.List(lstUsers.ListIndex)
            AssocUsers.RemoveItemString (SelectedUser)
            Call CMF.PopulateListWithDynList(AssocUsers, lstUsers)
        End If
    End If
End Sub
