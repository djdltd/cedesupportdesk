Attribute VB_Name = "IdleHandler"
Option Explicit

Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer

Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

Public Type POINTAPI
    X As Long
    Y As Long
    End Type

Public Function CheckIdleState() As Boolean

    Dim kKey As Integer 'Stores Each Key On the keyboard In the For Next Loop
    Dim CurrentMousePos As POINTAPI 'Used To store the current mouse position
    Static OldMousePos As POINTAPI 'Static-keeps the old mouse position
    Static IdleTime As Date 'Stores the Time In a Date variable
    Dim SystemIdle As Boolean 'Stores weather the systme Is idle Or Not
    SystemIdle = True 'Sets the idle value To True

    For kKey = 1 To 256 'steps through Each key On the keyboard it detect If

        If GetAsyncKeyState(kKey) <> 0 Then 'any of the keys have been pressed
            Debug.Print "Key Pressed"
            SystemIdle = False 'Sets the idle value To False
            Exit For 'Exits the For Next Loop so that it will move On To the Next Step
        End If

    Next

    GetCursorPos CurrentMousePos 'Gets the current cursor position And stores it
    If CurrentMousePos.X <> OldMousePos.X Or _
    CurrentMousePos.Y <> OldMousePos.Y Then 'Checks To see If the cursor has moved
    Debug.Print "Mouse Moved"
    SystemIdle = False 'since the last Time it was checked
End If

OldMousePos = CurrentMousePos 'Stores the current mouse position For comparring positons the
'next Time through

If SystemIdle = True Then 'If a key hasn't been pressed And the mouse hasn't moved

    'If DateDiff("s", IdleTime, Now) >= 60 Then 'it sets the Return value To the elapsed Time value
    '    IdleTime = Now 'Resets the Time To check the Next Minute For idle
    '    CheckIdleState = CheckIdleState + 1 'sets the Return value In minutes of being idle
    'End If
    CheckIdleState = True
Else
    'IdleTime = Now 'Sets the New Current Idle Time To check For elapsed Time
    CheckIdleState = False
End If

End Function

