VERSION 5.00
Begin VB.Form frmStats 
   Caption         =   "Database Statistics"
   ClientHeight    =   9270
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8475
   Icon            =   "frmStats.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9270
   ScaleWidth      =   8475
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   6660
      TabIndex        =   28
      Top             =   8760
      Width           =   1575
   End
   Begin VB.Label lblNum23 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3660
      TabIndex        =   51
      Top             =   8400
      Width           =   4575
   End
   Begin VB.Label lblNum22 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3900
      TabIndex        =   50
      Top             =   8100
      Width           =   4335
   End
   Begin VB.Label lblNum21 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3840
      TabIndex        =   49
      Top             =   7800
      Width           =   4395
   End
   Begin VB.Label lblNum20 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3540
      TabIndex        =   48
      Top             =   7500
      Width           =   4695
   End
   Begin VB.Label lblNum19 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3660
      TabIndex        =   47
      Top             =   7200
      Width           =   4575
   End
   Begin VB.Label lblNum18 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3960
      TabIndex        =   46
      Top             =   6900
      Width           =   4275
   End
   Begin VB.Label lblNum17 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3900
      TabIndex        =   45
      Top             =   6600
      Width           =   4335
   End
   Begin VB.Label lblNum16 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3600
      TabIndex        =   44
      Top             =   6300
      Width           =   4635
   End
   Begin VB.Label lblNum15 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3360
      TabIndex        =   43
      Top             =   5760
      Width           =   4875
   End
   Begin VB.Label lblNum14 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3300
      TabIndex        =   42
      Top             =   5460
      Width           =   4935
   End
   Begin VB.Label lblNum13 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   2940
      TabIndex        =   41
      Top             =   5160
      Width           =   5295
   End
   Begin VB.Label lblNum12 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3960
      TabIndex        =   40
      Top             =   4620
      Width           =   4275
   End
   Begin VB.Label lblNum11 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4020
      TabIndex        =   39
      Top             =   4320
      Width           =   4215
   End
   Begin VB.Label lblNum10 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   5220
      TabIndex        =   38
      Top             =   4020
      Width           =   3015
   End
   Begin VB.Label lblNum9 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   5220
      TabIndex        =   37
      Top             =   3720
      Width           =   3015
   End
   Begin VB.Label lblNum8 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   5040
      TabIndex        =   36
      Top             =   3420
      Width           =   3195
   End
   Begin VB.Label lblNum7 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   5100
      TabIndex        =   35
      Top             =   3120
      Width           =   3135
   End
   Begin VB.Label lblNum6 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3540
      TabIndex        =   34
      Top             =   2580
      Width           =   4695
   End
   Begin VB.Label lblNum5 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3600
      TabIndex        =   33
      Top             =   2280
      Width           =   4635
   End
   Begin VB.Label lblNum4 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3480
      TabIndex        =   32
      Top             =   1980
      Width           =   4755
   End
   Begin VB.Label lblNum3 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3540
      TabIndex        =   31
      Top             =   1680
      Width           =   4695
   End
   Begin VB.Label lblNum2 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3180
      TabIndex        =   30
      Top             =   1380
      Width           =   5055
   End
   Begin VB.Label lblNum1 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3240
      TabIndex        =   29
      Top             =   1080
      Width           =   4995
   End
   Begin VB.Label lblUserLoggedTotal 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Logged in Total:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   27
      Top             =   7200
      Width           =   3495
   End
   Begin VB.Label lblUserClosedTotal 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Closed in Total:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   26
      Top             =   8400
      Width           =   3495
   End
   Begin VB.Label lblUserClosedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Closed this Month: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   25
      Top             =   8100
      Width           =   3735
   End
   Begin VB.Label lblUserClosedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Closed this Week: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   24
      Top             =   7800
      Width           =   3675
   End
   Begin VB.Label lblUserClosedToday 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Closed Today: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   23
      Top             =   7500
      Width           =   3375
   End
   Begin VB.Label lblUserLoggedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Logged this Month:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   22
      Top             =   6900
      Width           =   3795
   End
   Begin VB.Label lblUserLoggedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Logged this Week:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   21
      Top             =   6600
      Width           =   3735
   End
   Begin VB.Label lblUserLoggedToday 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests I've Logged Today:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   20
      Top             =   6300
      Width           =   3435
   End
   Begin VB.Label Label6 
      Caption         =   "MY STATISTICS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   19
      Top             =   6060
      Width           =   8055
   End
   Begin VB.Label Label5 
      Caption         =   "CATEGORIES"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   18
      Top             =   4920
      Width           =   8055
   End
   Begin VB.Label Label4 
      Caption         =   "AVERAGES"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   17
      Top             =   2880
      Width           =   8055
   End
   Begin VB.Label Label3 
      Caption         =   "GENERAL STATISTICS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   16
      Top             =   840
      Width           =   8055
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000C&
      Caption         =   "This Month's most popular Category:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   15
      Top             =   5760
      Width           =   3195
   End
   Begin VB.Label lblCategoryWeek 
      BackColor       =   &H8000000C&
      Caption         =   "This Week's most popular Category:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   14
      Top             =   5460
      Width           =   3135
   End
   Begin VB.Label lblCategoryDay 
      BackColor       =   &H8000000C&
      Caption         =   "Today's most popular Category:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   13
      Top             =   5160
      Width           =   2775
   End
   Begin VB.Label lblAvgClosedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Closed per Month:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   12
      Top             =   4620
      Width           =   3795
   End
   Begin VB.Label lblAvgLoggedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Logged per Month:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   11
      Top             =   4320
      Width           =   3855
   End
   Begin VB.Label lblAvgClosedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Closed per Week (for the Month):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   10
      Top             =   4020
      Width           =   5055
   End
   Begin VB.Label lblAvgLoggedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Logged per Week (for the Month):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   9
      Top             =   3720
      Width           =   5055
   End
   Begin VB.Label lblAvgClosedDay 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Closed per Day (for the Month):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   8
      Top             =   3420
      Width           =   4875
   End
   Begin VB.Label lblAvgLoggedDay 
      BackColor       =   &H8000000C&
      Caption         =   "Avg. Number of Requests Logged per Day (for the Month):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   7
      Top             =   3120
      Width           =   4935
   End
   Begin VB.Label lblClosedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Closed this Month:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   6
      Top             =   2580
      Width           =   3375
   End
   Begin VB.Label lblLoggedMonth 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Logged this Month:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   5
      Top             =   2280
      Width           =   3435
   End
   Begin VB.Label lblClosedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Closed this Week:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   4
      Top             =   1980
      Width           =   3315
   End
   Begin VB.Label lblLoggedWeek 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Logged this Week:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   3
      Top             =   1680
      Width           =   3375
   End
   Begin VB.Label lblClosedToday 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Closed Today:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   2
      Top             =   1380
      Width           =   3015
   End
   Begin VB.Label lblLoggedToday 
      BackColor       =   &H8000000C&
      Caption         =   "Number of Requests Logged Today: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   1
      Top             =   1080
      Width           =   3075
   End
   Begin VB.Line Line1 
      X1              =   4800
      X2              =   300
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "CedeSupportDesk Statistics"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4980
      TabIndex        =   0
      Top             =   60
      Width           =   3555
   End
   Begin VB.Image Image2 
      Height          =   615
      Left            =   0
      Picture         =   "frmStats.frx":0442
      Top             =   -180
      Width           =   18000
   End
   Begin VB.Image Image1 
      Height          =   435
      Left            =   -660
      Picture         =   "frmStats.frx":24516
      Stretch         =   -1  'True
      Top             =   300
      Width           =   10470
   End
End
Attribute VB_Name = "frmStats"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Label7_Click()

End Sub

Private Sub cmdClose_Click()
    lblNum1.Caption = ""
    lblNum2.Caption = ""
    lblNum3.Caption = ""
    lblNum4.Caption = ""
    lblNum5.Caption = ""
    lblNum6.Caption = ""
    lblNum7.Caption = ""
    lblNum8.Caption = ""
    lblNum9.Caption = ""
    lblNum10.Caption = ""
    lblNum11.Caption = ""
    lblNum12.Caption = ""
    lblNum13.Caption = ""
    lblNum14.Caption = ""
    lblNum15.Caption = ""
    lblNum16.Caption = ""
    lblNum17.Caption = ""
    lblNum18.Caption = ""
    lblNum19.Caption = ""
    lblNum20.Caption = ""
    lblNum21.Caption = ""
    lblNum22.Caption = ""
    lblNum23.Caption = ""
    
    frmStats.Hide
End Sub
