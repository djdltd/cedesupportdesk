VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDateChooser 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2325
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   2640
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2325
   ScaleWidth      =   2640
   StartUpPosition =   3  'Windows Default
   Begin MSComCtl2.MonthView MonthView1 
      Height          =   2310
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2640
      _ExtentX        =   4657
      _ExtentY        =   4075
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   0
      StartOfWeek     =   4063234
      CurrentDate     =   38126
   End
End
Attribute VB_Name = "frmDateChooser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DateCombo As ComboBox
Public SelectedDate As String

Private Sub MonthView1_DateClick(ByVal DateClicked As Date)
    GetSelectedDate
    frmDateChooser.Visible = False
    DateCombo.Text = SelectedDate
End Sub

Public Function GetSelectedDate()
    Dim strDay As String
    Dim strMonth As String
    Dim strYear As String
    Dim strDate As String
    
    strDay = MonthView1.Day
    strMonth = MonthView1.Month
    strYear = MonthView1.Year
    
    strDate = strDay + "/" + strMonth + "/" + strYear
            
    SelectedDate = Format(strDate, "DD/MM/YYYY")
End Function

