VERSION 5.00
Begin VB.Form frmCAOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Additional Options"
   ClientHeight    =   3960
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5805
   Icon            =   "frmCAOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3960
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Additional Options"
      Height          =   735
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Width           =   5595
      Begin VB.CheckBox chkDontEmail 
         Caption         =   "Don't send a confirmation email when creating and closing requests."
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   300
         Width           =   5355
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Call Allocation Settings"
      Height          =   1635
      Left            =   120
      TabIndex        =   2
      Top             =   900
      Width           =   5595
      Begin VB.CheckBox chkAutoStatus 
         Caption         =   "Mark me as Present when I open CedeSupportDesk, and Absent when CedeSupportDesk is closed."
         Height          =   435
         Left            =   120
         TabIndex        =   7
         Top             =   1140
         Width           =   5355
      End
      Begin VB.CheckBox chkDisplayCAStatus 
         Caption         =   "Display Call Allocation Status options."
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   3075
      End
      Begin VB.TextBox txtIdleMinutes 
         Height          =   255
         Left            =   780
         TabIndex        =   3
         Text            =   "5"
         Top             =   480
         Width           =   675
      End
      Begin VB.CheckBox chkIdleCheck 
         Caption         =   "Automatically mark me as Absent if I'm away from my desk for more than                 minutes."
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   5115
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   2940
      TabIndex        =   1
      Top             =   3480
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4380
      TabIndex        =   0
      Top             =   3480
      Width           =   1335
   End
   Begin VB.Line Line1 
      X1              =   2640
      X2              =   120
      Y1              =   300
      Y2              =   300
   End
   Begin VB.Label Label6 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Additional Settings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2820
      TabIndex        =   5
      Top             =   120
      Width           =   2955
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "frmCAOptions.frx":0442
      Top             =   0
      Width           =   18000
   End
End
Attribute VB_Name = "frmCAOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    frmCAOptions.Hide
End Sub

Private Sub cmdOk_Click()
    
    If chkDisplayCAStatus.Value = 1 Then
        Main.DisplayCAOptions = True
        frmMain.lblStatusLabel.Visible = True
        frmMain.cmbSASpecialist.Visible = True
        frmMain.cmbSAStatus.Visible = True
        Main.PopulateCANames
    Else
        frmMain.lblStatusLabel.Visible = False
        frmMain.cmbSASpecialist.Visible = False
        frmMain.cmbSAStatus.Visible = False
        Main.DisplayCAOptions = False
    End If
    
    If chkIdleCheck.Value = 1 Then Main.UseAutoAbsence = True Else Main.UseAutoAbsence = False
    If chkAutoStatus.Value = 1 Then Main.UseAutoStatus = True Else Main.UseAutoStatus = False
    If chkDontEmail.Value = 1 Then Main.DontEmail = True Else Main.DontEmail = False
    
    If ValidateIdleMinutes(txtIdleMinutes.Text) = False And chkIdleCheck.Value = 1 Then
        MsgBox "Please enter an idle time between 0 and 60 minutes.", vbExclamation, "Cede SupportDesk"
        Exit Sub
    End If
    
    Main.IdleMinutes = Val(txtIdleMinutes.Text)
    
    frmCAOptions.Hide
End Sub

Private Function ValidateIdleMinutes(strMin As String) As Boolean
    If Val(strMin) > 0 And Val(strMin) <= 60 Then
        ValidateIdleMinutes = True
    Else
        ValidateIdleMinutes = False
    End If
End Function
