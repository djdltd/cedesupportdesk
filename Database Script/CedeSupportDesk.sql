/*
Cede Support Desk Database creation script. CedeSoft Ltd. All Rights Reserved.

TODO: Open / Paste this script using SQL Query Analyzer, on a blank / new database only.
All Tables and Views required by CedeSupportDesk will be created by this script. Sample records
in critical tables will also be added. These include Sample Categories, Action Types, Priorities,
Status, Sample Specialists, and Callers.

*/

CREATE TABLE [dbo].[AssociatedCallers] (
	[RequestID] [int] NOT NULL ,
	[CallerID] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallActionTemplates] (
	[ID] [int] NULL ,
	[ActionType] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ActionDetails] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TemplateName] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TemplateComments] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallActionTypes] (
	[ID] [int] NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Action] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallActions] (
	[ID] [int] NOT NULL ,
	[RequestID] [int] NOT NULL ,
	[ActionTypeID] [int] NOT NULL ,
	[ActionDate] [datetime] NOT NULL ,
	[ActionByID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallActivities] (
	[ID] [int] NOT NULL ,
	[RequestID] [int] NOT NULL ,
	[ActionTypeID] [int] NOT NULL ,
	[ActivityDate] [datetime] NOT NULL ,
	[ActivityByID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LoggedByID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallAttachDetails] (
	[ID] [int] NULL ,
	[RequestID] [int] NULL ,
	[FileName] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ActualPath] [char] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallAttachPath] (
	[AttachPath] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallAttachments] (
	[CallerID] [int] NULL ,
	[RequestID] [int] NULL ,
	[FileName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[att] [image] NULL ,
	[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UserID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateAdded] [datetime] NULL ,
	[FileSize] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallCategories] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[parent] [int] NOT NULL ,
	[Description] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallPriorities] (
	[ID] [int] NOT NULL ,
	[Description] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Ranking] [smallint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallStatus] (
	[ID] [int] NOT NULL ,
	[Description] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CallTemplates] (
	[ID] [int] NULL ,
	[PriorityDescription] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDescription] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CategoryDescription] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Summary] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Details] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Resolution] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TemplateName] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TemplateComments] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Callers] (
	[CallerID] [int] IDENTITY (1, 1) NOT NULL ,
	[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Lastname] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Firstname] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Telephone] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mobile] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Department] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Location] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Specialists] (
	[ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GroupID] [int] NOT NULL ,
	[MobileCountryCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MobileAreaCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MobileNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Firstname] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Lastname] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SupportCallLog] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[RequestID] [int] NOT NULL ,
	[DateTime] [datetime] NOT NULL ,
	[UserID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Cause] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Status] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Priority] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ActiveStatus] [bit] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SupportCalls] (
	[ID] [int] NOT NULL ,
	[TotalOnHold] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TimeOnHold] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChangeIncident] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StyleID] [int] NOT NULL ,
	[ElapsedTime] [int] NULL ,
	[PriorityID] [int] NOT NULL ,
	[GroupID] [int] NOT NULL ,
	[StatusID] [int] NOT NULL ,
	[CategoriesID] [int] NOT NULL ,
	[SpecialistID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LoggedbyID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LoggedDate] [datetime] NOT NULL ,
	[OpenTime] [int] NOT NULL ,
	[DateStamp] [datetime] NOT NULL ,
	[SLA] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Summary] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Details] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Resolution] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[SupportGroups] (
	[ID] [int] NOT NULL ,
	[Description] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.ResolvedRequests
AS
SELECT     dbo.SupportCalls.ID AS RequestID, dbo.SupportCalls.SpecialistID, dbo.SupportCalls.LoggedbyID AS LoggedBy, dbo.SupportCalls.LoggedDate, dbo.SupportCalls.DateStamp, 
                      dbo.SupportCalls.OpenTime, dbo.SupportCalls.Summary, dbo.SupportCalls.Details, dbo.SupportCalls.Resolution, dbo.CallStatus.Description AS Status, 
                      dbo.SupportGroups.Description AS CSTGroup, dbo.CallPriorities.Description AS Priority, dbo.CallCategories.Description AS Category, dbo.Callers.Username AS Username, 
                      dbo.Callers.Lastname AS LastName, dbo.Callers.Firstname AS FirstName, dbo.Callers.Department AS Department, dbo.Callers.Location AS Location, 
                      dbo.Callers.Telephone AS Telephone
FROM         dbo.SupportCalls INNER JOIN
                      dbo.CallCategories ON dbo.SupportCalls.CategoriesID = dbo.CallCategories.ID INNER JOIN
                      dbo.CallPriorities ON dbo.SupportCalls.PriorityID = dbo.CallPriorities.ID INNER JOIN
                      dbo.CallStatus ON dbo.SupportCalls.StatusID = dbo.CallStatus.ID INNER JOIN
                      dbo.SupportGroups ON dbo.SupportCalls.GroupID = dbo.SupportGroups.ID INNER JOIN
                      dbo.AssociatedCallers ON dbo.SupportCalls.ID = dbo.AssociatedCallers.RequestID INNER JOIN
                      dbo.Callers ON dbo.AssociatedCallers.CallerID = dbo.Callers.CallerID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

INSERT INTO dbo.SupportGroups VALUES ('1', 'MAIN HELPDESK')
INSERT INTO dbo.SupportGroups VALUES ('2', 'HARDWARE TEAM')
INSERT INTO dbo.SupportGroups VALUES ('3', 'DEVELOPMENT TEAM')
INSERT INTO dbo.SupportGroups VALUES ('4', 'SERVER SUPPORT TEAM')
INSERT INTO dbo.SupportGroups VALUES ('5', 'ACTIVE DIRECTORY TEAM')
INSERT INTO dbo.SupportGroups VALUES ('6', 'NETWORK TEAM')

INSERT INTO dbo.CallActionTypes VALUES ('1', 'Investigate', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('2', 'Email', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('3', 'Advice', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('4', 'Phone Call', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('5', 'Desk Visit', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('6', 'Site Visit', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('7', 'Hardware Fix', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('8', 'Software Install', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('9', 'User/PC Configuration', 'Action', '0')
INSERT INTO dbo.CallActionTypes VALUES ('10', 'Meeting Arranged', 'Action', '0')

INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Desktop')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Laptop')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'PDA')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Mobile Phone')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Word')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Excel')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Powerpoint')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Outlook')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Internet Explorer')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Firefox')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Safari')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Mac OS X')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Broadband')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Windows XP')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Windows Server')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Windows 2000')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Photoshop')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Dreamweaver')
INSERT INTO dbo.CallCategories (parent, Description) VALUES ('0', 'Windows Media Player')

INSERT INTO dbo.Callers (Username, Lastname, Firstname, Telephone, Mobile, Email, Department, Location, Address) VALUES ('ANDREWSP', 'Andrews', 'Peter', '01234 123456', '09876 123456', 'peter.andrews@company.com', 'Sales', 'Building 10', '123 Sample Road')
INSERT INTO dbo.Callers (Username, Lastname, Firstname, Telephone, Mobile, Email, Department, Location, Address) VALUES ('SMITHS', 'Smith', 'Sarah', '01234 123456', '09876 123456', 'sarah.smith@company.com', 'Development', 'Building 12', '123 Sample Road')
INSERT INTO dbo.Callers (Username, Lastname, Firstname, Telephone, Mobile, Email, Department, Location, Address) VALUES ('ANDREWSP2', 'Andrews', 'Phillip', '01234 123456', '09876 123456', 'phillip.andrews@company.com', 'Research', 'Building 15', '123 Sample Road')

INSERT INTO dbo.CallPriorities VALUES ('1', 'Low - 8 Hours', '4')
INSERT INTO dbo.CallPriorities VALUES ('2', 'Medium - 4 Hours', '3')
INSERT INTO dbo.CallPriorities VALUES ('3', 'High - 1 Hour', '2')
INSERT INTO dbo.CallPriorities VALUES ('4', 'Urgent - 20 Mins', '1')
INSERT INTO dbo.CallPriorities VALUES ('5', '2 days', '5')
INSERT INTO dbo.CallPriorities VALUES ('6', 'Critical - Overdue', '0')

INSERT INTO dbo.CallStatus VALUES ('1', 'Open')
INSERT INTO dbo.CallStatus VALUES ('2', 'Closed')
INSERT INTO dbo.CallStatus VALUES ('3', 'Re-Opened')
INSERT INTO dbo.CallStatus VALUES ('4', 'On Hold')
INSERT INTO dbo.CallStatus VALUES ('5', 'With Caller')
INSERT INTO dbo.CallStatus VALUES ('6', 'In Progress')

INSERT INTO dbo.Specialists VALUES ('SMITHJ', 'John Smith', 'john.smith@company.com', '1', NULL, NULL, '01234 567890', 'John', 'Smith', 'password')
INSERT INTO dbo.Specialists VALUES ('DOEJ', 'Jane Doe', 'jane.doe@company.com', '1', NULL, NULL, '01234 567890', 'Jane', 'Doe', 'password')
INSERT INTO dbo.Specialists VALUES ('ADMIN', 'Sys Admin', 'sys.Admin@company.com', '1', NULL, NULL, '01234 567890', 'Sys', 'Admin', 'password')
